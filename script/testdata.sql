truncate table console_menu;
insert into `console_menu` (`id`, `name`, `parent_id`, `priority`, `branch`, `icon`, `url`, `label`) values('10','餐厅管理','0','10','1','','','');
insert into `console_menu` (`id`, `name`, `parent_id`, `priority`, `branch`, `icon`, `url`, `label`) values('30','平台运营','0','30','1','','','');
insert into `console_menu` (`id`, `name`, `parent_id`, `priority`, `branch`, `icon`, `url`, `label`) values('99','系统管理','0','99','1','','','');
insert into `console_menu` (`id`, `name`, `parent_id`, `priority`, `branch`, `icon`, `url`, `label`) values('1010','餐厅管理','10','1010','0','','/restaurant/manager/','');
insert into `console_menu` (`id`, `name`, `parent_id`, `priority`, `branch`, `icon`, `url`, `label`) values('1030','餐厅分类管理','10','1030','0','','/restaurant/category/','');
insert into `console_menu` (`id`, `name`, `parent_id`, `priority`, `branch`, `icon`, `url`, `label`) values('1020','菜品管理','10','1020','0','','/dish/manager/','');
insert into `console_menu` (`id`, `name`, `parent_id`, `priority`, `branch`, `icon`, `url`, `label`) values('1040','菜品分类管理','10','1040','0','','/dish/category/','');
insert into `console_menu` (`id`, `name`, `parent_id`, `priority`, `branch`, `icon`, `url`, `label`) values('3000','订单管理','30','3000','1','','','');
insert into `console_menu` (`id`, `name`, `parent_id`, `priority`, `branch`, `icon`, `url`, `label`) values('3010','广告位管理','30','3010','0','','/advertisement/','');
insert into `console_menu` (`id`, `name`, `parent_id`, `priority`, `branch`, `icon`, `url`, `label`) values('3020','客户信息','30','3020','0','','/user/','');
insert into `console_menu` (`id`, `name`, `parent_id`, `priority`, `branch`, `icon`, `url`, `label`) values('3030','活动管理','30','3030','0','','/activity/','');
insert into `console_menu` (`id`, `name`, `parent_id`, `priority`, `branch`, `icon`, `url`, `label`) values('9910','全局配置','99','9910','0','','/system/configuration/','');
insert into `console_menu` (`id`, `name`, `parent_id`, `priority`, `branch`, `icon`, `url`, `label`) values('9920','Android客户端版本更新','99','9920','1','','','');
insert into `console_menu` (`id`, `name`, `parent_id`, `priority`, `branch`, `icon`, `url`, `label`) values('9930','意见反馈','99','9930','0','','/system/feedback/','');
insert into `console_menu` (`id`, `name`, `parent_id`, `priority`, `branch`, `icon`, `url`, `label`) values('9940','员工管理','99','9940','0','','/system/employee/','');
insert into `console_menu` (`id`, `name`, `parent_id`, `priority`, `branch`, `icon`, `url`, `label`) values('9950','区域定制','99','9950','0','','/system/area/','');
insert into `console_menu` (`id`, `name`, `parent_id`, `priority`, `branch`, `icon`, `url`, `label`) values('9960','系统用户管理','99','9950','0','','/system/user/','');
insert into `console_menu` (`id`, `name`, `parent_id`, `priority`, `branch`, `icon`, `url`, `label`) values('300010','分配取菜员','3000','300010','0','','/payment/unallot/deliver/','');
insert into `console_menu` (`id`, `name`, `parent_id`, `priority`, `branch`, `icon`, `url`, `label`) values('300020','分配配送员','3000','300020','0','','/payment/unallot/courier/','');
insert into `console_menu` (`id`, `name`, `parent_id`, `priority`, `branch`, `icon`, `url`, `label`) values('300030','回退订单','3000','300030','0','','/payment/back/','');
insert into `console_menu` (`id`, `name`, `parent_id`, `priority`, `branch`, `icon`, `url`, `label`) values('300040','全局订单','3000','300040','0','','/payment/global/','');
insert into `console_menu` (`id`, `name`, `parent_id`, `priority`, `branch`, `icon`, `url`, `label`) values('992010','用户APP','9920','992010','0','','/system/customer/update/','');
insert into `console_menu` (`id`, `name`, `parent_id`, `priority`, `branch`, `icon`, `url`, `label`) values('992020','取菜员APP','9920','992020','0','','/system/deliver/update/','');
insert into `console_menu` (`id`, `name`, `parent_id`, `priority`, `branch`, `icon`, `url`, `label`) values('992030','配送员APP','9920','992030','0','','/system/courier/update/','');



insert into rel_console_user_role (id, user_id, role_id) values(null, 1, 1);

INSERT INTO console_user (username, PASSWORD, real_name, STATUS, `type`, skin, navbar_fixed, menu_fixed, breadcrumb_fixed, petty) 
	VALUES ('admin', 'e10adc3949ba59abbe56e057f20f883e', 'Administrator', 1, 9, '', FALSE, FALSE, FALSE, FALSE);


truncate table static_resource;
insert into `static_resource` values ('service.tel', '熊仔电话', '0551578465');
insert into `static_resource` values ('service.aboutus', '关于我们', '<div>...</div>');
insert into `static_resource` values ('service.support', '技术支持', '2CWM0NNDRQN8LQHAQIDVX5HED9TAMKVJ9MY99DMLM0ED4RV08OBYEKJ74W836LW4T07Q0VRY5 TY5320UTNG939NN2HX0MCB1S3UY4DSK8K3FMQK VI1MR 8RKHBR1B1ERI8K5NQASTRE7F87HH86KTOFQJMQ9SIL7Q9VYQNV71O8TMU8OKCDKT2TDCXSOP8D G5MNMR1L59NEOIX36F7W0P6W13LTXXPITT7BAINMHV3WWOCW2BFN<div>...</div>');
insert into `static_resource` values ('service.license','许可协议','<p>Copyright (c) 1998 著作权由加州大学董事会所有。著作权人保留一切权利。</p><p>这份授权条款，在使用者符合以下三条件的情形下，授予使用者使用及再散播本软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：</p><ul><li>对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以及下述的免责声明。</li><li>对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述的免责声明。</li><li>未获事前取得书面许可，不得使用柏克莱加州大学或本软件贡献者之名称，来为本软件之衍生物做任何表示支持、认可或推广、促销之行为。</li></ul><p>免责声明：本软件是由加州大学董事会及本软件之贡献者以现状（"as is"）提供，本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目的的适用性为默示性担保。加州大学董事会及本软件之贡献者，无论任何条件、无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。</p>');
insert into `static_resource` values ('service.cash.rule','代金券使用规则','<ol><li>本代金卷只能在泰迪熊订餐网站及APP软件上使用。</li><li>代金卷不可叠加使用，一次消费返现一张代金卷。</li><li>代金卷不支持拆分使用，须一次使用完成。</li><li>每人每个代金卷限领一张。</li><li>同一个账户同一个手机号符合任意条件均视为同一用户。</li><li>用户下单并支付成功后可登陆泰迪熊手机客户端或网络，在“我的代金劵”内查询卷面详情。</li><li>如您需要发票，给您开具的发票金额将仅限于您实际付款金额。</li><li>通过系统漏洞、利用黑客工具、虚假交易等非正常方式参与活动（如恶意套现等）泰迪熊将取消用户的参与资格，并有权撤销违规交易，回收用户或商户所获得的利益。</li></ol>');
insert into `static_resource` values ('config.comment.withContent', '显示文字评论', 'false');
insert into `static_resource` values ('config.score.scorePerMoney', '送积分规则（多少元送1积分）', '10');
insert into `static_resource` values ('config.promotion.costScore', '积分抵现规则', '100,10');
insert into `static_resource` values ('config.deliver.autoAssignable', '是否自动分配取菜员', 'true');
insert into `static_resource` values ('config.courier.autoAssignable', '是否自动分配配送员', 'true');
insert into `static_resource` values ('config.vip.experience','vip的经验阙值','1000');
insert into `static_resource` values ('config.employee.passwd.initialization','员工初始密码','123456');
insert into `static_resource` values ('share.payment','分享订单可获代金券','5');
insert into `static_resource` values ('share.receive','领取分享可获代金券','5');

truncate table attachment;
INSERT INTO `attachment`(id,`path`) VALUES (1,'http://teddy.hylapp.com/static/category/3.png')
,(2,'http://teddy.hylapp.com/static/category/2.png')
,(3,'http://teddy.hylapp.com/static/category/1.png')
,(4,'http://teddy.hylapp.com/static/category/4.png')
,(5,'http://teddy.hylapp.com/static/category/5.png')
,(6,'http://teddy.hylapp.com/static/category/6.png')
,(10,'http://teddy.hylapp.com/temp/__42891370__6430421.jpg')
,(11,'http://teddy.hylapp.com/temp/__42891395__4696685.jpg')
,(12,'http://teddy.hylapp.com/temp/__42891396__8885841.jpg')
,(13,'http://teddy.hylapp.com/temp/__42891397__9136050.jpg')
,(14,'http://teddy.hylapp.com/temp/__42891399__1971479.jpg')
,(15,'http://teddy.hylapp.com/temp/f7129ba4176634f530c272c27d95ef1838031.jpg')
,(16,'http://teddy.hylapp.com/temp/2f601cbeb3df11a39f320cc61e94f0af164355.jpg')
,(17,'http://teddy.hylapp.com/temp/447bfdecf3acd7fd3a7ac8dc33400d86152643.jpg')
,(18,'http://teddy.hylapp.com/temp/a47dbf716785ac05c11e235a6f3e5e77152242.jpg')
,(19,'http://teddy.hylapp.com/temp/d21447b7736f77f1ea4667dcadc9836a161679.jpg')
,(20,'http://teddy.hylapp.com/temp/0b9041b8fa1fdc6851fd554b9983e39227173.jpg')
,(21,'http://teddy.hylapp.com/temp/267fa6beedb4c8d0d646124f0ef31a5b31710.jpg')
,(22,'http://teddy.hylapp.com/temp/14d2ffc96c7b1c81afd44ff43516701231931.jpg')
;

truncate table customer;
INSERT INTO `customer` (`id`, `mobile`, `password`, `nickname`, `score_highest`, `score`, `avatar`, `reg_time`, `push_key`, `disabled`) VALUES(1, '12345678998', '123', 'shuaige', 34, 412, '/static/avatar/0.png', '0743-11-14 08:06:32', '', 0);

truncate table cust_cash;
insert into cust_cash (customer_id,cash,`count`) values (1,5,2),(1,10,4),(1,20,1);

truncate table cust_address;
INSERT INTO `cust_address` (`id`, `customer_id`, `name`, `mobile`, `location`, `longitude`, `latitude`, `default`, `region_id`) VALUES (1, 1, '王大发', '13665441452', '蔚蓝商务港B坐', 136231221, 37211234, 1, 0)
,(2, 1, '王大发', '13665441452', '蔚蓝商务港X座', 136231221, 37211234, 0, 0)
,(3, 1, '王大发', '13665441452', '蔚蓝商务港C坐', 136231221, 37211234, 0, 0)
;

truncate table restaurant;
insert into `restaurant`(`name`,icon,images,price,sold,package,delivery,promotion,location,tel,star,description) values
('吴山贡鹅1', '10', '10,11,12,13,14', '5000', '120', '800', '900', '亲爱的用户，本次团购有效期延长至2014年11月12日，其中不可用日期：2014年5月13日、10月1日至10月7日，感谢您的支持，祝消费愉快~', '庐阳区蒙城北路与茨河路交口', '055165654066','9','<dl id="yui_3_16_0_1_1413530609649_5053"><dt>有效期</dt><dd>2014.5.15 至 2014.11.12</dd><dt>不可用日期</dt><dd>2014年5月13日、10月1日至10月7日</dd><dt>使用时间</dt><dd>11:00-13:30,17:00-20:30</dd><dt>预约提醒</dt><dd>请提前24小时预约</dd><dt>限购限用提醒</dt><dd id="yui_3_16_0_1_1413530609649_5052">每人最多使用1张美团券每张美团券建议10至12人使用</dd><dt>包间</dt><dd>可免费使用包间</dd><dt>堂食外带</dt><dd>仅限堂食，不提供餐前外带，餐毕未吃完可免费打包</dd><dt>温馨提示</dt><dd id="yui_3_16_0_1_1413530609649_5062">团购用户不可同时享受商家其他优惠酒水饮料等问题，请致电商家咨询，以商家反馈为准如部分菜品因时令或其他不可抗因素导致无法提供，店内会用等价菜品替换，具体事宜请与店内协商</dd><dt>商家服务</dt><dd>免费提供15个停车位</dd></dl>'),
('永冠蛋糕2', '15', '16,17,18,19', '3900', '250', '200', '900', '团购用户不可同时享受商家其他优惠', '包河区合巢路三十二中学旁', '055163430805','10','<ul class="list" id="yui_3_16_0_1_1413533293920_1561"><li>西点小蛋糕规格：约1.5<strong class="acronym">磅<sup title=" 1磅≈0.45千克 ">1</sup></strong>，园形</li><li>生产日期：自提当日生产</li><li>保存须知：0-4℃保存24小时</li><li id="yui_3_16_0_1_1413533293920_1560">免费提供包装</li></ul>'),
('吴山贡鹅3', '10', '10,11,12,13,14', '5000', '120', '800', '900', '亲爱的用户，本次团购有效期延长至2014年11月12日，其中不可用日期：2014年5月13日、10月1日至10月7日，感谢您的支持，祝消费愉快~', '庐阳区蒙城北路与茨河路交口', '055165654066','9','<dl id="yui_3_16_0_1_1413530609649_5053"><dt>有效期</dt><dd>2014.5.15 至 2014.11.12</dd><dt>不可用日期</dt><dd>2014年5月13日、10月1日至10月7日</dd><dt>使用时间</dt><dd>11:00-13:30,17:00-20:30</dd><dt>预约提醒</dt><dd>请提前24小时预约</dd><dt>限购限用提醒</dt><dd id="yui_3_16_0_1_1413530609649_5052">每人最多使用1张美团券每张美团券建议10至12人使用</dd><dt>包间</dt><dd>可免费使用包间</dd><dt>堂食外带</dt><dd>仅限堂食，不提供餐前外带，餐毕未吃完可免费打包</dd><dt>温馨提示</dt><dd id="yui_3_16_0_1_1413530609649_5062">团购用户不可同时享受商家其他优惠酒水饮料等问题，请致电商家咨询，以商家反馈为准如部分菜品因时令或其他不可抗因素导致无法提供，店内会用等价菜品替换，具体事宜请与店内协商</dd><dt>商家服务</dt><dd>免费提供15个停车位</dd></dl>'),
('永冠蛋糕4', '10', '10,11,12,13,14', '5000', '120', '800', '900', '亲爱的用户，本次团购有效期延长至2014年11月12日，其中不可用日期：2014年5月13日、10月1日至10月7日，感谢您的支持，祝消费愉快~', '庐阳区蒙城北路与茨河路交口', '055165654066','9','<dl id="yui_3_16_0_1_1413530609649_5053"><dt>有效期</dt><dd>2014.5.15 至 2014.11.12</dd><dt>不可用日期</dt><dd>2014年5月13日、10月1日至10月7日</dd><dt>使用时间</dt><dd>11:00-13:30,17:00-20:30</dd><dt>预约提醒</dt><dd>请提前24小时预约</dd><dt>限购限用提醒</dt><dd id="yui_3_16_0_1_1413530609649_5052">每人最多使用1张美团券每张美团券建议10至12人使用</dd><dt>包间</dt><dd>可免费使用包间</dd><dt>堂食外带</dt><dd>仅限堂食，不提供餐前外带，餐毕未吃完可免费打包</dd><dt>温馨提示</dt><dd id="yui_3_16_0_1_1413530609649_5062">团购用户不可同时享受商家其他优惠酒水饮料等问题，请致电商家咨询，以商家反馈为准如部分菜品因时令或其他不可抗因素导致无法提供，店内会用等价菜品替换，具体事宜请与店内协商</dd><dt>商家服务</dt><dd>免费提供15个停车位</dd></dl>'),
('吴山贡鹅5', '10', '10,11,12,13,14', '5000', '120', '800', '900', '亲爱的用户，本次团购有效期延长至2014年11月12日，其中不可用日期：2014年5月13日、10月1日至10月7日，感谢您的支持，祝消费愉快~', '庐阳区蒙城北路与茨河路交口', '055165654066','9','<dl id="yui_3_16_0_1_1413530609649_5053"><dt>有效期</dt><dd>2014.5.15 至 2014.11.12</dd><dt>不可用日期</dt><dd>2014年5月13日、10月1日至10月7日</dd><dt>使用时间</dt><dd>11:00-13:30,17:00-20:30</dd><dt>预约提醒</dt><dd>请提前24小时预约</dd><dt>限购限用提醒</dt><dd id="yui_3_16_0_1_1413530609649_5052">每人最多使用1张美团券每张美团券建议10至12人使用</dd><dt>包间</dt><dd>可免费使用包间</dd><dt>堂食外带</dt><dd>仅限堂食，不提供餐前外带，餐毕未吃完可免费打包</dd><dt>温馨提示</dt><dd id="yui_3_16_0_1_1413530609649_5062">团购用户不可同时享受商家其他优惠酒水饮料等问题，请致电商家咨询，以商家反馈为准如部分菜品因时令或其他不可抗因素导致无法提供，店内会用等价菜品替换，具体事宜请与店内协商</dd><dt>商家服务</dt><dd>免费提供15个停车位</dd></dl>'),
('永冠蛋糕6', '10', '10,11,12,13,14', '5000', '120', '800', '900', '亲爱的用户，本次团购有效期延长至2014年11月12日，其中不可用日期：2014年5月13日、10月1日至10月7日，感谢您的支持，祝消费愉快~', '庐阳区蒙城北路与茨河路交口', '055165654066','9','<dl id="yui_3_16_0_1_1413530609649_5053"><dt>有效期</dt><dd>2014.5.15 至 2014.11.12</dd><dt>不可用日期</dt><dd>2014年5月13日、10月1日至10月7日</dd><dt>使用时间</dt><dd>11:00-13:30,17:00-20:30</dd><dt>预约提醒</dt><dd>请提前24小时预约</dd><dt>限购限用提醒</dt><dd id="yui_3_16_0_1_1413530609649_5052">每人最多使用1张美团券每张美团券建议10至12人使用</dd><dt>包间</dt><dd>可免费使用包间</dd><dt>堂食外带</dt><dd>仅限堂食，不提供餐前外带，餐毕未吃完可免费打包</dd><dt>温馨提示</dt><dd id="yui_3_16_0_1_1413530609649_5062">团购用户不可同时享受商家其他优惠酒水饮料等问题，请致电商家咨询，以商家反馈为准如部分菜品因时令或其他不可抗因素导致无法提供，店内会用等价菜品替换，具体事宜请与店内协商</dd><dt>商家服务</dt><dd>免费提供15个停车位</dd></dl>'),
('吴山贡鹅7', '10', '10,11,12,13,14', '5000', '120', '800', '900', '亲爱的用户，本次团购有效期延长至2014年11月12日，其中不可用日期：2014年5月13日、10月1日至10月7日，感谢您的支持，祝消费愉快~', '庐阳区蒙城北路与茨河路交口', '055165654066','9','<dl id="yui_3_16_0_1_1413530609649_5053"><dt>有效期</dt><dd>2014.5.15 至 2014.11.12</dd><dt>不可用日期</dt><dd>2014年5月13日、10月1日至10月7日</dd><dt>使用时间</dt><dd>11:00-13:30,17:00-20:30</dd><dt>预约提醒</dt><dd>请提前24小时预约</dd><dt>限购限用提醒</dt><dd id="yui_3_16_0_1_1413530609649_5052">每人最多使用1张美团券每张美团券建议10至12人使用</dd><dt>包间</dt><dd>可免费使用包间</dd><dt>堂食外带</dt><dd>仅限堂食，不提供餐前外带，餐毕未吃完可免费打包</dd><dt>温馨提示</dt><dd id="yui_3_16_0_1_1413530609649_5062">团购用户不可同时享受商家其他优惠酒水饮料等问题，请致电商家咨询，以商家反馈为准如部分菜品因时令或其他不可抗因素导致无法提供，店内会用等价菜品替换，具体事宜请与店内协商</dd><dt>商家服务</dt><dd>免费提供15个停车位</dd></dl>');

truncate table restaurant_category;
insert into `restaurant_category` (`name`,icon) values
('中餐', 1),
('火锅', 2),
('西餐', 3),
('奶茶甜品', 4),
('小吃', 5),
('韩式料理',6);

truncate table rel_rest_category;
INSERT INTO `rel_rest_category`(category_id,restaurant_id) VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7)
,(2,1),(2,2),(2,3),(2,4),(2,5),(2,6),(2,7)
,(3,1),(3,2),(3,5),(3,6),(3,7)
,(4,1),(4,2),(4,3),(4,4),(4,5)
,(5,1),(5,2),(5,7)
,(6,1),(6,2),(6,3),(6,4),(6,5),(6,6);


truncate table restaurant_promotion;
insert into `restaurant_promotion` (`restaurant_id`, `pattern`, `param0`, `param1`, `param2`, `param3`) values
('1','WIN_CASH','50','1','5','0'),
('1','VIP_DISCOUNT','80','0','0','0'),
('1','COST_CASH','0','0','0','0'),
('1','COST_SCORE','50','10','1','0'),
('1','FREE_POSTAGE','100','0','0','0'),
('2','VIP_DISCOUNT','90','0','0','0'),
('2','FREE_POSTAGE','30','0','0','0'),
('3','COST_CASH','0','0','0','0'),
('3','COST_SCORE','100','10','1','0');

truncate table dish_category;
INSERT INTO `dish_category`(`name`) VALUES ('美味'),('可口'),('香甜'),('甜品'),('川菜'),('徽菜'),('海鲜');

truncate table dish;
insert into `dish` (`restaurant_id`,`category_id`,`name`,`image`, `price`, `sold`, `description`)values(1,2,'脆椒小皮蛋',11,1200,221,'脆椒皮蛋拌豆腐是一道非常好吃的传统凉菜，具有味道香浓、口感丰富、制作简便、老少皆宜的特点。')
,(1,1,'山药炒木耳',12,2600,130,'山药炒木耳是一道清爽可口的养胃菜，其主料山药有健脾益胃助消化的作用，而木耳是公认的清道夫。吃这道菜有助于调节暴饮暴食后的身体。')
,(1,6,'时蔬一道',13,1800,186,'时蔬一道')
,(1,5,'酸辣海带丝',14,1200,251,'酸辣海带丝编辑')
,(1,1,'山药炒木耳2',12,2600,130,'山药炒木耳是一道清爽可口的养胃菜，其主料山药有健脾益胃助消化的作用，而木耳是公认的清道夫。吃这道菜有助于调节暴饮暴食后的身体。')
,(1,6,'时蔬一道2',13,1800,186,'时蔬一道')
,(1,5,'酸辣海带丝2',14,1200,251,'酸辣海带丝编辑')
,(2,4,'D005',20,1500,1256,'重量约1.5磅 圆形')
,(2,4,'D001',21,1500,1132,'重量约1.5磅 圆形')
,(2,4,'D004',22,1500,983,'重量约1.5磅 圆形')
,(4,4,'D005x4',20,1500,1256,'重量约1.5磅 圆形')
,(4,4,'D001x4',21,1500,1132,'重量约1.5磅 圆形')
,(4,4,'D004x4',22,1500,983,'重量约1.5磅 圆形')
,(4,4,'D005y4',20,1500,1256,'重量约1.5磅 圆形')
,(4,4,'D001y4',21,1500,1132,'重量约1.5磅 圆形')
,(4,4,'D004y4',22,1500,983,'重量约1.5磅 圆形')
;

truncate table employee;
INSERT INTO `employee` VALUES ('1', 'QC001', '123', 'qqq', '1',null,null,null, '8', '112233','',0);
INSERT INTO `employee` VALUES ('8', 'PS001', '123', 'aaa', '3',null,null,null,null, '112233','',0);
INSERT INTO `employee` VALUES ('9', 'FP001', '123', 'zzz', '2', '1',116380122,39997011,0, '112233','',1);

truncate table advertisement;
INSERT INTO `advertisement`(category,image,url) VALUES (1,10,'http://hf.meituan.com/deal/25022486.html?acm=UwunyailsW8284250493260139189.1&mtt=1.index%2Fdefault%2Ffilter.id.1.i1d7m645&cks=5747')
,(1,15,'http://hf.meituan.com/deal/26374011.html?acm=UwunyailsW4263178099258448614.3&mtt=1.index%2Fdefault%2Ffilter.id.7.i1d7m645&cks=26748')
,(2,17,'http://hf.meituan.com/deal/26374011.html?acm=UwunyailsW4263178099258448614.3&mtt=1.index%2Fdefault%2Ffilter.id.7.i1d7m645&cks=26748')
,(2,22,'http://hf.meituan.com/deal/26374011.html?acm=UwunyailsW4263178099258448614.3&mtt=1.index%2Fdefault%2Ffilter.id.7.i1d7m645&cks=26748')
;