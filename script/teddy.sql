drop table if exists activity;
drop table if exists advertisement;
drop table if exists attachment;
drop table if exists console_authority;
drop table if exists console_menu;
drop table if exists console_role;
drop table if exists console_user;
drop table if exists cush_cash;
drop table if exists cust_address;
drop table if exists cust_cash;
drop table if exists cust_favoraties;
drop table if exists cust_message;
drop table if exists customer;
drop table if exists dish_category;
drop table if exists dish;
drop table if exists employee;
drop table if exists en_craw_news;
drop table if exists feedback;
drop table if exists payment_goods;
drop table if exists payment_promotion;
drop table if exists payment_times;
drop table if exists payment;
drop table if exists region_point;
drop table if exists region;
drop table if exists rel_console_user_role;
drop table if exists rel_rest_category;
drop table if exists reply;
drop table if exists restaurant_category;
drop table if exists restaurant_promotion;
drop table if exists restaurant;
drop table if exists static_resource;
drop table if exists version_courier;
drop table if exists version_customer;
drop table if exists version_deliver;
drop table if exists share_record;
drop table if exists alipay_refund_request;
drop table if exists alipay_refund_response;

/*==============================================================*/
/* Table: activity                                              */
/*==============================================================*/
create table activity
(
   id                   int not null auto_increment,
   name                 varchar(20) NOT NULL COMMENT '活动名',
   url                  varchar(255),
   start_time           datetime not null comment '开始时间',
   end_time             datetime not null comment '结束时间',
   push_time            datetime comment '推送时间',
   push_content         varchar(60),
   primary key (id)
)ENGINE=innodb default charset utf8;

alter table activity comment '活动';

/*==============================================================*/
/* Table: advertisement                                         */
/*==============================================================*/
create table advertisement
(
   id                   int not null auto_increment,
   category             tinyint(2) not null default 1 comment '1-首页广告位 2-餐厅广告位 3-网页广告位',
   image                int,
   url                  varchar(255),
   primary key (id),
   index(category)
) ENGINE=innodb default charset utf8;

alter table advertisement comment '广告';

/*==============================================================*/
/* Table: attachment                                            */
/*==============================================================*/
create table attachment
(
   id                   int not null auto_increment,
   file                 varchar(255) not null default '',
   path                 varchar(255) not null default '',
   create_time          datetime not null default '2000-01-01',
   `temporary`          tinyint(1) not null default 0,
   primary key (id),
   index(`temporary`),
   index(`create_time`)
) ENGINE=innodb default charset utf8;

alter table attachment comment '附件表';

/*==============================================================*/
/* Table: console_authority                                     */
/*==============================================================*/
create table console_authority
(
   id                   int(11) not null auto_increment,
   role_id              int(11) not null default 0,
   menu_id              int(11) not null default 0,
   primary key (id),
   index(`role_id`),
   index(`menu_id`)
) ENGINE=innodb default charset utf8;

alter table console_authority comment '权限关联表';

/*==============================================================*/
/* Table: console_menu                                          */
/*==============================================================*/
create table console_menu
(
   id                   int(11) not null auto_increment,
   name                 national varchar(20) not null comment '菜单名称',
   parent_id            int(11) not null default 0 comment '父级菜单ID 0表示根节点',
   priority             int(6) not null default 0 comment '菜单顺序',
   branch               tinyint(1) not null default 0 comment '是否是枝节点 1-是 0-否',
   icon                 national varchar(20) not null comment '菜单图标样式',
   url                  national varchar(255) not null comment '菜单链接地址 总是以‘/’开头，相对于项目根路径',
   `label`              national varchar(40) not null comment '菜单标签 用于对同名菜单加以区分',
   primary key (id),
   index(`parent_id`),
   index(`priority`)
) ENGINE=innodb default charset utf8;

alter table console_menu comment '后台菜单表';

/*==============================================================*/
/* Table: console_role                                          */
/*==============================================================*/
create table console_role
(
   id                   int(11) not null auto_increment,
   name                 national varchar(60) not null comment '角色名称',
   primary key (id)
) ENGINE=innodb default charset utf8;

alter table console_role comment '后台角色表';

/*==============================================================*/
/* Table: console_user                                          */
/*==============================================================*/
create table console_user
(
   id                   int(11) not null auto_increment,
   username             varchar(60) not null comment '登录名',
   password             char(32) not null default '' comment '登录密码',
   real_name            varchar(40) not null default '' comment '用户姓名',
   status               tinyint(2) not null default 0 comment '用户状态 0-尚未初始化 1-启用',
   type                 tinyint(2) not null default 0 comment '用户类型 9-系统内置用户',
   skin                 varchar(10) not null default '' comment '配置——皮肤',
   navbar_fixed         tinyint(1) not null default 0 comment '配置——固定导航栏',
   menu_fixed           tinyint(1) not null default 0 comment '配置——固定菜单栏',
   breadcrumb_fixed     tinyint(1) not null default 0 comment '配置——固定位置信息',
   petty                tinyint(1) not null default 0 comment '配置——窄屏',
   primary key (id),
   unique index(`username`)
) ENGINE=innodb default charset utf8;

alter table console_user comment '后台管理员用户表';

/*==============================================================*/
/* Table: cust_address                                          */
/*==============================================================*/
create table cust_address
(
   id                   int not null auto_increment,
   customer_id          int,
   name                 varchar(20),
   mobile               varchar(16),
   location             varchar(120),
   longitude            int(9),
   latitude             int(8),
   `default`            tinyint(1),
   region_id            int,
   primary key (id),
   index(`customer_id`),
   index(`region_id`)
) ENGINE=innodb default charset utf8;

alter table cust_address comment '客户送餐地址';

/*==============================================================*/
/* Table: cust_cach                                             */
/*==============================================================*/
drop table if exists cust_cash;
create table cust_cash
(
   id                   int not null auto_increment,
   customer_id          int not null default 0 comment '客户ID',
   cash                 int(4) not null default 0 comment '代金券面值',
   `count`                int(4) not null default 0 comment '数量',
   primary key (id),
   index(`customer_id`),
   index(`cash`)
) ENGINE=innodb default charset utf8;

alter table cust_cash comment '客户代金券';

/*==============================================================*/
/* Table: cust_favoraties                                       */
/*==============================================================*/
create table cust_favoraties
(
   id                   int not null auto_increment,
   customer_id          int,
   type                 tinyint(2) comment '1-店铺收藏
            2-菜品收藏',
   target_id            int,
   primary key (id),
   index(`customer_id`),
   index(`type`)
) ENGINE=innodb default charset utf8;

alter table cust_favoraties comment '用户收藏';

/*==============================================================*/
/* Table: cust_message                                          */
/*==============================================================*/
create table cust_message
(
   id                   int not null auto_increment,
   customer_id          int,
   title                varchar(100),
   message              varchar(60),
   create_time          datetime not null default '2000-01-01',
   primary key (id),
   index(`customer_id`),
   index(`create_time`)
) ENGINE=innodb default charset utf8;

alter table cust_message comment '客户消息';

/*==============================================================*/
/* Table: customer                                              */
/*==============================================================*/
create table customer (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mobile` char(11) NOT NULL DEFAULT '' COMMENT '手机号码',
  `password` char(32) NOT NULL COMMENT '密码',
  `nickname` varchar(20) NOT NULL COMMENT '昵称',
  `score_highest` int(12) NOT NULL DEFAULT '0' COMMENT '经验值',
  `score` int(12) NOT NULL DEFAULT '0' COMMENT '用户积分',
  `avatar` varchar(255) NOT NULL COMMENT '头像',
  `reg_time` datetime NOT NULL DEFAULT '2000-01-01 00:00:00' COMMENT '注册时间',
  `push_key` varchar(100) NOT NULL DEFAULT '' COMMENT 'jpush机器号',
  `disabled` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否禁用 1是 0否',
  primary key (`id`),
  unique key `mobile` (`mobile`)
) ENGINE=innodb default charset utf8;

alter table customer comment '个人中心. 住户信息表';

/*==============================================================*/
/* Table: dish                                                  */
/*==============================================================*/
create table dish
(
   id                   int not null auto_increment,
   restaurant_id        int not null default 0,
   category_id          int not null default 0,
   name                 varchar(20),
   image                int,
   price                int(6) not null default 0,
   sold                 int(8) not null default 0,
   description          varchar(80),
   hot                  tinyint(1) not null default 0,
   enabled              tinyint(1) not null default 1,
   primary key (id),
   index(`restaurant_id`),
   index(`sold`)
) ENGINE=innodb default charset utf8;

alter table dish comment '菜品表';

/*==============================================================*/
/* Table: dish_category                                         */
/*==============================================================*/
create table dish_category
(
   id                   int not null auto_increment,
   name                 varchar(20) not null default '',
   primary key (id),
   UNIQUE KEY `name` (`name`)
) ENGINE=innodb default charset utf8;

alter table dish_category comment '菜品分类';

/*==============================================================*/
/* Table: employee                                              */
/*==============================================================*/
create table employee
(
   id                   int not null auto_increment,
   username             varchar(40),
   password             varchar(128),
   name                 varchar(40),
   type                 tinyint(2) comment '1-取菜员 2-配送员,3-分配员',
   longitude            int(9) comment '配送员有效',
   latitude             int(8) comment '配送员有效',
   region_id            int comment '配送员有效',
   center_id            int comment '取菜员对应分配员id' default 0,
   mobile               varchar(15) comment '员工电话号码',
   push_key             varchar(100) comment 'jpush机器号',
   status               tinyint(2) not null default 0 comment '状态 0-闲置 1-工作中 9-休息',
   primary key (id),
   index(`username`),
   index(`type`),
   index(`status`)
) ENGINE=innodb default charset utf8;

alter table employee comment '取菜员、分配员、 配送员 ';

/*==============================================================*/
/* Table: feedback                                              */
/*==============================================================*/
create table feedback
(
   id                   int(11) not null auto_increment,
   content              national varchar(2000) not null comment '反馈意见内容',
   create_time          datetime not null default '2000-01-01' comment '意见反馈时间',
   `read`               tinyint(2) not null default 0 comment '是否已读 1-已读 0-新提交未阅读',
   customer_id          int(11) not null default 0 comment '提交用户id.',
   primary key (id),
   index(`customer_id`),
   index(`create_time`),
   index(`read`)
) ENGINE=innodb default charset utf8;

alter table feedback comment '反馈意见';

/*==============================================================*/
/* Table: payment                                               */
/*==============================================================*/
create table payment
(
   id                   int not null auto_increment,
   orderno              char(12) not null comment '订单号',
   deliver_id           int not null default 0 comment '取菜员',
   courier_id           int not null default 0 comment '配送员',
   address_id           int not null default 0 comment '送餐地址ID',
   customer_id          int not null default 0 comment '客户ID',
   region_id            int not null default 0 comment '区域ID',
   restaurant_id        int not null default 0 comment '餐厅ID',
   restaurant_name      varchar(40) not null default '' comment '餐厅名称 不再使用',
   package              int(6) not null default 1 comment '打包费',
   delivery             int(6) not null default 9 comment '配送费',
   status               tinyint(2) not null default 0 comment '订单状态 0-未付款 1-支付失败 2-未指派 3-配餐中 4- 缺失菜品 5-配餐完成 6-配送中 8-已完成 9- 已取消',
   status_desc          varchar(40) not null default '' comment '订单状态描述',
   memo                 varchar(60) not null default '' comment '客户订餐备注',
   cash                 int not null default 0 comment '客户花费的代金券面值 单位：元',
   score                int not null default 0 comment '客户花费的积分',
   create_time          datetime not null default '2000-01-01',
   ready_time           datetime comment '取菜员确认时间（配餐完成）',
   assign_time          datetime comment '后台分配配送员时间（配送员接单时间）',
   star                 int(4) not null default 0 comment '星评',
   `comment`            varchar(100) comment '评论',
   comment_time         datetime not null default '2000-01-01' comment '客户评论时间',
   editable             tinyint(1) not null default 0 comment '是否可编辑，用于客户加菜',
   paid                 int(12) not null default 0 comment '订单已付的费用',
   alipay_no            char(64) not null default '' comment '支付宝订单ID',
   vip                  tinyint(1) not null default 0 comment '客户在下订单时是否是VIP',
   visible              tinyint(1) not null default 1 comment '是否显示在客户订单列表中',
   shared		int(4) NOT NULL DEFAULT '-1' comment '分型获得的积分 -1为默认未分享',
   primary key (id),
   index(`orderno`),
   index(`deliver_id`),
   index(`courier_id`),
   index(`create_time`),
   index(`status`),
   index(`visible`),
   index(`status`),
   index(`restaurant_id`),
   index(`comment_time`),
   index(`star`)
) ENGINE=innodb default charset utf8;

alter table payment comment '客户订单主表';

/*==============================================================*/
/* Table: payment_goods                                         */
/*==============================================================*/
create table payment_goods
(
   id                   int not null auto_increment,
   payment_id           int,
   dish_id              int,
   dish_name            varchar(40),
   price                int(5),
   count                int(4),
   status               tinyint(2),
   primary key (id),
   index(`payment_id`)
) ENGINE=innodb default charset utf8;

alter table payment_goods comment '订单菜品明细';

/*==============================================================*/
/* Table: payment_promotion                                     */
/*==============================================================*/
create table payment_promotion
(
   id                   int not null auto_increment,
   payment_id           int,
   pattern              varchar(20),
   param0               int,
   param1               int,
   param2               int,
   param3               int,
   money                int(6) not null default 0 comment '折扣 常为负值',
   primary key (id),
   index(`payment_id`)
) ENGINE=innodb default charset utf8;

alter table payment_promotion comment '订单优惠政策';

/*==============================================================*/
/* Table: payment_times                                         */
/*==============================================================*/
create table payment_times
(
   id                   int not null auto_increment,
   payment_id           int,
   status               tinyint(2),
   create_time          datetime not null default '2000-01-01',
   description          varchar(40) not null default '' comment '订单状态描述',
   cost_change          int not null default 0 comment '订单状态变化导致的费用变化量 正数表示加多少钱，负数表示退款多少钱，单位分。',
   primary key (id),
   index(`payment_id`)
) ENGINE=innodb default charset utf8;

alter table payment_times comment '订单状态变动';

/*==============================================================*/
/* Table: region                                                */
/*==============================================================*/
create table region
(
   id                   int not null auto_increment,
   name                 varchar(20) not null,
   primary key (id)
) ENGINE=innodb default charset utf8;

alter table region comment '区域';

/*==============================================================*/
/* Table: region_point                                          */
/*==============================================================*/
create table region_point
(
   id                   int not null auto_increment,
   region_id            int,
   longitude            int(9),
   latitude             int(8),
   primary key (id)
) ENGINE=innodb default charset utf8;

alter table region_point comment '区域顶点';

/*==============================================================*/
/* Table: rel_console_user_role                                 */
/*==============================================================*/
create table rel_console_user_role
(
   id                   int(11) not null auto_increment,
   user_id              int(11) not null,
   role_id              int(11) not null,
   primary key (id),
   index(`user_id`),
   index(`role_id`)
) ENGINE=innodb default charset utf8;

alter table rel_console_user_role comment '后台角色与用户关联表';

/*==============================================================*/
/* Table: rel_rest_category                                     */
/*==============================================================*/
create table rel_rest_category
(
   id                   int not null auto_increment,
   category_id          int,
   restaurant_id        int,
   primary key (id),
   index(`category_id`),
   index(`restaurant_id`)
) ENGINE=innodb default charset utf8;

alter table rel_rest_category comment '餐厅种类关联表';

/*==============================================================*/
/* Table: reply                                                 */
/*==============================================================*/
create table reply
(
   id                   int not null auto_increment,
   feedback_id          int,
   content              varchar(80),
   create_time          datetime,
   primary key (id),
   index(`feedback_id`)
) ENGINE=innodb default charset utf8;

alter table reply comment '反馈意见回复';

/*==============================================================*/
/* Table: restaurant                                            */
/*==============================================================*/
create table restaurant
(
   id                   int not null auto_increment,
   name                 varchar(60) not null default '' comment '餐厅名字',
   icon                 int not null default 0 comment '餐厅列表图标',
   images               varchar(32) not null default '' comment '餐厅详情图片. 所有图片存放在附件表，此处存放附件ID，多个用","分隔',
   price                int(6) not null default 0 comment '餐厅价位 单位：分',
   sold                 int(6) not null default 0 comment '销量',
   package              int(6) not null default 0 comment '打包费 单位：分',
   delivery             int(6) not null default 0 comment '配送费 单位：分',
   promotion            varchar(120) not null default '' comment '促销信息',
   open_time            time not null default '10:00' comment '营业时间--开始',
   close_time           time not null default '22:00' comment '营业时间-结束',
   description          longtext comment '餐厅简介',
   location             varchar(80) not null default '' comment '地址',
   tel                  varchar(18) not null default '' comment '联系电话',
   star                 int(4) not null default 0 comment '星评',
   hot_defined          tinyint(1) not null default 0 comment '是否配置了热门菜',
   enabled              tinyint(1) not null default 1 comment '是否处于开店状态 1-是 0-否',
   primary key (id),
   index(`star`),
   index(`price`)
) ENGINE=innodb default charset utf8;

alter table restaurant comment '餐厅';

/*==============================================================*/
/* Table: restaurant_category                                   */
/*==============================================================*/
create table restaurant_category
(
   id                   int not null auto_increment,
   name                 varchar(40),
   icon                 int,
   primary key (id)
) ENGINE=innodb default charset utf8;

alter table restaurant_category comment '餐厅种类表';

/*==============================================================*/
/* Table: restaurant_promotion                                  */
/*==============================================================*/
create table restaurant_promotion
(
   id                   int not null auto_increment,
   restaurant_id        int,
   pattern              varchar(20),
   param0               int,
   param1               int,
   param2               int,
   param3               int,
   primary key (id),
   index(`restaurant_id`),
   index(`pattern`)
) ENGINE=innodb default charset utf8;

alter table restaurant_promotion comment '订单优惠政策';

/*==============================================================*/
/* Table: static_resource                                       */
/*==============================================================*/
create table static_resource
(
   code                 varchar(40) not null comment '配置主键. 不使用自动递增，业务代码控制各种配置的id不重复',
   `desc`               varchar(40) not null comment '配置项说明',
   content              text,
   primary key (code)
) ENGINE=innodb default charset utf8;

alter table static_resource comment '静态数据、配置表';

/*==============================================================*/
/* Table: version_courier                                       */
/*==============================================================*/
create table version_courier
(
   id                   int(11) not null auto_increment,
   version              varchar(10) not null comment '版本',
   code                 int(6) not null default 0 comment '版本号数字. 高版本始终大于低版本',
   log                  varchar(500) not null comment '更新日志',
   download_url         int not null comment '下载地址',
   primary key (id),
   index(`code`)
) ENGINE=innodb default charset utf8;

alter table version_courier comment '版本更新表 配送员';

/*==============================================================*/
/* Table: version_customer                                      */
/*==============================================================*/
create table version_customer
(
   id                   int(11) not null auto_increment,
   version              varchar(10) not null comment '版本',
   code                 int(6) not null default 0 comment '版本号数字. 高版本始终大于低版本',
   log                  varchar(500) not null comment '更新日志',
   download_url         int not null comment '下载地址',
   download_times       int(10) not null default 0 comment '下载次数',
   primary key (id),
   index(`code`)
) ENGINE=innodb default charset utf8;

alter table version_customer comment '版本更新表';

/*==============================================================*/
/* Table: version_deliver                                       */
/*==============================================================*/
create table version_deliver
(
   id                   int(11) not null auto_increment,
   version              varchar(10) not null comment '版本',
   code                 int(6) not null default 0 comment '版本号数字. 高版本始终大于低版本',
   log                  varchar(500) not null comment '更新日志',
   download_url         int not null comment '下载地址',
   primary key (id),
   index(`code`)
) ENGINE=innodb default charset utf8;

alter table version_deliver comment '版本更新表 配菜员';

/*==============================================================*/
/* Table: share_record                                      */
/*==============================================================*/
CREATE TABLE share_record (
    id  int(11) not null auto_increment,
    orderno varchar(20) NOT NULL,
    customer_id int(11) NOT NULL,
    cash_value  int(11) NOT NULL,
    primary key (id),
    index (orderno),
    index (customer_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分享记录表';

create table alipay_refund_request(
    id int(11) not null auto_increment,
    batch_no char(29) not null,
    orderno char(14) not null,
    refund_date char(19) not null,
    detail_data varchar(127) not null,
    status tinyint(1) not null default -1,
    `type`  varchar(10) not null default '',
    primary key (id),
    unique key `batch_no` (`batch_no`),
    index (`type`)
) DEFAULT charset=utf8 comment='支付宝退款请求记录表';

create table alipay_refund_response(
    id int(11) not null auto_increment,
    batch_no char(29) not null,
    success_num int(3) not null,
    details varchar(127) not null,
    primary key (id),
    index (batch_no)
) default charset=utf8 comment='支付宝退款回复记录表';