<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
    </head>

    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf" %>
        <form class="form-horizontal validate" method="post" role="form">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="name"><span class="text-danger">*</span>活动名：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="name" name="name" value="${entity.name}" data-rule-required="true" data-rule-maxlength="25"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="name"></label>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="url"><span class="text-danger">*</span>活动地址：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="url" name="url" value="${entity.url}" data-rule-required="true" data-rule-maxlength="255"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="url"></label>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="startTime"><span class="text-danger">*</span>开始时间：</label>
                <div class="col-sm-5">
                    <span class="input-icon">
                        <input id="startTime" type="datetime" name="startTime" value="<fmt:formatDate value="${entity.startTime}" pattern="yyyy-MM-dd HH:mm"/>" placeholder="开始时间" data-rule-required="true"/>
                        <i class="icon-time"></i>
                    </span>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="startTime"></label>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="endTime"><span class="text-danger">*</span>结束时间：</label>
                <div class="col-sm-5">
                    <span class="input-icon">
                        <input id="endTime" type="datetime" name="endTime" value="<fmt:formatDate value="${entity.endTime}" pattern="yyyy-MM-dd HH:mm"/>" placeholder="结束时间"  data-rule-required="true"/>
                        <i class="icon-time"></i>
                    </span>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="endTime"></label>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="pushContent"><span class="text-danger">*</span>活动内容：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="pushContent" name="pushContent" value="${entity.pushContent}" data-rule-maxlength="60" data-rule-required="true"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="pushContent"></label>
            </div>
                <div class="clearfix form-actions">
                    <div class="col-sm-offset-3 col-sm-9">
                        <button type="submit" class="btn btn-primary">
                            <i class="icon-ok bigger-110"></i>
                            确定提交
                        </button>

                        <button type="button" class="btn btn-info action-back">
                            <i class="icon-undo bigger-110"></i>
                            取消返回
                        </button>
                    </div>
                </div>
            </form>
        <%@include file="/WEB-INF/jspf/body-last.jspf" %>
    </body>
</html>
