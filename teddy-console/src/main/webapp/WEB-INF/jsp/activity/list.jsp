<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
    </head>
    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf" %>
        <div class="row row-nocol">
            <form class="form-inline lookup" role="form" method="post">
                <div class="form-group">
                    <label class="sr-only" for="lookup-time-from">活动时间</label>
                    <span class="input-icon">
                        <input id="lookup-time-from" type="datetime" name="timeFrom" value="<fmt:formatDate value="${lookup.timeFrom}" pattern="yyyy-MM-dd HH:mm"/>" placeholder="活动时间"/>
                        <i class="icon-time"></i>
                    </span>
                </div>
                <div class="form-group">
                    <label class="sr-only" for="lookup-name">活动名</label>
                    <input id="lookup-customer" type="text" name="name" value="${lookup.name}" placeholder="活动名"/>
                </div>
                <div class="form-group">
                    <input name="_unpush" value="on" type="hidden"/>
                    <input type="checkbox" class="ace ace-switch ace-switch-6" id="lookup-unpush" name="unpush" value="true" ${lookup.unpush ? 'checked':''}/>
                    <label class="lbl" for="lookup-unpush">只显示未推送信息</label>
                </div>
                <div class="form-group form-btn-bar">
                    <button type="submit" class="btn btn-primary btn-sm"><i class="icon-search"></i> 查询</button>
                    <button type="button" class="btn btn-info btn-sm reset"><i class="icon-undo"></i> 重置</button>
                </div>
            </form>
            <hr/>
        </div>
        <c:if test="${not empty remind}">
            <div class="alert alert-${remind.level}">
                <button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>
                    ${remind.message}
            </div>
        </c:if>
        <form class="row row-nocol" method="post">
            <div class="action-bar">
                <a class="btn btn-sm btn-success" href="0"><i class="icon-file"></i> 添加</a>
                <button class="btn btn-sm btn-danger action-post" type="button"
                        data-href="remove"
                        data-checkbox-require="pks"
                        data-confirm="true"><span class="icon-trash"></span> 删除</button>
            </div>
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="center"><input type="checkbox" data-member="pks"/></th>
                        <th>序号</th>
                        <th>活动名</th>
                        <th>活动地址</th>
                        <th>开始时间</th>
                        <th>结束时间</th>
                        <th>推送时间</th>
                        <th>推送内容</th>
                        <th class="text-info">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${pager.elements}" var="cur" varStatus="status">
                        <tr>
                            <td class="center"><input type="checkbox" name="pks" value="${cur.id}"/></td>
                            <td>${status.count}</td>
                            <td>${cur.name}</td>
                            <td>${cur.url}</td>
                            <td><fmt:formatDate value="${cur.startTime}" pattern="yyyy-MM-dd HH:mm"/></td>
                            <td><fmt:formatDate value="${cur.endTime}" pattern="yyyy-MM-dd HH:mm"/></td>
                            <td><fmt:formatDate value="${cur.pushTime}" pattern="yyyy-MM-dd HH:mm"/></td>
                            <td>${fn:substring(cur.pushContent, 0, 26)}${fn:length(cur.pushContent) gt 26 ? "..." : ""}</td>
                            <td>
                                <div class="btn-group">
                                    <c:if test="${!cur.editable}">
                                        <a class="btn btn-xs btn-info tooltip-info" href="${cur.id}"
                                           data-rel="tooltip" data-original-title="编辑"> <i
                                                class="icon-edit bigger-120"></i>
                                        </a>
                                    </c:if>
                                    <c:if test="${cur.pushTime==null}">
                                        <button type="button" class="btn btn-xs btn-success action-post" data-href="${cur.id}/push" data-rel="tooltip" data-original-title="推送"data-confirm=" " >
                                            <i class="icon-link bigger-120"></i>
                                        </button>
                                    </c:if>
                                    <button
                                        class="btn btn-xs btn-danger tooltip-warning action-post" type="button"
                                        data-href="${cur.id}/remove" data-rel="tooltip"
                                        data-original-title="删除"
                                        data-confirm=" ">
                                        <i class="icon-trash bigger-120"></i> 
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    <c:forEach begin="${fn:length(pager.elements)}" end="${pager.size-1}">
                        <tr>
                            <td>&nbsp;</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </form>
        <%@include file="/WEB-INF/jspf/pager.jspf" %>
        <%@include file="/WEB-INF/jspf/body-last.jspf" %>
    </body>
</html>
