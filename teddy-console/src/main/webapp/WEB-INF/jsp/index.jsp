<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
        <style type="text/css">
            @media(min-width:1040px){
                .width-100 > div{
                    margin:4em;
                }
            }
        </style>
    </head>
    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf" %>
        <div class="width-100">
            <div class="text-center">
                <img class="img-responsive" src="" alt="首页" style="margin:auto;"/>
            </div>
        </div>
        <%@include file="/WEB-INF/jspf/body-last.jspf" %>
    </body>
</html>
