<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
    </head>
    
    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf" %>
        
        
        <form class="form-horizontal validate" method="post" role="form" data-ignore="">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="name"><span class="text-danger">*</span>分类：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="name" name="name" value="${entity.name}" data-rule-required="true" data-rule-maxlength="60"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="name"></label>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="shop-image"><span class="text-danger">*</span>分类图标：</label>
                <div class="col-sm-5">
                    <input type="hidden" name="icon" value="${entity.icon}" data-rule-required="true" />
                    <input class="fileupload" id="shop-icon" type="file" data-display-id="icon" data-preview="true" data-rule-accept="image/*"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="shop-icon"></label>
            </div>
            <div class="clearfix form-actions">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary">
                        <i class="icon-ok bigger-110"></i>
                        确定提交
                    </button>

                    <button type="button" class="btn btn-info action-back">
                        <i class="icon-undo bigger-110"></i>
                        取消返回
                    </button>
                </div>
            </div>
        </form>
        <%@include file="/WEB-INF/jspf/body-last.jspf" %>
    </body>
</html>
