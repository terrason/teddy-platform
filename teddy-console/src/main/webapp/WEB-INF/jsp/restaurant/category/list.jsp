<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
    </head>
    
    
    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf" %>
        </div>
                
        <c:if test="${not empty remind}">
            <div class="alert alert-${remind.level}">
                <button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>
                    ${remind.message}
            </div>
        </c:if>
        
        <form class="row row-nocol" method="post">
        
             <div class="action-bar">
                <a class="btn btn-sm btn-success" href="0"><i class="icon-file"></i> 添加</a>
                <button class="btn btn-sm btn-danger action-post" type="button"
                        data-href="remove"
                        data-checkbox-require="pks"
                        data-confirm="true"><span class="icon-trash"></span> 删除</button>
            </div> 
            
            
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="center"><input type="checkbox" data-member="pks"/></th>
                        <th>序号</th>
                        <th>类别</th>
                        <th>图标</th>
                        <th class="text-info">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${pager.elements}" var="cur" varStatus="status">
                        <tr>
                            <td class="center"><input type="checkbox" name="pks" value="${cur.id}"/></td>
                            <td>${status.count}</td>
                            <td>${cur.name}</td>
                            <td><img data-attachment="${cur.icon}" class="amplifier"/></td>
                            <td>
                                <div class="btn-group">
                                    <a class="btn btn-xs btn-info tooltip-info" href="${cur.id}" data-rel="tooltip" data-original-title="编辑">
                                        <i class="icon-edit bigger-120"></i>
                                    </a>
                                    <button class="btn btn-xs btn-danger tooltip-warning action-post" type="button" data-href="${cur.id}/remove" data-rel="tooltip" data-original-title="删除">
                                        <i class="icon-trash bigger-120"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </form>
        <%@include file="/WEB-INF/jspf/body-last.jspf" %>
    </body>
</html>
