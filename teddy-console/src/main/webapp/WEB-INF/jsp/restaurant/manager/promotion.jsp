<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf"%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8" />
        <%@include file="/WEB-INF/jspf/head.jspf"%>
        <title><spring:message code="application.title" /></title>
    </head>

    <body
        class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf"%>
        <form class="form-horizontal validate" >
            <div class="row row-nocol">
                <div class="col-sm-5" >
                    <h1>${entity.name} <small>优惠标签管理</small></h1>
                    <input type="hidden" value="${entity.id }" id="restaurantId">
                </div>
            </div>
        </form>


        <c:if test="${not empty remind}">
            <div class="alert alert-${remind.level}">
                <button data-dismiss="alert" class="close" type="button">
                    <i class="icon-remove"></i>
                </button>
                ${remind.message}
            </div>
        </c:if>


        <form class="row row-nocol" method="post">

            <div class="action-bar">
                <a class="btn btn-sm btn-success" onclick="addRow()"><i class="icon-file"></i>
                    添加</a> 
                <a class="btn btn-sm btn-success" href="back"><i class="icon-file"></i>
                    返回</a> 
            </div>


            <table class="table table-striped table-bordered table-hover" id="tableId">
                <thead>
                    <tr>
                        <th>序号</th>
                        <th>优惠标签</th>
                        <th>标签说明</th>
                        <th class="text-info">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${promotions}" var="cur" varStatus="status">
                    <input type="hidden" class="hidden-pattern" value="${cur.pattern }"/>
                    <tr>
                        <td>${status.count }</td>

                        <td data-parrern="${cur.pattern }" data-curid="${cur.id }"><span class="label label-danger" >${cur.tag }</span></td>

                        <td>${cur.description }<input type="hidden"  value="${cur.params }"></td>
                        <td>
                            <div class="btn-group">

                                <button type="button"  class="btn btn-xs btn-danger tooltip-warning "  data-rel="tooltip" type="button" data-original-title="编辑" data-confirm="" onclick="toEdit(this);" >
                                    <i class="icon-edit bigger-120"></i>
                                </button>
                                <button class="btn btn-xs btn-danger tooltip-warning action-get" type="button" data-href="${cur.id}/remove" data-rel="tooltip" data-original-title="删除" data-confirm="">
                                    <i class="icon-remove bigger-120"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                </c:forEach>

                </tbody>
            </table>
        </form>

        <div class="hidden" id="selectId">
            <select  class="form-control"  value=""  onchange="selectTag(this);">
                <option value="">--请选择优惠标签--</option>
                <option value="WIN_CASH">返</option>
                <option value="VIP_DISCOUNT">VIP</option>
                <option value="COST_CASH">券</option>
                <option value="COST_SCORE"> 积</option>
                <option value="FREE_POSTAGE"> 免</option>
            </select>
        </div>	
        <div id="hidden_btn" class="hidden">
            <div class="btn-group">
                <!-- <button type="button"  class="btn btn-xs btn-danger tooltip-warning " data-rel="tooltip" type="button" data-original-title="编辑" data-confirm="">
            <i class="icon-edit bigger-120"></i>
        </button> -->
                <button class="btn btn-xs btn-danger tooltip-warning " type="button" onclick="removeRow(this);" data-original-title="删除" data-confirm="">
                    <i class="icon-remove bigger-120"></i>
                </button>
                <button class="btn btn-xs btn-danger tooltip-warning " type="button"  data-rel="tooltip" data-original-title="确定" data-confirm="" onclick="save(this);">
                    <i class="icon-ok bigger-120"></i>
                </button>
            </div>
        </div>

        <div id="bind" >
            <div id="fan" class="hidden">满<input  type="text" class="param0" onkeyup="this.value = this.value.replace(/\D/g, '')" onafterpaste="this.value=this.value.replace(/\D/g,'')">元送<input type="text" class="param1" onkeyup="this.value = this.value.replace(/\D/g, '')" onafterpaste="this.value=this.value.replace(/\D/g,'')">张<input type="text" class="param2" onkeyup="this.value = this.value.replace(/\D/g, '')" onafterpaste="this.value=this.value.replace(/\D/g,'')">元代金券</div>
            <div id="vip" class="hidden">会员优惠<input type="text" class="param0" onkeyup="this.value = this.value.replace(/\D/g, '')" onafterpaste="this.value=this.value.replace(/\D/g,'')">折 </div>
            <div id="quan" class="hidden">可使用代金券</div>
            <div id="ji" class="hidden">满<input type="text" class="param0" onkeyup="this.value = this.value.replace(/\D/g, '')" onafterpaste="this.value=this.value.replace(/\D/g,'')">元可用<input type="text" class="param1" onkeyup="this.value = this.value.replace(/\D/g, '')" onafterpaste="this.value=this.value.replace(/\D/g,'')">积分抵1元现金 </div>
            <div id="mian" class="hidden">满<input type="text" class="param0" onkeyup="this.value = this.value.replace(/\D/g, '')" onafterpaste="this.value=this.value.replace(/\D/g,'')">元免邮费</div>
        </div>	

        <div id="con_btn" class="hidden">
            <button class="btn btn-xs btn-danger tooltip-warning " type="button"  data-rel="tooltip" data-original-title="确定" data-confirm="" onclick="save(this);">
                <i class="icon-ok bigger-120"></i>
            </button>
        </div>							
        <%-- <%@include file="/WEB-INF/jspf/pager.jspf"%> --%>
        <%@include file="/WEB-INF/jspf/body-last.jspf"%>
    </body>
    <script type="text/javascript">
        var allPatterns = "WIN_CASH,VIP_DISCOUNT,COST_CASH,COST_SCORE,FREE_POSTAGE".split(",");
        console.info(allPatterns);
        //进入页面时候获得已有标签
        var patterns = new Array();
        $.each($(".hidden-pattern"), function (i, pa) {
            patterns.push($(pa).val());
            //添加时候 删除已有的优惠标签
            /* $("#selectId select option[value="+$(pa).val()+"]").remove();
             var index = allPatterns.indexOf($(pa).val());
             if(index >-1){
             allPatterns.splice(index,1);
             } */
        });

        //添加一行数据
        function addRow() {
            if (patterns.length >= 5) {
                alert("一个餐厅最多只能添加五种优惠标签");
                return;
            }
            if ($("#tableId select").length > 0) {
                alert("上有标签未完成编辑，请完成编辑后再新增");
                return;
            }
            //1 行号
            var rowLength = $("#tableId tr").length;
            console.info("追加row  " + rowLength);
            //2 优惠标签
            $.each(patterns, function (i, p) {
                $("#selectId select option[value=" + p + "]").remove();
            });
            var selectHtml = $("#selectId").html();
            //3 按钮
            var btnHtml = $("#hidden_btn").html();
            var text = "<tr><td>" + rowLength + "</td> <td>" + selectHtml + "</td> <td></td><td>" + btnHtml + "</td></tr>";
            $("#tableId tbody").append(text);
        }
        //选择下拉列表时候触发
        function selectTag(obj, param) {
            var pattern = $(obj).val();
            console.info("pattern:" + pattern);
           
            if (pattern != "") {
                //patterns.push(pattern);
                //$("#selectId select option[value="+pattern+"]").remove();
                var descHtml = "";
                if (pattern == "WIN_CASH") {
                    descHtml = $("#fan").html();
                } else if (pattern == "VIP_DISCOUNT") {
                    descHtml = $("#vip").html();
                } else if (pattern == "COST_CASH") {
                    descHtml = $("#quan").html();
                } else if (pattern == "COST_SCORE") {
                    descHtml = $("#ji").html();
                } else if (pattern == "FREE_POSTAGE") {
                    descHtml = $("#mian").html();
                }
                console.info("选择下拉别表的时候descHtml:"+descHtml);
                $(obj).parent().next("td").html(descHtml);

                
                //var  params = $(obj).parent().next().find("input").val();
                var params = [];
             	if(undefined != param){
             		params = param.split(',');
             	}
                 console.info("params:"+params);
                if (params.length > 0) {
                    var $param0 = $(obj).parent().next().find(".param0");
                    var $param1 = $(obj).parent().next().find(".param1");
                    var $param2 = $(obj).parent().next().find(".param2");
                    if (pattern == "WIN_CASH") {
                        $($param0).val(params[0]);
                        $($param1).val(params[1]);
                        $($param2).val(params[2]);
                    } else if (pattern == "VIP_DISCOUNT") {
                        $($param0).val(params[0]);
                    } else if (pattern == "COST_CASH") {

                    } else if (pattern == "COST_SCORE") {
                        $($param0).val(params[0]);
                        $($param1).val(params[1]);
                    } else if (pattern == "FREE_POSTAGE") {
                        $($param0).val(params[0]);
                    }
                }
                
            }
        }
        /**确认保存*/
        function save(obj) {
            var td2 = $(obj).parents("tr").find("td")[1];
            var promotionId = $(td2).data("curid");
            if (promotionId == undefined) {
                promotionId = 0;
            }
            var pattern = $(obj).parents("tr").find("select").val();
            if (pattern == "") {
                return;
            } else {
                //数据
                var restaurantId = $("#restaurantId").val();
                var param0 = 0;
                var param1 = 0;
                var param2 = 0;
                if (pattern == "WIN_CASH") {
                    param0 = $(obj).parents("tr").find(".param0").val();
                    param1 = $(obj).parents("tr").find(".param1").val();
                    param2 = $(obj).parents("tr").find(".param2").val();
                    if (param0 == "" || param1 == "" || param2 == "") {
                        alert("请输入参数");
                        return;
                    }
                    param0 = $(obj).parents("tr").find(".param0").val();
                    param1 = $(obj).parents("tr").find(".param1").val();
                    param2 = $(obj).parents("tr").find(".param2").val();
                } else if (pattern == "VIP_DISCOUNT" || pattern == "FREE_POSTAGE") {
                    param0 = $(obj).parents("tr").find(".param0").val();
                    console.info("update vip" + param0);
                    if (param0 == "") {
                        alert("请输入参数");
                        return;
                    }

                } else if (pattern == "COST_SCORE") {
                    param0 = $(obj).parents("tr").find(".param0").val();
                    param1 = $(obj).parents("tr").find(".param1").val();
                    if (param0 == "" || param1 == "") {
                        alert("请输入参数");
                        return;
                    }
                    param2 = 1;
                }

                var data = "id=" + restaurantId + "&promotionId=" + promotionId + "&pattern=" + pattern + "&param0=" + (param0 || 0) + "&param1=" + (param1 || 0) + "&param2=" + (param2 || 0);
                console.info("data " + data);
                var url = "${ctx}" + "/restaurant/manager/promotion/add";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    async: false,
                    dataType: "text",
                    success: function (msg) {
                        console.info(msg);
                        if (msg != "success") {
                            alert("修改失败");
                        } else {//修改成功刷新页面
                            location.reload();
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("修改失败:" + XMLHttpRequest);
                    }


                });
            }

        }
        function removeRow(obj) {
            //删除行
            if ($("#tableId select").length > 0) {
                var tr = $(obj).parent().parent().parent();
                tr.remove();
            }
        }
        //编辑行数据
        function toEdit(obj) {
            if ($("#tableId select").length > 0) {
                alert("上有标签未完成编辑，请先完成编辑再操作");
                return;
            }
            var conbtnHtml = $("#con_btn").html();
            //添加确定按钮
            $(obj).parent().append(conbtnHtml);
            //获得 标签所在的td

            //添加编辑的数据
            var td2 = $(obj).parents("tr").find("td")[1];
            //编辑时候，原来的优惠标签的参数
            var params = $(obj).parents("tr").find("td input").val();
            console.info("params(td3):"+params);
            var pattern = $(td2).data("parrern");//当前行标签
            var index = patterns.indexOf(pattern);
            if (index > -1) {
                patterns.splice(index, 1);
            }

            $.each(patterns, function (i, p) {
                $("#selectId select option[value=" + p + "]").remove();
            });

            var editTd2 = $("#selectId").html();

            $(td2).html(editTd2);

            var $select = $(obj).parents("tr").find("select");
            $($select).val(pattern);
            selectTag($select, params);
            //删除 edit按钮
            $(obj).remove();


        }


    </script>
</html>
