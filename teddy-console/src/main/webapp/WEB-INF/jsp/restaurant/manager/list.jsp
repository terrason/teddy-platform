<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf"%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8" />
        <%@include file="/WEB-INF/jspf/head.jspf"%>
        <title><spring:message code="application.title" /></title>
    </head>


    <body
        class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf"%>
        <div class="row row-nocol">
            <form class="form-inline lookup" role="form" method="post">
                <div class="form-group">
                    <label class="sr-only" for="lookup-name">餐厅名称</label> <input
                        id="lookup-name" class="form-control" name="name"
                        value="${lookup.name}" placeholder="餐厅名称" />
                </div>

                <div class="form-group">
                    <label class="sr-only" for="lookup-categoryId">餐厅分类</label>
                    <select class="form-control" id="lookup-categoryId" name="categoryId" data-value="${lookup.categoryId}">
                        <option value="">--餐厅分类--</option>
                        <c:forEach items="${restaurantCategories}" var="cur">
                            <option value="${cur.id}">${cur.name}</option>
                        </c:forEach>
                    </select>
                </div>
                    <label class="sr-only" for="lookup-hot"></label>
                <div class="form-group">
                    <select class="form-control" id="lookup-hot" name="enabled" data-value="${lookup.enabled }"  >
                        <option value="">--餐厅状态--</option>
                        <option value="1">营业中</option>
                        <option value="0">已关闭</option>
                    </select>	
                </div>  


                <div class="form-group form-btn-bar">
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="icon-search"></i> 查询
                    </button>
                    <button type="button" class="btn btn-info btn-sm reset">
                        <i class="icon-undo"></i> 重置
                    </button>
                </div>

            </form>

        </div>

        <c:if test="${not empty remind}">
            <div class="alert alert-${remind.level}">
                <button data-dismiss="alert" class="close" type="button">
                    <i class="icon-remove"></i>
                </button>
                ${remind.message}
            </div>
        </c:if>
    <form class="row row-nocol" method="post">
        <div class="action-bar">
            <a class="btn btn-sm btn-success" href="0"><i class="icon-file"></i> 添加</a>
            <button class="btn btn-sm btn-danger action-post" type="button"
                    data-href="remove"
                    data-checkbox-require="pks"
                    data-confirm="true"><span class="icon-trash"></span> 删除</button>
        </div>
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th class="center"><input type="checkbox" data-member="pks" /></th>
                    <th>序号</th>
                    <th>餐厅名</th>
                    <th>图标</th>
                    <th>电话</th>
                    <th>状态</th>
                    <th class="text-info">操作</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${pager.elements}" var="cur" varStatus="status">
                    <tr>
                        <td class="center"><input type="checkbox" name="pks"
                                                  value="${cur.id}" /></td>
                        <td>${status.count}</td>
                        <td>${cur.name}</td>
                        <td><img data-attachment="${cur.icon}" class="amplifier"/></td>
                        <td>${cur.tel}</td>
                        <td><span class="label label-${cur.status==1 ? 'info' : (cur.status==0 ? 'danger':'success')} arrowed arrowed-right">${cur.statusName}</span></td>
                        <td>
                            <div class="btn-group">
                                <c:if test="${cur.enabled }">
                                    <button class="btn btn-xs btn-danger tooltip-warning action-post" type="button" data-href="${cur.id}/close" data-rel="tooltip" data-original-title="关闭餐厅" data-confirm="true">
                                        <i class="icon-lock bigger-120"></i>
                                    </button>
                                </c:if>  
                                <c:if test="${!cur.enabled}">
                                    <button class="btn btn-xs btn-success tooltip-success action-post" type="button" data-href="${cur.id}/open" data-rel="tooltip" data-original-title="打开餐厅">
                                        <i class="icon-unlock bigger-120"></i>
                                    </button> 
                                </c:if>   

                                <a class="btn btn-xs btn-info tooltip-info" href="${cur.id}"
                                   data-rel="tooltip" data-original-title="编辑"> <i
                                        class="icon-edit bigger-120"></i>
                                </a>

                                <a class="btn btn-xs btn-info btn-success" href="${cur.id}/detail"
                                   data-rel="tooltip" data-original-title="评论详情"> <i
                                        class="icon-comments bigger-120"></i>
                                </a>
                                
                                 <a class="btn btn-xs btn-info btn-info" href="${cur.id}/promotion/"
                                   data-rel="tooltip" data-original-title="优惠政策"> <i
                                        class="icon-money bigger-120"></i>
                                </a>
                                   
                                <a class="btn btn-xs btn-info btn-success" href="${cur.id}/dish/"
                                   data-rel="tooltip" data-original-title="菜品管理"> <i
                                        class="icon-exchange bigger-120"></i>
                                </a>

                                <button class="btn btn-xs btn-danger tooltip-warning action-post" type="button"
                                    data-href="${cur.id}/remove" data-rel="tooltip"
                                    data-original-title="删除"
                                    data-confirm=" ">
                                    <i class="icon-trash bigger-120"></i> 
                                </button>

                            </div>
                        </td>
                    </tr>
                </c:forEach>
                <c:forEach begin="${fn:length(pager.elements)}" end="${pager.size-1}">
                    <tr>
                        <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        </form>
        <%@include file="/WEB-INF/jspf/pager.jspf"%>
        <%@include file="/WEB-INF/jspf/body-last.jspf"%>
    </body>
</html>
