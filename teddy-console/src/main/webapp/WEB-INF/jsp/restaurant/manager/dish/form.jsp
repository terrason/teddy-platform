<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
    </head>
    
    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf" %>
        
        
        <form class="form-horizontal validate" method="post" role="form" data-ignore="">
           <div class="form-group">
                <label class="col-sm-3 control-label" for="name"><span class="text-danger">*</span>菜品名称：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="name" name="name" value="${entity.name}" data-rule-required="true" data-rule-maxlength="20"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="name"></label>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label" for="imagePath">菜品图标：</label>
                <div class="col-sm-5">
                    <input type="hidden" id="image" name="image" value="${entity.image}"/>
                    <input class="fileupload" id="imagePath" type="file" data-display-id="image"  data-preview="true" data-rule-accept="image/*"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="image"></label>
            </div>
            
           <div class="form-group">
                <label class="col-sm-3 control-label" for="categoryId"><span class="text-danger">*</span>菜品种类：</label>
                <div class="col-sm-5">
                    <select class="form-control" id="categoryId" name="categoryId" data-value="${entity.categoryId}" data-rule-required="true">
                        <option value="">----选择种类----</option>
                        <c:forEach items="${dishCategories}" var="cur">
                            <option value="${cur.id}">${cur.name}</option>
                        </c:forEach>
                    </select>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="categoryId"></label>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label" for="price"><span class="text-danger">*</span>价格：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="price" name="price" value="${entity.price}" data-rule-required="true" data-rule-number="true" data-rule-maxlength="11"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="price"></label>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label" for="sold"><span class="text-danger">*</span>销量：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="name" name="sold" value="${entity.sold}" data-rule-required="true" data-rule-digits="true" data-rule-maxlength="11"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="sold"></label>
            </div>
            
             <div class="form-group">
                <label class="col-sm-3 control-label" for="description">描述：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="description" name="description" value="${entity.description}" data-rule-maxlength="200"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="description"></label>
            </div>
            <div class="clearfix form-actions">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary">
                        <i class="icon-ok bigger-110"></i>
                        确定提交
                    </button>

                    <button type="button" class="btn btn-info action-back">
                        <i class="icon-undo bigger-110"></i>
                        取消返回
                    </button>
                </div>
            </div>
        </form>
        <%@include file="/WEB-INF/jspf/body-last.jspf" %>
    </body>
</html>
