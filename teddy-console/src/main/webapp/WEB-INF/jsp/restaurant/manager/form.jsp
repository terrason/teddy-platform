<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
    </head>

    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf" %>
        <form class="form-horizontal validate" method="post" role="form" data-ignore="">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="name"><span class="text-danger">*</span>餐厅名称：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="name" name="name" value="${entity.name}" data-rule-required="true" data-rule-maxlength="20"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="name"></label>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="categoryPks">餐厅种类：</label>
                <div class="col-sm-5">
                    <c:forEach items="${restaurantCategories}" var="cur" varStatus="status">
                        <label><input type="checkbox" name="categoryPks" value="${cur.id}" ${cur.checked} />${cur.name}</label>
                        </c:forEach>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="categoryPks"></label>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="price"><span class="text-danger"></span>人均消费：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="price" name="price" value="${entity.price}" data-rule-number="true" data-rule-maxlength="11"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="price"></label>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="packageFee"><span class="text-danger"></span>打包费：<br />（单位：分）</label>
                <div class="col-sm-5">
                    <input class="form-control" id="packageFee" name="packageFee" value="${entity.packageFee}" data-rule-digits="true" data-rule-maxlength="11"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="packageFee"></label>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="delivery"><span class="text-danger"></span>配送费：<br />（单位：分）</label>
                <div class="col-sm-5">
                    <input class="form-control" id="delivery" name="delivery" value="${entity.delivery}" data-rule-digits="true" data-rule-maxlength="11"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="delivery"></label>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="shop-icon"><span class="text-danger">*</span>餐厅图标：</label>
                <div class="col-sm-5">
                    <input type="hidden" name="icon" value="${entity.icon}" data-rule-required="true" />
                    <input class="fileupload" id="shop-icon" type="file" data-display-id="icon" data-preview="true" data-rule-accept="image/*"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="shop-icon"></label>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="shop-images"><span class="text-danger">*</span>餐厅轮播图片：</label>
                <div class="col-sm-5">
                    <input type="hidden" name="images" value="${entity.images}" />
                    <input class="fileupload" id="shop-images" type="file" multiple data-display-id="images" data-rule-required="true" data-rule-accept="image/*"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="shop-images"></label>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="promotion">促销信息：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="promotion" name="promotion" value="${entity.promotion}" data-rule-maxlength="500"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="promotion"></label>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="description">描述：</label>
                <div class="col-sm-6">
                    <div class="form-control wysiwyg-editor" id="description" data-bind-input="description">${entity.description}</div>
                    <input type="hidden"  name="description"/>
                </div>
                <label class="col-sm-3 help-inline form-control-static" for="description"></label>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="openTime">开始时间：</label>
                <div class="col-sm-5">
                    <span class="input-icon">
                        <input id="closeTime" type="time" name="openTime" value="<fmt:formatDate value="${entity.openTime}" pattern="HH:mm"/>" placeholder="开始时间" data-rule-required="true"/>
                        <i class="icon-time"></i>
                    </span>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="openTime"></label>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="closeTime">结束时间：</label>
                <div class="col-sm-5">
                    <span class="input-icon">
                        <input id="closeTime" type="time" name="closeTime" value="<fmt:formatDate value="${entity.closeTime}" pattern="HH:mm"/>" placeholder="结束时间"  data-rule-required="true"/>
                        <i class="icon-time"></i>
                    </span>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="closeTime"></label>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="location"><span class="text-danger">*</span>地址：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="location" name="location" value="${entity.location}" data-rule-required="true" data-rule-maxlength="200"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="location"></label>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="location">联系电话：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="tel" name="tel" value="${entity.tel}" data-rule-maxlength="20"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="tel"></label>
            </div>
            <c:if test="${not empty dishes}">
                <div class="form-group" id="hotId">
                    <label class="col-sm-3 control-label" for="shop-hotDefined">&nbsp;</label>
                    <div class="col-sm-5">
                        <div class="row row-nocol">
                            <input name="_hotDefined" value="on" type="hidden"/>
                            <input class="ace ace-switch ace-switch-5" type="checkbox" id="shop-hotDefined" name="hotDefined" ${entity.hotDefined ? "checked":""}>
                            <label class="lbl" for="shop-hotDefined">自定义热菜</label>
                        </div>
                        <div class="row row-nocol" id="hotDefinedContent" style="margin-top:1em;">
                            <select class="width-100 tag-input-style" id="hotDishes" name="hotDishes" data-placeholder="选择热门菜" multiple data-rule-required="true">
                                <c:forEach items="${dishes}" var="cur">
                                    <option value="${cur.id}" ${cur.hot?"selected":""}>${cur.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <label class="col-sm-4 help-inline form-control-static" for="hotDishes"></label>
                </div>
            </c:if>
            <div class="clearfix form-actions">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary">
                        <i class="icon-ok bigger-110"></i>
                        确定提交
                    </button>

                    <button type="button" class="btn btn-info action-back">
                        <i class="icon-undo bigger-110"></i>
                        取消返回
                    </button>
                </div>
            </div>
        </form>
        <%@include file="/WEB-INF/jspf/body-last.jspf" %>

        <script type="text/javascript">
            $(document).ready(function () {
                var $hotDishes = $("#hotDishes");
                var $hotDefinedContent = $("#hotDefinedContent");
                $hotDishes.chosen({
                    inherit_select_classes: true,
                    max_selected_options: 3
                });
                $("#shop-hotDefined").change(function () {
                    $hotDishes.prop("disabled", !this.checked);
                    if (this.checked) {
                        $hotDefinedContent.show();
                    } else {
                        $hotDefinedContent.hide();
                    }
                    $hotDishes.trigger("chosen:updated");
                }).change();
            });
        </script>
    </body>
</html>
