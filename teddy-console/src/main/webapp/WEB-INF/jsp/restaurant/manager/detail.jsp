<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
    </head>
    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf" %>
        <h1>${restaurant.name}
            <small>
            <c:forEach  begin="1" end="${restaurant.star/2}">
                <i class="icon-star bigger-120"></i>
            </c:forEach>
            <c:if test="${restaurant.star%2==1}">
                <i class="icon-star-half bigger-120"></i>
            </c:if>
            <c:forEach  begin="1" end="${(10-restaurant.star)/2}">
                <i class="icon-star-empty bigger-120"></i>
            </c:forEach>
            </small>
        </h1>
        <form class="form-horizontal validate" method="post">
            <div style="clear:both">
                <h2><c:if test="${comment.size()<=0}">暂无评论</c:if></h2>
                </div>

                <div style="border:1px solid #dde4ed">
                <c:forEach items="${comment}" var="cur">
                    <div class="reply dialogdiv" >
                        <div class="body" >
                            <div class="name"><span class="blue">${cur.nickname}:</span></div>
                            <div class="text">
                                <c:forEach  begin="1" end="${cur.star/2}">
                                    <i class="icon-star bigger-120"></i>
                                </c:forEach>
                                <c:if test="${cur.star%2==1}">
                                    <i class="icon-star-half bigger-120"></i>
                                </c:if>
                                <c:forEach  begin="1" end="${(10-cur.star)/2}">
                                    <i class="icon-star-empty bigger-120"></i>
                                </c:forEach>
                            </div>
                            <div class="text"><pre>${cur.comment}</pre></div>
						<div class="text"><span class="grey"><fmt:formatDate value="${cur.commentTime}" pattern="yyyy-MM-dd HH:mm"/></span></div>
					</div>
                                        <br>
				</div>
                </c:forEach>
            </div>
            
            <div class="clearfix form-actions">   
	                <button type="button" class="btn btn-info action-back">
	                        <i class="icon-undo bigger-110"></i>返回
	                </button>
                </div>
            </div>
        </form>
    <%@include file="/WEB-INF/jspf/body-last.jspf" %>
    </body>
</html>
