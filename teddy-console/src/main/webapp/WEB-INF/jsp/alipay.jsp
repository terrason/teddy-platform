<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <form id="pay-form" action="https://mapi.alipay.com/gateway.do" method="GET">
            <c:forEach items="${data}" var="cur">
                <input type="hidden" name="${cur.key}" value="${cur.value}"/>
            </c:forEach>
            <button type="submit" style="display:none;">submit</button>
        </form>
        <script type="text/javascript">
            document.getElementById("pay-form").submit();
        </script>
    </body>
</html>
