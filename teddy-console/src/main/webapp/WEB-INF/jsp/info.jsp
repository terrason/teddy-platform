<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
    </head>
    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf" %>
        <div class="error-container">
            <div class="well">
                <h1 class="grey lighter smaller">
                    <span class="blue bigger-125">
                        <i class="icon-cloud"></i>
                    </span>
                    操作失败
                </h1>

                <hr />

                <pre class="alert alert-danger">${exception.message}</pre>
                <hr />
                <h3 class="lighter smaller">
                    ${message}
                </h3>

                <div class="space"></div>

                <div>
                    <h4 class="lighter smaller">Meanwhile, try one of the following:</h4>

                    <ul class="list-unstyled spaced inline bigger-110 margin-15">
                        <li>
                            <i class="icon-hand-right blue"></i>
                            Read the faq
                        </li>

                        <li>
                            <i class="icon-hand-right blue"></i>
                            Give us more info on how this specific error occurred!
                        </li>
                    </ul>
                </div>

                <hr />
                <div class="space"></div>

                <div class="center">
                    <a href="javascript:window.history.back();" class="btn btn-grey">
                        <i class="icon-arrow-left"></i>
                        Go Back
                    </a>
                    <c:if test="${not empty alter}">
                        <a href="${ctx}${alter.url}" class="btn btn-${alter.level}">
                            <i class="icon-dashboard"></i>
                            ${alter.name}
                        </a>
                    </c:if>
                </div>
            </div>
        </div>
        <%@include file="/WEB-INF/jspf/body-last.jspf" %>
    </body>
</html>
