<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
    </head>
    <body class="login-layout">
        <div class="main-container">
            <div class="main-content">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="login-container" style="width:400px">
                            <div class="center">
                                <h1>
                                    <i class="icon-home green"></i>
                                    <span class="red"><spring:message code="application.title"/></span>
                                    <span class="white">后台管理系统</span>
                                </h1>
                                <h4 class="blue">&copy; 鸿云来科技</h4>
                            </div>

                            <div class="space-6"></div>

                            <div class="position-relative">
                                <div class="login-box visible widget-box no-border" id="login-box">
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <h4 class="header blue lighter bigger">
                                                <i class="icon-coffee green"></i>
                                                用户登录
                                            </h4>

                                            <div class="space-6"></div>

                                            <form class="validate" action="${ctx}/login" method="post">
                                                <fieldset>
                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input class="form-control" name="username" placeholder="登录名" data-rule-required="true">
                                                            <i class="icon-user"></i>
                                                        </span>
                                                    </label>

                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input class="form-control" type="password" name="password" placeholder="登录密码" data-rule-required="true">
                                                            <i class="icon-lock"></i>
                                                        </span>
                                                    </label>

                                                    <div class="space"></div>

                                                    <div class="clearfix">
                                                        <label class="inline">
                                                            <input type="checkbox" class="ace" name="rememberme">
                                                            <span class="lbl">下次自动登录</span>
                                                        </label>

                                                        <button class="width-35 pull-right btn btn-sm btn-primary" type="submit">
                                                            <i class="icon-key"></i>
                                                            登录
                                                        </button>
                                                    </div>

                                                    <div class="space-4"></div>
                                                </fieldset>
                                            </form>
                                        </div><!-- /widget-main -->
                                    </div><!-- /widget-body -->
                                </div><!-- /login-box -->
                            </div><!-- /position-relative -->
                        </div>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div>
        </div>
    </body>
</html>
