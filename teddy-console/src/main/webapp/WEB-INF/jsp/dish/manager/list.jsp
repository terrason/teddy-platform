<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf"%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8" />
        <%@include file="/WEB-INF/jspf/head.jspf"%>
        <title><spring:message code="application.title" /></title>
    </head>


    <body
        class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf"%>


        <div class="row row-nocol">
            <form class="form-inline lookup" role="form" method="post">

                <div class="form-group">
                    <label class="sr-only" for="lookup-restaurantId">所属餐厅</label>
                    <select class="form-control" id="lookup-restaurantId" name="restaurantId" data-value="${lookup.restaurantId}">
                        <option value="">--所属餐厅--</option>
                        <c:forEach items="${restaurants}" var="cur">
                            <option value="${cur.id}">${cur.name}</option>
                        </c:forEach>
                    </select>
                </div>

                <div class="form-group">
                    <label class="sr-only" for="lookup-name">菜品名称</label> 
                    <input id="lookup-name" class="form-control" name="name"
                           value="${lookup.name}" placeholder="菜品名称" />
                </div>

                <div class="form-group">
                    <label class="sr-only" for="lookup-categoryId">菜品分类</label>
                    <select class="form-control" id="lookup-categoryId" name="categoryId" data-value="${lookup.categoryId}">
                        <option value="">--菜品分类--</option>
                        <c:forEach items="${dishCategories}" var="cur">
                            <option value="${cur.id}">${cur.name}</option>
                        </c:forEach>
                    </select>
                </div>

                <div class="form-group">
                    <input name="_disabled" value="on" type="hidden"/>
                    <input type="checkbox" class="ace ace-switch ace-switch-6" id="lookup-disabled" name="disabled" value="true" ${lookup.disabled ? 'checked':''}/>
                    <label class="lbl" for="lookup-disabled">只显示关闭的菜品</label>
                </div>  


                <div class="form-group form-btn-bar">
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="icon-search"></i> 查询
                    </button>
                    <button type="button" class="btn btn-info btn-sm reset">
                        <i class="icon-undo"></i> 重置
                    </button>
                </div>

            </form>

        </div>

        <c:if test="${not empty remind}">
            <div class="alert alert-${remind.level}">
                <button data-dismiss="alert" class="close" type="button">
                    <i class="icon-remove"></i>
                </button>
                ${remind.message}
            </div>
        </c:if>

        <form class="row row-nocol" method="post">

            <div class="action-bar">
                <a class="btn btn-sm btn-success" href="0"><i class="icon-file"></i>
                    添加</a>
                <button class="btn btn-sm btn-danger action-post" type="button"
                        data-href="remove" data-checkbox-require="pks" data-confirm="true">
                    <span class="icon-trash"></span> 批量删除
                </button>

            </div>

            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="center"><input type="checkbox" data-member="pks" /></th>
                        <th>序号</th>
                        <th>菜名</th>
                        <th>图标</th>
                        <th>所属餐厅</th>
                        <th>菜品分类</th>
                        <th>价格</th>
                        <th>销量</th>
                        <th>描述</th>
                        <th>状态</th>
                        <th class="text-info">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${pager.elements}" var="cur" varStatus="status">
                        <tr>
                            <td class="center"><input type="checkbox" name="pks"
                                                      value="${cur.id}" /></td>
                            <td>${status.count}</td>
                            <td>${cur.name}</td>
                            <td><img data-attachment="${cur.image}" src="" alt="" class="amplifier"/> </td>
                            <td>${cur.restaurantName}</td>
                            <td>${cur.categoryName}</td>
                            <td>${cur.price}</td>
                            <td>${cur.sold}</td>
                            <td>${fn:substring(cur.description, 0, 30)}${fn:length(cur.description) gt 30 ? "..." : ""}</td>
                            <td>
                                <c:if test="${cur.hot}"><span class="badge badge-danger">热</span></c:if>
                                <c:if test="${!cur.enabled}"><span class="badge badge-inverse">缺</span></c:if>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <c:if test="${cur.enabled }">
                                        <button class="btn btn-xs btn-danger tooltip-warning action-post" type="button" data-href="${cur.id}/close" data-rel="tooltip" data-original-title="关闭菜品" data-confirm="若该菜品被关闭，则客户无法选择该菜品无法选择！">
                                            <i class="icon-lock bigger-120"></i>
                                        </button>
                                    </c:if>  
                                    <c:if test="${!cur.enabled}">
                                        <button class="btn btn-xs btn-success tooltip-warning action-post" type="button" data-href="${cur.id}/open" data-rel="tooltip" data-original-title="打开菜品" data-confim="若该菜品被打开，则客户可以选择该菜品！">
                                            <i class="icon-unlock bigger-120"></i>
                                        </button> 
                                    </c:if>   
                                    <a class="btn btn-xs btn-info tooltip-info" href="${cur.id}"
                                       data-rel="tooltip" data-original-title="编辑"> <i
                                            class="icon-edit bigger-120"></i>
                                    </a>

                                    <button class="btn btn-xs btn-danger tooltip-warning action-post" type="button"
                                        data-href="${cur.id}/remove" data-rel="tooltip"
                                        data-original-title="删除"
                                        data-confirm=" ">
                                        <i class="icon-trash bigger-120"></i> 
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    <c:forEach begin="${fn:length(pager.elements)}"
                               end="${pager.size-1}">
                        <tr>
                            <td>&nbsp;</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </form>
        <%@include file="/WEB-INF/jspf/pager.jspf"%>
        <%@include file="/WEB-INF/jspf/body-last.jspf"%>
    </body>
</html>
