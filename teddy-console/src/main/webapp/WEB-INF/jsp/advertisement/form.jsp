<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
    </head>

    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf" %>
        <form class="form-horizontal validate" method="post" role="form">
            <div class="form-group" id="category">
                <label class="col-sm-3 control-label" for="category">*广告位类别：</label>
                <div class="col-sm-5">
                    <select class="form-control"name="category" data-value="${entity.category}" data-rule-required="true">
                        <option value="1" >首页广告位</option>
                        <option value="2" >餐厅广告位</option>
                        <option value="3" >网站广告位</option>
                    </select>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="hotDefined"></label>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="image"><span class="text-danger">*</span>图片：</label>
                <div class="col-sm-5">
                    <input type="hidden" name="image" value="${entity.image}" data-rule-required="true" />
                    <input class="fileupload" id="shop-image" type="file" data-display-id="image" data-preview="true" data-rule-accept="image/*"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="image"></label>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="url"><span class="text-danger">*</span>广告跳转地址：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="url" name="url" value="${entity.url}" data-rule-required="true" data-rule-maxlength="250"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="url"></label>
            </div>
                <div class="clearfix form-actions">
                    <div class="col-sm-offset-3 col-sm-9">
                        <button type="submit" class="btn btn-primary">
                            <i class="icon-ok bigger-110"></i>
                            确定提交
                        </button>

                        <button type="button" class="btn btn-info action-back">
                            <i class="icon-undo bigger-110"></i>
                            取消返回
                        </button>
                    </div>
                </div>
            </form>
        <%@include file="/WEB-INF/jspf/body-last.jspf" %>
    </body>
</html>
