<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf"%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8" />
        <%@include file="/WEB-INF/jspf/head.jspf"%>
        <title><spring:message code="application.title" /></title>
    </head>


    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf"%>
        <div class="row row-nocol">
            <h1>客户 <strong>${customer.nickname}</strong> 代金券管理</h1>
        </div>
        <c:if test="${not empty remind}">
            <div class="alert alert-${remind.level}">
                <button data-dismiss="alert" class="close" type="button">
                    <i class="icon-remove"></i>
                </button>
                ${remind.message}
            </div>
        </c:if>
        <div class="action-bar">
                <a class="btn btn-sm btn-success" href="0"><i class="icon-file"></i> 添加</a>
                <a class="btn btn-sm btn-success" href="back"><i class="icon-file"></i> 返回</a>
        </div>
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>序号</th>
                    <th>面值</th>
                    <th>数量</th>
                    <th class="text-info">操作</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${cash}" var="cur" varStatus="status">
                    <tr>
                        <td>${status.count}</td>
                        <td>${cur.cash}</td>
                        <td>${cur.count}</td>
                        <td>
                            <div class="btn-group">
                                    <a class="btn btn-xs btn-info tooltip-info" href="${cur.id}"
                                            data-rel="tooltip" data-original-title="编辑"> <i
                                            class="icon-edit bigger-120"></i>
                                    </a>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
                <c:if test="${empty cash}">
                    <tr><td colspan="6" class="text-warning">客户还未获得任何代金券...</td></tr>
                </c:if>
            </tbody>
        </table>
    <%@include file="/WEB-INF/jspf/body-last.jspf"%>
</body>
</html>
