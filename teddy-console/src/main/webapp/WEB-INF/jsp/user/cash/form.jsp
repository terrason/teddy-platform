<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
    </head>

    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf" %>
        <form class="form-horizontal validate" method="post" role="form">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="cash"><span class="text-danger">*</span>代金券面值：</label>
                <div class="col-sm-5">
                    <select class="form-control" id="cash" name="cash" data-value="${entity.cash }" data-rule-required="true" >
                        <option value="5">5元</option>
                        <option value="10">10元</option>
                        <option value="20">20元</option>
                        <option value="50">50元</option>
                        <option value="100">100元</option>
                    </select>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="cash"></label>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label" for="count"><span class="text-danger">*</span>代金券数量：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="count" name="count" value="${entity.count}" data-rule-required="true" data-rule-number="true" data-rule-maxlength="4"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="count"></label>
            </div>
            <div class="clearfix form-actions">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary">
                        <i class="icon-ok bigger-110"></i>
                        确定提交
                    </button>

                    <button type="button" class="btn btn-info action-back">
                        <i class="icon-undo bigger-110"></i>
                        取消返回
                    </button>
                </div>
            </div>
        </form>
        <%@include file="/WEB-INF/jspf/body-last.jspf" %>
    </body>
</html>
