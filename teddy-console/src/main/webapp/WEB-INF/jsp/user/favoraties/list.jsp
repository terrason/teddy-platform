<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf"%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8" />
        <%@include file="/WEB-INF/jspf/head.jspf"%>
        <title><spring:message code="application.title" /></title>
    </head>


    <body
        class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf"%>

        <c:if test="${not empty remind}">
            <div class="alert alert-${remind.level}">
                <button data-dismiss="alert" class="close" type="button">
                    <i class="icon-remove"></i>
                </button>
                ${remind.message}
            </div>
        </c:if>
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>序号</th>
                    <th>收藏类别</th>
                    <th>收藏物品</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${favoraties}" var="cur" varStatus="status">
                    <tr>
                        <td>${status.count}</td>
                        <td>${cur.type==1  ? '餐厅收藏' : '菜品收藏'}</td>
                        <td>${cur.name}</td>
                    </tr>
                </c:forEach>
                <c:if test="${empty favoraties}">
                    <tr><td colspan="3" class="text-warning">客户还未收藏过任何东东...</td></tr>
                </c:if>
            </tbody>
        </table>
        <a class="btn btn-sm btn-success" href="back"><i class="icon-file"></i> 返回</a>
    <%@include file="/WEB-INF/jspf/body-last.jspf"%>
</body>
</html>
