<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf"%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8" />
        <%@include file="/WEB-INF/jspf/head.jspf"%>
        <title><spring:message code="application.title" /></title>
    </head>


    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf"%>


        <div class="row row-nocol">
            <form class="form-inline lookup" role="form" method="post">
                <div class="form-group">
                    <label class="sr-only" for="lookup-nickname">客户昵称</label> <input
                        id="lookup-nickname" class="form-control" name="nickname"
                        value="${lookup.nickname}" placeholder="客户昵称" />
                </div>
                <div class="form-group">
                    <label class="sr-only" for="lookup-mobile">客户号码</label> <input
                        id="lookup-mobile" class="form-control" name="mobile"
                        value="${lookup.mobile}" placeholder="客户号码" />
                </div>

                <div class="form-group form-btn-bar">
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="icon-search"></i> 查询
                    </button>
                    <button type="button" class="btn btn-info btn-sm reset">
                        <i class="icon-undo"></i> 重置
                    </button>
                </div>

            </form>

        </div>

        <c:if test="${not empty remind}">
            <div class="alert alert-${remind.level}">
                <button data-dismiss="alert" class="close" type="button">
                    <i class="icon-remove"></i>
                </button>
                ${remind.message}
            </div>
        </c:if>
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>序号</th>
                    <th>昵称</th>
                    <th>头像</th>
                    <th>电话号码</th>
                    <th>积分</th>
                    <th class="text-info">操作</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${pager.elements}" var="cur" varStatus="status">
                    <tr>
                        <td>${status.count}</td>
                        <td>${cur.nickname}</td>
                        <td><img data-src="${cur.avatar }" src="" alt="" class="amplifier"/> </td>
                        <td>${cur.mobile}</td>
                        <td>${cur.score}</td>
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-xs btn-info tooltip-info" href="${cur.id}"
                                   data-rel="tooltip" data-original-title="编辑"> <i
                                        class="icon-edit bigger-120"></i>
                                </a>
                                <a class="btn btn-xs btn-success tooltip-success" href="${cur.id}/favoraties/"
                                   data-rel="tooltip" data-original-title="收藏详情"> <i
                                        class="icon-heart bigger-120"></i>
                                </a>
                                <a class="btn btn-xs btn-warning tooltip-warning" href="${cur.id}/cash/"
                                   data-rel="tooltip" data-original-title="代金券详情"> <i
                                        class="icon-jpy bigger-120"></i>
                                </a>
                                <a class="btn btn-xs btn-purple" href="${cur.id}/address/"
                                   data-rel="tooltip" data-original-title="送餐地址详情"> <i
                                        class="icon-file-text bigger-120"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
                <c:forEach begin="${fn:length(pager.elements)}"
                           end="${pager.size-1}">
                    <tr>
                        <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <%@include file="/WEB-INF/jspf/pager.jspf"%>
        <%@include file="/WEB-INF/jspf/body-last.jspf"%>
    </body>
</html>
