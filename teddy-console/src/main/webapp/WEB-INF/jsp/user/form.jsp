<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
    </head>

    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf" %>
        <form class="form-horizontal validate" method="post" role="form">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="nickname"><span class="text-danger">*</span>客户昵称：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="nickname" name="nickname" value="${entity.nickname}" data-rule-required="true" data-rule-maxlength="20"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="nickname"></label>
            </div>
                
            <div class="form-group">
                <label class="col-sm-3 control-label" for="mobile"><span class="text-danger">*</span>客户号码：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="mobile" name="mobile" value="${entity.mobile}" data-rule-required="true" data-rule-mobile="true"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="mobile"></label>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="avatar"><span class="text-danger">*</span>头像：</label>
                <div class="col-sm-5">
                    <input type="hidden" name="avatar" value="${entity.avatar}" />
                    <input class="fileupload" id="shop-image" type="file" data-display-value="avatar" data-rule-required="false" data-preview="true" data-rule-accept="image/*"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="avatar"></label>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="score"><span class="text-danger"></span>积分：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="score" name="score" value="${entity.score}" data-rule-number="true" data-rule-maxlength="11"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="score"></label>
            </div>

            <div class="clearfix form-actions">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary">
                        <i class="icon-ok bigger-110"></i>
                        确定提交
                    </button>

                    <button type="button" class="btn btn-info action-back">
                        <i class="icon-undo bigger-110"></i>
                        取消返回
                    </button>
                </div>
            </div>
        </form>
        <%@include file="/WEB-INF/jspf/body-last.jspf" %>
    </body>
</html>
