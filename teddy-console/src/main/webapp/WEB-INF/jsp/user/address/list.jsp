<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf"%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8" />
        <%@include file="/WEB-INF/jspf/head.jspf"%>
        <title><spring:message code="application.title" /></title>
    </head>


    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf"%>
        <div class="row row-nocol">
            <h1>客户 <strong>${customer.nickname}</strong> 送餐地址管理</h1>
        </div>
        <c:if test="${not empty remind}">
            <div class="alert alert-${remind.level}">
                <button data-dismiss="alert" class="close" type="button">
                    <i class="icon-remove"></i>
                </button>
                ${remind.message}
            </div>
        </c:if>
        <div class="action-bar">
                <a class="btn btn-sm btn-success" href="0"><i class="icon-file"></i> 添加</a>
                <a class="btn btn-sm btn-success" href="back"><i class="icon-file"></i> 返回</a>
        </div>
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>序号</th>
                    <th>姓名</th>
                    <th>电话</th>
                    <th>地址</th>
                    <th>是否默认地址</th>
                    <th class="text-info">操作</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${address}" var="cur" varStatus="status">
                    <tr>
                        <td>${status.count}</td>
                        <td>${cur.name}</td>
                        <td>${cur.mobile}</td>
                        <td>${cur.location}</td>
                        <td><span class="label label-${cur.defaultAddress ? 'info' : 'danger'} arrowed arrowed-right">${cur.defaultAddress  ? '是' : '否'}</td>
                        <td>
                            <div class="btn-group">
                                    <a class="btn btn-xs btn-info tooltip-info" href="${cur.id}"
                                            data-rel="tooltip" data-original-title="编辑"> <i
                                            class="icon-edit bigger-120"></i>
                                    </a>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
                <c:if test="${empty address}">
                    <tr><td colspan="6" class="text-warning">客户还未添加任何送餐地址...</td></tr>
                </c:if>
            </tbody>
        </table>
    <%@include file="/WEB-INF/jspf/body-last.jspf"%>
</body>
</html>
