<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
    </head>

    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf" %>
        <form class="form-horizontal validate" method="post" role="form">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="name"><span class="text-danger">*</span>姓名：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="name" name="name" value="${entity.name}" data-rule-required="true" data-rule-maxlength="20"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="name"></label>
            </div>
                
            <div class="form-group">
                <label class="col-sm-3 control-label" for="mobile"><span class="text-danger">*</span>号码：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="mobile" name="mobile" value="${entity.mobile}" data-rule-required="true" data-rule-mobile="true"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="mobile"></label>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="location"><span class="text-danger">*</span>地址：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="location" name="location" value="${entity.location}" data-rule-required="true" data-rule-maxlength="110"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="location"></label>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="longitude"><span class="text-danger">*</span>经度：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="latitude" name="longitude" value="${entity.longitude}" data-rule-longitudeString="true" data-rule-required="true" data-rule-maxlength="14"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="longitude"></label>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="latitude"><span class="text-danger">*</span>纬度：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="latitude" name="latitude" value="${entity.latitude}" data-rule-longitudeString="true" data-rule-required="true" data-rule-maxlength="14"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="latitude"></label>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="defaultAddress">*是否默认地址：</label>
                <div class="col-sm-5">
                    <select class="form-control" id="defaultAddress" name="defaultAddress" data-value="${entity.defaultAddress}" data-rule-required="true">
                        <option value="false" >否</option>
                        <option value="true" >是</option>
                    </select>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="defaultAddress"></label>
            </div> 
            <div class="form-group">
                <label class="col-sm-3 control-label" for="regionId"><span class="text-danger"></span>区域名：</label>
                <div class="col-sm-5">
                    <select class="form-control" id="region-id" name="regionId" data-value="${entity.regionId}" data-rule-required="true">
                        <c:forEach items="${region}" var="cur">
                            <option value="${cur.id}">${cur.name}</option>
                        </c:forEach>
                    </select>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="regionId"></label>
            </div>
            <div class="clearfix form-actions">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary">
                        <i class="icon-ok bigger-110"></i>
                        确定提交
                    </button>

                    <button type="button" class="btn btn-info action-back">
                        <i class="icon-undo bigger-110"></i>
                        取消返回
                    </button>
                </div>
            </div>
        </form>
        <%@include file="/WEB-INF/jspf/body-last.jspf" %>
    </body>
</html>
