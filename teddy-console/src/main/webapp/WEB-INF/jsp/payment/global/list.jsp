<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf"%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8" />
        <%@include file="/WEB-INF/jspf/head.jspf"%>
        <title><spring:message code="application.title" /></title>
    </head>


    <body
        class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf"%>


        <div class="row row-nocol">
            <form class="form-inline lookup" role="form" method="post">
                <div class="form-group">

                    <label class="sr-only" for="lookup-restaurantId">所属餐厅</label>
                    <select class="form-control" id="lookup-restaurantId" name="restaurantId" data-value="${lookup.restaurantId}">
                        <option value="">--所属餐厅--</option>
                        <c:forEach items="${restaurants}" var="cur">
                            <option value="${cur.id}">${cur.name}</option>
                        </c:forEach>
                    </select>
                </div>

                <div class="form-group">
                    <label class="sr-only" for="lookup-orderno">订单号</label> <input
                        id="lookup-orderno" class="form-control" name="orderno"
                        value="${lookup.orderno}" placeholder="订单号" />
                </div>

                <div class="form-group">

                    <label class="sr-only" for="lookup-restaurantId">订单状态</label>
                    <select class="form-control" id="lookup-restaurantId" name="status" data-value="${lookup.status}">
                        <option value="10">--全部状态--</option>
                        <c:forEach items="${statusMap}" var="cur">
                            <option value="${cur.key}">${cur.value}</option>
                        </c:forEach>
                    </select>
                </div>

                <div class="form-group">
                    <label class="sr-only" for="lookup-customerName">客户姓名</label> <input
                        id="lookup-customerName" class="form-control" name="customerName"
                        value="${lookup.customerName}" placeholder="客户姓名" />
                </div>

                <div class="form-group">
                    <label class="sr-only" for="lookup-mobile">客户电话</label> <input
                        id="lookup-mobile" class="form-control" name="mobile"
                        value="${lookup.mobile}" placeholder="客户电话" />
                </div>

                <div class="form-group">
                    <label class="sr-only" for="lookup-localtion">送餐地址</label> <input
                        id="lookup-mobile" class="form-control" name="location"
                        value="${lookup.location}" placeholder="送餐地址" />
                </div>
                <div class="form-group">
                    <input name="_showToday" value="on" type="hidden"/>
                    <input type="checkbox" class="ace ace-switch ace-switch-6" id="lookup-showToday" name="showToday" value="true" ${lookup.showToday ? 'checked':''}/>
                    <label class="lbl" for="lookup-showToday">只显示今天订单</label>
                </div>

                <div class="form-group form-btn-bar">
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="icon-search"></i> 查询
                    </button>
                    <button type="button" class="btn btn-info btn-sm reset">
                        <i class="icon-undo"></i> 重置
                    </button>
                </div>
            </form>
        </div>

        <c:if test="${not empty remind}">
            <div class="alert alert-${remind.level}">
                <button data-dismiss="alert" class="close" type="button">
                    <i class="icon-remove"></i>
                </button>
                ${remind.message}
            </div>
        </c:if>


        <form class="row row-nocol" method="post">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>序号</th>
                        <th>餐厅名称</th>
                        <th>订单号</th>
                        <th>客户姓名</th>
                        <th>客户电话</th>
                        <th>所在区域</th>
                        <th>送餐地址</th>
                        <th>已付费用</th>
                        <th>订单状态</th>
                        <th class="text-info">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${pager.elements}" var="cur" varStatus="status">
                        <tr>
                            <td>${status.count}</td>
                            <td>${cur.restaurantName}</td>
                            <td>${cur.orderno}</td>
                            <td>${cur.customerName}</td>
                            <td>${cur.mobile}</td>
                            <td>${cur.regionName}</td>
                            <td>${cur.location}</td>
                            <td>${cur.paid}</td>
                            <td>${cur.orderStatus}</td>
                            <td>
                                <div class="btn-group">
                                    <a class="btn btn-xs btn-info tooltip-info" href="${cur.orderno}"
                                       data-rel="tooltip" data-original-title="详情"> <i
                                            class="icon-edit bigger-120"></i>
                                    </a>
                                    <a class="btn btn-xs btn-purple" href="${cur.id}/timeline"
                                       data-rel="tooltip" data-original-title="订单时间轴"> <i
                                            class="icon-calendar bigger-120"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    <c:forEach begin="${fn:length(pager.elements)}" end="${pager.size-1}">
                        <tr>
                            <td>&nbsp;</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </form>
        <%@include file="/WEB-INF/jspf/pager.jspf"%>
        <%@include file="/WEB-INF/jspf/body-last.jspf"%>
    </body>
</html>
