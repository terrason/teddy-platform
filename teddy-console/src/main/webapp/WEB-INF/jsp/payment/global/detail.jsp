<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
    </head>
    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf" %>
        <form class="form-horizontal validate" >
            <div class="form-group">
                <label class="col-sm-2 control-label">订单号:</label>
                <div class="col-sm-10">
                    ${entity.orderno}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">下单时间：</label>
                <div class="col-sm-10">
                    ${entity.createTime}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" >订单状态：</label>
                <div class="col-sm-10">
                    <label class="control-label">${entity.orderStatus}</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" >订单状态描述：</label>
                <div class="col-sm-10">
                    <label class="control-label">${entity.statusDesc}</label>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" >订单费用：</label>
                <div class="col-sm-10">
                    <label class="control-label">${entity.cost}</label>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" >打包费：</label>
                <div class="col-sm-10">
                    <label class="control-label">${entity.packageFee}</label>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" >配送费：</label>
                <div class="col-sm-10">
                    <label class="control-label">${entity.delivery}</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" >取菜员确认时间：</label>
                <div class="col-sm-10">
                    <label class="control-label">${entity.readyTime}</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" >配送员接单时间：</label>
                <div class="col-sm-10">
                    <label class="control-label">${entity.assignTime}</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" >配送员编号：</label>
                <div class="col-sm-10">
                    <label class="control-label">${entity.courierNumber}</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" >配送员姓名：</label>
                <div class="col-sm-10">
                    <label class="control-label">${entity.courierName}</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" >配送员联系方式：</label>
                <div class="col-sm-10">
                    <label class="control-label">${entity.courierContact}</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" >用户备注：</label>
                <div class="col-sm-10">
                    <label class="control-label">${entity.memo}</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" >是否VIP：</label>
                <div class="col-sm-10">
                    <label class="control-label">${entity.vip?"是":"否"}</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" >使用代金券：</label>
                <div class="col-sm-10">
                    <label class="control-label">${entity.cash}</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" >使用积分：</label>
                <div class="col-sm-10">
                    <label class="control-label">${entity.score}</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" >用户评分：</label>
                <div class="col-sm-10">
                    <label class="control-label">${entity.star}</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" >用户评价：</label>
                <div class="col-sm-10">
                    <label class="control-label">${entity.comment}</label>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" >订单可否编辑：</label>
                <div class="col-sm-10">
                    <label class="control-label">${entity.editable?"可编辑":"不可编辑"}</label>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">餐厅名称:</label>
                <div class="col-sm-10">
                    <label class="control-label">${entity.restaurantName}</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">餐厅电话:</label>
                <div class="col-sm-10">
                    <label class="control-label">${entity.restaurantContact}</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">餐厅地址:</label>
                <div class="col-sm-10">
                    <label class="control-label">${entity.restaurantLocation}</label>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">餐厅图片：</label>
                <div class="col-sm-10">
                    <img data-src="${entity.restaurantImg}"></img>
                </div>
            </div>   


            <div class="form-group">
                <label class="col-sm-2 control-label">菜品明细</label>
                <div class="col-sm-10">
                    <table class="table table-striped table-condensed">
                        <thead>
                            <tr>
                                <td>菜品</td><td>数量</td><td>价格</td><td>状态</td><td>类别</td><!-- <td>操作</td> -->
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${entity.goods}" var="item">
                                <tr id="dish-${item.dishId}">
                                    <td>${item.name}</td>
                                    <td>${item.count}</td>
                                    <td>${item.price}</td>
                                    <td>
                                        <c:if test="${item.status eq 0}">初始化</c:if>
                                        <c:if test="${item.status eq 1}">已取菜</c:if>
                                        <c:if test="${item.status gt 1}">已退菜</c:if>
                                        </td>
                                        <td>${item.dishCategoryName}</td>
                                    <%-- <td><c:if test="${item.status<2}">
                                    <button class="btn btn-info btn-xs action-get" type="button" data-href="${ctx}/payment/backdish/${item.id}" data-orderno="${orderno}">退菜</button></c:if>
                                    
                                    </td> --%>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>   

            <div class="form-group">
                <label class="col-sm-2 control-label">优惠标签</label>
                <div class="col-sm-10">
                    <table class="table table-striped table-condensed">
                        <thead>
                            <tr>
                                <td>标签</td><td>参数</td><td>Money</td>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${entity.promotions}" var="pro">
                                <tr>
                                    <td>${pro.tag}</td>
                                    <td>${pro.description}</td>
                                    <td>${pro.money}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>   





            <div class="clearfix form-actions">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="button" class="btn btn-info action-back">
                        <i class="icon-undo bigger-110"></i>返回
                    </button>
                </div>
            </div>
        </form>


        <%@include file="/WEB-INF/jspf/body-last.jspf" %>
    </body>
</html>
