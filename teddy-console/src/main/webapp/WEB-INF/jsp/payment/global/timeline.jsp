<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf"%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<link rel="stylesheet" type="text/css" href="${ctx}/css/timeline.css" />
<title><spring:message code="application.title" /></title>
</head>
<body
	class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
	<%@include file="/WEB-INF/jspf/body-first.jspf"%>
	<form class="form-horizontal validate">
		<div style="margin-left:30%;font-size:24px;color:#3594cb;">订单时间轴</div>
		
		<ul class="cbp_tmtimeline">
			 <c:forEach items="${times}" var="cur" >		 
			<li><time class="cbp_tmtime" datetime="${cur.createTime}">
					<span><fmt:formatDate value="${cur.createTime }" pattern="yyyy-MM-dd "/></span> 
					<span><fmt:formatDate value="${cur.createTime }" pattern="HH:mm"/></span>
				</time>
				<div class="cbp_tmicon"></div>
				<div class="cbp_tmlabel">
					<h2>${cur.orderStatus}</h2>
					<p>${cur.description}</p>
				</div>
			</li>
			</c:forEach>
			 
		</ul>





		<div class="clearfix form-actions">
			<div class="col-sm-offset-3 col-sm-9">
				<button type="button" class="btn btn-info action-back">
					<i class="icon-undo bigger-110"></i>返回
				</button>
			</div>
		</div>
	</form>


	<%@include file="/WEB-INF/jspf/body-last.jspf"%>
</body>
</html>
