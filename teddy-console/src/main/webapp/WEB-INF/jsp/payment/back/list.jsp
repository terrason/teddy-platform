<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf"%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8" />
        <%@include file="/WEB-INF/jspf/head.jspf"%>
        <title><spring:message code="application.title" /></title>
        <link rel="stylesheet" href="${ctx}/css/jquery.gritter.css" />
    </head>


    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf"%>

        <div class="row row-nocol">
            <form class="form-inline lookup" role="form" method="post">
                <div class="form-group">
                    <label class="sr-only" for="lookup-orderno">订单号</label>
                    <input id="lookup-orderno" class="form-control" name="orderno" value="${lookup.mobile}" placeholder="订单号" />
                </div>

                <div class="form-group">
                    <label class="sr-only" for="lookup-mobile">客户电话</label>
                    <input id="lookup-mobile" class="form-control" name="mobile" value="${lookup.mobile}" placeholder="客户电话" />
                </div>

                <div class="form-group">
                    <input name="_editable" value="on" type="hidden"/>
                    <input type="checkbox" class="ace ace-switch ace-switch-6" id="lookup-editable" name="editable" value="true" ${lookup.editable ? 'checked':''}/>
                    <label class="lbl" for="lookup-editable">只显示换菜中的订单</label>
                </div>

                <div class="form-group form-btn-bar">
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="icon-search"></i> 查询
                    </button>
                    <button type="button" class="btn btn-info btn-sm reset">
                        <i class="icon-undo"></i> 重置
                    </button>
                </div>

            </form>

        </div>

        <c:if test="${not empty remind}">
            <div class="alert alert-${remind.level}">
                <button data-dismiss="alert" class="close" type="button">
                    <i class="icon-remove"></i>
                </button>
                ${remind.message}
            </div>
        </c:if>


        <form class="row row-nocol" method="post" target="_blank">

            <div class="action-bar">
            </div>


            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>序号</th>
                        <th>订单号</th>
                        <th>客户姓名</th>
                        <th>客户电话</th>
                        <th>订单状态</th>
                        <th class="text-info">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${pager.elements}" var="cur" varStatus="status">
                        <tr>
                            <td>${status.count}</td>
                            <td>${cur.orderno}</td>
                            <td>${cur.customerName}</td>
                            <td>${cur.mobile}</td>
                            <td><span class="label label-${cur.editable ? 'info' : 'danger'} arrowed arrowed-right">${cur.editable  ? '换菜中' : '未处理'}</span></td>
                            <td>
                                <div class="btn-group">
                                    <a class="btn btn-xs btn-info tooltip-info" href="${cur.orderno}"
                                       data-rel="tooltip" data-original-title="详情">
                                        <i class="icon-file-alt  bigger-120"></i>
                                    </a>
                                    <c:if test="${!cur.editable }">
                                        <button class="btn btn-xs btn-warning tooltip-warning action-post" type="button"
                                                autocomplete="off" data-alipay="${cur.orderno}" data-method="exchange"
                                                data-href="${cur.orderno}/change"
                                                data-rel="tooltip" data-original-title="换菜"
                                                data-confirm="客户将获得订单加菜权限，确定换菜吗？">
                                            <i class="${cur.synchronization.contains('exchange') ? 'icon-spinner icon-spin':'icon-resize-small'} bigger-120"></i>
                                        </button>
                                    </c:if>  

                                    <button
                                        autocomplete="off" data-alipay="${cur.orderno}" data-method="giveup"
                                        class="btn btn-xs btn-pink tooltip-success action-post" type="button"
                                        data-href="${cur.orderno}/backorder" data-rel="tooltip"
                                        data-original-title="退菜"
                                        data-confirm="true">
                                        <i class="${cur.synchronization.contains('giveup') ? 'icon-spinner icon-spin':'icon-repeat'} bigger-120"></i>
                                    </button>
                                    <button
                                        autocomplete="off" data-alipay="${cur.orderno}" data-method="cancel"
                                        class="btn btn-xs btn-inverse action-post" type="button"
                                        data-href="${cur.orderno}/cancel" data-rel="tooltip"
                                        data-original-title="取消订单"
                                        data-confirm="true">
                                        <i class="${cur.synchronization.contains('cancel') ? 'icon-spinner icon-spin':'icon-remove'} bigger-120"></i>
                                    </button>

                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    <c:forEach begin="${fn:length(pager.elements)}" end="${pager.size-1}">
                        <tr>
                            <td>&nbsp;</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </form>
        <%@include file="/WEB-INF/jspf/pager.jspf"%>
        <%@include file="/WEB-INF/jspf/body-last.jspf"%>
        <script type="text/javascript" src="${ctx}/js/jquery.gritter.min.js"></script>
        <script type="text/javascript" src="${ctx}/js/jquery.timer.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $(document).on("click", '[data-alipay]', function () {
                    var $this = $(this);
                    var orderno = $this.data("alipay");
                    var method = $this.data("method");
                    if (!orderno || !method) {
                        return;
                    }
                    $this.find("i").addClass("icon-spinner icon-spin");
                    $this.prop("disabled", true);

                    var t = $.timer(function () {
                        $.ajax("${ctx}/ajax/alipay-beats", {
                            async: false,
                            data: {
                                orderno: orderno,
                                method: method
                            }
                        }).done(function (successful) {
                            if (successful === false) {
                                $this.find("i").removeClass("icon-spinner icon-spin").addClass("icon-ok");
                                t.stop();
                                $.gritter.add({
                                    title: '退款完成',
                                    text: "订单[" + orderno + "]退款已完成，请刷新页面！",
                                    class_name: 'gritter-success'
                                });
                            }
                        });
                    }, 2000, true);
                });
            });
        </script>
    </body>
</html>
