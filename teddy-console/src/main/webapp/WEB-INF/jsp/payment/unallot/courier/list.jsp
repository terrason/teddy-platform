<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf"%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8" />
        <%@include file="/WEB-INF/jspf/head.jspf"%>
        <title><spring:message code="application.title" /></title>
    </head>


    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf"%>

        <div class="row row-nocol">
            <form class="form-inline lookup" role="form" method="post">
                <div class="form-group">
                    <label class="sr-only" for="lookup-orderno">订单号</label> <input
                        id="lookup-orderno" class="form-control" name="orderno"
                        value="${lookup.orderno}" placeholder="订单号" />
                </div>

                <div class="form-group">
                    <label class="sr-only" for="lookup-mobile">客户电话</label>
                    <input id="lookup-mobile" class="form-control" name="mobile" value="${lookup.mobile}" placeholder="客户电话" />
                </div>
                <div class="form-group">
                    <label class="sr-only" for="lookup-regionId">所属区域</label>
                    <select class="form-control" id="lookup-regionId" name="regionId" data-value="${lookup.regionId}">
                        <option value="">--所属区域--</option>
                        <c:forEach items="${region}" var="cur">
                            <option value="${cur.id}">${cur.name}</option>
                        </c:forEach>
                    </select>
                </div>

                <div class="form-group form-btn-bar">
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="icon-search"></i> 查询
                    </button>
                    <button type="button" class="btn btn-info btn-sm reset">
                        <i class="icon-undo"></i> 重置
                    </button>
                </div>
            </form>
        </div>

        <c:if test="${not empty remind}">
            <div class="alert alert-${remind.level}">
                <button data-dismiss="alert" class="close" type="button">
                    <i class="icon-remove"></i>
                </button>
                ${remind.message}
            </div>
        </c:if>


        <form class="row row-nocol" method="post">
            <div class="action-bar">
                <c:if test="${lookup.regionId > 0}">
                    <button class="btn btn-sm btn-info action-get" type="button"
                            data-href="allotdeliver"
                            data-checkbox-require="pks"
                            data-form="true"
                            data-confirm="true"><span class="icon-link"></span> 批量分配</button>
                </c:if>
            </div>
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="center"><input type="checkbox" data-member="pks" /></th>
                        <th>序号</th>
                        <th>订单号</th>
                        <th>取菜员</th>
                        <th>配送员</th>
                        <th>客户姓名</th>
                        <th>送餐地址</th>
                        <th>区域</th>
                        <th>客户电话</th>
                        <th>订单状态</th>
                        <!-- <th>订单可否编辑</th> -->
                        <th class="text-info">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${pager.elements}" var="cur" varStatus="status">
                        <tr>
                            <td class="center"><input type="checkbox" name="pks"
                                                      value="${cur.id}" /></td>
                            <td>${status.count}</td>
                            <td>${cur.orderno}</td>
                            <td>${cur.deliverName}</td>
                            <td>${cur.courierName}</td>
                            <td>${cur.customerName}</td>
                            <td>${cur.address}</td>
                            <td></td>
                            <td>${cur.mobile}</td>
                            <td>${cur.orderStatus}</td>
                            <%-- <td><span class="label label-${cur.editable ? 'info' : 'danger'} arrowed arrowed-right">${cur.editable  ? '可编辑' : '不可编辑'}</span></td> --%>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-xs btn-info action-get" data-href="${cur.orderno}/allotcourier" data-rel="tooltip" data-original-title="分配配送员" >
                                        <i class="icon-link bigger-120"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    <c:forEach begin="${fn:length(pager.elements)}" end="${pager.size-1}">
                        <tr>
                            <td>&nbsp;</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </form>
        <%@include file="/WEB-INF/jspf/pager.jspf"%>
        <%@include file="/WEB-INF/jspf/body-last.jspf"%>
    </body>
</html>
