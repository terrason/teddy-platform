<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
    </head>

    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf" %>


        <form class="form-horizontal validate" method="post" role="form">

            <div class="form-group">
                <div class="hidden">
                    <c:forEach items="${pks}" var="cur">
                        <input type="hidden" name="pks" value="${cur}" />
                    </c:forEach>
                </div>
                <label class="col-sm-3 control-label" for="employeeId"><span class="text-danger">*</span>${flag}：</label>
                <div class="col-sm-5">
                    <select class="form-control" id="employeeId" name="employeeId" data-value="${payment.deliverId}" data-rule-required="false">
                        <option value="0">----${flag}----</option>
                        <c:forEach items="${employees}" var="cur">
                            <option value="${cur.id}">${cur.name}</option>
                        </c:forEach>
                    </select>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="employeeId"></label>
            </div>


            <div class="clearfix form-actions">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary">
                        <i class="icon-ok bigger-110"></i>
                        确定提交
                    </button>

                    <button type="button" class="btn btn-info action-back">
                        <i class="icon-undo bigger-110"></i>
                        取消返回
                    </button>
                </div>
            </div>
        </form>
        <%@include file="/WEB-INF/jspf/body-last.jspf" %>
    </body>
</html>
