<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
    </head>
    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf" %>
        <form class="form-horizontal validate" method="post" role="form">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="username"><span class="text-danger">*</span>用户登录名：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="username" name="username" value="${entity.username}" data-rule-required="true" data-rule-maxlength="60"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="username"></label>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="realName"><span class="text-danger">*</span>用户姓名：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="realName" name="realName" value="${entity.realName}" data-rule-required="true" data-rule-maxlength="40"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="realName"></label>
            </div>
            <div class="clearfix form-actions">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary">
                        <i class="icon-ok bigger-110"></i>
                        确定提交
                    </button>

                    <button type="button" class="btn btn-info action-back">
                        <i class="icon-undo bigger-110"></i>
                        取消返回
                    </button>
                </div>
            </div>
        </form>
        <%@include file="/WEB-INF/jspf/body-last.jspf" %>
    </body>
</html>
