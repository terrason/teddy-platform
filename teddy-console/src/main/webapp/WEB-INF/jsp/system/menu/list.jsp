<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
    </head>
    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf" %>
        <div class="row row-nocol">
            <form class="form-inline lookup clearfix" role="form" action="${ctx}/book/home" method="post">
                <div class="form-group">
                    <label class="sr-only" for="lookup-name">菜单名称</label>
                    <input id="lookup-name" class="form-control" name="lookup.name" value="${lookup.name}" placeholder="菜单名称"/>
                </div>
                <div class="form-btn-bar">
                    <button type="submit" class="form-control btn btn-primary"><i class="icon-search"></i> 查询</button>
                    <button type="button" class="form-control btn btn-info reset"><i class="icon-undo"></i> 重置</button>
                </div>
            </form>
            <hr/>
        </div>
        <c:if test="${not empty alert}">
            <div class="alert alert-${alert.level}">
                <button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>
                ${alert.message}
            </div>
        </c:if>
        <form class="row row-nocol" method="post">
            <div class="action-bar">
                <a class="btn btn-info" href="input"><i class="icon-file"></i> 添加</a>
                <button class="btn btn-info action-post" type="button"
                        data-href="remove"
                        data-checkbox-require="pks"
                        data-confirm="true"><span class="icon-trash"></span> 删除</button>
            </div>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th style="width:1em;"><input type="checkbox" data-member="pks"/></th>
                        <th style="width:2.5em;">序号</th>
                        <th>菜单名称</th>
                        <th>图标</th>
                        <th>链接</th>
                        <th>发布日期</th>
                        <th class="text-info">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${pager.elements}" var="cur" varStatus="status">
                        <tr>
                            <td><input type="checkbox" name="pks" value="${book.id}"/></td>
                            <td>${status.count}</td>
                            <td>${cur.categoryName}</td>
                            <td>${cur.name}</td>
                            <td>${cur.author}</td>
                            <td><fmt:formatDate value="${cur.publishTime}" pattern="yyyy-MM-dd HH:mm" /></td>
                            <th>
                                <a class="btn btn-link" href="edit?id=${cur.id}"><span class="glyphicon glyphicon-edit"></span> 编辑</a>
                                <button class="btn btn-link action-post" type="button" data-confirm="true" data-href="remove" data-id="${cur.id}"><span class="glyphicon glyphicon-trash"></span> 删除</button>
                            </th>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </form>
        <%@include file="/WEB-INF/jspf/pager.jspf" %>
        <%@include file="/WEB-INF/jspf/body-last.jspf" %>
    </body>
</html>
