<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
        <script type="text/javascript" src="http://api.map.baidu.com/api?v=1.5&ak=qNehHrGKxNI3fYegvD5lB0wL"></script>
        <script type="text/javascript" src="${ctx}/js/baidumap.js"></script>
        <script type="text/javascript">
	      	$(function(){
	      		//创建出所有的区域
	      		createAllRegion($(".all-region"));
	    	});
        </script>        
    </head>
    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf" %>
        <form class="form-horizontal validate" id="entity-detail" method="post" role="form" data-ignore=""> 
        
        	<c:forEach items="${regions}" var="cur" >
        		<input class="hidden all-region" data-areaname ="${cur.areaName }" value="${cur.xy }"/>
        	</c:forEach>
			<div style="width:80%;height:550px;border:#ccc solid 1px;margin: 0 auto" data-ready="baidumap" >
			</div>
			
            <div class="clearfix form-actions">
                <div class="col-sm-offset-3 col-sm-9">

                    <button type="button" class="btn btn-info action-back">
                        <i class="icon-undo bigger-110"></i>
                        返回
                    </button>
                </div>
            </div>
        </form>
        <%@include file="/WEB-INF/jspf/body-last.jspf" %>
    </body>
</html>
