<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
        <script type="text/javascript" src="http://api.map.baidu.com/api?v=1.5&ak=qNehHrGKxNI3fYegvD5lB0wL"></script>
        <script type="text/javascript" src="${ctx}/js/baidumap.js"></script>
        <script type="text/javascript">
	      	$(function(){
	      		var flag = $("#flag").val();
	      		console.info("flag:" + flag);
	      		
	      		if("edit" == flag) {
	      			var xys = $("#xy").val().split(",");
	      			console.info("before "+xys);
		    		createPolygon(xys);
	      		}else if("add" == flag){
	      			toAddPolygon();
	      		} 
	    		
	    	});
        </script>        
    </head>
    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf" %>
        <form class="form-horizontal validate" id="entity-detail" method="post" role="form" data-ignore=""> 
        
        	<input type="hidden" id="flag" value="${flag }" />
        	<input class="hidden" id="xy" value="${entity.xy}"/>
        	<c:forEach items="${others}" var="cur">
        		<input class="hidden others-region" data-areaname ="${cur.areaName }" value="${cur.xy }"/>
        	</c:forEach>
        	
        	<input class="hidden" name="points" id="points">
        
        	<div class="form-group">
                <label class="col-sm-3 control-label" for="longitudeContent"><span class="text-danger">*</span>区域名称：</label>
                <div class="col-sm-6">
                    <input class="form-control" id="name" name="name" value="${entity.areaName}"  data-rule-required="true" data-rule-maxlength="14"/>
                </div>
                <label class="col-sm-3 help-inline form-control-static" for="name"></label>
            </div>
            
        		
        		
        	<div class="form-group form-btn-bar">
        	<div class="col-sm-3"></div>
        	<div class="col-sm-6">
        	
				<button type="button" class="btn btn-primary btn-sm" onclick="couldEdit()">
					<i class="icon-edit"></i> 编辑区域
				</button>
				<button type="button" class="btn btn-primary btn-sm" onclick="again()">
					<i class="icon-repeat "></i> 重新编辑
				</button>
				<button type="button" class="btn btn-info btn-sm reset" onclick="finishEdit()">
					<i class="icon-ok"></i> 完成编辑
				</button>
				</div>
			</div>	
			<div style="width:80%;height:550px;border:#ccc solid 1px;margin: 0 auto" data-ready="baidumap" >
			</div>
			
            <div class="clearfix form-actions">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary" onclick="finishEdit();">
                        <i class="icon-ok bigger-110"></i>
                        确定提交
                    </button> 

                    <button type="button" class="btn btn-info action-back">
                        <i class="icon-undo bigger-110"></i>
                        返回
                    </button>
                </div>
            </div>
        </form>
        <%@include file="/WEB-INF/jspf/body-last.jspf" %>
    </body>
</html>
