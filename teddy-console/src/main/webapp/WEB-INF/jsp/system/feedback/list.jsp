<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
    </head>
    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf" %>
        <div class="row row-nocol">
            <form class="form-inline lookup" role="form" method="post">
                <div class="form-group">
                    <label class="sr-only" for="lookup-time-from">反馈时间——最早时间</label>
                    <span class="input-icon">
                        <input id="lookup-time-from" type="datetime" name="timeFrom" value="<fmt:formatDate value="${lookup.timeFrom}" pattern="yyyy-MM-dd HH:mm"/>" placeholder="反馈时间——最早时间"/>
                        <i class="icon-time"></i>
                    </span>
                </div>
                <div class="form-group">
                    <label class="sr-only" for="lookup-time-to">反馈时间——最迟时间</label>
                    <span class="input-icon input-icon-right">
                        <input id="lookup-time-to" type="datetime" name="timeTo" value="<fmt:formatDate value="${lookup.timeTo}" pattern="yyyy-MM-dd HH:mm"/>" placeholder="反馈时间——最迟时间"/>
                        <i class="icon-time"></i>
                    </span>
                </div>
                <div class="form-group">
                    <label class="sr-only" for="lookup-customer">反馈客户</label>
                    <input id="lookup-customer" type="text" name="customer" value="${lookup.customer}" placeholder="反馈客户"/>
                </div>
                <div class="form-group">
                    <input name="_unread" value="on" type="hidden"/>
                    <input type="checkbox" class="ace ace-switch ace-switch-6" id="lookup-unread" name="unread" value="true" ${lookup.unread ? 'checked':''}/>
                    <label class="lbl" for="lookup-unread">只显示未读反馈信息</label>
                </div>
                <div class="form-group form-btn-bar">
                    <button type="submit" class="btn btn-primary btn-sm"><i class="icon-search"></i> 查询</button>
                    <button type="button" class="btn btn-info btn-sm reset"><i class="icon-undo"></i> 重置</button>
                </div>
            </form>
            <hr/>
        </div>
        <c:if test="${not empty remind}">
            <div class="alert alert-${remind.level}">
                <button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>
                    ${remind.message}
            </div>
        </c:if>
        <form class="row row-nocol" method="post">
            <div class="action-bar">
                <button class="btn btn-sm btn-danger action-post" type="button"
                        data-href="remove"
                        data-checkbox-require="pks"
                        data-confirm="true"><span class="icon-trash"></span> 删除</button>
            </div>
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="center"><input type="checkbox" data-member="pks"/></th>
                        <th>序号</th>
                        <th>反馈内容</th>
                        <th>反馈时间</th>
                        <th>反馈客户</th>
                        <th>状态</th>
                        <th class="text-info">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${pager.elements}" var="cur" varStatus="status">
                        <tr>
                            <td class="center"><input type="checkbox" name="pks" value="${cur.id}"/></td>
                            <td>${status.count}</td>
                            <td>${fn:substring(cur.content, 0, 26)}${fn:length(cur.content) gt 26 ? "..." : ""}</td>
                            <td><fmt:formatDate value="${cur.createTime}" pattern="yyyy-MM-dd HH:mm"/></td>
                            <td>${cur.nickname}</td>
                            <td><span class="label label-${cur.read ? 'success':'warning'}">${cur.read ? "已读":"未读"}</span></td>
                            <td>
                                <div class="btn-group">
                                    <a class="btn btn-xs btn-info tooltip-info" href="${cur.id}" data-rel="tooltip" data-original-title="查看">
                                        <i class="icon-eye-open bigger-120"></i>
                                    </a>
                                    <button class="btn btn-xs btn-danger tooltip-warning action-post" data-href="${cur.id}/remove" data-rel="tooltip" data-confirm="true" data-original-title="删除">
                                        <i class="icon-trash bigger-120"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    <c:forEach begin="${fn:length(pager.elements)}" end="${pager.size-1}">
                        <tr>
                            <td>&nbsp;</td><td></td><td></td><td></td><td></td><td></td><td></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </form>
        <%@include file="/WEB-INF/jspf/pager.jspf" %>
        <%@include file="/WEB-INF/jspf/body-last.jspf" %>
    </body>
</html>
