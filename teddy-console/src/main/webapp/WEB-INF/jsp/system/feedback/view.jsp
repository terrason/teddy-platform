<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
    </head>
    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf" %>
        <form class="form-horizontal" method="post" role="form">
            <div class="form-group">
                <label class="col-sm-3 control-label">反馈时间：</label>
                <div class="col-sm-5">
                    <label class="form-control-static"><fmt:formatDate value="${entity.createTime}" pattern="yyyy-MM-dd HH:mm" /></label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">反馈用户：</label>
                <div class="col-sm-5">
                    <label class="form-control-static">${entity.nickname}</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">反馈内容：</label>
                <div class="col-sm-5">
                    <div class="form-control-static">${entity.content}</div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="content"><span class="text-danger"></span>回复内容：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="content" name="content" value="${content}" data-rule-maxlength="80"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="content"></label>
            </div>
            <div class="clearfix form-actions">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary">
                            <i class="icon-ok bigger-110"></i>
                            确定提交
                    </button>
                    <a class="btn btn-info " href="${ctx}/system/feedback/"><i class="icon-undo bigger-110"></i> 返回</a>
                </div>
            </div>
        </form>
        <%@include file="/WEB-INF/jspf/body-last.jspf" %>
    </body>
</html>
