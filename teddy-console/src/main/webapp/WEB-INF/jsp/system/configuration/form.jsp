<%@page contentType="text/html" pageEncoding="UTF-8"  %>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
        <script type="text/javascript">
        //发送请求
        function send(data){
        	var url = "${ctx}" +"/system/configuration/update";
        	console.info("url:"+url);
        	console.info("data:"+data);
        	$.ajax({
        		type:"POST",
        		url:url,
        		data:data,
        		async:false,
        		dataType:"text",
        		success:function(msg){
        			console.info(msg);
        			if(msg != "success"){
        				alert("修改失败");
        			}
        			
        		},
        		error:function(XMLHttpRequest, textStatus, errorThrown){
        			alert("修改失败:" + XMLHttpRequest);
        		}
        	});
        }
        //1 是否显示文字评论
        //2 是否自动分配配送员
        //3 是否自动分配取菜员
        //5 送积分规则
        //6 VIP经验阈值
        
        //10熊仔电话 
        //11分享订单可获代金券
        //12领取分享可获代金券
        function whenChange(obj){
        	var val = $(obj).val();
        	var code =$(obj).data("code");
        	var data = "param=" + val +"&code="+code;
        	send(data);
        }
       
      //7 关于我们 
        //8 许可协议
        //9 技术支持
         function whenBlur(obj){
        	var val = $(obj).text();
        	var code =$(obj).data("code");
        	var data = "param=" + val +"&code="+code;
        	send(data);
        }
        
        //4 积分抵现规则
        function scoreRule(obj){
        	if(!window.confirm("此操作将改变全局餐厅的积分抵现规则，确认改变吗")){
        		return ;
        	}
        	var code ="config.promotion.costScore";
        	var param = $("#rule_p1").val() +","+$("#rule_p2").val();
        	var data = "param=" + param + "&code="+code;
        	send(data);
        }
        
       function check(obj){
		if($(obj).val()<=0 || $(obj).val()%10 !=0){
			alert("请输入10的倍数");
			$(obj).focus();
			}
        }
        </script>
    </head>
    
    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf" %>
        
        
        <form class="form-horizontal validate" method="post" role="form">
        
        	<div class="form-group">
                <label class="col-sm-3 control-label" for="withContent"><span class="text-danger">*</span>是否显示文字评论：</label>
                <div class="col-sm-5">
                    <select class="form-control" onchange="whenChange(this);" id="withContent"  data-code="config.comment.withContent" name="withContent" data-value="${entity.withContent}" data-rule-required="true">
                        <option value="true">是</option>
                        <option value="false">否</option>
                    </select>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="withContent"></label>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label" for="autoCourier"><span class="text-danger">*</span>是否自动分配配送员：</label>
                <div class="col-sm-5">
                    <select class="form-control" onchange="whenChange(this);"  data-code="config.courier.autoAssignable" id="autoCourier" name="autoCourier" data-value="${entity.autoCourier}" data-rule-required="true">
                        <option value="true">是</option>
                        <option value="false">否</option>
                    </select>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="autoCourier"></label>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label" for="autoDeliver"><span class="text-danger">*</span>是否自动分配取菜员：</label>
                <div class="col-sm-5">
                    <select class="form-control" onchange="whenChange(this);" id="autoDeliver" data-code="config.deliver.autoAssignable" name="autoDeliver" data-value="${entity.autoDeliver}" data-rule-required="true">
                        <option value="true">是</option>
                        <option value="false">否</option>
                    </select>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="autoCourier"></label>
            </div>
        
        
        	 <div class="form-group">
                <label class="col-sm-3 control-label" for="sharePayment"><span class="text-danger">*</span>分享订单可获代金券：</label>
                <div class="col-sm-5">
                    <select class="form-control" onchange="whenChange(this);"  data-code="share.payment" id="sharePayment" name="autoCourier" data-value="${entity.sharePayment}" data-rule-required="true">
                        <option value="5">5元</option>
                        <option value="10">10元</option>
                        <option value="20">20元</option>
                        <option value="50">50元</option>
                        <option value="100">100元</option>
                    </select>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="sharePayment"></label>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label" for="shareReceive"><span class="text-danger">*</span>领取分享可获代金券：</label>
                <div class="col-sm-5">
                    <select class="form-control" onchange="whenChange(this);"  data-code="share.receive" id="shareReceive" name="autoCourier" data-value="${entity.shareReceive}" data-rule-required="true">
                        <option value="5">5元</option>
                        <option value="10">10元</option>
                        <option value="20">20元</option>
                        <option value="50">50元</option>
                        <option value="100">100元</option>
                    </select>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="shareReceive"></label>
            </div>
        
        
            <div class="form-group">
                <label class="col-sm-3 control-label" for="costScore"><span class="text-danger"></span>积分抵现规则：</label>
                <div class="col-sm-5" >
                   <input type="hidden" name="costScore" value="${entity.costScore}" id="hiddenCostScore"/>
                   <span>满</span><input class="input-small" id="rule_p1" style="width:3em;" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" /><span>元，可使用</span>
                   <input class="input-small" id="rule_p2" style="width:3em;" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"  onchange="check(this);"/><span>积分抵现金1元</span>
                    <button class="btn btn-xs btn-danger" type="button" onclick="scoreRule();">
                       <i class="icon-ok bigger-120"></i>
                       应用到所有餐厅
                   </button>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="costScore"></label>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label" for="scorePerMoney"><span class="text-danger"></span>送积分规则：</label>
                <div class="col-sm-5">
                    <input class="input-small" id="scorePerMoney" name="scorePerMoney" onchange="whenChange(this);"  data-code="config.score.scorePerMoney" value="${entity.scorePerMoney}" data-rule-required="true" data-rule-maxlength="40"/><span>元送1积分</span>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="scorePerMoney"></label>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label" for="experience"><span class="text-danger"></span>VIP经验阈值：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="scorePerMoney" name="experience" onchange="whenChange(this);"  value="${entity.experience}" data-code="config.vip.experience"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="experience">用户历史最高积分达到该值，就升级为vip.</label>
            </div>
            
             <div class="form-group">
                <label class="col-sm-3 control-label" for="tel"><span class="text-danger"></span>熊仔电话：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="tel" name="tel" onchange="whenChange(this);" data-code="service.tel" value="${entity.tel}" />
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="tel"></label>
            </div>
            
             <div class="form-group">
                <label class="col-sm-3 control-label" for="longMessage">关于我们：</label>
                <div class="col-sm-6">
                    <div class="form-control wysiwyg-editor" id="aboutus" name="experience" onblur="whenBlur(this);" data-code="service.aboutus" data-bind-input="aboutus">${entity.aboutus}</div>
                </div>
                <label class="col-sm-3 help-inline form-control-static" for="aboutus"></label>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label" for="license">许可协议：</label>
                <div class="col-sm-6">
                    <div class="form-control wysiwyg-editor" id="license" onblur="whenBlur(this);" data-code="service.license" data-bind-input="license">${entity.license}</div>
                    <input type="hidden"  name="license"/>
                </div>
                <label class="col-sm-3 help-inline form-control-static" for="license"></label>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label" for="support">技术支持：</label>
                <div class="col-sm-6">
                    <div class="form-control wysiwyg-editor" id="support" onblur="whenBlur(this);" data-code="service.support" data-bind-input="support">${entity.support}</div>
                    <input type="hidden"  name="support"/>
                </div>
                <label class="col-sm-3 help-inline form-control-static" for="support"></label>
            </div>
           <!--  <div class="clearfix form-actions">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary">
                        <i class="icon-ok bigger-110"></i>
                        确定提交
                    </button>

                    <button type="button" class="btn btn-info action-back">
                        <i class="icon-undo bigger-110"></i>
                        取消返回
                    </button>
                </div>
            </div> -->
        </form>
        <%@include file="/WEB-INF/jspf/body-last.jspf" %>
    </body>
    <script type="text/javascript">
    $(function(){
    	var costScoreArr = $("#hiddenCostScore").val().split(',');
    	$("#rule_p1").val(costScoreArr[0]);
    	$("#rule_p2").val(costScoreArr[1]);
    	console.info(costScoreArr);
    });
    </script>
</html>
