<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
    </head>
    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf" %>
        <form class="form-horizontal validate" id="entity-detail" method="post" role="form" data-ignore=""> 
        
        	<div class="form-group" id="username_div">
                <label class="col-sm-3 control-label" for="version"><span class="text-danger">*</span>版本：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="version" name="version" value="${entity.version}" />
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="version"></label>
            </div>
            
            <div class="form-group" >
                <label class="col-sm-3 control-label" for="code"><span class="text-danger">*</span>版本号数字：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="code" name="code" value="${entity.code}" data-rule-required="true" data-rule-number="true"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="code"></label>
            </div> 
            
        	 <div class="form-group">
                <label class="col-sm-3 control-label" for="log"><span class="text-danger">*</span>更新日志：</label>
                <div class="col-sm-6">
                    <div class="form-control wysiwyg-editor" id="log" data-bind-input="log">${entity.log}</div>
                    <input type="hidden"  name="log"/>
                </div>
                <label class="col-sm-3 help-inline form-control-static" for="log"></label>
            </div>
        	
        	<div class="form-group">
                <label class="col-sm-3 control-label" for="uploadUrlString">上传文件：</label>
                <div class="col-sm-5">
                    <input type="hidden" id="downloadUrl" name="downloadUrl" value="${entity.downloadUrl}" data-rule-required="true" />																	
                    <input class="fileupload" id="downloadUrlString" type="file"  data-display-id="downloadUrl" data-rule-extension="apk"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="uploadUrlString"></label>
            </div>  
            
            <div class="clearfix form-actions">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary">
                        <i class="icon-ok bigger-110"></i>
                        确定提交
                    </button>

                    <button type="button" class="btn btn-info action-back">
                        <i class="icon-undo bigger-110"></i>
                        取消返回
                    </button>
                </div>
            </div>
        </form>
        <%@include file="/WEB-INF/jspf/body-last.jspf" %>
    </body>
</html>
