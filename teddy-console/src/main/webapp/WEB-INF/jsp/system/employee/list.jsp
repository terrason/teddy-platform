<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf"%>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<title><spring:message code="application.title" /></title>
</head>

<body
	class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
	<%@include file="/WEB-INF/jspf/body-first.jspf"%>
	<div class="row row-nocol">
		<form class="form-inline lookup" role="form" method="post">
			<div class="form-group">
				<label class="sr-only" for="lookup-type">员工类型</label>
				<select class="form-control" id="lookup-type" name="type" data-value="${lookup.type }" >
					<option value="">--选择员工类型--</option>
					<option value="1">取菜员</option>
					<option value="2">配送员</option>
					<option value="3">分配员</option>
				</select>
			</div>
			
			<div class="form-group">
				<label class="sr-only" for="lookup-username">员工号</label>
				<input class="form-control" id="lookup-username" name="username" value="${lookup.username }" placeholder="员工号" />
			</div>
			
			<div class="form-group">
				<label class="sr-only" for="lookup-name">姓名</label>
				<input class="form-control" id="lookup-name" name="name" value="${lookup.name }" placeholder="姓名" />
			</div>
			
			<div class="form-group">
				<label class="sr-only" for="lookup-mobile">电话</label>
				<input class="form-control" id="lookup-mobile" name="mobile" value="${lookup.mobile }" placeholder="电话" />
			</div>
			
			<div class="form-group">
				<label class="sr-only" for="lookup-status">员工状态</label>
				<select class="form-control" id="lookup-status" name="status" data-value="${lookup.status }" >
					<option value="10">--选择员工状态全部--</option>
					<option value="0">闲置</option>
					<option value="1">工作中</option>
				</select>
			</div>

			
			<div class="form-group form-btn-bar">
				<button type="submit" class="btn btn-primary btn-sm">
					<i class="icon-search"></i> 查询
				</button>
				<button type="button" class="btn btn-info btn-sm reset">
					<i class="icon-undo"></i> 重置
				</button>
			</div>

		</form>
 
	</div>

	<c:if test="${not empty remind}">
		<div class="alert alert-${remind.level}">
			<button data-dismiss="alert" class="close" type="button">
				<i class="icon-remove"></i>
			</button>
			${remind.message}
		</div>
	</c:if>


	<form class="row row-nocol" method="post">

		<div class="action-bar">
			<a class="btn btn-sm btn-success" href="site"><i class="icon-file"></i>
				地图定位</a> 
			<a class="btn btn-sm btn-success" href="0"><i class="icon-file"></i>
				添加</a> 
			
		</div>


		<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th class="center"><input type="checkbox" data-member="pks" /></th>
					<th>序号</th>
					<th>员工类型</th>
					<th>姓名</th>
					<th>员工号</th>
					<th>电话</th>
					<th>状态</th>
					<th class="text-info">操作</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${pager.elements}" var="cur" varStatus="status">
					<tr>
						<td class="center"><input type="checkbox" name="pks"
							value="${cur.id}" /></td>
						<td>${status.count}</td>	
						<td>${cur.employeeType}</td>
						<td>${cur.name}</td>	
						<td>${cur.username}</td>
						<td>${cur.mobile}</td>
						<td>${cur.employeeStatus}</td>	
						<td>
							<div class="btn-group">
                                  
                                  
								<%-- <a class="btn btn-xs btn-info tooltip-info" 
									data-href="${cur.id}/edit"
									data-rel="tooltip" data-original-title="编辑"> <i
									class="icon-edit bigger-120"></i>
								</a> --%>
								
								<button type="button"  class="btn btn-xs btn-danger tooltip-warning action-get" data-href="${cur.id}" data-rel="tooltip" type="button" data-original-title="编辑" data-confirm="">
                                        <i class="icon-edit bigger-120"></i>
                                </button>
                                <button class="btn btn-xs btn-danger tooltip-warning action-get" type="button" data-href="${cur.id}/remove" data-rel="tooltip" data-original-title="删除" data-confirm="${cur.type eq 3 ? '删除的同时将移除取菜员中对应的当前分配员' : '确定删除当前员工'}">
                                       <i class="icon-trash bigger-120"></i>
                                       <i class="action-param" data-key="type" data-value="${cur.type }"></i>
                                </button>
							    
							</div>
						</td>
					</tr>
				</c:forEach>
				<c:forEach begin="${fn:length(pager.elements)}"
					end="${pager.size-1}">
					<tr>
						<td>&nbsp;</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</form>
	<%@include file="/WEB-INF/jspf/pager.jspf"%>
	<%@include file="/WEB-INF/jspf/body-last.jspf"%>
</body>
</html>
