<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
        <script type="text/javascript">
        	$(function(){
        		var flag = $("#flag").val();
        		if("edit" == flag) {
        			$("#type").attr("disabled",true);
        		} 
        		if("add" == flag){
        			$("#type").attr("disabled",false);
        			$("#username_div").hide();
        		}
        		var type = $("#type").val();
        		if(1 == type){
        			$("#center_div").show();
        		}
        		if(2 == type){
        			$("#region_div").show();
        		}
        	});
        	
        	function changeType(){
        		var type = $("#type").val();
        		if(1==type){
        			$("#center_div").show();
        			$("#region_div").hide();
        		}else if(2 == type){
        			$("#center_div").hide();
        			$("#region_div").show();
        		}else{
        			if( 3==type){
        				var count = $("#exsitsCentersCount").val();
            			if(count > 0 ){
            				alert("系统已存在分配员，不能重复添加");
            				$("#type").val(0);
            			}
        			}
        			$("#center_div").hide();
        			$("#region_div").hide();
        		}
        	}
        </script>
    </head>
    <body class="${principal.skin} ${principal.navbarFixed?'navbar-fixed':''}">
        <%@include file="/WEB-INF/jspf/body-first.jspf" %>
        <form class="form-horizontal validate" id="entity-detail" method="post" role="form" data-ignore=":hidden"> 
        	<input type="hidden" id="flag" value="${flag}">
        	<input type="hidden" id="exsitsCentersCount" value="${fn:length(centers)}"/>
        	<div class="form-group" >
                <label class="col-sm-3 control-label" for="type"><span class="text-danger">*</span>员工类型：</label>
                <div class="col-sm-5">
                    <select class="form-control" id="type" name="type" data-value="${entity.type}" onchange="changeType();"  data-rule-required="true">
                    	<option value="0">--选择员工类型--</option>
                        <option value="1">取菜员</option>
                        <option value="2">配送员</option>
                        <option value="3">分配员</option>
                    </select>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="type" id="type_label"></label>
            </div> 
        
        	 <div class="form-group" id="username_div">
                <label class="col-sm-3 control-label" for="username"><span class="text-danger">*</span>员工号：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="username" name="username" value="${entity.username}" readonly="true" disabled="true" />
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="usernam"></label>
            </div>  
        	
        	 <div class="form-group">
                <label class="col-sm-3 control-label" for="name"><span class="text-danger">*</span>姓名：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="name" name="name" value="${entity.name}" data-rule-required="true" data-rule-maxlength="40"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="name"></label>
            </div> 
            
            <div class="form-group">
                <label class="col-sm-3 control-label" for="mobile"><span class="text-danger">*</span>手机号码：</label>
                <div class="col-sm-5">
                    <input class="form-control" id="mobile" name="mobile" value="${entity.mobile}" data-rule-required="true" data-rule-mobile="true"/>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="mobile"></label>
            </div>  
        
        	<div class="form-group">
                <label class="col-sm-3 control-label" for="status">员工状态：</label>
                <div class="col-sm-5">
                    <select class="form-control" id="status" name="status" data-value="${entity.status}" >
                        <option value="0">闲置</option>
                        <option value="1">工作中</option>
                    </select>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="status"></label>
            </div> 
        	
        	<div class="form-group"  style="display:none" id="center_div">
                <label class="col-sm-3 control-label" for="centerId"><span class="text-danger">*</span>对应分配员：</label>
                <div class="col-sm-5">
                    <select class="form-control" id="centerId" name="centerId" data-value="${entity.centerId}" data-rule-required="true">
                        <c:forEach items="${centers}" var="cur">
                            <option value="${cur.id}">${cur.center}</option>
                        </c:forEach>
                    </select>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="centerId"></label>
            </div>
            
            <div class="form-group"  style="display:none" id="region_div">
                <label class="col-sm-3 control-label" for="regionId"><span class="text-danger">*</span>配送区域：</label>
                <div class="col-sm-5">
                    <select class="form-control" id="regionId" name="regionId" data-value="${entity.regionId}" data-rule-required="true">
                        <option value="">-选择配送区域-</option>
                        <c:forEach items="${regions}" var="cur">
                            <option value="${cur.id}">${cur.name}</option>
                        </c:forEach>
                    </select>
                </div>
                <label class="col-sm-4 help-inline form-control-static" for="regionId"></label>
            </div>
            <div class="clearfix form-actions">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary">
                        <i class="icon-ok bigger-110"></i>
                        确定提交
                    </button>

                    <button type="button" class="btn btn-info action-back">
                        <i class="icon-undo bigger-110"></i>
                        取消返回
                    </button>
                </div>
            </div>
        </form>
        <%@include file="/WEB-INF/jspf/body-last.jspf" %>
    </body>
</html>
