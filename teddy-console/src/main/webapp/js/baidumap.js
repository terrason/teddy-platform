var map;
var areaPlygon = new Array();//存放区域的点
var polygon;
(function() {
    function initMap($map) {
    	var mapId=$map.attr("id");
        map = new BMap.Map(mapId); // 在百度地图容器中创建一个地图
        var point = new BMap.Point(117.2877640000,31.8616650000); // 定义一个中心点坐标
	    map.centerAndZoom(point,12); // 初始化地图,设置中心点坐标和地图级别
	    var marker = new BMap.Marker(point);  // 创建标注
	    map.addOverlay(marker);              // 将标注添加到地图中
	    marker.setAnimation(BMAP_ANIMATION_BOUNCE); // 跳动的动画
        map.disableDoubleClickZoom(); // 禁用双击放大
        map.enableScrollWheelZoom(); // 启用地图滚轮放大缩小
        map.addControl(new BMap.NavigationControl()); // 添加平移缩放控件
        map.addControl(new BMap.ScaleControl()); // 添加比例尺控件
        map.addControl(new BMap.OverviewMapControl());// 添加缩略地图控件
        map.addControl(new BMap.MapTypeControl());  // 添加地图类型控件
        map.setCurrentCity("合肥"); // 设置地图显示的城市 此项是必须设置的
    }
        
    $(document).ready(function() {
        var i=1;
        $("[data-ready=baidumap]").each(function() {
            var $map = $(this);
            if($("#"+$map.attr("id")).length<=0){
                $map.attr("id","baidumap"+i++);
            }
            initMap($map);
        });
    });
})();

/**遍历所有配送员的位置*/
function createDeliverPoint(){
	console.info("ajax 调用所有配送员的信息");
	var url = ctx+ "/system/employee/ajax";
	console.info("url:"+url);
	$.ajax({
        	type:"POST",
    		url:url,
    		async:false,
    		dataType:"JSON",
    		success:function(msg){
    			console.info(msg.length);
    			for(var i =0; i<msg.length;i++){
    				if(i==0){
    		    		map.centerAndZoom(p,12); // 初始化地图,设置中心点坐标和地图级别
    		    		map.clearOverlays(); 
    		    	}
    				var x = msg[i].longitudeString;
    		    	var y = msg[i].latitudeString;
    		    	console.info("x-y" + x + y);
    		    	var label = msg[i].username;
    		    	var p = new BMap.Point(x,y);
    		    	var mark = new BMap.Marker(p);
    		    	console.info(mark + "--" + label);
    		    	map.addOverlay(mark);
    		    	mark.setLabel(new BMap.Label(label,{offset:new BMap.Size(20,-10)}));
    		    	mark.setAnimation(BMAP_ANIMATION_BOUNCE); // 跳动的动画
    			}
    		}
        });
	
}
/**
 * 编辑区域
 * 创建一个多边形
 * @param xys
 */
function createPolygon(xys){
	map.clearOverlays(); 
	createAllRegion($(".others-region"));//把非当前区域加入地图
	$.each(xys, function(i,xy){
		var xyArr = xy.split("-");
		var point = new BMap.Point(xyArr[0], xyArr[1]);
		//map.addOverlay(new BMap.Marker(point));
		areaPlygon.push(point);
	});
	polygon = new BMap.Polygon(areaPlygon,{strokeColor:"blue", strokeWeight:2, strokeOpacity:0.5});
	map.addOverlay(polygon);//新增多边形
	polygon.enableEditing();
}

/**新增区域*/

function toAddPolygon(){
	map.clearOverlays(); 
	createAllRegion($(".others-region"));//把非当前区域加入地图
	// 新增点击事件:获得坐标点 
	map.addEventListener("dblclick", function(e){
		var point = new BMap.Point(e.point.lng, e.point.lat);
		var marker = new BMap.Marker(point);
		areaPlygon.push(point);
		map.addOverlay(marker);
		if(areaPlygon.length>2){
			map.clearOverlays(); 
			createAllRegion($(".others-region"));//把非当前区域加入地图
			polygon = new BMap.Polygon(areaPlygon,{strokeColor:"blue", strokeWeight:2, strokeOpacity:0.5});
			map.addOverlay(polygon);//新增多边形
		}

	}); 
}
//编辑多边形
function couldEdit(){
	if(areaPlygon.length < 3){
		alert("请至少选择三个点以上以确定一个区域");
	}
	map.clearOverlays(); 
	createAllRegion($(".others-region"));//把非当前区域加入地图
	console.info("编辑多边形" + $(".others-region"));
	map.addOverlay(polygon);
	polygon.enableEditing();
};
//完成编辑
function finishEdit(){
	polygon.disableEditing();
	var points = "";
	var $points = $("#points");
	$.each(polygon.getPath(),function(i, p){
		if(i==0){
			points += p.lng+"-"+p.lat;
		}else{
			points += "," + p.lng + "-" + p.lat;
		}
	});
	$points.val(points);
};
//重新编辑
function again(){
	polygon=null;
	areaPlygon=[];
	map.clearOverlays(); 
	var flag = $("#flag").val();
	if("edit" == flag) {
			var xys = $("#xy").val().split(",");
		createPolygon(xys);
		}else if("add" == flag){
			toAddPolygon();
		} 
};


/**显示所有区域**/
function createAllRegion($allRegion){
	for(var i=0; i<$allRegion.length;i++){
		if(i==0){
			map.clearOverlays(); 	
		}
		var areaname = $($allRegion[i]).data("areaname");
		var xys = $($allRegion[i]).val().split(",");
		var points = [];
		$.each(xys, function(i,xy){
			var xyArr = xy.split("-");
			var point = new BMap.Point(xyArr[0], xyArr[1]);
			points.push(point);
		});
		console.info((i+1)+"个多边形的所有点 " +points);
		var polygon = new BMap.Polygon(points,{strokeColor:"blue", strokeWeight:2, strokeOpacity:0.5});
		map.addOverlay(polygon);//新增多边形
		var center = polygon.getBounds().getCenter();
		var mark = new BMap.Marker(center);
		mark.setLabel(new BMap.Label(areaname,{offset:new BMap.Size(20,-10)}));
		map.addOverlay(mark);
		
	}
}


