(function() {
    function initMap($map) {
        var map = new BMap.Map($map.attr("id")); //在百度地图容器中创建一个地图
        
        var mapId=$map.attr("id");
        var x = $("#"+mapId).data("x");
        var y = $("#"+mapId).data("y");
        var point = new BMap.Point(x, y); //定义一个中心点坐标
        map.centerAndZoom(point, 16); // 初始化地图,设置中心点坐标和地图级别
        var marker = new BMap.Marker(point);  // 创建标注
        map.addOverlay(marker);              // 将标注添加到地图中
        marker.setAnimation(BMAP_ANIMATION_BOUNCE); //跳动的动画
        map.disableDoubleClickZoom(); //禁用双击放大
        
        map.enableScrollWheelZoom(); //启用地图滚轮放大缩小
        map.addControl(new BMap.NavigationControl()); // 添加平移缩放控件
        map.addControl(new BMap.ScaleControl()); // 添加比例尺控件
        map.addControl(new BMap.OverviewMapControl());//添加缩略地图控件
        map.addControl(new BMap.MapTypeControl());  //添加地图类型控件
        map.setCurrentCity("合肥"); // 设置地图显示的城市 此项是必须设置的
         
    }
    $(document).ready(function() {
        var i=1;
        $("[data-ready=baidumap]").each(function() {
            var $map = $(this);
            if($("#"+$map.attr("id")).length<=0){
                $map.attr("id","baidumap"+i++);
            }
            initMap($map);
        });
    });
})();
