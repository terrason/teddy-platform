/**
 * 放大图片
 */
jQuery(function ($) {
    var $body = $("body");
    var $template = $("<img style='display:none;' src='' alt=''/>");
    $("img.amplifier").mouseover(function (e) {
        var $this = $(this);
        var $amplifier = $this.data("hyl-dashboard-amplifier");
        if (!$amplifier) {
            $amplifier = $template.clone();
            $amplifier.attr("src", this.src);
            $this.data("hyl-dashboard-amplifier", $amplifier);
            $body.append($amplifier);
        }
        $amplifier.css({
            "position": "absolute",
            "top": (e.pageY + 10) + "px",
            "left": (e.pageX + 10) + "px"
        }).show("fast");
    }).mouseout(function () {
        var $this = $(this);
        var $amplifier = $this.data("hyl-dashboard-amplifier");
        if ($amplifier) {
            $amplifier.hide();
        }
    }).mousemove(function (e) {
        var $this = $(this);
        var $amplifier = $this.data("hyl-dashboard-amplifier");
        if ($amplifier) {
            $amplifier.css({
                "top": (e.pageY + 10) + "px",
                "left": (e.pageX + 10) + "px"
            });
        }
    });
});