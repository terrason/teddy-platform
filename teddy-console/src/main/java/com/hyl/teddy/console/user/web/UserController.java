package com.hyl.teddy.console.user.web;

import com.hyl.teddy.console.AbstractController;
import com.hyl.teddy.console.Remind;
import com.hyl.teddy.console.entity.CustAddress;
import com.hyl.teddy.console.entity.CustCash;
import com.hyl.teddy.console.entity.Customer;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;
import com.hyl.teddy.console.user.UserLookup;
import com.hyl.teddy.console.user.UserService;
import com.hyl.teddy.console.util.CacheEvictor;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * 用户信息管理
 *
 * @author Administrator
 */
@Controller
public class UserController extends AbstractController {

    @Resource
    private UserService userService;
    @Resource
    private CacheEvictor cacheEvictor;

    /**
     * 客户信息列表
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/user/", method = RequestMethod.GET)
    public String home(Model model) {
        Pager<Customer> pager = userService.queryCustomer(getLookup());
        model.addAttribute("pager", pager);
        getLookup().setTotal(pager.getTotal());
        return "user/list";
    }

    @Override
    protected Lookup instanceLookup() {
        // 封装查询条件
        return new UserLookup();
    }

    /**
     * 接受用户提交的查询信息
     *
     * @param lookup
     * @return
     */
    @RequestMapping(value = "/user/", method = RequestMethod.POST)
    public String search(UserLookup lookup) {
        setLookup(lookup);
        return "redirect:/user/";
    }

    /**
     * To edit
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable int id, Model model) {
        if (id <= 0) {
            throw new IllegalArgumentException("id参数不合法");
        }
        Customer entity = userService.findCustomerById(id);
        model.addAttribute("entity", entity);
        return "user/form";
    }

    /**
     * 保存客户信息
     *
     * @param id
     * @param entity
     * @param redirectAttrs
     * @return
     */
    @RequestMapping(value = "/user/{id}", method = RequestMethod.POST)
    public String save(@PathVariable int id, Customer entity, RedirectAttributes redirectAttrs) {
        entity.setId(id);
        if (id > 0) {
            if (userService.updateCustomer(entity)) {
                redirectAttrs.addFlashAttribute(Remind.success().appendMessage("修改成功。"));
            } else {
                redirectAttrs.addFlashAttribute(Remind.warning().appendMessage("修改失败。"));
            }

        }
        return "redirect:/user/";
    }

    /**
     * 客户收藏列表
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/user/{id}/favoraties/", method = RequestMethod.GET)
    public String findFavoraties(@PathVariable int id, Model model) {
        model.addAttribute("favoraties", userService.findFavoratiesById(id));
        return "user/favoraties/list";
    }

    /**
     * 客户代金券列表
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/user/{id}/cash/", method = RequestMethod.GET)
    public String findCash(@PathVariable int id, Model model) {
        Customer customer = userService.findCustomerById(id);
        model.addAttribute("customer", customer);
        model.addAttribute("cash", userService.findCashById(id));
        return "user/cash/list";
    }

    /**
     * 从代金券列表返回用户列表
     *
     * @return
     */
    @RequestMapping(value = "/user/{id}/cash/back", method = RequestMethod.GET)
    public String backCashHome() {
        return "redirect:/user/";
    }

    /**
     * 从代金券列表返回用户列表
     *
     * @return
     */
    @RequestMapping(value = "/user/{id}/favoraties/back", method = RequestMethod.GET)
    public String backFavoratiesHome() {
        return "redirect:/user/";
    }

    /**
     * 从代金券列表返回用户列表
     *
     * @return
     */
    @RequestMapping(value = "/user/{id}/address/back", method = RequestMethod.GET)
    public String backAddressHome() {
        return "redirect:/user/";
    }

    /**
     * To add
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/user/{customerId}/cash/0", method = RequestMethod.GET)
    public String addCash() {
        return "user/cash/form";
    }

    /**
     * To edit
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/user/{customerId}/cash/{id}", method = RequestMethod.GET)
    public String editCash(@PathVariable int id, @PathVariable int customerId, Model model) {
        if (id < 0) {
            throw new IllegalArgumentException("id参数不合法");
        }
        CustCash entity = userService.queryCashById(id);
        model.addAttribute("entity", entity);
        return "user/cash/form";
    }

    /**
     * 保存客户代金券信息
     *
     * @param id
     * @param entity
     * @param redirectAttrs
     * @return
     */
    @RequestMapping(value = "/user/{customerId}/cash/{id}", method = RequestMethod.POST)
    public String saveCash(@PathVariable int id, CustCash entity, @PathVariable int customerId, RedirectAttributes redirectAttrs) {
        entity.setId(id);
        entity.setCustomerId(customerId);
        if (id > 0) {
            if (userService.updateCash(entity)) {
                redirectAttrs.addFlashAttribute(Remind.success().appendMessage("修改成功。"));
                cacheEvictor.evictCache("customer", customerId);
            } else {
                redirectAttrs.addFlashAttribute(Remind.warning().appendMessage("修改失败。"));
            }

        } else if (id == 0) {
            if (userService.updateOrCreateCash(entity)) {
                redirectAttrs.addFlashAttribute(Remind.success().appendMessage("添加成功。"));
                cacheEvictor.evictCache("customer", customerId);
            } else {
                redirectAttrs.addFlashAttribute(Remind.warning().appendMessage("添加失败。"));
            }
        }
        return "redirect:/user/{customerId}/cash/";
    }

    /**
     * 客户地址列表
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/user/{id}/address/", method = RequestMethod.GET)
    public String findAddress(@PathVariable int id, Model model) {
        Customer customer = userService.findCustomerById(id);
        model.addAttribute("address", userService.findAddressById(id));
        model.addAttribute("customer", customer);
        return "user/address/list";
    }

    /**
     * To add
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/user/{customerId}/address/0", method = RequestMethod.GET)
    public String addAddress(Model model) {
        model.addAttribute("region", userService.findAllRegion());
        return "user/address/form";
    }

    /**
     * To edit
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/user/{customerId}/address/{id}", method = RequestMethod.GET)
    public String editAddress(@PathVariable int id, @PathVariable int customerId, Model model) {
        if (id < 0) {
            throw new IllegalArgumentException("id参数不合法");
        }
        model.addAttribute("region", userService.findAllRegion());
        CustAddress entity = userService.queryAddressById(id);
        model.addAttribute("entity", entity);
        return "user/address/form";
    }

    /**
     * 保存客户地址信息
     *
     * @param id
     * @param entity
     * @param redirectAttrs
     * @return
     */
    @RequestMapping(value = "/user/{customerId}/address/{id}", method = RequestMethod.POST)
    public String saveAddress(@PathVariable int id, CustAddress entity, @PathVariable int customerId, RedirectAttributes redirectAttrs) {
        entity.setId(id);
        String longitude = entity.getLongitude().replace(".", "");
        String latitude = entity.getLatitude().replace(".", "");
        entity.setLongitudeValue(Integer.parseInt(longitude));
        entity.setLatitudeValue(Integer.parseInt(latitude));
        if (id > 0) {
            if (userService.updateAddress(entity)) {
                redirectAttrs.addFlashAttribute(Remind.success().appendMessage("修改成功。"));
            } else {
                redirectAttrs.addFlashAttribute(Remind.warning().appendMessage("修改失败。"));
            }

        } else if (id == 0) {
            entity.setCustomerId(customerId);
            if (userService.createAddress(entity)) {
                redirectAttrs.addFlashAttribute(Remind.success().appendMessage("添加成功。"));
            } else {
                redirectAttrs.addFlashAttribute(Remind.warning().appendMessage("添加失败。"));
            }
        }
        return "redirect:/user/{customerId}/address/";
    }
}
