package com.hyl.teddy.console.activity.web;

import com.hyl.teddy.console.AbstractController;
import com.hyl.teddy.console.Remind;
import com.hyl.teddy.console.activity.ActivityLookup;
import com.hyl.teddy.console.activity.ActivityService;
import com.hyl.teddy.console.entity.Activity;
import com.hyl.teddy.console.jpush.CustomerJpushService;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;
import java.util.regex.Pattern;
import javax.annotation.Resource;
import javax.imageio.metadata.IIOMetadataFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *活动管理
 * @author Administrator
 */
@Controller
public class ActivityController extends AbstractController{
    @Resource
    private ActivityService activityService;
    @Resource
    private CustomerJpushService jpushService;
    
    private final Pattern resource = Pattern.compile("^(https?|ftp)://.*");
    /**
     * 活动列表
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/activity/", method = RequestMethod.GET)
    public String home(Model model) {
        Pager<Activity> pager = activityService.queryActivity(getLookup());
        model.addAttribute("pager", pager);
        getLookup().setTotal(pager.getTotal());
        return "activity/list";
    }

    @Override
    protected Lookup instanceLookup() {
        // 封装查询条件
        return new ActivityLookup();
    }

    /**
     * 接受用户提交的查询信息
     *
     * @param lookup
     * @return
     */
    @RequestMapping(value = "/activity/", method = RequestMethod.POST)
    public String search(ActivityLookup lookup) {
        setLookup(lookup);
        return "redirect:/activity/";
    }
    /**
     * 删除活动
     *
     * @param id
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = "/activity/{id}/remove", method = RequestMethod.POST)
    public String remove(@PathVariable int id, RedirectAttributes redirectAttributes) {
        if (id <= 0) {
            throw new IllegalArgumentException("id参数不合法");
        }
        activityService.removeActivity(id);
        redirectAttributes.addFlashAttribute(Remind.warning().appendMessage("成功删除"));
        return "redirect:/activity/";
    }

    /**
     * 批量删除活动
     *
     * @param id
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = "/activity/remove", method = RequestMethod.POST)
    public String removes(@RequestParam int[] pks, RedirectAttributes redirectAttributes) {
        if (pks.length <= 0) {
            throw new IllegalArgumentException("id参数不合法");
        }
        activityService.removeActivity(pks);
        redirectAttributes.addFlashAttribute(Remind.warning().appendMessage("批量删除成功"));
        return "redirect:/activity/";
    }

    /**
     * To add
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/activity/0", method = RequestMethod.GET)
    public String input(Model model) {
        return "activity/form";
    }

    /**
     * To edit
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/activity/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable int id, Model model) {
        if (id <= 0) {
            throw new IllegalArgumentException("id参数不合法");
        }
        model.addAttribute("entity", activityService.findActivityById(id));
        return "activity/form";
    }

    /**
     * 保存活动
     *
     * @param id
     * @param entity
     * @param redirectAttrs
     * @return
     */
    @RequestMapping(value = "/activity/{id}", method = RequestMethod.POST)
    public String save(@PathVariable int id, Activity entity, RedirectAttributes redirectAttrs) {
        entity.setId(id);
        if(!resource.matcher(entity.getUrl()).matches()){
            entity.setUrl("http://"+entity.getUrl());
        }
        if (id > 0) {
            activityService.updateActivity(entity);
            redirectAttrs.addFlashAttribute(Remind.success().appendMessage("修改成功。"));
        } else if (id == 0) {
            activityService.createActivity(entity);
            redirectAttrs.addFlashAttribute(Remind.success().appendMessage("创建成功。"));
        } else {
            throw new IllegalArgumentException("信息不合法");
        }
        return "redirect:/activity/";
    }
    /**
     * 推送
     * @return 
     */
    @RequestMapping(value = "/activity/{id}/push", method = RequestMethod.POST)
    public String push(@PathVariable int id){
        Activity activity = activityService.findActivityById(id);
        String title = activity.getName() +":"  +activity.getPushContent();
        String url = activity.getUrl();
        if(!url.contains("?")){
        	url += "?1=1"; 
        }
        jpushService.create(title).data("module", jpushService.MODULE_ACITIVITY).data("url", url).push();
        return "redirect:/activity/";
    }

}
