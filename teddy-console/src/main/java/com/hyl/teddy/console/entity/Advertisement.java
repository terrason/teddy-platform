package com.hyl.teddy.console.entity;
/**
 * 广告位  advertisement
 * @author Administrator
 *
 */
public class Advertisement {
	private int id;
	private int category;//类型
	private Integer image;// 附件表id(图片) attachment表
	private String url;//广告跳转地址
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCategory() {
		return category;
	}
	public void setCategory(int category) {
		this.category = category;
	}
	public Integer getImage() {
		return image;
	}
	public void setImage(Integer image) {
		this.image = image;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
}
