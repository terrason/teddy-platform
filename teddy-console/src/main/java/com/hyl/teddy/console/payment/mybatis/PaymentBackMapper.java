package com.hyl.teddy.console.payment.mybatis;

import java.util.List;

import com.hyl.teddy.console.MybatisMapper;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.payment.web.PaymentBack;
import java.util.Collection;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

@MybatisMapper
public interface PaymentBackMapper {

    public int countPaymentBack(Lookup lookup);

    public List<PaymentBack> selectPaymentBackList(Lookup lookup);

    public List<Map<String, String>> selectPaymentThreads(@Param("pks") Collection<String> pks);
}
