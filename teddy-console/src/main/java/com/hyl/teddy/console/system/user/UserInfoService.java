package com.hyl.teddy.console.system.user;

import com.hyl.teddy.console.DuplicatedEntityException;
import com.hyl.teddy.console.pager.Pager;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.system.user.mybatis.UserInfoMapper;
import java.util.Collections;
import java.util.List;
import javax.annotation.Resource;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;

/**
 * 处理用户相关服务.
 *
 * @author lip
 */
@Service
public class UserInfoService {

    @Resource
    private UserInfoMapper userInfoMapper;

    /**
     * 分页查询用户列表.
     *
     * @param lookup 用户查询参数.
     * @return 用户列表
     */
    public Pager<UserInfo> queryUserList(Lookup lookup) {
        int total = userInfoMapper.countUser(lookup);
        Pager<UserInfo> pager = new Pager<>();
        pager.setPage(lookup.getPage());
        pager.setSize(lookup.getSize());
        pager.setTotal(total);

        List<UserInfo> entities = pager.isOverflowed() ? Collections.EMPTY_LIST : userInfoMapper.selectUserList(lookup);
        pager.setElements(entities);
        return pager;
    }

    /**
     * 根据ID查询用户实体.
     *
     * @param id 用户ID
     * @return 用户实体
     */
    public UserInfo findUserById(int id) {
        return userInfoMapper.selectUserById(id);
    }

    /**
     * 根据用户登录名查找用户实体.
     *
     * @param username 用户登录名
     * @return 用户实体
     */
    public UserInfo findUserByUsername(String username) {
        return userInfoMapper.selectUserByUsername(username);
    }

    /**
     * 保存用户信息的修改.
     *
     * @param entity 用户信息
     */
    public void saveUser(UserInfo entity) {
        userInfoMapper.updateUser(entity);
    }

    /**
     * 保存用户配置信息.
     *
     * @param entity 用户
     */
    public void saveUserSettings(UserInfo entity) {
        userInfoMapper.updateUserSettings(entity);
    }

    /**
     * 创建新用户.
     *
     * @param entity 用户信息
     */
    public void createUser(UserInfo entity) {
        if (userInfoMapper.countUsername(entity.getUsername()) > 0) {
            throw new DuplicatedEntityException("登陆名");
        }
        userInfoMapper.insertUser(entity);

    }

    /**
     * 根据用户ID删除用户.
     *
     * @param id 用户ID
     */
    public void removeUser(int id) {
        userInfoMapper.deleteUser(id);
    }

    /**
     * 批量删除用户.
     *
     * @param pks 需要批量删除的用户ID数组.
     */
    public void removeUserByPks(int[] pks) {
        userInfoMapper.deleteUserByPks(pks);
    }

    /**
     * 设置密码.
     *
     * @param email 账号邮箱.
     * @param password 新的密码
     */
    public void passwd(int id, String password) {
        userInfoMapper.updateUserPassword(id, DigestUtils.md5Hex(password));
    }
}
