package com.hyl.teddy.console.entity;

/**
 * 员工表 employee
 * 
 * @author Administrator
 * 
 */
public class Employee {
	private int id;
	private String username;//工号
	private String password;
	private String name;//姓名
	private int type;// '1-取菜员\n 2-配送员' 3 分配员,
	private Integer regionId;// 区域 只对配送员有效
	private String regionName;
	private String mobile; //员工号码
	private int longitude;//配送员有效
	private String longitudeString;
	private int latitude;//配送员有效
	private String latitudeString;
	private int centerId;//取菜员对应分配员id
	private String pushKey;//jpush机器号
	private int status;//状态 0-闲置 1-工作中 9-休息
	
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Integer getRegionId() {
		return regionId;
	}

	public void setRegionId(Integer regionId) {
		this.regionId = regionId;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public int getLongitude() {
		return longitude;
	}

	public void setLongitude(int longitude) {
		this.longitude = longitude;
	}

	public String getLongitudeString() {
		return longitudeString;
	}

	public void setLongitudeString(String longitudeString) {
		this.longitudeString = longitudeString;
	}

	public int getLatitude() {
		return latitude;
	}

	public void setLatitude(int latitude) {
		this.latitude = latitude;
	}

	public String getLatitudeString() {
		return latitudeString;
	}

	public void setLatitudeString(String latitudeString) {
		this.latitudeString = latitudeString;
	}

	public int getCenterId() {
		return centerId;
	}

	public void setCenterId(int centerId) {
		this.centerId = centerId;
	}

	public String getPushKey() {
		return pushKey;
	}

	public void setPushKey(String pushKey) {
		this.pushKey = pushKey;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	public String getEmployeeType(){
		switch(type){
		case 1:
			return "取菜员";
		case 2:
			return "配送员";
		case 3:
			return "分配员";
		}
		return "";
	}
	
	public String getEmployeeStatus(){
		switch (status) {
		case 0:
			return "闲置";
		case 1:
			return "工作中";
		case 9:
			return "休息";

		}
		return "";
	}
}
