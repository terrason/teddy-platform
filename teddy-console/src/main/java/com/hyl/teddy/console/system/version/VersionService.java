package com.hyl.teddy.console.system.version;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hyl.teddy.console.attachment.AttachmentService;
import com.hyl.teddy.console.entity.Version;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;
import com.hyl.teddy.console.system.version.mybatis.VersionMapper;

/**
 * 版本更新Service
 * @author Administrator
 *
 */
@Service
@Transactional
public class VersionService {
	
	@Resource
	private VersionMapper mapper;

	@Resource
    private AttachmentService attachmentService;
	/**
	 *版本更新列表
	 * @param lookup
	 * @param vERSION_CUSTOMER
	 * @return
	 */
	public Pager<Version> queryVersionList(Lookup lookup, String table) {
		System.out.println("service");
		Pager<Version> pager = new Pager<Version>();
		pager.setPage(lookup.getPage());
		pager.setSize(lookup.getSize());
		int count = mapper.queryCount(table);
		pager.setTotal(count);
		List<Version> list = pager.isOverflowed() ? Collections.EMPTY_LIST : mapper.queryVersionList(lookup, table);
		pager.setElements(list);
		return pager;
	}

	/**
	 * 删除记录
	 * @param id
	 * @param table
	 */
	public void remove(int id, String table) {
		Version version = findVersion(id, table);
		if (version != null){
			attachmentService.changeAttachmentTemporary(version.getDownloadUrl(), true);
		}
		mapper.delete(id,table);
		
	}
		

	/**
	 * 新增
	 * @param entity
	 * @param table
	 */
	public void addVersion(Version entity, String table) {
		mapper.insert(entity,table);
		//把对应附件表改为非临时态
		attachmentService.changeAttachmentTemporary(entity.getDownloadUrl(), false);
		
	}
	 /** 修改
	 * @param entity
	 * @param table
	 */
	public  void updateVersion(Version entity, String table) {
		Version version = findVersion(entity.getId(), table);
		if(version != null){
			if(version.getDownloadUrl() != entity.getDownloadUrl()){
				attachmentService.changeAttachmentTemporary(version.getDownloadUrl(), true);
				attachmentService.changeAttachmentTemporary(entity.getDownloadUrl(), false);
			}
		}
		mapper.update(entity, table);
		
		
	}

	/**
	 * 根据id和表名查找Version
	 * @param id
	 * @param converUrlToTable
	 * @return
	 */
	public  Version findVersion(int id, String table) {
		return mapper.findVersion(id, table);
	}

}
