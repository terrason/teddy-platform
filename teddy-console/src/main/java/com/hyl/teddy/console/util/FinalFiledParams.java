package com.hyl.teddy.console.util;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *  怡秒手机常用常量 
 * @author VIC
 *
 */
public class FinalFiledParams {

	/** 1 申明常量***/
	/**预约方式*/
	public static final Map<Integer,String> PAYMENT_STATUS = new LinkedHashMap<Integer, String>(5);
	
	/**对常量初始化*/
	static{
		/**
		 * 0-未付款 1-支付失败 2-未指派 3-配餐中 4- 缺失菜品 5-配餐完成 6-配送中 8-已完成 9- 已取消
		 */
		PAYMENT_STATUS.put(0, "未付款");
		PAYMENT_STATUS.put(1, "支付失败");
		PAYMENT_STATUS.put(2, "未指派");
		PAYMENT_STATUS.put(3, "配餐中");
		PAYMENT_STATUS.put(4, "缺失菜品");
		PAYMENT_STATUS.put(5, "配餐完成");
		PAYMENT_STATUS.put(6, "配送中");
		PAYMENT_STATUS.put(8, "已完成");
		PAYMENT_STATUS.put(9, "已取消");
		
	}
	
}
