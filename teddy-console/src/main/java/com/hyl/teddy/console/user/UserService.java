package com.hyl.teddy.console.user;

import com.hyl.teddy.console.entity.CustAddress;
import com.hyl.teddy.console.entity.CustCash;
import com.hyl.teddy.console.entity.CustFavoraties;
import com.hyl.teddy.console.entity.Customer;
import com.hyl.teddy.console.entity.Region;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;
import com.hyl.teddy.console.user.mybatis.UserMapper;
import com.hyl.teddy.console.util.CacheEvictor;
import java.util.Collections;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * 客户信息管理
 *
 * @author Administrator
 */
@Service
public class UserService {

    @Resource
    private UserMapper userMapper;
    @Resource
    private CacheEvictor cacheEvictor;

    public List<Region> findAllRegion() {
        return userMapper.findAllRegion();
    }

    /**
     * 分页查询
     *
     * @param lookup
     * @return
     */
    public Pager<Customer> queryCustomer(Lookup lookup) {
        Pager<Customer> pager = new Pager<Customer>();
        int total = userMapper.countCustomer(lookup);
        pager.setPage(lookup.getPage());
        pager.setSize(lookup.getSize());
        pager.setTotal(total);
        List<Customer> entiters = pager.isOverflowed() ? Collections.EMPTY_LIST : userMapper.selectCustomerList(lookup);
        pager.setElements(entiters);
        return pager;
    }

    /**
     * 根据id查询客户信息
     *
     * @param id
     * @return
     */
    public Customer findCustomerById(int id) {
        return userMapper.findCustomerById(id);
    }

    /**
     * 修改客户信息
     *
     * @param entity
     */
    public boolean updateCustomer(Customer entity) {
        userMapper.updateCustomer(entity);
        cacheEvictor.evictCache("customer", entity.getId());
        return true;

    }

    /**
     * 根据用户ID获取收藏列表
     *
     * @return
     */
    public List<CustFavoraties> findFavoratiesById(int id) {
        return userMapper.findFavoratiesListById(id);
    }

    /**
     * 根据用户ID获取代金券列表
     *
     * @return
     */
    public List<CustCash> findCashById(int id) {
        return userMapper.findCashListById(id);
    }

    /**
     * 根据用户ID获取地址列表
     *
     * @return
     */
    public List<CustAddress> findAddressById(int id) {
        return userMapper.findAddressListById(id);
    }

    /**
     * 根据ID获取收藏
     *
     * @return
     */
    public CustFavoraties queryFavoratiesById(int id) {
        return userMapper.queryFavoratiesById(id);
    }

    /**
     * 根据ID获取代金券
     *
     * @return
     */
    public CustCash queryCashById(int id) {
        return userMapper.queryCashById(id);
    }

    /**
     * 根据ID获取地址
     *
     * @return
     */
    public CustAddress queryAddressById(int id) {
        return userMapper.queryAddressById(id);
    }

    /**
     * 编辑代金券
     *
     * @param entity
     * @return
     */
    public boolean updateCash(CustCash entity) {
        userMapper.updateCash(entity);
        cacheEvictor.evictCache("customer", entity.getCustomerId());
        return true;
    }

    /**
     * 编辑地址
     *
     * @param entity
     * @return
     */
    public boolean updateAddress(CustAddress entity) {
        try {
            userMapper.updateAddress(entity);
            cacheEvictor.evictCache("address", entity.getId());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 增加代金券
     *
     * @param entity
     * @return
     */
    public boolean updateOrCreateCash(CustCash entity) {
        try {
            int c = userMapper.updateCash4Increase(entity.getCustomerId(), entity.getCash(), entity.getCount());
            if (c == 0) {
                userMapper.createCash(entity);
            }
            cacheEvictor.evictCache("customer", entity.getCustomerId());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 增加地址
     *
     * @param entity
     * @return
     */
    public boolean createAddress(CustAddress entity) {
        try {
            userMapper.createAddress(entity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
