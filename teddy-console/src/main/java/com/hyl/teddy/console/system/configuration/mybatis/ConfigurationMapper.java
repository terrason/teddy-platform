package com.hyl.teddy.console.system.configuration.mybatis;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.hyl.teddy.console.MybatisMapper;
import com.hyl.teddy.console.entity.StaticResource;

@MybatisMapper
public interface ConfigurationMapper {
	
	public List<StaticResource> queryAll();

	public void updateConfiguration(@Param("code")String code, @Param("content")String content);

	public void updateAllPromotion(@Param("param0")String param0, @Param("param1")String param1);
	
}
