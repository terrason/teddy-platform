package com.hyl.teddy.console.restaurant.web;

import com.hyl.teddy.console.AbstractController;
import com.hyl.teddy.console.Remind;
import com.hyl.teddy.console.dish.DishCategoryService;
import com.hyl.teddy.console.dish.DishLookup;
import com.hyl.teddy.console.dish.DishManagerService;
import com.hyl.teddy.console.entity.Dish;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;
import com.hyl.teddy.console.restaurant.RestaurantService;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * 餐厅菜品管理
 *
 * @author Administrator
 */
@Controller
public class RestaurantDishController extends AbstractController{

    @Resource
    private DishManagerService dishManagerService;
    @Resource
    private RestaurantService restaurantService;
    @Resource
    private DishCategoryService dishCategoryService;

    @RequestMapping(value = "/restaurant/manager/{id}/dish/", method = RequestMethod.GET)
    public String home(@PathVariable int id, Model model) {
        model.addAttribute("restaurant", restaurantService.findRestaurantById(id));
        DishLookup lookup = (DishLookup) getLookup();
        lookup.setRestaurantId(id);
        Pager<Dish> pager = dishManagerService.queryDishList(lookup);
        model.addAttribute("pager", pager);
        getLookup().setTotal(pager.getTotal());
        return "restaurant/manager/dish/list";
    }
    @Override
    protected Lookup instanceLookup() {
        return new DishLookup();
    }
    /**
     * 接受用户提交的查询信息
     *
     * @param lookup
     * @return
     */
    @RequestMapping(value = "/restaurant/manager/{id}/dish/", method = RequestMethod.POST)
    public String search(@PathVariable int id,DishLookup lookup) {
        setLookup(lookup);
        return "redirect:/restaurant/manager/{id}/dish/";
    }

    /**
     * 删除菜品
     *
     * @param id
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = "/restaurant/manager/{restaurantId}/dish/{id}/remove", method = RequestMethod.POST)
    public String remove(@PathVariable int id, RedirectAttributes redirectAttributes) {
        if (id <= 0) {
            throw new IllegalArgumentException("id参数不合法");
        }
        dishManagerService.removeDish(id);
        redirectAttributes.addFlashAttribute(Remind.warning().appendMessage("成功删除"));
        return "redirect:/restaurant/manager/{restaurantId}/dish/";
    }

    /**
     * 批量删除菜品
     *
     * @param id
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = "/restaurant/manager/{id}/dish/remove", method = RequestMethod.POST)
    public String removes(@RequestParam int[] pks, RedirectAttributes redirectAttributes) {
        if (pks.length <= 0) {
            throw new IllegalArgumentException("id参数不合法");
        }
        dishManagerService.removeDishes(pks);
        redirectAttributes.addFlashAttribute(Remind.warning().appendMessage("批量删除成功"));
        return "redirect:/restaurant/manager/{id}/dish/";
    }

    /**
     * To add
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/restaurant/manager/{restaurantId}/dish/0", method = RequestMethod.GET)
    public String input(@PathVariable int restaurantId,Model model) {
        model.addAttribute("dishCategories", dishCategoryService.queryAllCategories());
        model.addAttribute("restaurants", restaurantService.queryAllRestaurant());
        Dish dish = new Dish();
        dish.setPrice(10.00);
        dish.setSold(100);
        dish.setRestaurantId(restaurantId);
        model.addAttribute("entity", dish);
        return "restaurant/manager/dish/form";
    }

    /**
     * To edit
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/restaurant/manager/{restaurantId}/dish/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable int id, Model model) {
        if (id <= 0) {
            throw new IllegalArgumentException("id参数不合法");
        }
        Dish dish = dishManagerService.findDishById(id);
        model.addAttribute("entity", dish);
        model.addAttribute("restaurants", restaurantService.queryAllRestaurant());
        model.addAttribute("dishCategories", dishCategoryService.queryAllCategories());
        return "restaurant/manager/dish/form";
    }

    /**
     * 保存菜品
     *
     * @param id
     * @param entity
     * @param redirectAttrs
     * @return
     */
    @RequestMapping(value = "/restaurant/manager/{restaurantId}/dish/{id}", method = RequestMethod.POST)
    public String save(@PathVariable int id, Dish entity, RedirectAttributes redirectAttrs) {
        entity.setId(id);
        if (id > 0) {
            if (dishManagerService.updateDish(entity)) {
                redirectAttrs.addFlashAttribute(Remind.success().appendMessage("修改成功。"));
            } else {
                redirectAttrs.addFlashAttribute(Remind.warning().appendMessage("修改失败。"));
            }

        } else if (id == 0) {
            if (dishManagerService.createDish(entity)) {
                redirectAttrs.addFlashAttribute(Remind.success().appendMessage("创建成功。"));
            } else {
                redirectAttrs.addFlashAttribute(Remind.warning().appendMessage("创建失败。"));
            }

        } else {
            throw new IllegalArgumentException("信息不合法");
        }
        return "redirect:/restaurant/manager/{restaurantId}/dish/";
    }
    /**
	 *  关闭菜品
	 * @param id
	 * @param redirectAttrs
	 * @return
	 */
	@RequestMapping(value = "/restaurant/manager/{restaurantId}/dish/{id}/close",method = RequestMethod.POST)
	public String close(@PathVariable int id,RedirectAttributes redirectAttrs) {
		if(id < 0) {
			throw new IllegalArgumentException("id 参数不合法");
		}
		dishManagerService.closeDish(id);
		redirectAttrs.addFlashAttribute(Remind.warning().appendMessage("该菜品被关闭！"));
		return "redirect:/restaurant/manager/{restaurantId}/dish/";
	}
	
	/**
	 *  打开菜品
	 * @param id
	 * @param redirectAttrs
	 * @return
	 */
	@RequestMapping(value = "/restaurant/manager/{restaurantId}/dish/{id}/open",method = RequestMethod.POST)
	public String open(@PathVariable int id,RedirectAttributes redirectAttrs) {
		if(id < 0) {
			throw new IllegalArgumentException("id 参数不合法");
		}
		dishManagerService.openDish(id);
		redirectAttrs.addFlashAttribute(Remind.success().appendMessage("该菜品被打开！"));
		return "redirect:/restaurant/manager/{restaurantId}/dish/";
	}
	

}
