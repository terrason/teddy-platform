package com.hyl.teddy.console.dish;

import java.util.Collections;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.hyl.teddy.console.dish.mybaties.DishCategoryMapper;
import com.hyl.teddy.console.entity.DishCategory;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;

@Service
public class DishCategoryService {

	@Resource
	private DishCategoryMapper categoryMapper;
	/**
	 *  分页查询
	 * @param lookup
	 * @return
	 */
	public Pager<DishCategory> queryCategoryList(Lookup lookup) {
		Pager<DishCategory> pager = new Pager<DishCategory>();
		int total = categoryMapper.countCategory(lookup);
		pager.setPage(lookup.getPage());
		pager.setSize(lookup.getSize());
		pager.setTotal(total);
		List<DishCategory> entiters = pager.isOverflowed() ? Collections.EMPTY_LIST : categoryMapper.selectCategoryList(lookup);
		pager.setElements(entiters);
		return pager;
	}
	/**
	 * 删除菜品分类
	 * @param id
	 */
	public void removeCategory(int id) {
		categoryMapper.deleteCategory(id);
		
	}
	/**
	 * 根据id查询菜品分类
	 * @param id
	 * @return
	 */
	public DishCategory findCategoryById(int id) {
		return categoryMapper.findCategoryById(id);
	}
	/**
	 * 修改分类
	 * @param entity
	 */
	public boolean updateCategory(DishCategory entity) {
		try{
			categoryMapper.updateCategory(entity);
			return true;
		}catch(Exception e) {
			return false;
		}
		
		
	}
	/**
	 * 新增分类
	 * @param entity
	 */
	public boolean createCategory(DishCategory entity) {
		try{
			categoryMapper.createCategory(entity);
			return true;
		}catch(Exception e) {
			return false;
		}
		
		
	}
	/**
	 * 查询全部的分类
	 * @return
	 */
	public List<DishCategory> queryAllCategories() {
		return categoryMapper.queryAllCategories();
	}


}
