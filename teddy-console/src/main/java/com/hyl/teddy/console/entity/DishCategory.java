package com.hyl.teddy.console.entity;
/**
 *  菜品类别 dish_category
 * @author Administrator
 *
 */
public class DishCategory {
	private int id;
	private String name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
