package com.hyl.teddy.console.system.configuration.web;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hyl.teddy.console.system.configuration.Configuration;
import com.hyl.teddy.console.system.configuration.ConfigurationService;

@Controller
public class ConfigurationController {

	@Resource
	private ConfigurationService service;
	
	private final Logger logger = LoggerFactory.getLogger(ConfigurationController.class);
	@RequestMapping(value="/system/configuration/")
	public String home(Model model){
		Configuration entity = service.queryAll();
		model.addAttribute("entity", entity);
		return "system/configuration/form";
	}
	
	/**
	 * 更新全局配置
	 * @param withContent
	 * @return
	 */
	@RequestMapping(value = "/system/configuration/update", method = RequestMethod.POST)
	@ResponseBody
	public String updat(@RequestParam String param, @RequestParam String code){
		System.out.println("值：" + param + "   " + code);
		String msg;
		try{
			service.updateConfiguration(code, param);
			msg= "success";
		}catch(Exception ex){
			logger.debug("更新全局配置出错",ex);
			msg = "error";
		}
		return msg;
	}
	
	
	
}
