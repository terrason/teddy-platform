package com.hyl.teddy.console.advertisement;

import com.hyl.teddy.console.advertisement.mybatis.AdvertisementMapper;
import com.hyl.teddy.console.attachment.AttachmentService;
import com.hyl.teddy.console.entity.Advertisement;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;
import java.util.Collections;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service
public class AdvertisementService {

    @Resource
    private AdvertisementMapper advertisementMapper;
    @Resource
    private AttachmentService attachmentService;

    /**
     * 分页查询
     *
     * @param lookup
     * @return
     */
    public Pager<Advertisement> queryAdvertisement(Lookup lookup) {
        Pager<Advertisement> pager = new Pager<Advertisement>();
        int total = advertisementMapper.countAdvertisement(lookup);
        pager.setPage(lookup.getPage());
        pager.setSize(lookup.getSize());
        pager.setTotal(total);
        List<Advertisement> entiters = pager.isOverflowed() ? Collections.EMPTY_LIST : advertisementMapper.selectAdvertisementList(lookup);
        pager.setElements(entiters);
        return pager;
    }

    /**
     * 删除广告位
     *
     * @param id
     */
    @Transactional
    public void removeAdvertisement(int id) {
        Integer attachmentId = advertisementMapper.queryAttachment(id);
        advertisementMapper.deleteAdvertisement(id);
        if (attachmentId != null) {
            attachmentService.changeAttachmentTemporary(attachmentId, true);
        }
    }

    /**
     * 批量删除
     */
    @Transactional
    public void removeAdvertisements(int[] pks) {
        String attachmentIds = queryAttachments(pks);
        advertisementMapper.deleteAdvertisementByPks(pks);
        attachmentService.changeAttachmentTemporary(attachmentIds, true);

    }

    /**
     * 根据id查询广告位
     *
     * @param id
     * @return
     */
    public Advertisement findAdvertisementById(int id) {
        return advertisementMapper.findAdvertisementById(id);
    }

    /**
     * 修改广告位
     *
     * @param entity
     */
    public boolean updateAdvertisement(Advertisement entity) {
        try {
            advertisementMapper.updateAdvertisement(entity);

            return true;
        } catch (Exception e) {
            return false;
        }

    }

    /**
     * 新增广告位
     *
     * @param entity
     */
    public boolean createAdvertisement(Advertisement entity) {
        try {
            advertisementMapper.createAdvertisement(entity);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    private String queryAttachments(int[] pks) {
        List<Advertisement> attachments = advertisementMapper.queryAttachments(pks);
        StringBuilder ids = new StringBuilder();
        for (Advertisement attachment : attachments) {
            ids.append(",").append(attachment.getImage());
        }
        if (ids.length() > 0) {
            ids.deleteCharAt(0);
        }
        return ids.toString();
    }
}
