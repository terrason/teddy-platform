package com.hyl.teddy.console.favoraties.mybatis;


import com.hyl.teddy.console.MybatisMapper;
import org.apache.ibatis.annotations.Param;
@MybatisMapper
public interface FavoratiesMapper {

	
	/**删除我的收藏   type1-餐厅 2-菜品*/
	void removeFavoraties(@Param("id")int id,  @Param("type")int type);

	/**批量删除我的收藏   type1-餐厅 2-菜品*/
	void batchRemoveFavoraties(@Param("pks")int[] pks, @Param("type")int type);

}
