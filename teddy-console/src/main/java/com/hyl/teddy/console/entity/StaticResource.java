package com.hyl.teddy.console.entity;

/**
 * 静态资源表（全局配置）
 * @author Administrator
 *
 */
public class StaticResource {
	private String code;
	private String desc;
	private String content;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	
}
