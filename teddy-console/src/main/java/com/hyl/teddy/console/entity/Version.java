package com.hyl.teddy.console.entity;
/**
 * 版本更新表
 * 对应用户、配送员、取菜员
 * @author Administrator
 *
 */

public class Version {
	private int id;
	private String version;//版本
	private int code;//版本号数字. 高版本始终大于低版本
	private String log;//更新日志
	private Integer downloadUrl;//下载地址 附件表id
	private String downloadUrlString;//下载地址
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getLog() {
		return log;
	}
	public void setLog(String log) {
		this.log = log;
	}
	public Integer getDownloadUrl() {
		return downloadUrl;
	}
	public void setDownloadUrl(Integer downloadUrl) {
		this.downloadUrl = downloadUrl;
	}
	public String getDownloadUrlString() {
		return downloadUrlString;
	}
	public void setDownloadUrlString(String downloadUrlString) {
		this.downloadUrlString = downloadUrlString;
	}
	
}
