package com.hyl.teddy.console.advertisement;
import com.hyl.teddy.console.lookup.Lookup;

/**
 *
 * @author Administrator
 */
public class AdvertisementLookup extends Lookup {
    private Integer category;

   
    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }
}
