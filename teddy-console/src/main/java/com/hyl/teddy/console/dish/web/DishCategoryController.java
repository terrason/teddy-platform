package com.hyl.teddy.console.dish.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hyl.teddy.console.AbstractController;
import com.hyl.teddy.console.Remind;
import com.hyl.teddy.console.dish.DishCategoryService;
import com.hyl.teddy.console.entity.DishCategory;
import com.hyl.teddy.console.lookup.CommonLookup;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;

@Controller
@RequestMapping
public class DishCategoryController extends AbstractController {

	@Resource
	private DishCategoryService dishCategoryService;

	/**
	 * 进入菜品分类 主页面列表
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/dish/category/", method = RequestMethod.GET)
	public String home(Model model) {
		Pager<DishCategory> pager = dishCategoryService
				.queryCategoryList(getLookup());
		model.addAttribute("pager", pager);
		getLookup().setTotal(pager.getTotal());
		return "dish/category/list";
	}

	@Override
	protected Lookup instanceLookup() {
		// 封装查询条件
		return new CommonLookup();
	}

	/**
	 *  接受用户提交的查询信息
	 * @param lookup
	 * @return
	 */
	@RequestMapping(value = "/dish/category/", method = RequestMethod.POST)
	public String search(CommonLookup lookup) {
		setLookup(lookup);
		return "redirect:/dish/category/";
	}
	
	/**
	 * 删除菜品分类
	 * @param id
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping( value ="dish/category/{id}/remove", method = RequestMethod.POST)
	public String remove(@PathVariable int id, RedirectAttributes redirectAttributes) {
		if(id<= 0) {
			throw new IllegalArgumentException("id参数不合法");
		}
		dishCategoryService.removeCategory(id);
		redirectAttributes.addFlashAttribute(Remind.warning().appendMessage("成功删除"));
		return "redirect:/dish/category/";
	}
	/**
	 *   To add
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/dish/category/0", method = RequestMethod.GET)
	public String input(Model model) {
		return "dish/category/form"; 
	}
	/**
	 * To edit
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/dish/category/{id}", method = RequestMethod.GET)
	public String edit(@PathVariable int id, Model model) {
		if(id <=0 ){
			throw new IllegalArgumentException("id参数不合法");
		}
		DishCategory ca = dishCategoryService.findCategoryById(id);
		model.addAttribute("entity", ca);
		return 	"dish/category/form";
	}
	
	/**
	 *  保存菜品分类
	 * @param id
	 * @param entity
	 * @param redirectAttrs
	 * @return
	 */
	@RequestMapping( value = "/dish/category/{id}", method = RequestMethod.POST)
	public String save(@PathVariable int id, DishCategory entity, RedirectAttributes redirectAttrs) {
		entity.setId(id);
		if(id > 0) {
			if(dishCategoryService.updateCategory(entity)){
				redirectAttrs.addFlashAttribute(Remind.success().appendMessage("修改成功。"));
			}else {
				redirectAttrs.addFlashAttribute(Remind.warning().appendMessage("修改失败。"));
			}
			
		}else if (id == 0 ) {
			if(dishCategoryService.createCategory(entity)){
				redirectAttrs.addFlashAttribute(Remind.success().appendMessage("创建成功。"));
			}else {
				redirectAttrs.addFlashAttribute(Remind.warning().appendMessage("创建失败。"));
			}
			
		}else {
            throw new IllegalArgumentException("信息不合法");
		}
		return "redirect:/dish/category/";
	}

}
