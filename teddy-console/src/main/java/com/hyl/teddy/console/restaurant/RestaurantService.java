/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.console.restaurant;

import com.hyl.teddy.console.attachment.AttachmentService;
import com.hyl.teddy.console.entity.Payment;
import com.hyl.teddy.console.entity.Restaurant;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;
import com.hyl.teddy.console.restaurant.mybatis.RestaurantMapper;
import com.hyl.teddy.console.entity.RestaurantPromotion;
import com.hyl.teddy.console.favoraties.mybatis.FavoratiesMapper;
import com.hyl.teddy.console.util.CacheEvictor;

import java.util.Collections;
import java.util.List;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service
public class RestaurantService {

    private final Logger logger = LoggerFactory.getLogger(RestaurantService.class);
    @Resource
    private RestaurantMapper restaurantMapper;
    @Resource
    private AttachmentService attachmentService;
    @Resource
    private CacheEvictor cacheEvictor;
    @Resource
    private FavoratiesMapper favoratiesMapper;

    /**
     * 餐厅所有名称
     *
     * @return
     */
    public List<Restaurant> queryAllRestaurant() {
        return restaurantMapper.queryAllRestaurant();
    }

    /**
     * 分页查询
     *
     * @param lookup
     * @return
     */
    public Pager<Restaurant> queryRestaurant(Lookup lookup) {
        Pager<Restaurant> pager = new Pager<Restaurant>();
        int total = restaurantMapper.countRestaurant(lookup);
        pager.setPage(lookup.getPage());
        pager.setSize(lookup.getSize());
        pager.setTotal(total);
        List<Restaurant> entiters = pager.isOverflowed() ? Collections.EMPTY_LIST : restaurantMapper.selectRestaurantList(lookup);
        pager.setElements(entiters);
        return pager;
    }

    /**
     * 关闭餐厅
     */
    public void closeRestaurant(int id) {
        restaurantMapper.closeRestaurant(id);
    }

    /**
     * 打开餐厅
     */
    public void openRestaurant(int id) {
        restaurantMapper.openRestaurant(id);

    }

    /**
     * 删除餐厅
     *
     * @param id
     */
    @Transactional
    public void removeRestaurant(int id) {
        String attachmentIds = queryAttachments(id);
        restaurantMapper.deleteRestaurant(id);
        attachmentService.changeAttachmentTemporary(attachmentIds, true);
        cacheEvictor.evictCache("restaurant", id);
        favoratiesMapper.removeFavoraties(id, 1);
    }

    /**
     * 批量删除
     */
    @Transactional
    public void removeRestaurants(int[] pks) {
        String attachmentIds = queryAttachments(pks);
        restaurantMapper.deleteRestaurantByPks(pks);
        attachmentService.changeAttachmentTemporary(attachmentIds, true);
        cacheEvictor.evictCache("restaurant", null);
        favoratiesMapper.batchRemoveFavoraties(pks, 1);
    }

    /**
     * 根据id查询餐厅
     *
     * @param id
     * @return
     */
    public Restaurant findRestaurantById(int id) {
        return restaurantMapper.findRestaurantById(id);
    }

    /**
     * 修改餐厅
     *
     * @param entity
     */
    public boolean updateRestaurant(Restaurant entity) {
        try {
            restaurantMapper.updateRestaurant(entity);
            cacheEvictor.evictCache("restaurant", entity.getId());
            return true;
        } catch (Exception ex) {
            logger.error("修改餐厅出错", ex);
            return false;
        }

    }

    /**
     * 新增餐厅
     *
     * @param entity
     */
    public boolean createRestaurant(Restaurant entity) {
        try {
            restaurantMapper.createRestaurant(entity);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    /**
     * 删除餐厅该类别
     *
     * @param id
     */
    public void deleteCategory(int id) {
        restaurantMapper.deleteCategory(id);
        cacheEvictor.evictCache("restaurant", id);
    }

    /**
     * 添加餐厅该类别
     *
     * @param id
     * @param categoryId
     */
    public void createCategory(int id, int[] categoryPks) {
        for (int categoryId : categoryPks) {
            restaurantMapper.createCategory(id, categoryId);
        }
    }

    /**
     * 餐厅评论列表
     *
     * @param id
     * @return
     */
    public List<Payment> queryCommentList(int id) {
        return restaurantMapper.queryCommentList(id);
    }

    private String queryAttachments(int... id) {
        List<Restaurant> attachments = restaurantMapper.queryAttachments(id);
        StringBuilder pks = new StringBuilder();
        for (Restaurant attachment : attachments) {
            pks.append(",").append(attachment.getIcon());
            String images = attachment.getImages();
            if (StringUtils.isNotBlank(images)) {
                pks.append(",").append(images);
            }
        }
        if (pks.length() > 0) {
            pks.deleteCharAt(0);
        }
        return pks.toString();
    }

    /**
     * 查询某个餐厅的优惠标签
     *
     * @param id
     * @return
     */
    public List<RestaurantPromotion> findPromotions(int id) {

        return restaurantMapper.findPromotions(id);
    }

    /**
     * 删除餐厅的优惠标签
     *
     * @param promotionId
     */
    public void removePromotion(int promotionId, int restaurantId) {
        restaurantMapper.removePromotion(promotionId);
        cacheEvictor.evictCache("restaurant_promotions", restaurantId);
    }

    /**
     * 新增优惠标签
     *
     * @param rp
     */
    public void addPromotion(RestaurantPromotion rp, int restaurantId) {
        restaurantMapper.addPromotion(rp);
        cacheEvictor.evictCache("restaurant_promotions", restaurantId);

    }

    /**
     * 修改优惠标签
     *
     * @param rp
     */
    public void updatePromotion(RestaurantPromotion rp, int restaurantId) {
        restaurantMapper.updatePromotion(rp);
        cacheEvictor.evictCache("restaurant_promotions", restaurantId);

    }
}
