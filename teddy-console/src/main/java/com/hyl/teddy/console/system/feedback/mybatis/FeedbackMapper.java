package com.hyl.teddy.console.system.feedback.mybatis;

import com.hyl.teddy.console.MybatisMapper;
import com.hyl.teddy.console.entity.Feedback;
import java.util.List;
import org.apache.ibatis.annotations.Param;
@MybatisMapper
public interface FeedbackMapper {

    public int countFeedback(Object params);

    public List<Feedback> selectFeedbackList(Object params);

    public Feedback selectFeedbackById(@Param("id") int id);

    public void updateFeedbackStatus(@Param("read") boolean read,@Param("id") int id);

    public void deleteFeedback(@Param("id") int id);

    public void deleteFeedbackByPks(@Param("pks") int[] pks);
    
    public void reply(@Param("id") int id,@Param("content") String content);
    
    public String selectReplyById(@Param("id") int id);
}
