package com.hyl.teddy.console.payment;

import com.hyl.teddy.console.CommonRuntimeException;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;
import org.codehaus.jackson.map.ObjectMapper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.hyl.teddy.console.entity.Payment;
import com.hyl.teddy.console.entity.PaymentTimes;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;
import com.hyl.teddy.console.payment.mybatis.GlobalPaymentMapper;
import com.hyl.teddy.console.payment.web.GlobalPayment;
import com.hyl.teddy.console.util.ResponseToData;
import org.springframework.http.HttpStatus;

@Service
public class GlobalPaymentService {

    @Value("#{settings['url.apis']}")
    private String host;

    private final Logger logger = LoggerFactory.getLogger(GlobalPaymentService.class);

    @Resource
    private GlobalPaymentMapper mapper;

    public Pager<GlobalPayment> queryGlobalList(Lookup lookup) {
        Pager<GlobalPayment> pager = new Pager<>();
        int total = mapper.countGlobal(lookup);
        pager.setPage(lookup.getPage());
        pager.setSize(lookup.getSize());
        pager.setTotal(total);
        List<GlobalPayment> entities = pager.isOverflowed() ? Collections.EMPTY_LIST : mapper.selectGlobalList(lookup);
        pager.setElements(entities);
        return pager;
    }
    /**
     * 从数据库中查询订单基本信息. 
     */
    public Payment getLocalPayment(String orderno){
        return mapper.selectPayment(orderno);
    }
    public Payment getPayment(String orderno) {
        try {
            String url=host + "/payment/" + orderno;
            logger.debug("GET {}",url);
            Document document = Jsoup.connect(url).ignoreContentType(true).get();
            String text = document.body().text();
            String paymentText = ResponseToData.conversion(text);
            Payment payment = new ObjectMapper().readValue(paymentText, Payment.class);
            return payment;
        } catch (Exception ex) {
            logger.debug("调用订单详情接口出错", ex);
            throw new CommonRuntimeException(HttpStatus.BAD_GATEWAY, "接口调用出错", ex);
        }
    }

    /**
     * 根据订单id查询订单时间轴
     *
     * @param paymentId
     * @return
     */
    public List<PaymentTimes> selectTimeline(int paymentId) {
        // TODO Auto-generated method stub
        return mapper.selectTimeline(paymentId);
    }

}
