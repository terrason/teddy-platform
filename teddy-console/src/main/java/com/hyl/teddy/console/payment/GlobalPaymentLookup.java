package com.hyl.teddy.console.payment;

import com.hyl.teddy.console.lookup.Lookup;

public class GlobalPaymentLookup extends Lookup{
	private String orderno;
	private String mobile;
	private Integer editable;
	private Integer status;
	private String location;
	private Integer restaurantId;
	private String customerName;
        private Boolean showToday;
	
	
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Integer getRestaurantId() {
		return restaurantId;
	}
	public void setRestaurantId(Integer restaurantId) {
		this.restaurantId = restaurantId;
	}
	public String getOrderno() {
		return orderno;
	}
	public void setOrderno(String orderno) {
		this.orderno = orderno;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public Integer getEditable() {
		return editable;
	}
	public void setEditable(Integer editable) {
		this.editable = editable;
	}

    public Boolean getShowToday() {
        return showToday;
    }

    public void setShowToday(Boolean showToday) {
        this.showToday = showToday;
    }
	
	
}
