package com.hyl.teddy.console.payment.mybatis;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.hyl.teddy.console.MybatisMapper;
import com.hyl.teddy.console.entity.Employee;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.payment.web.PaymentUnallot;
@MybatisMapper
public interface PaymentUnallotMapper {

	public int countPaymentUnallot(Lookup lookup);
	
	public List<PaymentUnallot> selectPaymentUnallotList(Lookup lookup);

	public List<Employee> findCouriers(@Param("orderno")String orderno);
	
	public List<Employee> findDelivers();
	
	public void saveCourier(@Param("orderno")String orderno, @Param("id")int id);

	public void saveDeliver(@Param("orderno")String orderno, @Param("id")int id);

	public int queryCourierPaymentCount(Lookup lookup);

	public List<PaymentUnallot> queryCourierPaymentList(Lookup lookup);

	public List<PaymentUnallot> queryDeliverPaymentList(Lookup lookup);

	public int queryDeliverPaymentCount(Lookup lookup);
}
