package com.hyl.teddy.console.payment.web;

import java.io.IOException;

import javax.annotation.Resource;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.hyl.teddy.console.AbstractController;
import com.hyl.teddy.console.entity.Payment;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;
import com.hyl.teddy.console.payment.PaymentBackLookup;
import com.hyl.teddy.console.payment.PaymentBackService;
import com.hyl.teddy.console.payment.PaymentUnallotService;
import java.util.HashMap;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PaymentBackController extends AbstractController {

    @Resource
    private PaymentBackService paymentBackService;
    @Resource
    private PaymentUnallotService paymentUnallotService;

    /**
     * 订单列表
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/payment/back/", method = RequestMethod.GET)
    public String home(Model model) {
        Pager<PaymentBack> pager = paymentBackService.queryPaymentBackList(getLookup());
        model.addAttribute("pager", pager);
        getLookup().setTotal(pager.getTotal());

        return "payment/back/list";
    }

    @Override
    protected Lookup instanceLookup() {
        return new PaymentBackLookup();
    }

    @RequestMapping(value = "/payment/back/", method = RequestMethod.POST)
    public String search(PaymentBackLookup lookup) {
        setLookup(lookup);
        return "redirect:/payment/back/";
    }

    /**
     * 订单详情
     *
     * @param orderno
     * @param model
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    @RequestMapping(value = "/payment/back/{orderno}", method = RequestMethod.GET)
    public String detail(@PathVariable String orderno, Model model) throws JsonParseException, JsonMappingException, IOException {
        Payment entity = paymentBackService.getPayment(orderno);
        model.addAttribute("entity", entity);
        return "payment/back/detail";
    }

    /**
     * 退订单明细 调用接口
     *
     * @param orderno
     * @param id
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/payment/backdish/{id}", method = RequestMethod.GET)
    public String backdish(@PathVariable int id, @RequestParam String orderno, Model model) throws Exception {
        paymentUnallotService.callRemoteInterface(String.format("/inner/payment/%s/dish/%d", orderno, id));
        return "redirect:/payment/back/" + orderno;
    }

    /**
     * 确认换菜
     *
     * @param id
     * @param redirectAttrs
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/payment/back/{orderno}/change", method = RequestMethod.POST)
    public String change(@PathVariable String orderno, Model ui) {
        HashMap<String, String> data = api("/inner/create-refund/exchange")
                .data("orderno", orderno)
                .get(HashMap.class);
        ui.addAttribute("data", data);
        return "/alipay";
    }

    /**
     * 取消订单 调用接口
     *
     * @param orderno
     * @param redirectAttrs
     * @return
     */
    @RequestMapping(value = "/payment/back/{orderno}/cancel", method = RequestMethod.POST)
    public String cancel(@PathVariable String orderno, Model ui) {
        HashMap<String, String> data = api("/inner/create-refund/cancel")
                .data("orderno", orderno)
                .get(HashMap.class);
        ui.addAttribute("data", data);
        return "/alipay";
    }

    /**
     * 退菜 调用接口
     *
     * @param orderno
     * @param redirectAttrs
     * @return
     */
    @RequestMapping(value = "/payment/back/{orderno}/backorder", method = RequestMethod.POST)
    public String backorder(@PathVariable String orderno, Model ui) {
        HashMap<String, String> data = api("/inner/create-refund/giveup")
                .data("orderno", orderno)
                .get(HashMap.class);
        ui.addAttribute("data", data);
        return "/alipay";
    }

    @RequestMapping("/ajax/alipay-beats")
    @ResponseBody
    public boolean isAlipayOccupied(@RequestParam String orderno) {
        return api("/inner/alipay-beats")
                .data("orderno", orderno)
                .get(Boolean.class);
    }
}
