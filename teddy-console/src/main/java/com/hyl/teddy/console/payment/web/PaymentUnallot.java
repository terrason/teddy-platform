package com.hyl.teddy.console.payment.web;

public class PaymentUnallot {
	private int id;
	private String orderno;
	private String customerName;
	private String orderStatus;
	private String mobile;
	private int status;
	private boolean editable;
	private String deliverName;//取菜员
	private String courierName;//配送员
	private String address;//送餐地址
        private String regionName;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getOrderno() {
		return orderno;
	}
	public void setOrderno(String orderno) {
		this.orderno = orderno;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	//订单状态 0-未付款 1-支付失败 2-未指派 3-配餐中 4- 缺失菜品 5-配餐完成 6-配送中 8-已完成 9- 已取消
	public String getOrderStatus() {
		switch (status) {
		case 0:
			return "未付款";
		case 1:
			return "支付失败";
		case 2:
			return "未指派";
		case 3:
			return "配餐中";
		case 4:
			return "缺失菜品";
		case 5:
			return "配餐完成";
		case 6:
			return "配送中";
		case 8:
			return "已完成";
		case 9:
			return "已取消";	

		}
				
		return "";
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public boolean isEditable() {
		return editable;
	}
	public void setEditable(boolean editable) {
		this.editable = editable;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getDeliverName() {
		return deliverName;
	}
	public void setDeliverName(String deliverName) {
		this.deliverName = deliverName;
	}
	public String getCourierName() {
		return courierName;
	}
	public void setCourierName(String courierName) {
		this.courierName = courierName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }
	
}
