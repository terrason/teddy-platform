package com.hyl.teddy.console.advertisement.web;

import com.hyl.teddy.console.AbstractController;
import com.hyl.teddy.console.Remind;
import com.hyl.teddy.console.advertisement.AdvertisementLookup;
import com.hyl.teddy.console.advertisement.AdvertisementService;
import com.hyl.teddy.console.attachment.AttachmentService;
import com.hyl.teddy.console.entity.Advertisement;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *广告位管理
 * @author Administrator
 */
@Controller
public class AdvertisementController extends AbstractController{
    @Resource
    private AdvertisementService advertisementService;
    @Resource
    private AttachmentService attachmentService;
    
    private final Pattern resource = Pattern.compile("^(https?|ftp)://.*");
    /**
     * 广告位列表
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/advertisement/", method = RequestMethod.GET)
    public String home(Model model) {
        Pager<Advertisement> pager = advertisementService.queryAdvertisement(getLookup());
        model.addAttribute("pager", pager);
        getLookup().setTotal(pager.getTotal());
        return "advertisement/list";
    }

    @Override
    protected Lookup instanceLookup() {
        // 封装查询条件
        return new AdvertisementLookup();
    }

    /**
     * 接受用户提交的查询信息
     *
     * @param lookup
     * @return
     */
    @RequestMapping(value = "/advertisement/", method = RequestMethod.POST)
    public String search(AdvertisementLookup lookup) {
        setLookup(lookup);
        return "redirect:/advertisement/";
    }
    /**
     * 删除广告位
     *
     * @param id
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = "/advertisement/{id}/remove", method = RequestMethod.POST)
    public String remove(@PathVariable int id, RedirectAttributes redirectAttributes) {
        if (id <= 0) {
            throw new IllegalArgumentException("id参数不合法");
        }
        advertisementService.removeAdvertisement(id);
        redirectAttributes.addFlashAttribute(Remind.warning().appendMessage("成功删除"));
        return "redirect:/advertisement/";
    }

    /**
     * 批量删除广告位
     *
     * @param id
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = "/advertisement/remove", method = RequestMethod.POST)
    public String removes(@RequestParam int[] pks, RedirectAttributes redirectAttributes) {
        if (pks.length <= 0) {
            throw new IllegalArgumentException("id参数不合法");
        }
        advertisementService.removeAdvertisements(pks);
        redirectAttributes.addFlashAttribute(Remind.warning().appendMessage("批量删除成功"));
        return "redirect:/advertisement/";
    }

    /**
     * To add
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/advertisement/0", method = RequestMethod.GET)
    public String input(Model model) {
        return "advertisement/form";
    }

    /**
     * To edit
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/advertisement/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable int id, Model model) {
        if (id <= 0) {
            throw new IllegalArgumentException("id参数不合法");
        }
        model.addAttribute("entity", advertisementService.findAdvertisementById(id));
        return "advertisement/form";
    }

    /**
     * 保存广告位
     *
     * @param id
     * @param entity
     * @param redirectAttrs
     * @return
     */
    @RequestMapping(value = "/advertisement/{id}", method = RequestMethod.POST)
    @Transactional
    public String save(@PathVariable int id, Advertisement entity, RedirectAttributes redirectAttrs) {
        entity.setId(id);
        if(!resource.matcher(entity.getUrl()).matches()){
            entity.setUrl("http://"+entity.getUrl());
        }
        if (id > 0) {
            Advertisement old = advertisementService.findAdvertisementById(id);
            advertisementService.updateAdvertisement(entity);
            attachmentService.changeAttachmentTemporary(old.getImage(), true);
            attachmentService.changeAttachmentTemporary(entity.getImage(), false);
            redirectAttrs.addFlashAttribute(Remind.success().appendMessage("修改成功。"));

        } else if (id == 0) {
            advertisementService.createAdvertisement(entity);
            attachmentService.changeAttachmentTemporary(entity.getImage(), false);
            redirectAttrs.addFlashAttribute(Remind.success().appendMessage("创建成功。"));

        } else {
            throw new IllegalArgumentException("信息不合法");
        }
        return "redirect:/advertisement/";
    }


}
