package com.hyl.teddy.console.dish.mybaties;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.hyl.teddy.console.MybatisMapper;
import com.hyl.teddy.console.entity.DishCategory;

@MybatisMapper
public interface DishCategoryMapper {
	
	public int countCategory(Object params);
	
	public List<DishCategory> selectCategoryList(Object params);

	public void deleteCategory(@Param("id")int id);

	public DishCategory findCategoryById(@Param("id")int id);

	public void updateCategory(DishCategory entity);

	public void createCategory(DishCategory entity);

	public List<DishCategory> queryAllCategories();

	public void removeCategories(@Param("pks")int[] pks);
}
