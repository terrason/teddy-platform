package com.hyl.teddy.console.dish;

import com.hyl.teddy.console.lookup.Lookup;

public class DishLookup extends Lookup {

    private String name;
    private Integer restaurantId;
    private Integer categoryId;
    private Integer hot;
    private Integer enabled;

    public void setDisabled(Boolean disabled) {
        if(disabled!=null && disabled){
            enabled=0;
        }else{
            enabled=null;
        }
    }

    public Boolean getDisabled() {
        return enabled == null ? false : enabled == 0;
    }

    public Integer getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Integer restaurantId) {
        this.restaurantId = restaurantId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getHot() {
        return hot;
    }

    public void setHot(Integer hot) {
        this.hot = hot;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
