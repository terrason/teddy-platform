package com.hyl.teddy.console.entity;

import java.util.Date;

/**
 * 反馈意见 feedback
 * @author Administrator
 *
 */
public class Feedback {
	private int id;
	private String content;//反馈意见内容
	private Date createTime;//反馈时间
	private boolean read;//是否已读 1-已读 0-新提交未阅读',
	private int customerId;//提交用户id.
        private String nickname;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public boolean getRead() {
		return read;
	}
	public void setRead(boolean read) {
		this.read = read;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }
	
	
}
