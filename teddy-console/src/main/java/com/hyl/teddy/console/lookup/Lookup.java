package com.hyl.teddy.console.lookup;

/**
 * 查询条件. 用来封装查询条件（包括分页条件.）
 *
 * @author Administrator
 */
public class Lookup {

    /**
     * 分页页码.
     */
    private int page = 1;
    /**
     * 分页大小.
     */
    private int size = 12;
    /**
     * 总数据量.
     */
    private int total;
    /**
     * 小区ID
     */
    private Integer communityId;
    /**
     * 生活帮的类型
     * add by tfj
     */
    private int bbsType;
    /**
     * 当前数据索引.
     * @return 数据索引
     */
    public int getIndex() {
        return (getPage() - 1) * getSize();
    }

    /**
     * 总数据量.
     *
     * @return the total
     */
    public int getTotal() {
        return total;
    }

    /**
     * 总数据量.
     *
     * @param total the total to set
     */
    public void setTotal(int total) {
        this.total = total;
    }

    /**
     * 分页页码.
     * @return the page
     */
    public int getPage() {
        return page;
    }

    /**
     * 分页页码.
     * @param page the page to set
     */
    public void setPage(int page) {
        this.page = page;
    }

    /**
     * 分页大小.
     * @return the size
     */
    public int getSize() {
        return size;
    }

    /**
     * 分页大小.
     * @param size the size to set
     */
    public void setSize(int size) {
        this.size = size;
    }

    public Integer getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Integer communityId) {
        this.communityId = communityId;
    }

	public int getBbsType() {
		return bbsType;
	}

	public void setBbsType(int bbsType) {
		this.bbsType = bbsType;
	}

}
