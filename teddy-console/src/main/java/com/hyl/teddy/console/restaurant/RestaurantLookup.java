package com.hyl.teddy.console.restaurant;

import com.hyl.teddy.console.lookup.Lookup;

/**
 *
 * @author Administrator
 */
public class RestaurantLookup extends Lookup {

    private String name;
    private Integer categoryId;
    private Integer enabled;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }
}
