package com.hyl.teddy.console.system.feedback;

import com.hyl.teddy.console.lookup.Lookup;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

public class FeedbackLookup extends Lookup {

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date timeFrom;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date timeTo;
    private Boolean unread;
    private String customer;

    /**
     * @return the timeFrom
     */
    public Date getTimeFrom() {
        return timeFrom;
    }

    /**
     * @param timeFrom the timeFrom to set
     */
    public void setTimeFrom(Date timeFrom) {
        this.timeFrom = timeFrom;
    }

    /**
     * @return the timeTo
     */
    public Date getTimeTo() {
        return timeTo;
    }

    /**
     * @param timeTo the timeTo to set
     */
    public void setTimeTo(Date timeTo) {
        this.timeTo = timeTo;
    }

    /**
     * @return the unread
     */
    public Boolean getUnread() {
        return unread;
    }

    /**
     * @param unread the unread to set
     */
    public void setUnread(Boolean unread) {
        this.unread = unread;
    }

    /**
     * @return the customer
     */
    public String getCustomer() {
        return customer;
    }

    /**
     * @param customer the customer to set
     */
    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public boolean isRead() {
        return unread != null && !unread;
    }
}
