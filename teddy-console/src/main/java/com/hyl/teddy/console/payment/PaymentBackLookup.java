package com.hyl.teddy.console.payment;

import com.hyl.teddy.console.lookup.Lookup;

public class PaymentBackLookup extends Lookup {

    private String orderno;
    private String mobile;
    private Boolean editable;

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Boolean getEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }

}
