/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.console.jpush;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Message;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.Notification;

/**
 * 泰迪熊后台买家推送SERVICE
 * 
 * @author terrason
 */
@Service
public class CustomerJpushService {

	@Value("#{settings['jpush.buyer_appkey']}")
	private String appkey;
	@Value("#{settings['jpush.buyer_mastersecret']}")
	private String secret;
	@Value("#{settings['jpush.apns_production']}")
	private boolean production;
	@Value("#{settings['jpush.time_to_live']}")
	private int liveTime;

	/** 推送功能标识 0-活动 */
	public static final Integer MODULE_ACITIVITY = 0;
	/** 推送功能标识 1-取菜员取菜 */
	public static final Integer MODULE_DELIVER_TAKE = 1;
	/** 推送功能标识 2-配送员配送 */
	public static final Integer MODULE_COURIER_TAKE = 2;
	/** 推送功能标识 3-通知用户缺菜 */
	public static final Integer MODULE_MISSING_DISH = 3;
	/** 推送功能标识 4-通知用户完成订单 */
	public static final Integer MODULE_ORDER_FINISH = 1;
	
	private Logger logger = LoggerFactory.getLogger(CustomerJpushService.class);
	public PushClient create(String title) {
		return new PushClient(title);
	}

	public class PushClient {
		private final String title;
		private final Map<String, Object> extras = new LinkedHashMap<String, Object>();
		private final Collection<String> to= new ArrayList<String>();

		public PushClient(String title) {
			this.title = title;
		}

		public PushClient data(String key, Object value) {
			extras.put(key, value);
			return this;
		}
		public PushClient to(String registerId){
			to.add(registerId);
			return this;
		}
		public boolean push() {
			JPushClient jpush = new JPushClient(secret, appkey, liveTime);
			Message.Builder message = Message.newBuilder().setMsgContent("extraMsg");
			for (Map.Entry<String, Object> entrySet : extras.entrySet()) {
				String key = entrySet.getKey();
				Object value = entrySet.getValue();
				if (value == null) {
				} else if (value instanceof Boolean) {
					message.addExtra(key, (Boolean) value);
				} else if (value instanceof Number) {
					message.addExtra(key, (Number) value);
				} else if (value instanceof String) { 
					message.addExtra(key, (String) value);
				} else {
					message.addExtra(key, value.toString());
				}
			}
			Audience audience;
			if(to.isEmpty()){
				audience=Audience.all();
			}else{
				audience=Audience.registrationId(to);
			}
			PushPayload pushPayload = PushPayload
					.newBuilder()
					.setPlatform(Platform.all())
					.setAudience(audience)
					.setNotification(
							Notification.newBuilder().setAlert(title).build())
					.setMessage(message.build())
					.setOptions(
							Options.newBuilder().setApnsProduction(production)
									.setSendno(Integer.parseInt((new Date().getTime()/1000) + "")).build()).build();
			logger.debug("jpush:{}", pushPayload);
			boolean flag = false;
			try{
				PushResult result=jpush.sendPush(pushPayload);
				if(!result.isResultOK()){
					logger.warn("推送失败");
				}else{
					flag = true;
				}
			}catch(Exception ex){
				logger.warn("推送失败",ex);
			}
			return flag;
		}
	}
}
