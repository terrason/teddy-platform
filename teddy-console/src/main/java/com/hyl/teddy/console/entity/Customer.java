package com.hyl.teddy.console.entity;

import java.util.Date;

/**
 *  个人中心  用户信息表 customer
 * @author Administrator
 *
 */
public class Customer {
	private int id;
	private String mobile;//手机号码
	private String password;//密码
	private String nickname;//昵称
	private int scoreHighest;//积分总计
	private int score;//积分
	private String avatar;//头像
	private Date regTime;//注册时间
	private String pushKey;//jpush机器号
	private boolean disabled;//是否禁用 0 否 1 是
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public int getScoreHighest() {
		return scoreHighest;
	}
	public void setScoreHighest(int scoreHighest) {
		this.scoreHighest = scoreHighest;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public Date getRegTime() {
		return regTime;
	}
	public void setRegTime(Date regTime) {
		this.regTime = regTime;
	}
	public String getPushKey() {
		return pushKey;
	}
	public void setPushKey(String pushKey) {
		this.pushKey = pushKey;
	}
	public boolean isDisabled() {
		return disabled;
	}
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
	
}
