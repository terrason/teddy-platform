package com.hyl.teddy.console.system.feedback;

import com.hyl.teddy.console.entity.Feedback;
import com.hyl.teddy.console.pager.Pager;
import com.hyl.teddy.console.system.feedback.mybatis.FeedbackMapper;
import com.hyl.teddy.console.lookup.Lookup;
import java.util.Collections;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * 处理客户反馈信息相关服务.
 *
 * @author 
 */
@Service
public class FeedbackService {

    @Resource
    private FeedbackMapper feedbackMapper;

    /**
     * 分页查询客户反馈信息列表.
     *
     * @param lookup 客户反馈信息查询参数.
     * @return 客户反馈信息列表
     */
    public Pager<Feedback> queryFeedbackList(Lookup lookup) {
        int total = feedbackMapper.countFeedback(lookup);
        Pager<Feedback> pager = new Pager<>();
        pager.setPage(lookup.getPage());
        pager.setSize(lookup.getSize());
        pager.setTotal(total);

        List<Feedback> entities = pager.isOverflowed() ? Collections.EMPTY_LIST : feedbackMapper.selectFeedbackList(lookup);
        pager.setElements(entities);
        return pager;
    }

    /**
     * 根据ID查询客户反馈信息实体.
     *
     * @param id 客户反馈信息ID
     * @return 客户反馈信息实体
     */
    public Feedback findFeedbackById(int id) {
        return feedbackMapper.selectFeedbackById(id);
    }

    /**
     * 保存客户反馈信息信息的修改.
     *
     * @param entity 客户反馈信息信息
     */
    public void saveFeedbackStatus(boolean read, int id) {
        feedbackMapper.updateFeedbackStatus(read, id);
    }

    /**
     * 根据客户反馈信息ID删除客户反馈信息.
     *
     * @param id 客户反馈信息ID
     */
    public void removeFeedback(int id) {
        feedbackMapper.deleteFeedback(id);
    }

    /**
     * 批量删除客户反馈信息.
     *
     * @param pks 需要批量删除的客户反馈信息ID数组.
     */
    public void removeFeedbackByPks(int[] pks) {
        feedbackMapper.deleteFeedbackByPks(pks);
    }
    /**
     * 回复
     * @param id
     * @param content
     * @return 
     */
    public boolean reply(int id,String content){
        try {
            feedbackMapper.reply(id, content);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    public String selectReplyById(int id){
        return feedbackMapper.selectReplyById(id);
    }
}
