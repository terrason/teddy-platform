package com.hyl.teddy.console.activity;

import com.hyl.teddy.console.lookup.Lookup;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

public class ActivityLookup extends Lookup {
    
    private String name;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date timeFrom;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date timeTo;
    private Boolean unpush;

    /**
     * @return the timeFrom
     */
    public Date getTimeFrom() {
        return timeFrom;
    }

    /**
     * @param timeFrom the timeFrom to set
     */
    public void setTimeFrom(Date timeFrom) {
        this.timeFrom = timeFrom;
    }

    /**
     * @return the timeTo
     */
    public Date getTimeTo() {
        return timeTo;
    }

    /**
     * @param timeTo the timeTo to set
     */
    public void setTimeTo(Date timeTo) {
        this.timeTo = timeTo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getUnpush() {
        return unpush;
    }

    public void setUnpush(Boolean unpush) {
        this.unpush = unpush;
    }
}
