package com.hyl.teddy.console.activity.mybatis;

import com.hyl.teddy.console.MybatisMapper;
import com.hyl.teddy.console.entity.Activity;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 *
 * @author Administrator
 */
@MybatisMapper
public interface ActivityMapper {
    
    public int countActivity(Object params);

    public List<Activity> selectActivityList(Object params);

    public void deleteActivity(@Param("id") int id);

    public void deleteActivityByPks(@Param("pks") int[] pks);

    public Activity findActivityById(@Param("id") int id);
    
    public void updateActivity(Activity entity);

    public void createActivity(Activity entity);
}
