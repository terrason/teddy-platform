package com.hyl.teddy.console.payment.mybatis;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.hyl.teddy.console.MybatisMapper;
import com.hyl.teddy.console.entity.Payment;
import com.hyl.teddy.console.entity.PaymentTimes;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.payment.web.GlobalPayment;

@MybatisMapper
public interface GlobalPaymentMapper {

    public int countGlobal(Lookup lookup);

    public List<GlobalPayment> selectGlobalList(Lookup lookup);

    public List<PaymentTimes> selectTimeline(@Param("paymentId") int payment);

    public Payment selectPayment(@Param("orderno") String orderno);

}
