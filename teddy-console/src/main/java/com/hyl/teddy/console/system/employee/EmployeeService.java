package com.hyl.teddy.console.system.employee;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hyl.teddy.console.entity.Employee;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;
import com.hyl.teddy.console.system.employee.mybatis.EmployeeMapper;
import com.hyl.teddy.console.util.CacheEvictor;

@Service
@Transactional
public class EmployeeService {

    @Resource
    private EmployeeMapper mapper;
    @Resource
    private CacheEvictor cacheEvictor;

    public Pager<Employee> queryList(Lookup lookup) {
        Pager<Employee> pager = new Pager<Employee>();
        pager.setPage(lookup.getPage());
        pager.setSize(lookup.getSize());
        int total = mapper.countList(lookup);
        pager.setTotal(total);
        List<Employee> list = pager.isOverflowed() ? Collections.EMPTY_LIST : mapper.selectlist(lookup);
        pager.setElements(list);
        return pager;
    }

    /**
     * 根据id查询用户信息
     *
     * @param id
     * @return
     */
    public Employee findEmployeeById(int id) {
        return mapper.findEmployeeById(id);
    }

    /**
     * 查询分配员
     *
     * @return
     */
    public List<Map<String, Object>> findCenter() {
        return mapper.findCenter();
    }

    /**
     * 查询所有区域
     *
     * @return
     */
    public List<Map<String, Object>> allRegion() {
        return mapper.allRegion();
    }

    /**
     * 修改员工信息
     *
     * @param entity
     */
    public void updateEmployee(Employee entity) {
        mapper.updateEmployee(entity);
        cacheEvictor.evictCache("employees", null);
    }

    @Transactional
    public void addEmployee(Employee entity) {
        int type = entity.getType();
        String username = "";
        switch (type) {
            case 1:
                username = "QC" + mapper.maxUsername(type);
                break;
            case 2:
                username = "PS" + mapper.maxUsername(type);
                break;
            case 3:
                username = "FP" + "0001";
                break;
        }
        entity.setUsername(username);
        entity.setPassword("123456");
        mapper.addEmployee(entity);
        cacheEvictor.evictCache("employees", null);
    }

    /**
     * 删除员工
     *
     * @param id
     * @param type
     */
    public void removeEmployee(int id, int type) {
        mapper.removeEmployee(id);
        if (type == 3) {
            mapper.removeCenter(id);
        }
        cacheEvictor.evictCache("employees", null);
    }

    public List<Employee> allDeliver() {
        List<Employee> list = mapper.allDeliver();
        for (Employee e : list) {
            e.setLongitudeString(String.format("%.6f", e.getLongitude() * Math.pow(10, -6)));
            e.setLatitudeString(String.format("%.6f", e.getLatitude() * Math.pow(10, -6)));
        }
        return list;
    }
}
