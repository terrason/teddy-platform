package com.hyl.teddy.console.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;
import com.hyl.teddy.console.Principal;
import com.hyl.teddy.console.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;

public class AuthenticationInterceptor extends HandlerInterceptorAdapter {

    public static final String USER_SESSION_KEY = "principal";

    private final Logger logger = LoggerFactory.getLogger(AuthenticationInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        logger.debug("apply authc interceptor");
        
        Principal principal = (Principal) WebUtils.getSessionAttribute(request, USER_SESSION_KEY);
        if (principal != null) {
        	//判断当前用户资源访问权限
        	if(!Utils.getRequestUrl(request).startsWith("/estate/")&&!Utils.getRequestUrl(request).equals("/")
        			&&principal.getType()==Principal.WUYE){
        		throw new IllegalArgumentException("您没有访问该资源的权限！");
        	}
        	
            if (request.getMethod().equals(HttpMethod.GET.toString())) {
                principal.setLocation(Utils.getRequestUrl(request));
            }
            return true;
        } else {
            request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
            return false;
        }
    }

}
