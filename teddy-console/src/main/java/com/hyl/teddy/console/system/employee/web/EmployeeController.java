package com.hyl.teddy.console.system.employee.web;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hyl.teddy.console.AbstractController;
import com.hyl.teddy.console.Remind;
import com.hyl.teddy.console.entity.Employee;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;
import com.hyl.teddy.console.system.employee.EmployeeLookup;
import com.hyl.teddy.console.system.employee.EmployeeService;
/**
 * 员工管理
 * @author Administrator
 *
 */
@Controller
public class EmployeeController extends AbstractController{

	@Resource
	private EmployeeService service;
	/**
	 * 员工列表
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/system/employee/", method = RequestMethod.GET)
	public String home(Model model){
		Pager<Employee> pager = service.queryList(getLookup()); 
		model.addAttribute("pager", pager);
		return "system/employee/list";
	}
	
	
	
	@Override
	protected Lookup instanceLookup() {
		return new EmployeeLookup();
	}

	/**
	 * 查询
	 * @param lookup
	 * @return
	 */
	@RequestMapping(value="/system/employee/", method = RequestMethod.POST)
	public String search(EmployeeLookup lookup){
		setLookup(lookup);
		return "redirect:/system/employee/";
	}

	/**
	 * To  Add
	 * @return
	 */
	@RequestMapping(value = "/system/employee/0", method = RequestMethod.GET)
	public String input(Model model){
		List<Map<String,Object>> center = service.findCenter();
		model.addAttribute("centers", center);//分配员
		List<Map<String,Object>> regions = service.allRegion();
		model.addAttribute("regions", regions);//区域
		model.addAttribute("flag","add");
		return "system/employee/form";
	}
	/**
	 * TO edit
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/system/employee/{id}", method = RequestMethod.GET)
	public String edit(@PathVariable int id, Model model){
		if (id <= 0) {
            throw new IllegalArgumentException("id 参数不合法");
        }
		Employee entity = service.findEmployeeById(id);
		model.addAttribute("entity", entity);
		List<Map<String,Object>> center = service.findCenter();
		model.addAttribute("centers", center);//分配员
		List<Map<String,Object>> regions = service.allRegion();
		model.addAttribute("regions", regions);//区域
		model.addAttribute("flag","edit");
		return "system/employee/form";
	}
	/**
	 *  Do Save
	 * @param id
	 * @param entity
	 * @param redirectAttrs
	 * @return
	 */
	@RequestMapping(value = "/system/employee/{id}", method = RequestMethod.POST)
	public String save(@PathVariable int id, Employee entity, RedirectAttributes redirectAttrs) {
        entity.setId(id);
        if (id > 0) {
            service.updateEmployee(entity);
            redirectAttrs.addFlashAttribute(Remind.success().appendMessage("员工信息已修改。"));
        } else if (id == 0) {
            service.addEmployee(entity);
            redirectAttrs.addFlashAttribute(Remind.success().appendMessage("员工信息已创建。"));
        } else {
            throw new IllegalArgumentException("员工信息不合法");
        }
        return "redirect:/system/employee/";
    }
	@RequestMapping(value = "/system/employee/{id}/remove", method = RequestMethod.GET)
	public String remove(@PathVariable int id,@RequestParam int type){
		service.removeEmployee(id, type);
		return "redirect:/system/employee/";
	}
	/**
	 * 地图定位 分配员
	 * @return
	 */
	@RequestMapping(value = "/system/employee/site", method = RequestMethod.GET)
	public String location(Model model){
		List<Employee> list = service.allDeliver();
		System.out.println(list.size());
		model.addAttribute("delivers", list);
		return "system/employee/site";
	}
	
	@RequestMapping(value = "/system/employee/ajax", method = RequestMethod.POST)
	@ResponseBody
	public  List<Employee> location(){
		List<Employee> list = service.allDeliver();
		System.out.println(list.size());
//	model.addAttribute("delivers", list);
		return list;
	}
	
}
