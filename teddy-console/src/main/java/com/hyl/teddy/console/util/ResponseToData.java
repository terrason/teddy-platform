package com.hyl.teddy.console.util;

/**
 * HTTP请求接口返回的BaseResponse 截取当中的DATA部分的JSON字符串
 * @author Administrator
 *
 */
public class ResponseToData {
	public static String conversion(String text){
		try{
			String res = text.split("\"data\"")[1];
			return res.substring(1,res.length()-1);
		}catch(Exception e){
			return "";
		}
	}
}
