package com.hyl.teddy.console.pager;

import java.util.List;

/**
 *
 * @author Administrator
 */
public interface SqlWhereBuilder {
    /**
     * 创建sql语句where部分.
     * @param where 查询条件构造器. 已包含{@code from}语句。
     * @param params 参数列表. 当前参数列表为空列表。
     */
    public void build(StringBuilder where,List<Object> params);
}
