package com.hyl.teddy.console.system;

import com.hyl.teddy.console.AbstractController;
import com.hyl.teddy.console.Principal;
import com.hyl.teddy.console.system.authority.AuthService;
import com.hyl.teddy.console.system.authority.Menu;
import com.hyl.teddy.console.system.user.UserInfo;
import com.hyl.teddy.console.system.user.UserInfoService;
import com.hyl.teddy.console.util.Utils;
import java.util.List;
import javax.annotation.Resource;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping
public class HomeController extends AbstractController {

    @Resource
    private UserInfoService userInfoService;
    @Resource
    private AuthService authService;

    /**
     * 跳转到首页.
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home() {
        return "index";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@RequestParam String username, @RequestParam String password) {
        UserInfo userInfo = userInfoService.findUserByUsername(username);
        if (userInfo == null) {
            throw new RuntimeException("用户名或密码不正确");
        }
        if (!userInfo.getPassword().equals(DigestUtils.md5Hex(password))) {
            throw new RuntimeException("用户名或密码不正确");
        }
        if (userInfo.getStatus() != 1) {
            throw new RuntimeException("帐号被禁用，请联系系统管理员。");
        }

        List<Menu> authority =null;
        if(userInfo.getType()==Principal.SUPER){
        	authority = authService.buildSuperAuthority();
        }else if(userInfo.getType()==Principal.WUYE){
        	authority = authService.buildWuyeAuthority();
        }else{
        	authority =authService.buildPrincipalAuthority(userInfo.getId());
        }

        Principal principal = new Principal(userInfo);
        principal.setMenus(authority);

        setSessionAttribute(SESSIONKEY_PRINCIPAL, principal);
        return "redirect:" + Utils.retrieveSavedRequest();
    }
    
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public String logout(){
    	setSessionAttribute(SESSIONKEY_PRINCIPAL, null);
    	return "redirect:" + Utils.retrieveSavedRequest();
    }
}
