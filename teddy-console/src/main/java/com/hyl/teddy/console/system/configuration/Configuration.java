package com.hyl.teddy.console.system.configuration;

/**
 * 全局配置
 * @author Administrator
 *
 */
public class Configuration {
	private String withContent;//显示文字评论
	private String autoCourier;//是否自动分配配送员
	private String autoDeliver;//是否自动分配取菜员
	private String costScore;//积分抵现规则
	private String scorePerMoney;//送积分规则（多少元送一积分）
	private String experience;//VIP的经验阈值
	private String aboutus;//关于我们
	private String license;//许可协议
	private String support;//技术支持
	private String tel;//熊仔电话
	private String sharePayment;//分享订单可获代金券
	private String shareReceive;//领取分享可获代金券
	
	
	public String getSharePayment() {
		return sharePayment;
	}
	
	public void setSharePayment(String sharePayment) {
		this.sharePayment = sharePayment;
	}
	public String getShareReceive() {
		return shareReceive;
	}
	public void setShareReceive(String shareReceive) {
		this.shareReceive = shareReceive;
	}
	public String getWithContent() {
		return withContent;
	}
	public void setWithContent(String withContent) {
		this.withContent = withContent;
	}
	public String getAutoCourier() {
		return autoCourier;
	}
	public void setAutoCourier(String autoCourier) {
		this.autoCourier = autoCourier;
	}
	public String getAutoDeliver() {
		return autoDeliver;
	}
	public void setAutoDeliver(String autoDeliver) {
		this.autoDeliver = autoDeliver;
	}
	public String getCostScore() {
		return costScore;
	}
	public void setCostScore(String costScore) {
		this.costScore = costScore;
	}
	public String getScorePerMoney() {
		return scorePerMoney;
	}
	public void setScorePerMoney(String scorePerMoney) {
		this.scorePerMoney = scorePerMoney;
	}
	public String getExperience() {
		return experience;
	}
	public void setExperience(String experience) {
		this.experience = experience;
	}
	public String getAboutus() {
		return aboutus;
	}
	public void setAboutus(String aboutus) {
		this.aboutus = aboutus;
	}
	public String getLicense() {
		return license;
	}
	public void setLicense(String license) {
		this.license = license;
	}
	public String getSupport() {
		return support;
	}
	public void setSupport(String support) {
		this.support = support;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	
	
}
