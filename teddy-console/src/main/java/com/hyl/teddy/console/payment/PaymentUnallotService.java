package com.hyl.teddy.console.payment;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.codehaus.jackson.map.ObjectMapper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.hyl.teddy.console.BaseResponse;
import com.hyl.teddy.console.CommonRuntimeException;
import com.hyl.teddy.console.entity.Employee;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;
import com.hyl.teddy.console.payment.mybatis.PaymentUnallotMapper;
import com.hyl.teddy.console.payment.web.PaymentUnallot;
import org.springframework.http.HttpStatus;

@Service
public class PaymentUnallotService {

    @Value("#{settings['url.apis']}")
    private String host;

    private final Logger logger = LoggerFactory.getLogger(PaymentUnallotService.class);
    @Resource
    private PaymentUnallotMapper unallotMapper;

    /**
     * 分配取菜员的订单列表
     *
     * @param lookup
     * @return
     */
    public Pager<PaymentUnallot> queryCourierPaymentList(Lookup lookup) {
        int total = unallotMapper.queryCourierPaymentCount(lookup);
        Pager<PaymentUnallot> pager = new Pager<>();
        pager.setPage(lookup.getPage());
        pager.setSize(lookup.getSize());
        pager.setTotal(total);
        List<PaymentUnallot> entities = pager.isOverflowed() ? Collections.EMPTY_LIST : unallotMapper.queryCourierPaymentList(lookup);
        pager.setElements(entities);
        return pager;
    }

    /**
     * 分配配送员的订单列表
     *
     * @param lookup
     * @return
     */
    public Pager<PaymentUnallot> queryDeliverPaymentList(Lookup lookup) {
        int total = unallotMapper.queryDeliverPaymentCount(lookup);
        Pager<PaymentUnallot> pager = new Pager<>();
        pager.setPage(lookup.getPage());
        pager.setSize(lookup.getSize());
        pager.setTotal(total);
        List<PaymentUnallot> entities = pager.isOverflowed() ? Collections.EMPTY_LIST : unallotMapper.queryDeliverPaymentList(lookup);
        pager.setElements(entities);
        return pager;
    }

    /**
     * 订单分配配送员
     *
     * @param orderno
     * @return
     */
    public List<Employee> findCouriers(String orderno) {
        return unallotMapper.findCouriers(orderno);
    }

    /**
     * 订单分配取菜员
     *
     * @param orderno
     * @return
     */
    public List<Employee> findDelivers() {
        return unallotMapper.findDelivers();
    }

    /**
     * 保存配送员
     *
     * @param orderno
     * @param employeeId
     */
    public boolean saveCourier(String orderno, int employeeId) {
        String url = String.format("/inner/payment/%s/courier/%d", orderno, employeeId);
        return callRemoteInterface(url);

    }

    /**
     * 保存取菜员
     *
     * @param orderno
     * @param employeeId
     */
    public boolean saveDeliver(String orderno, int employeeId) {
        String url = String.format("/inner/payment/%s/deliver/%d", orderno, employeeId);
        return callRemoteInterface(url);

    }

    /**
     * 调用远程接口
     *
     * @param url 路径 + 参数
     * @return 返 boolean
     */
    public boolean callRemoteInterface(String url) {
        boolean flag = false;

        try {
            url = host + url;
            logger.debug("GET {}", url);
            Document document = Jsoup.connect(url).timeout(30000).ignoreContentType(true).get();
            String text = document.body().text();
            BaseResponse br = new ObjectMapper().readValue(text, BaseResponse.class);
            if (br != null && br.getCode() == 0) {
                flag = true;
            }
        } catch (IOException ex) {
            logger.debug("调用接口出错", ex);
            throw new CommonRuntimeException(HttpStatus.BAD_GATEWAY, "调用接口出错", ex);
        }

        return flag;
    }
}
