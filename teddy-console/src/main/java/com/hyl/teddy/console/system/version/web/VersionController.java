package com.hyl.teddy.console.system.version.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hyl.teddy.console.AbstractController;
import com.hyl.teddy.console.Remind;
import com.hyl.teddy.console.entity.Version;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;
import com.hyl.teddy.console.system.version.VersionService;

@Controller
public class VersionController extends AbstractController {

	private final String VERSION_COURIER_TABLE = "version_courier";//配送员版本更新表
	private final String VERSION_CUSTOMER_TABLE ="version_customer";//用户版本更新表
	private final String VERSION_DELIVER_TABLE = "version_deliver";//取菜员版本更新表
	@Resource
	private VersionService versionService;
	/**
	 * 版本更新列表
	 * @param model
	 * @return
	 */
	
	@RequestMapping(value="/system/{type}/update/", method = RequestMethod.GET)
	public String customerList(@PathVariable String type, Model model){
		String tableName = converUrlToTable(type);
		Pager<Version> pager = versionService.queryVersionList(getLookup(), tableName);
		model.addAttribute("pager",pager);
		return "system/version/list";
	}
	
	
	@Override
	protected Lookup instanceLookup() {
		// TODO Auto-generated method stub
		return super.instanceLookup();
	}

	@RequestMapping(value="/system/{type}/update/", method = RequestMethod.POST)
	public String serach(Lookup lookup,@PathVariable String type){
		return "redirect:/system/"+type+"/update/";
	}

	/**
	 * 根据删除记录
	 * @param type
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/system/{type}/update/{id}/remove", method = RequestMethod.GET)
	public String remove(@PathVariable String type,@PathVariable int id,RedirectAttributes attr){
		if(id <=0){
			throw new IllegalArgumentException("id参数不合法");
		}
		versionService.remove(id, converUrlToTable(type));
		attr.addFlashAttribute(Remind.success().appendMessage("成功删除"));
		return "redirect:/system/"+type+"/update/";
	}

	/**
	 * TO ADD
	 * @return
	 */
	@RequestMapping(value = "/system/{type}/update/0", method = RequestMethod.GET)
	public String input(){
		return "system/version/form";
	}
	/**
	 * TO EDIT
	 * @param type
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/system/{type}/update/{id}", method = RequestMethod.GET)
	public String edit(@PathVariable String type,@PathVariable int id, Model model){
		Version entity = versionService.findVersion(id, converUrlToTable(type));
		model.addAttribute("entity", entity);
		return "system/version/form";
	}
	/**
	 * TO SAVE
	 * @param id
	 * @param type
	 * @param entity
	 * @param redirectAttr
	 * @return
	 */
	@RequestMapping(value = "/system/{type}/update/{id}", method = RequestMethod.POST)
	public String save(@PathVariable int id,@PathVariable String type, Version entity, RedirectAttributes attr){
		entity.setId(id);
		String table = converUrlToTable(type);
		if( id == 0 ) {//add
			versionService.addVersion(entity, table);
			attr.addFlashAttribute(Remind.success().appendMessage("新增成功"));
		}else if(id > 0){//edit
			versionService.updateVersion(entity, table);
			attr.addFlashAttribute(Remind.success().appendMessage("修改成功"));
		}else {
			throw new IllegalArgumentException("信息不合法");
		}
		return "redirect:/system/"+type+"/update/";
	}
	
	
	/**
	 * 根据路径获得数据来源表名
	 * @param type
	 * @return
	 */
	public String converUrlToTable(String type){
		if("customer".equals(type)){
			return VERSION_CUSTOMER_TABLE;
		}else if("deliver".equals(type)){
			return VERSION_DELIVER_TABLE;
		}else if("courier".equals(type)){
			return VERSION_COURIER_TABLE;
		}
		return "";
	}
	
	
}
