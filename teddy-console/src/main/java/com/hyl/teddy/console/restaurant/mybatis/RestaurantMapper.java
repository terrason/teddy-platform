package com.hyl.teddy.console.restaurant.mybatis;

import com.hyl.teddy.console.MybatisMapper;
import com.hyl.teddy.console.entity.Payment;
import com.hyl.teddy.console.entity.Restaurant;
import com.hyl.teddy.console.entity.RestaurantPromotion;

import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 *
 * @author Administrator
 */
@MybatisMapper
public interface RestaurantMapper {

    public int countRestaurant(Object params);

    public List<Restaurant> selectRestaurantList(Object params);

    public void deleteRestaurant(@Param("id") int id);

    public void deleteRestaurantByPks(@Param("pks") int[] pks);

    public Restaurant findRestaurantById(@Param("id") int id);

    public void updateRestaurant(Restaurant entity);

    public void createRestaurant(Restaurant entity);

    public void closeRestaurant(@Param("id") int id);

    public void openRestaurant(@Param("id") int id);

    public void deleteCategory(@Param("id") int id);

    public void createCategory(@Param("id") int id, @Param("categoryId") int categoryId);

    public List<Restaurant> queryAllRestaurant();

    public List<Payment> queryCommentList(@Param("id") int id);

    public List<Restaurant> queryAttachments(@Param("pks") int... id);

	public List<RestaurantPromotion> findPromotions(@Param("id")int id);

	
	public void removePromotion(@Param("id")int id);

	public void addPromotion(RestaurantPromotion rp);

	public void updatePromotion(RestaurantPromotion rp);
}
