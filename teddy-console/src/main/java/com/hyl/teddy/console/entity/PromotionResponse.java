package com.hyl.teddy.console.entity;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.format.annotation.NumberFormat;

/**
 * 订单详情中的优惠标签
 *
 * @author Administrator
 *
 */
public class PromotionResponse {

    @JsonIgnore
    private int id;
    private String tag;
    private int[] params;
    @JsonIgnore
    private int discount;

    public PromotionResponse() {
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int[] getParams() {
        return params;
    }

    public void setParams(int[] params) {
        this.params = params;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    @NumberFormat(pattern = "#.#")
    public double getMoney() {
        return 1.0 * discount / 100;
    }

    public void setMoney(double money) {
        this.discount = (int) money * 100;
    }

    public String getDescription() {
        if ("WIN_CASH".equals(tag)) {
            return "满" + params[0] + "元送" + params[1] + "张" + params[2] + "元代金券";
        }
        if ("VIP_DISCOUNT".equals(tag)) {
            return "会员优惠" + params[0] + "折 ";
        }
        if ("COST_CASH".equals(tag)) {
            return "用户可使用一张代金券";
        }
        if ("COST_SCORE".equals(tag)) {
            return "满" + params[0] + "元可用" + params[1] + "积分抵" + params[2] + "元现金";
        }
        if ("FREE_POSTAGE".equals(tag)) {
            return "满" + params[0] + "元免邮费 ";
        }
        return null;
    }

}
