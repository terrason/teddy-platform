package com.hyl.teddy.console.system.employee.mybatis;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.hyl.teddy.console.MybatisMapper;
import com.hyl.teddy.console.entity.Employee;
import com.hyl.teddy.console.entity.RestaurantPromotion;
import com.hyl.teddy.console.lookup.Lookup;

@MybatisMapper
public interface EmployeeMapper {

	public int countList(Lookup lookup);

	public List<Employee> selectlist(Lookup lookup);

	public Employee findEmployeeById(@Param("id")int id);
	
	public List<Map<String,Object>> findCenter();
	
	public List<Map<String,Object>> allRegion();

	public void updateEmployee(Employee entity);

	public void addEmployee(Employee entity);
	
	public String maxUsername(@Param("type")int type);

	public void removeEmployee(@Param("id")int id);

	public void removeCenter(@Param("id")int id);

	public List<Employee> allDeliver();

}
