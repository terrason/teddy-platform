package com.hyl.teddy.console.system.user.mybatis;

import com.hyl.teddy.console.MybatisMapper;
import com.hyl.teddy.console.system.user.UserInfo;
import java.util.List;
import org.apache.ibatis.annotations.Param;

@MybatisMapper
public interface UserInfoMapper {

    public int countUser(Object params);

    public List<UserInfo> selectUserList(Object params);

    public UserInfo selectUserById(@Param("id") int id);

    public UserInfo selectUserByUsername(@Param("username") String username);

    public int countUsername(@Param("username") String username);

    public void updateUser(UserInfo entity);

    public void updateUserSettings(UserInfo entity);

    public void insertUser(UserInfo entity);

    public void deleteUser(@Param("id") int id);

    public void deleteUserByPks(@Param("pks") int[] pks);

    public void updateUserPassword(@Param("id") int id, @Param("password") String password);

}
