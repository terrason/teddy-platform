package com.hyl.teddy.console.advertisement.mybatis;

import com.hyl.teddy.console.MybatisMapper;
import com.hyl.teddy.console.entity.Advertisement;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 广告位管理
 * @author Administrator
 */
@MybatisMapper
public interface AdvertisementMapper {
    
    public int countAdvertisement(Object params);

    public List<Advertisement> selectAdvertisementList(Object params);

    public void deleteAdvertisement(@Param("id") int id);

    public void deleteAdvertisementByPks(@Param("pks") int[] pks);

    public Advertisement findAdvertisementById(@Param("id") int id);
    
    public Integer queryAttachment(@Param("id") int id);
    
    public List<Advertisement> queryAttachments(@Param("pks") int[] pks);

    public void updateAdvertisement(Advertisement entity);

    public void createAdvertisement(Advertisement entity);
}
