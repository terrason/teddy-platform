package com.hyl.teddy.console.dish.mybaties;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.hyl.teddy.console.MybatisMapper;
import com.hyl.teddy.console.entity.Dish;
import com.hyl.teddy.console.entity.Restaurant;

@MybatisMapper
public interface DishManagerMapper {

    public int countDish(Object params);

    public List<Dish> selectDishList(Object params);

    public List<Dish> queryAllDishesByRestaurantId(@Param("id") int id);

    public List<Dish> queryAllDishes();

    public Dish findDishById(@Param("id") int id);

    public void closeDish(@Param("id") int id);

    public void openDish(@Param("id") int id);

    public void updateDish(Dish entity);

    public void createDish(Dish entity);

    public void removeDish(int id);

    public void removeDishes(@Param("pks") int[] pks);

    public void closeDishes(@Param("pks") int[] pks);

    public void openDishes(@Param("pks") int[] pks);

    /**
     * 清理餐厅所有热门菜. 将餐厅的所有菜设为非热门菜。
     *
     * @param id 餐厅ID
     */
    public void clearRestaurantHots(@Param("id") int id);

    /**
     * 设置指定的菜品为热门菜.
     *
     * @param hotDishIds 菜品ID列表
     */
    public void updateDishHot(@Param("pks") int[] hotDishIds);
	
	public List<Dish> queryAttachments(@Param("pks") int... id);
}
