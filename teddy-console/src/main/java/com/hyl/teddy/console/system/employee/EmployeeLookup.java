package com.hyl.teddy.console.system.employee;

import com.hyl.teddy.console.lookup.Lookup;

/**
 * 员工查询条件
 * @author Administrator
 *
 */
public class EmployeeLookup extends Lookup {
	private String username;
	private Integer type;
	private String mobile;
	private String name;
	private Integer status;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
}
