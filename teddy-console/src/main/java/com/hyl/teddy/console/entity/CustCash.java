package com.hyl.teddy.console.entity;
/**
 * 客户代金券 cust_cach
 * @author Administrator
 *
 */
public class CustCash {
	private int id;
	private int  customerId;//用户id
	private int cash;//代金券
	private int count;//代金券数量
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public int getCash() {
		return cash;
	}
	public void setCash(int cash) {
		this.cash = cash;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
}
