package com.hyl.teddy.console.entity;
/**
 * 订单优惠政策
 * @author Administrator
 *
 */
public class PaymentPromotion{
	private int id;
	private int restaurantId;
	private String restaurantName;
	private String pattern;
	private int param0;
	private int param1;
	private int param2;
	private int param3;
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRestaurantId() {
		return restaurantId;
	}
	public void setRestaurantId(int restaurantId) {
		this.restaurantId = restaurantId;
	}
	public String getRestaurantName() {
		return restaurantName;
	}
	public void setRestaurantName(String restaurantName) {
		this.restaurantName = restaurantName;
	}
	public String getPattern() {
		return pattern;
	}
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}
	public int getParam0() {
		return param0;
	}
	public void setParam0(int param0) {
		this.param0 = param0;
	}
	public int getParam1() {
		return param1;
	}
	public void setParam1(int param1) {
		this.param1 = param1;
	}
	public int getParam2() {
		return param2;
	}
	public void setParam2(int param2) {
		this.param2 = param2;
	}
	public int getParam3() {
		return param3;
	}
	public void setParam3(int param3) {
		this.param3 = param3;
	}
	
	
}