package com.hyl.teddy.console.payment;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hyl.teddy.console.BaseResponse;
import com.hyl.teddy.console.entity.Payment;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;
import com.hyl.teddy.console.payment.mybatis.PaymentBackMapper;
import com.hyl.teddy.console.payment.web.PaymentBack;
import com.hyl.teddy.console.util.ResponseToData;
import java.util.HashMap;
import java.util.Map;

@Service
public class PaymentBackService {

    @Value("#{settings['url.apis']}")
    private String host;

    private final Logger logger = LoggerFactory.getLogger(PaymentBackService.class);
    @Resource
    private PaymentBackMapper paymentBackMapper;

    public Pager<PaymentBack> queryPaymentBackList(Lookup lookup) {
        int total = paymentBackMapper.countPaymentBack(lookup);
        Pager<PaymentBack> pager = new Pager<>();
        pager.setPage(lookup.getPage());
        pager.setSize(lookup.getSize());
        pager.setTotal(total);

        List<PaymentBack> entities = pager.isOverflowed() ? Collections.EMPTY_LIST : paymentBackMapper.selectPaymentBackList(lookup);
        pager.setElements(entities);

        if (!entities.isEmpty()) {
            Map<String, PaymentBack> payments = new HashMap<String, PaymentBack>();
            for (PaymentBack entity : entities) {
                payments.put(entity.getOrderno(), entity);
            }
            List<Map<String, String>> synchros = paymentBackMapper.selectPaymentThreads(payments.keySet());
            for (Map<String, String> synchro : synchros) {
                payments.get(synchro.get("orderno")).addSynchros(synchro.get("type"));
            }
        }
        return pager;
    }

    /**
     * 调用订单详情接口
     *
     * @param orderno
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    public Payment getPayment(String orderno) throws JsonParseException, JsonMappingException, IOException {
        Document document = null;
        try {
            document = Jsoup.connect(host + "/payment/" + orderno).ignoreContentType(true).get();
        } catch (IOException ex) {
            logger.debug("调用订单详情接口出错", ex);
            throw ex;
        }
        String text = document.body().text();
        String paymentText = ResponseToData.conversion(text);
        Payment payment = new ObjectMapper().readValue(paymentText, Payment.class);

        return payment;
    }

    /**
     * 退订单明细
     *
     * @param id
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public void backDish(int id) throws Exception {
        /*paymentBackMapper.backDish(id);
         evictCacheForApis.evictCache("payment");//清空payment缓存
         */
    }

    /**
     * 取消订单 调用取消订单接口
     *
     * @param orderno
     */
    public boolean cancel(String orderno) {
        String url = "/inner/payment-cancel/" + orderno;
        return callRemoteInterface(url);
    }

    /**
     * 退菜 调用退菜接口
     *
     * @param orderno
     * @throws IOException
     */
    public boolean backorder(String orderno) {
        String url = "/inner/payment-giveup/" + orderno;
        return callRemoteInterface(url);
    }

    /**
     * 调用远程接口
     *
     * @param url 路径 + 参数
     * @return 返 boolean
     */
    public boolean callRemoteInterface(String url) {
        boolean flag = false;

        try {
            Document document = Jsoup.connect(host + url).timeout(30000).ignoreContentType(true).get();
            String text = document.body().text();
            BaseResponse br = new ObjectMapper().readValue(text, BaseResponse.class);
            if (br != null && br.getCode() == 0) {
                flag = true;
            }
        } catch (IOException ex) {
            logger.debug("调用订单详情接口出错", ex);
        }

        return flag;
    }
}
