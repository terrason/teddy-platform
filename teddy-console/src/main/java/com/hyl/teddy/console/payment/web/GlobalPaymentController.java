package com.hyl.teddy.console.payment.web;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hyl.teddy.console.AbstractController;
import com.hyl.teddy.console.entity.Payment;
import com.hyl.teddy.console.entity.PaymentTimes;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;
import com.hyl.teddy.console.payment.GlobalPaymentLookup;
import com.hyl.teddy.console.payment.GlobalPaymentService;
import com.hyl.teddy.console.restaurant.RestaurantService;
import com.hyl.teddy.console.util.FinalFiledParams;
/**
 * 全局订单管理
 * @author Administrator
 *
 */
@Controller
public class GlobalPaymentController extends AbstractController{
	
	@Resource
	private GlobalPaymentService globalPaymentService;
	
	@Resource
	private RestaurantService restaurantService;
	
	/**
	 * 订单列表
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/payment/global/", method = RequestMethod.GET)
	public String home(Model  model){
		Pager<GlobalPayment> pager = globalPaymentService.queryGlobalList(getLookup());
		model.addAttribute("pager", pager);
		getLookup().setTotal(pager.getTotal());
		model.addAttribute("restaurants",restaurantService.queryAllRestaurant());
		model.addAttribute("statusMap",FinalFiledParams.PAYMENT_STATUS);
		return "payment/global/list";
	}
	
	
	@Override
	protected Lookup instanceLookup() {
		return new GlobalPaymentLookup();
	}

	@RequestMapping(value = "/payment/global/", method = RequestMethod.POST)
	public String search(GlobalPaymentLookup lookup){
		setLookup(lookup);
		return "redirect:/payment/global/";
	}
	/**
	 * 订单详情
	 * @param orderno
	 * @param model
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/payment/global/{orderno}", method = RequestMethod.GET)
	public String detail(@PathVariable String orderno,Model model) throws JsonParseException, JsonMappingException, IOException{
		Payment entity = globalPaymentService.getPayment(orderno);
		model.addAttribute("entity", entity);
		return "payment/global/detail";
	}
	/**
	 * 订单时间轴
	 * @param orderno
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/payment/global/{paymentId}/timeline", method = RequestMethod.GET)
	public String timeline(@PathVariable int paymentId, Model model){
		System.out.println("进入订单时间轴");
		List<PaymentTimes> list = globalPaymentService.selectTimeline(paymentId);
		model.addAttribute("times", list);
		return "payment/global/timeline";
	}
}
