/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.console.pager;

import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

public class JdbcTemplatePagination {

    private final JdbcTemplate jdbcTemplate;

    public JdbcTemplatePagination(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    
    /**
     * 分页查询.
     * @param <T> 实体类型》
     * @param sql sql语句前半部分. 不包括{@code where 子句}.
     * @param where {@code where 子句}构造器.
     * @param rowMapper 实体映射器.
     * @param page 页码
     * @param size 页大小
     * @return 分页结果.
     */
    public <T> Pager paginate(String sql,SqlWhereBuilder where,RowMapper<T> rowMapper,int page,int size){
        Pager pager=new Pager();
        pager.setPage(page);
        pager.setSize(size);
        
        StringBuilder builder=new StringBuilder();
        ArrayList params=new ArrayList();
        int fromIndex = sql.indexOf(" from");
        builder.append(sql.substring(fromIndex));
        builder.append(" where 1=1");
        where.build(builder,params);
        
        Integer totalSize = jdbcTemplate.queryForObject("select count(1)"+builder, Integer.class, params.toArray());
        pager.setTotal(totalSize);
        
        builder.append(" limit ?,?");
        params.add(pager.getIndex());
        params.add(pager.getSize());
        List<T> elements = jdbcTemplate.query(sql.substring(0,fromIndex)+builder,rowMapper,params.toArray());
        pager.setElements(elements);
        
        return pager;
    }
}
