package com.hyl.teddy.console.system.user.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hyl.teddy.console.AbstractController;
import com.hyl.teddy.console.AjaxResponse;
import com.hyl.teddy.console.Principal;
import com.hyl.teddy.console.Remind;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;
import com.hyl.teddy.console.system.user.UserInfo;
import com.hyl.teddy.console.system.user.UserInfoLookup;
import com.hyl.teddy.console.system.user.UserInfoService;

@Controller
@RequestMapping
public class UserInfoController extends AbstractController {

    @Resource
    private UserInfoService userInfoService;

    /**
     * 跳转到列表页面.
     */
    @RequestMapping(value = "/system/user/", method = RequestMethod.GET)
    public String home(Model model) {
        Pager<UserInfo> pager = userInfoService.queryUserList(getLookup());
        model.addAttribute("pager", pager);
        getLookup().setTotal(pager.getTotal());

        return "system/user/list";
    }

    @Override
    protected Lookup instanceLookup() {
        return new UserInfoLookup();
    }

    /**
     * 接收用户提交的查询信息.
     */
    @RequestMapping(value = "/system/user/", method = RequestMethod.POST)
    public String search(UserInfoLookup lookup) {
        setLookup(lookup);
        return "redirect:/system/user/";
    }

    /**
     * 跳转到新增页面.
     */
    @RequestMapping(value = "/system/user/0", method = RequestMethod.GET)
    public String input(Model model) {
        return "system/user/form";
    }

    /**
     * 跳转到编辑页面.
     */
    @RequestMapping(value = "/system/user/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable int id, Model model) {
        if (id <= 0) {
            throw new IllegalArgumentException("id 参数不合法");
        }
        UserInfo entity = userInfoService.findUserById(id);
        model.addAttribute("entity", entity);
        return "system/user/form";
    }

    /**
     * 保存用户信息.
     */
    @RequestMapping(value = "/system/user/{id}", method = RequestMethod.POST)
    public String save(@PathVariable int id, UserInfo entity, RedirectAttributes redirectAttrs) {
        entity.setId(id);
        if (id > 0) {
            userInfoService.saveUser(entity);
            redirectAttrs.addFlashAttribute(Remind.success().appendMessage("用户信息已修改。"));
        } else if (id == 0) {
            userInfoService.createUser(entity);
            redirectAttrs.addFlashAttribute(Remind.info().appendMessage("用户已创建但尚未激活。"));
        } else {
            throw new IllegalArgumentException("用户信息不合法");
        }
        return "redirect:/system/user/";
    }

    @RequestMapping(value = "/system/user/{id}/remove", method = RequestMethod.POST)
    public String remove(@PathVariable int id, RedirectAttributes redirectAttrs) {
        if (id <= 0) {
            throw new IllegalArgumentException("id 参数不合法");
        }
        userInfoService.removeUser(id);
        redirectAttrs.addFlashAttribute(Remind.warning().appendMessage("用户已删除"));
        return "redirect:/system/user/";
    }

    @RequestMapping(value = "/system/user/remove", method = RequestMethod.POST)
    public String remove(@RequestParam int[] pks, RedirectAttributes redirectAttrs) {
        userInfoService.removeUserByPks(pks);
        redirectAttrs.addFlashAttribute(Remind.warning().appendMessage(pks.length + "个用户成功删除"));
        return "redirect:/system/user/";
    }

    /**
     * 修改密码
     */
    @RequestMapping(value = "/system/user/{id}/passwd", method = RequestMethod.GET)
    public String showPasswd(@PathVariable int id, Model model) {
        if (id <= 0) {
            throw new IllegalArgumentException("id 参数不合法");
        }
        UserInfo entity = userInfoService.findUserById(id);
        model.addAttribute("entity", entity);
        return "system/user/passwd";
    }

    /**
     * 保存密码
     */
    @RequestMapping(value = "/system/user/{id}/passwd", method = RequestMethod.POST)
    public String savePasswd(@PathVariable int id, RedirectAttributes redirectAttrs, UserInfo entity) {
        userInfoService.passwd(id, entity.getPassword());
        redirectAttrs.addFlashAttribute(Remind.success().appendMessage("密码已修改。"));
        return "redirect:/system/user/";
    }

    @RequestMapping(value = "/system/user/settings", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResponse saveProfile(UserInfo entity) {
        Principal principal = getPrincipal();
        principal.setBreadcrumbFixed(entity.isBreadcrumbFixed());
        principal.setMenuFixed(entity.isMenuFixed());
        principal.setNavbarFixed(entity.isNavbarFixed());
        principal.setPetty(entity.isPetty());
        principal.setSkin(entity.getSkin());

        entity.setId(principal.getUserId());
        userInfoService.saveUserSettings(entity);
        return null;
    }
}
