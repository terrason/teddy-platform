/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.console.system.user;

import java.io.Serializable;

/**
 * 用户信息.
 *
 * @author lip
 */
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    private int id;
    /**
     * 登录名.
     */
    private String username;
    /**
     * 姓名.
     */
    private String realName;
    /**
     * 密码.
     */
    private String password;
    /**
     * 状态. 0-尚未初始化 1-正常
     */
    private int status;
    /**
     * 用户类型. 8-物业用户，9-系统内置用户.
     */
    private int type;
    /**
     * 配置——皮肤.
     */
    private String skin;
    /**
     * 配置——固定导航栏.
     */
    private boolean navbarFixed;
    /**
     * 配置——固定菜单栏.
     */
    private boolean menuFixed;
    /**
     * 配置——固定位置信息.
     */
    private boolean breadcrumbFixed;
    /**
     * 配置——窄屏.
     */
    private boolean petty;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 登录名.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * 登录名.
     *
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 姓名.
     *
     * @return the realName
     */
    public String getRealName() {
        return realName;
    }

    /**
     * 姓名.
     *
     * @param realName the realName to set
     */
    public void setRealName(String realName) {
        this.realName = realName;
    }

    /**
     * 密码.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * 密码.
     *
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 状态. 0-尚未初始化 1-正常
     *
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * 状态. 0-尚未初始化 1-正常
     *
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * 用户类型. 9-系统内置用户.
     *
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * 用户类型. 9-系统内置用户.
     *
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * 配置——皮肤.
     *
     * @return the skin
     */
    public String getSkin() {
        return skin;
    }

    /**
     * 配置——皮肤.
     *
     * @param skin the skin to set
     */
    public void setSkin(String skin) {
        this.skin = skin;
    }

    /**
     * 配置——固定导航栏.
     *
     * @return the navbarFixed
     */
    public boolean isNavbarFixed() {
        return navbarFixed;
    }

    /**
     * 配置——固定导航栏.
     *
     * @param navbarFixed the navbarFixed to set
     */
    public void setNavbarFixed(boolean navbarFixed) {
        this.navbarFixed = navbarFixed;
    }

    /**
     * 配置——固定菜单栏.
     *
     * @return the menuFixed
     */
    public boolean isMenuFixed() {
        return menuFixed;
    }

    /**
     * 配置——固定菜单栏.
     *
     * @param menuFixed the menuFixed to set
     */
    public void setMenuFixed(boolean menuFixed) {
        this.menuFixed = menuFixed;
    }

    /**
     * 配置——固定位置信息.
     *
     * @return the breadcrumbFixed
     */
    public boolean isBreadcrumbFixed() {
        return breadcrumbFixed;
    }

    /**
     * 配置——固定位置信息.
     *
     * @param breadcrumbFixed the breadcrumbFixed to set
     */
    public void setBreadcrumbFixed(boolean breadcrumbFixed) {
        this.breadcrumbFixed = breadcrumbFixed;
    }

    /**
     * 配置——窄屏.
     *
     * @return the petty
     */
    public boolean isPetty() {
        return petty;
    }

    /**
     * 配置——窄屏.
     *
     * @param petty the petty to set
     */
    public void setPetty(boolean petty) {
        this.petty = petty;
    }

}
