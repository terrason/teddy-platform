package com.hyl.teddy.console.system.configuration;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hyl.teddy.console.entity.StaticResource;
import com.hyl.teddy.console.system.configuration.mybatis.ConfigurationMapper;
import com.hyl.teddy.console.util.CacheEvictor;

@Service
public class ConfigurationService {

    @Resource
    public ConfigurationMapper mapper;
    @Resource
    public CacheEvictor cacheEvictor;

    public Configuration queryAll() {
        List<StaticResource> list = mapper.queryAll();
        return listToBean(list);
    }

    /**
     * 根据code更新全局配置
     *
     * @param code
     * @param content
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateConfiguration(String code, String content) throws Exception {
        mapper.updateConfiguration(code, content);
        if ("config.promotion.costScore".equals(code)) {
            String[] params = content.split(",");
            if (params.length != 2) {
                throw new Exception();
            }
            mapper.updateAllPromotion(params[0], params[1]);
        }
        cacheEvictor.evictCache("static_resource", null);
    }

    public Configuration listToBean(List<StaticResource> list) {
        Configuration c = new Configuration();
        for (StaticResource s : list) {
            if (null != s.getCode()) switch (s.getCode()) {
                case "config.comment.withContent":
                    c.setWithContent(s.getContent());
                    break;
                case "config.courier.autoAssignable":
                    c.setAutoCourier(s.getContent());
                    break;
                case "config.deliver.autoAssignable":
                    c.setAutoDeliver(s.getContent());
                    break;
                case "config.promotion.costScore":
                    c.setCostScore(s.getContent());
                    break;
                case "config.score.scorePerMoney":
                    c.setScorePerMoney(s.getContent());
                    break;
                case "config.vip.experience":
                    c.setExperience(s.getContent());
                    break;
                case "service.aboutus":
                    c.setAboutus(s.getContent());
                    break;
                case "service.license":
                    c.setLicense(s.getContent());
                    break;
                case "service.support":
                    c.setSupport(s.getContent());
                    break;
                case "service.tel":
                    c.setTel(s.getContent());
                    break;
                case "share.payment":
                    c.setSharePayment(s.getContent());
                    break;
                case "share.receive":
                    c.setShareReceive(s.getContent());
                    break;
            }

        }
        return c;
    }

}
