/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.console.system.authority;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.hyl.teddy.console.system.authority.mybatis.AuthMapper;
import com.hyl.teddy.console.system.user.mybatis.UserInfoMapper;

/**
 * 权限相关服务.
 *
 * @author Administrator
 */
@Service
public class AuthService {

    @Resource
    private AuthMapper authMapper;
    
    /**
     * 构建个人权限菜单.
     *
     * @return 根菜单列表. 包含全部树结构。
     */
    public List<Menu> buildPrincipalAuthority(int principal) {
        return buildAuthorityMenu(authMapper.selectPrincipalMenus(principal));
    }

    /**
     * 构建超级权限菜单.
     *
     * @return 根菜单列表. 包含全部树结构。
     */
    public List<Menu> buildSuperAuthority() {
        return buildAuthorityMenu(authMapper.selectSuperMenus());
    }
    
    /**
     * 构建物业权限菜单.
     *
     * @return 根菜单列表. 包含全部树结构。
     */
    public List<Menu> buildWuyeAuthority() {
        return buildAuthorityMenu(authMapper.selectWuyeMenus());
    }

    private List<Menu> buildAuthorityMenu(List<Menu> menus) {
    	Map<Integer,Menu> menuMap = new LinkedHashMap<Integer,Menu>();
    	for(Menu m:menus){
    		menuMap.put(m.getId(), m);
    	}
        ArrayList<Menu> roots = new ArrayList<Menu>();
        for (Map.Entry<Integer, Menu> entry : menuMap.entrySet()) {
            //Integer id = entry.getKey();
            Menu menu = entry.getValue();

            if (0 == menu.getParentId()) {
                roots.add(menu);
            } else {
                Menu parent = menuMap.get(menu.getParentId());
                parent.addChildren(menu);
            }
        }
        return roots;
    }
}
