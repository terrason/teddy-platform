package com.hyl.teddy.console.system.region.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hyl.teddy.console.AbstractController;
import com.hyl.teddy.console.Remind;
import com.hyl.teddy.console.entity.Region;
import com.hyl.teddy.console.pager.Pager;
import com.hyl.teddy.console.system.region.RegionService;
/**
 * 员工管理
 * @author Administrator
 *
 */
@Controller
public class RegionController extends AbstractController{

	@Resource
	private RegionService service;
	/**
	 * 区域列表
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/system/area/", method = RequestMethod.GET)
	public String home(Model model){
		Pager<Region> pager = service.queryList(getLookup()); 
		model.addAttribute("pager", pager);
		return "system/region/list";
	}
	
	
	
/*	@Override
	protected Lookup instanceLookup() {
		return new EmployeeLookup();
	}*/

	/**
	 * 查询
	 * @param lookup
	 * @return
	 */
/*	@RequestMapping(value="/system/employee/", method = RequestMethod.POST)
	public String search(EmployeeLookup lookup){
		setLookup(lookup);
		return "redirect:/system/employee/";
	}
*/
	/**
	 * To  Add
	 * @return
	 */
	@RequestMapping(value = "/system/area/0", method = RequestMethod.GET)
	public String input(Model model){
		model.addAttribute("flag","add");
		model.addAttribute("others",service.findOtherRegions(0));
		return "system/region/form";
	}
	/**
	 * TO edit
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/system/area/{id}", method = RequestMethod.GET)
	public String edit(@PathVariable int id, Model model){
		if (id <= 0) {
            throw new IllegalArgumentException("id 参数不合法");
        }
		Area entity = service.forEdit(id);
		model.addAttribute("entity", entity);
		model.addAttribute("flag","edit");
		List<Area> others = service.findOtherRegions(id);
		model.addAttribute("others", others);
		return "system/region/form";
	}
	/**
	 * To save
	 * @param id
	 * @param name
	 * @param points
	 * @return
	 */
	@RequestMapping( value="/system/area/{id}", method = RequestMethod.POST)
	public String save(@PathVariable int id, String name, @RequestParam String points, RedirectAttributes redirectAttrs){
		if(id==0){
			service.addRegion(name, points);
			redirectAttrs.addFlashAttribute(Remind.success().appendMessage("成功新增区域"));
		}else if(id >0 ){
			service.updateRegion(id, name,points);
			redirectAttrs.addFlashAttribute(Remind.success().appendMessage("成功修改区域"));
		}else {
			 throw new IllegalArgumentException("信息不合法");
		}
		return "redirect:/system/area/";
	}
	/**
	 * 删除区域
	 * @param id
	 * @param redirectAttrs
	 * @return
	 */
	@RequestMapping( value="/system/area/{id}/remove", method = RequestMethod.GET)
	public String remove(@PathVariable int id, RedirectAttributes redirectAttrs){
		service.removeRegion(id);
		redirectAttrs.addFlashAttribute(Remind.success().appendMessage("删除一个区域"));
		return "redirect:/system/area/";
	}
	/**
	 * 显示所有区域
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/system/area/all", method = RequestMethod.GET)
	public String allRegion(Model model){
		List<Area> list = service.findAllRegion();
		model.addAttribute("regions", list);
		return "system/region/all";
	}
	
	/**
	 *  Do Save
	 * @param id
	 * @param entity
	 * @param redirectAttrs
	 * @return
	 *//*
	@RequestMapping(value = "/system/employee/{id}", method = RequestMethod.POST)
	public String save(@PathVariable int id, Employee entity, RedirectAttributes redirectAttrs) {
        entity.setId(id);
        if (id > 0) {
            service.updateEmployee(entity);
            redirectAttrs.addFlashAttribute(Remind.success().appendMessage("员工信息已修改。"));
        } else if (id == 0) {
            service.addEmployee(entity);
            redirectAttrs.addFlashAttribute(Remind.success().appendMessage("员工信息已创建。"));
        } else {
            throw new IllegalArgumentException("员工信息不合法");
        }
        return "redirect:/system/employee/";
    }
	@RequestMapping(value = "/system/employee/{id}/remove", method = RequestMethod.GET)
	public String remove(@PathVariable int id,@RequestParam int type){
		service.removeEmployee(id, type);
		return "redirect:/system/employee/";
	}
	*//**
	 * 地图定位 分配员
	 * @return
	 *//*
	@RequestMapping(value = "/system/employee/site", method = RequestMethod.GET)
	public String location(Model model){
		List<Employee> list = service.allDeliver();
		System.out.println(list.size());
		model.addAttribute("delivers", list);
		return "system/employee/site";
	}*/
	
}
