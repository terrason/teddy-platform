/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.hyl.teddy.console;

/**
 * 接口返回值基础类.
 * @author Administrator
 */
public class BaseResponse {
    /**
     * 操作成功返回{@code 0}.
     * 101 密码错误,102 用户不存在,103 手机号重复,104 验证码错误,105缺少必要参数,106数据更新失败
     * 201 图片格式错误，202 图片大小错误
     * 
     */
    private int code;
    /**
     * 用户客户端提示的信息.
     */
    private String message;
    /**
     * 接口返回的对象.
     */
    private Object data;

    public BaseResponse() {
    }

    public BaseResponse(Object data) {
        this.data = data;
    }

    /**
     * 操作成功返回{@code 0}.
     * @return the code
     */
    public int getCode() {
        return code;
    }

    /**
     * 操作成功返回{@code 0}.
     * @param code the code to set
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * 用户客户端提示的信息.
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 用户客户端提示的信息.
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 接口返回的对象.
     * @return the data
     */
    public Object getData() {
        return data;
    }

    /**
     * 接口返回的对象.
     * @param data the data to set
     */
    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "{" + "code=" + code + ", message=" + message + ", data=" + data + '}';
    }
}
