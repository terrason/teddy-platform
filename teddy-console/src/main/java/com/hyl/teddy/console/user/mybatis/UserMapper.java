package com.hyl.teddy.console.user.mybatis;

import com.hyl.teddy.console.MybatisMapper;
import com.hyl.teddy.console.entity.CustAddress;
import com.hyl.teddy.console.entity.CustCash;
import com.hyl.teddy.console.entity.CustFavoraties;
import com.hyl.teddy.console.entity.Customer;
import com.hyl.teddy.console.entity.Region;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 客户信息
 * @author Administrator
 */
@MybatisMapper
public interface UserMapper {
    public int countCustomer(Object params);

    public List<Customer> selectCustomerList(Object params);

    public Customer findCustomerById(@Param("id") int id);

    public void updateCustomer(Customer entity);
    
    public List<CustFavoraties> findFavoratiesListById(@Param("id") int id);
    
    public List<CustCash> findCashListById(@Param("id") int id);
    
    public List<CustAddress> findAddressListById(@Param("id") int id);
    
    public CustFavoraties queryFavoratiesById(@Param("id") int id);
    
    public CustCash queryCashById(@Param("id") int id);
    
    public CustAddress queryAddressById(@Param("id") int id);
    
    public void updateCash(CustCash entity);
    /**
     * 为客户增加代金券.
     * @param customerId 客户ID
     * @param cash 面值
     * @param count 数量
     * @return 数据库更新条数
     */
    public int updateCash4Increase(@Param("customerId") int customerId,@Param("cash") int cash, @Param("count") int count);
    
    public void updateAddress(CustAddress entity);
    
    public void createCash(CustCash entity);
    
    public void createAddress(CustAddress entity);
    
    public List<Region> findAllRegion();

}
