package com.hyl.teddy.console.restaurant.web;

import com.hyl.teddy.console.AbstractController;
import com.hyl.teddy.console.Remind;
import com.hyl.teddy.console.attachment.AttachmentService;
import com.hyl.teddy.console.dish.DishManagerService;
import com.hyl.teddy.console.entity.Restaurant;
import com.hyl.teddy.console.entity.RestaurantPromotion;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;
import com.hyl.teddy.console.restaurant.RestaurantCategoryService;
import com.hyl.teddy.console.restaurant.RestaurantLookup;
import com.hyl.teddy.console.restaurant.RestaurantService;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping
public class RestaurantController extends AbstractController {

    @Resource
    private RestaurantService restaurantService;
    @Resource
    private AttachmentService attachmentService;
    @Resource
    private RestaurantCategoryService categoryService;
    @Resource
    private DishManagerService dishManagerService;

    /**
     * 餐厅列表
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/restaurant/manager/", method = RequestMethod.GET)
    public String home(Model model) {
        Pager<Restaurant> pager = restaurantService.queryRestaurant(getLookup());
        model.addAttribute("pager", pager);
        model.addAttribute("restaurantCategories", categoryService.queryAllCategories(0));
        getLookup().setTotal(pager.getTotal());
        return "restaurant/manager/list";
    }

    @Override
    protected Lookup instanceLookup() {
        // 封装查询条件
        return new RestaurantLookup();
    }

    /**
     * 接受用户提交的查询信息
     *
     * @param lookup
     * @return
     */
    @RequestMapping(value = "/restaurant/manager/", method = RequestMethod.POST)
    public String search(RestaurantLookup lookup) {
        setLookup(lookup);
        return "redirect:/restaurant/manager/";
    }

    /**
     * 关闭餐厅
     *
     * @param id
     * @param redirectAttrs
     * @return
     */
    @RequestMapping(value = "/restaurant/manager/{id}/close", method = RequestMethod.POST)
    public String close(@PathVariable int id, RedirectAttributes redirectAttrs) {
        if (id < 0) {
            throw new IllegalArgumentException("id 参数不合法");
        }
        restaurantService.closeRestaurant(id);
        redirectAttrs.addFlashAttribute(Remind.warning().appendMessage("餐厅已被关闭！"));
        return "redirect:/restaurant/manager/";
    }

    /**
     * 打开餐厅
     *
     * @param id
     * @param redirectAttrs
     * @return
     */
    @RequestMapping(value = "/restaurant/manager/{id}/open", method = RequestMethod.POST)
    public String open(@PathVariable int id, RedirectAttributes redirectAttrs) {
        if (id < 0) {
            throw new IllegalArgumentException("id 参数不合法");
        }
        restaurantService.openRestaurant(id);
        redirectAttrs.addFlashAttribute(Remind.success().appendMessage("餐厅已开启！"));
        return "redirect:/restaurant/manager/";
    }

    /**
     * 删除餐厅
     *
     * @param id
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = "/restaurant/manager/{id}/remove", method = RequestMethod.POST)
    public String remove(@PathVariable int id, RedirectAttributes redirectAttributes) {
        if (id <= 0) {
            throw new IllegalArgumentException("id参数不合法");
        }
        restaurantService.removeRestaurant(id);
        redirectAttributes.addFlashAttribute(Remind.warning().appendMessage("成功删除"));
        return "redirect:/restaurant/manager/";
    }

    /**
     * 批量删除餐厅
     *
     * @param id
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = "/restaurant/manager/remove", method = RequestMethod.POST)
    public String removes(@RequestParam int[] pks, RedirectAttributes redirectAttributes) {
        if (pks.length <= 0) {
            throw new IllegalArgumentException("id参数不合法");
        }
        restaurantService.removeRestaurants(pks);
        redirectAttributes.addFlashAttribute(Remind.warning().appendMessage("批量删除成功"));
        return "redirect:/restaurant/manager/";
    }

    /**
     * To add
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/restaurant/manager/0", method = RequestMethod.GET)
    public String input(Model model) {
        model.addAttribute("restaurantCategories", categoryService.queryAllCategories(0));
        Restaurant entity = new Restaurant();
        try {
            Date openTime = DateUtils.parseDate("8:30", "HH:mm");
            Date closeTime = DateUtils.parseDate("22:00", "HH:mm");
            entity.setOpenTime(openTime);
            entity.setCloseTime(closeTime);
        } catch (ParseException ex) {
            Logger.getLogger(RestaurantController.class.getName()).log(Level.SEVERE, null, ex);
        }
        entity.setDelivery(900);
        model.addAttribute("entity", entity);
        return "restaurant/manager/form";
    }

    /**
     * To edit
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/restaurant/manager/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable int id, Model model) {
        if (id <= 0) {
            throw new IllegalArgumentException("id参数不合法");
        }
        Restaurant entity = restaurantService.findRestaurantById(id);
        model.addAttribute("restaurantCategories", categoryService.queryAllCategories(id));
        model.addAttribute("dishes", dishManagerService.queryAllDishesByRestaurantId(id));
        model.addAttribute("entity", entity);
        return "restaurant/manager/form";
    }

    /**
     * 保存餐厅
     *
     * @param id
     * @param entity
     * @param redirectAttrs
     * @return
     */
    @RequestMapping(value = "/restaurant/manager/{id}", method = RequestMethod.POST)
    @Transactional
    public String save(@PathVariable int id,
            @RequestParam(value = "hotDishes", required = false) int[] hotDishIds,
            Restaurant entity,
            RedirectAttributes redirectAttrs) {
        entity.setId(id);
        if (id > 0) {
            Restaurant old = restaurantService.findRestaurantById(id);
            restaurantService.updateRestaurant(entity);
            if (entity.isHotDefined()) {
                dishManagerService.setRestaurantHots(hotDishIds, id);
            }
            restaurantService.deleteCategory(id);
            if(entity.getCategoryPks() != null ){
            	restaurantService.createCategory(id, entity.getCategoryPks());
            }

            attachmentService.changeAttachmentTemporary(old.getIcon(), true);
            attachmentService.changeAttachmentTemporary(old.getImages(), true);
            attachmentService.changeAttachmentTemporary(entity.getIcon(), false);
            attachmentService.changeAttachmentTemporary(entity.getImages(), false);
            redirectAttrs.addFlashAttribute(Remind.success().appendMessage("修改成功。"));

        } else if (id == 0) {
            entity.setEnabled(true);
            entity.setHotDefined(false);
            restaurantService.createRestaurant(entity);
            restaurantService.deleteCategory(entity.getId());
            if(entity.getCategoryPks() != null ){
            	restaurantService.createCategory(entity.getId(), entity.getCategoryPks());
            }
            attachmentService.changeAttachmentTemporary(entity.getIcon(), false);
            attachmentService.changeAttachmentTemporary(entity.getImages(), false);
            redirectAttrs.addFlashAttribute(Remind.success().appendMessage("创建成功。"));

        } else {
            throw new IllegalArgumentException("信息不合法");
        }
        return "redirect:/restaurant/manager/";
    }

    /**
     * 餐厅评论列表
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/restaurant/manager/{id}/detail", method = RequestMethod.GET)
    public String comment(@PathVariable int id, Model model) {
        model.addAttribute("restaurant", restaurantService.findRestaurantById(id));
        model.addAttribute("comment", restaurantService.queryCommentList(id));
        return "restaurant/manager/detail";
    }

    /**
     * 编辑优惠标签页面
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/restaurant/manager/{id}/promotion/", method = RequestMethod.GET)
    public String toPromotion(@PathVariable int id, Model model) {
        if (id <= 0) {
            throw new IllegalArgumentException("id参数不合法");
        }
        Restaurant entity = restaurantService.findRestaurantById(id);
        model.addAttribute("entity", entity);
        List<RestaurantPromotion> promotions = restaurantService.findPromotions(id);
        model.addAttribute("promotions", promotions);
        return "restaurant/manager/promotion";
    }

    /**
     * 从优惠标签页面返回列表页面
     *
     * @return
     */

    @RequestMapping(value = "/restaurant/manager/{id}/promotion/back", method = RequestMethod.GET)
    public String backHome() {
        return "redirect:/restaurant/manager/";
    }

    /**
     * 保存优惠标签
     *
     * @param id
     * @param promotionId
     * @param pattern
     * @param param0
     * @param param1
     * @param param2
     * @return
     */
    @RequestMapping(value = "/restaurant/manager/promotion/add", method = RequestMethod.POST)
    @ResponseBody
    public String savePromotion(int id, int promotionId, String pattern, int param0, int param1, int param2) {
        RestaurantPromotion rp = new RestaurantPromotion();
        rp.setId(promotionId);
        rp.setRestaurantId(id);
        rp.setPattern(pattern);
        rp.setParam0(param0);
        rp.setParam1(param1);
        rp.setParam2(param2);
        String msg = "error";
        if (promotionId == 0) {
            restaurantService.addPromotion(rp, id);
            msg = "success";
        } else if (promotionId > 0) {
            restaurantService.updatePromotion(rp, id);
            msg = "success";
        }
        return msg;
    }

    /**
     * 删除餐厅的某一个优惠政策
     */
    @RequestMapping(value = "/restaurant/manager/{id}/promotion/{promotionId}/remove", method = RequestMethod.GET)
    public String removePromotion(@PathVariable int id, @PathVariable int promotionId, RedirectAttributes redirectAttributes) {
        if (promotionId <= 0) {
            throw new IllegalArgumentException("id参数不合法");
        }
        restaurantService.removePromotion(promotionId, id);
        redirectAttributes.addFlashAttribute(Remind.success().appendMessage("成功删除"));
        return "redirect:/restaurant/manager/" + id + "/promotion/";
    }

}
