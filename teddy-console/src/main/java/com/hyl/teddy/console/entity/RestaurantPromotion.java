package com.hyl.teddy.console.entity;

/**
 * 餐厅优惠政策 restaurant_promotion
 * 
 * @author Administrator
 * 
 */
public class RestaurantPromotion {
	private int id;
	private int restaurantId;
	private String restaurantName;
	private String pattern;
	private int param0;
	private int param1;
	private int param2;
	private int param3;

	
	private String tag;
	
	private String description;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(int restaurantId) {
		this.restaurantId = restaurantId;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public int getParam0() {
		return param0;
	}

	public void setParam0(int param0) {
		this.param0 = param0;
	}

	public int getParam1() {
		return param1;
	}

	public void setParam1(int param1) {
		this.param1 = param1;
	}

	public int getParam2() {
		return param2;
	}

	public void setParam2(int param2) {
		this.param2 = param2;
	}

	public int getParam3() {
		return param3;
	}

	public void setParam3(int param3) {
		this.param3 = param3;
	}

	public String getRestaurantName() {
		return restaurantName;
	}

	public void setRestaurantName(String restaurantName) {
		this.restaurantName = restaurantName;
	}

	public String getDescription() {
		if ("WIN_CASH".equals(pattern)) {
			return "满" + param0 + "元送" + param1 + "张" + param2 + "元代金券";
		}
		if ("VIP_DISCOUNT".equals(pattern)) {
			return "会员优惠" + param0 + "折 ";
		}
		if ("COST_CASH".equals(pattern)) {
			return "用户可使用一张代金券";
		}
		if ("COST_SCORE".equals(pattern)) {
			return "满" + param0 + "元可用" + param1 + "积分抵" + param2 + "元现金";
		}
		if ("FREE_POSTAGE".equals(pattern)) {
			return "满" + param0 + "元免邮费 ";
		}
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTag() {
		if ("WIN_CASH".equals(pattern)) {
			return "返";
		}
		if ("VIP_DISCOUNT".equals(pattern)) {
			return "VIP ";
		}
		if ("COST_CASH".equals(pattern)) {
			return "券";
		}
		if ("COST_SCORE".equals(pattern)) {
			return "积";
		}
		if ("FREE_POSTAGE".equals(pattern)) {
			return "免 ";
		}
		return "";
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getParams(){
		return param0 + "," + param1 + "," + param2 + "," + param3;
	}
	

}
