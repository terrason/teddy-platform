/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.console.attachment.web;

import com.hyl.teddy.console.AjaxResponse;
import com.hyl.teddy.console.ResourceNotFoundException;
import com.hyl.teddy.console.attachment.AttachmentService;
import com.hyl.teddy.console.attachment.PathInfo;
import com.hyl.teddy.console.util.CnToSpell;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import javax.annotation.Resource;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件上传.
 *
 * @author lip
 */
@Controller
public class AttachmentController {

    private final Logger logger = LoggerFactory.getLogger(AttachmentController.class);
    @Resource
    private AttachmentService attachmentService;

    @RequestMapping(value = "/ajax/upfile", method = RequestMethod.POST, produces = "text/json;charset=UTF-8")
    @ResponseBody
    public AjaxResponse upfile(@RequestParam("upfile") MultipartFile file) {
        AjaxResponse response = new AjaxResponse();
        if (file.isEmpty()) {
            response.setCode(HttpStatus.NO_CONTENT.value());
            response.setMessage("没有上传任何文件");
            return response;
        }
        String fileOriginalFilename = CnToSpell.getFullSpell(file.getOriginalFilename());
        PathInfo pathInfo = attachmentService.createPath(fileOriginalFilename);
        File directory = pathInfo.getFile();
        String webPath = pathInfo.getUrl();
        if (!directory.exists()) {
            directory.mkdirs();
        }

        InputStream inputStream = null;
        try {
            inputStream = file.getInputStream();
            File repositoryFile = new File(directory, fileOriginalFilename);
            repositoryFile.createNewFile();
            FileUtils.copyInputStreamToFile(inputStream, repositoryFile);
            int id = attachmentService.createTemporaryAttachment(pathInfo.newFile(repositoryFile));
            response.setValue(webPath + "/" + fileOriginalFilename);
            response.setSize(repositoryFile.length());
            response.setId(id);
            return response;
        } catch (IOException e) {
            response.setCode(HttpStatus.SERVICE_UNAVAILABLE.value());
            response.setMessage(e.getMessage());
            return response;
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException ex) {
                logger.warn("未知的错误", ex);
            }
        }
    }

    @RequestMapping(value = "/ajax/upfile/{id}", method = RequestMethod.GET)
    public String view(@PathVariable int id) throws ResourceNotFoundException {
        String redirectUrl = attachmentService.view(id);
        if (StringUtils.isBlank(redirectUrl)) {
            throw new ResourceNotFoundException("找不到指定的附件【id=" + id + "】");
        }
        if (redirectUrl.startsWith("/")) {
            redirectUrl = attachmentService.getStaticResourceHost() + redirectUrl;
        }
        return "redirect:" + redirectUrl;
    }
}
