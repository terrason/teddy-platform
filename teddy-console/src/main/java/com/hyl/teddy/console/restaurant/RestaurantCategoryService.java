package com.hyl.teddy.console.restaurant;

import com.hyl.teddy.console.attachment.AttachmentService;
import com.hyl.teddy.console.entity.DishCategory;
import com.hyl.teddy.console.entity.RestaurantCategory;
import com.hyl.teddy.console.pager.Pager;
import com.hyl.teddy.console.restaurant.mybatis.RestaurantCategoryMapper;
import java.util.Collections;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 *
 * @author Administrator
 */
@Service
public class RestaurantCategoryService {

    @Resource
    private RestaurantCategoryMapper categoryMapper;
    @Resource
    private AttachmentService attachmentService;

    /**
     * 分类列表
     *
     * @return
     */
    public Pager<RestaurantCategory> queryRestaurantCategoryList() {
        Pager<RestaurantCategory> pager = new Pager<RestaurantCategory>();
        List<RestaurantCategory> entiters = categoryMapper.queryRestaurantCategoryList();
        pager.setElements(entiters);
        return pager;
    }

    /**
     * 删除分类
     *
     * @param id
     */
    public void removeCategory(int id) {
        attachmentService.changeAttachmentTemporary(categoryMapper.queryAttachment(id), true);
        categoryMapper.deleteCategory(id);

    }
    /**
     * 批量删除分类
     *
     * @param id
     */
    public void removeCategoryByPks(int[] pks){
        attachmentService.changeAttachmentTemporary(queryAttachments(pks), true);
        categoryMapper.deleteCategoryByPks(pks);
    }

    /**
     * 根据id查询分类
     *
     * @param id
     * @return
     */
    public RestaurantCategory findCategoryById(int id) {
        return categoryMapper.findCategoryById(id);
    }

    /**
     * 修改分类
     *
     * @param entity
     */
    public boolean updateCategory(RestaurantCategory entity) {
        try {
            categoryMapper.updateCategory(entity);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    /**
     * 新增分类
     *
     * @param entity
     */
    public boolean createCategory(RestaurantCategory entity) {
        try {
            categoryMapper.createCategory(entity);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public List<RestaurantCategory> queryAllCategories(int id) {
        return categoryMapper.queryAllCategories(id);
    }
    
    private String queryAttachments(int[] pks) {
        List<RestaurantCategory> attachments = categoryMapper.queryAttachments(pks);
        StringBuilder ids = new StringBuilder();
        for (RestaurantCategory attachment : attachments) {
            ids.append(",").append(attachment.getIcon());
        }
        if (ids.length() > 0) {
            ids.deleteCharAt(0);
        }
        return ids.toString();
    }
}
