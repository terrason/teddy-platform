package com.hyl.teddy.console.system.region;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.hyl.teddy.console.entity.Point;
import com.hyl.teddy.console.entity.Region;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;
import com.hyl.teddy.console.system.region.mybatis.RegionMapper;
import com.hyl.teddy.console.system.region.web.Area;
import com.hyl.teddy.console.util.CacheEvictor;

@Service
@Transactional
public class RegionService {

    @Resource
    private RegionMapper mapper;
    @Resource
    private CacheEvictor cacheEvictor;

    /**
     * 区域列表
     *
     * @param lookup
     * @return
     */
    public Pager<Region> queryList(Lookup lookup) {
        Pager<Region> pager = new Pager<Region>();
        pager.setPage(lookup.getPage());
        pager.setSize(lookup.getSize());
        int total = mapper.countList(lookup);

        pager.setTotal(total);
        List<Region> regions = pager.isOverflowed() ? Collections.EMPTY_LIST : mapper.selectlist(lookup);
        pager.setElements(regions);
        return pager;
    }

    /**
     * 查询出全部区域
     *
     * @return
     */
    public List<Area> findAllRegion() {
        List<Region> regions = mapper.selectAllRegion();
        List<Point> points = converCoordinate(mapper.seletcPoints());
        regions = findRegion(regions, points);
        List<Area> list = new ArrayList<Area>();
        for (Region re : regions) {
            Area a = regionToArea(re);
            list.add(a);
        }
        return list;
    }

    /**
     * 查询当前区域外的其他区域
     *
     * @param id 当前区域id
     * @return
     */
    public List<Area> findOtherRegions(int id) {
        List<Area> areas = findAllRegion();//全部区域
        if (id == 0) {
            return areas;
        } else {//去掉当前的区域
            List<Area> result = new ArrayList<Area>();
            for (Area a : areas) {
                if (a.getId() != id) {
                    result.add(a);
                }
            }
            return result;
        }
    }

    //把区域的顶点放入区域中
    public List<Region> findRegion(List<Region> regions, List<Point> points) {
        for (Region r : regions) {
            int regionId = r.getId();
            List<Point> regionPointList = new ArrayList<Point>();
            for (Point p : points) {
                if (regionId == p.getRegionId()) {
                    regionPointList.add(p);
                }
            }
            Point[] ps = regionPointList.toArray(new Point[regionPointList.size()]);
            r.setPoints(ps);
        }
        return regions;
    }

    /**
     * 根据id查询区域
     *
     * @param id
     * @return
     */
    public Region findRegionById(int id) {
        Region region = new Region();
        List<Point> points = converCoordinate(mapper.findPointsById(id));
        if (!points.isEmpty()) {
            region.setId(points.get(0).getRegionId());
            region.setName(points.get(0).getRegionName());
            Iterator<Point> it = points.iterator();
            while (it.hasNext()) {
                Point p = it.next();
                if (p.getId() == 0) {
                    it.remove();
                }
            }
            region.setPoints(points.toArray(new Point[points.size()]));
        }

        return region;
    }

    /**
     * for 编辑页面
     *
     * @param id
     * @return
     */
    public Area forEdit(int id) {
        Region region = findRegionById(id);
        Area area = regionToArea(region);
        return area;
    }

    /**
     * region 转化为Area
     *
     * @param id
     * @return
     */
    public Area regionToArea(Region r) {
        Area area = new Area();
        Point[] ps = r.getPoints();
        StringBuilder xy = new StringBuilder();
        for (int i = 0; i < ps.length; i++) {
            if (i == 0) {
                xy.append(ps[i].getLongitude()).append("-").append(ps[i].getLatitude());
            } else {
                xy.append(",").append(ps[i].getLongitude()).append("-").append(ps[i].getLatitude());
            }
        }
        area.setId(r.getId());
        area.setAreaName(r.getName());
        area.setXy(xy.toString());
        return area;
    }

    /**
     * 转换坐标 int to String
     *
     * @param list
     * @return
     */
    public List<Point> converCoordinate(List<Point> list) {
        for (Point p : list) {
            int x = p.getX();
            int y = p.getY();
            if (x != 0) {
                p.setLongitude(String.format("%.6f", x * Math.pow(10, -6)));
            }
            if (y != 0) {
                p.setLatitude(String.format("%.6f", y * Math.pow(10, -6)));
            }
        }
        return list;
    }

    /**
     * 新增区域
     *
     * @param name
     * @param points
     */
    @Transactional
    public void addRegion(String name, String points) {
        Region r = new Region();
        r.setName(name);
        mapper.insertRegion(r);
        int regionId = r.getId();
        List<Point> list = converToPoint(regionId, points);
        mapper.insertPoints(list);
        cacheEvictor.evictCache("regions", null);
    }

    /**
     * 更新区域
     *
     * @param id
     * @param name
     * @param points
     */
    @Transactional
    public void updateRegion(int id, String name, String points) {
        mapper.updateRegion(id, name);
        mapper.deleteOldPoint(id);
        List<Point> list = converToPoint(id, points);
        mapper.insertPoints(list);
        cacheEvictor.evictCache("regions", null);
    }

    /**
     * 坐标转化 String To int
     *
     * @param regionId
     * @param points
     * @return
     */
    public List<Point> converToPoint(int regionId, String points) {
        String[] pointArr = points.split(",");
        List<Point> list = new ArrayList<Point>();
        for (String pointArr1 : pointArr) {
            String[] strs = pointArr1.split("-");
            if (strs.length == 2) {
                Point p = new Point();
                p.setRegionId(regionId);
                String strX = strs[0].replace(".", "");
                String strY = strs[1].replace(".", "");
                if (strX.length() < 9) {
                    int subtractX = 9 - strX.length();
                    for (int j = 0; j < subtractX; j++) {
                        strX += 0;
                    }
                }
                if (strY.length() < 8) {
                    int subtractY = 8 - strY.length();
                    for (int k = 0; k < subtractY; k++) {
                        strY += 0;
                    }
                }
                int x = Integer.parseInt(strX);
                int y = Integer.parseInt(strY);
                p.setX(x);
                p.setY(y);
                list.add(p);
            }
        }
        return list;
    }

    /**
     * 删除区域
     *
     * @param id
     */
    @Transactional
    public void removeRegion(int id) {
        mapper.deleteRegion(id);
        mapper.deleteOldPoint(id);
        cacheEvictor.evictCache("regions", null);
    }
}
