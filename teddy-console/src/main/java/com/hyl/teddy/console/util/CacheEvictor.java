package com.hyl.teddy.console.util;

import org.codehaus.jackson.map.ObjectMapper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Value;

import com.hyl.teddy.console.BaseResponse;
import com.hyl.teddy.console.IORuntimeException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Connection;
import org.springframework.stereotype.Service;

@Service
public class CacheEvictor {

    @Value("#{settings['url.cache']}")
    private String host;

    public void evictCache(String cache, Object key) {
        try {
            System.out.println("EvictCacheForApis");
            Connection connect = Jsoup.connect(host + cache);
            if (key != null) {
                connect.data("key", key.toString());
            }
            Document document = connect.ignoreContentType(true).timeout(30000).post();
            String text = document.body().text();
            BaseResponse b = new ObjectMapper().readValue(text, BaseResponse.class);
            if (b.getCode() != 0) {
                throw new RuntimeException(b.getMessage());
            }
        } catch (IOException ex) {
            throw new IORuntimeException("清空缓存接口调用失败", ex);
        }
    }
}
