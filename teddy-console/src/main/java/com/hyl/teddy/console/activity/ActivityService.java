package com.hyl.teddy.console.activity;

import com.hyl.teddy.console.activity.mybatis.ActivityMapper;
import com.hyl.teddy.console.entity.Activity;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;
import java.util.Collections;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * 活动Service
 * @author Administrator
 */
@Service
public class ActivityService {

    @Resource
    private ActivityMapper activityMapper;

    /**
     * 分页查询
     *
     * @param lookup
     * @return
     */
    public Pager<Activity> queryActivity(Lookup lookup) {
        Pager<Activity> pager = new Pager<>();
        int total = activityMapper.countActivity(lookup);
        pager.setPage(lookup.getPage());
        pager.setSize(lookup.getSize());
        pager.setTotal(total);
        List<Activity> entiters = pager.isOverflowed() ? Collections.EMPTY_LIST : activityMapper.selectActivityList(lookup);
        pager.setElements(entiters);
        return pager;
    }

    /**
     * 删除活动
     *
     * @param id
     */
    public void removeActivity(int id) {
        activityMapper.deleteActivity(id);
    }

    /**
     * 批量删除
     */
    public void removeActivity(int[] pks) {
        activityMapper.deleteActivityByPks(pks);

    }

    /**
     * 根据id查询活动
     *
     * @param id
     * @return
     */
    public Activity findActivityById(int id) {
        return activityMapper.findActivityById(id);
    }

    /**
     * 修改活动
     *
     * @param entity
     */
    public void updateActivity(Activity entity) {
       activityMapper.updateActivity(entity);
    }

    /**
     * 新增活动
     *
     * @param entity
     */
    public void createActivity(Activity entity) {
        activityMapper.createActivity(entity);
    }
}
