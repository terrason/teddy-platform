package com.hyl.teddy.console.system.version.mybatis;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.hyl.teddy.console.MybatisMapper;
import com.hyl.teddy.console.entity.Version;
import com.hyl.teddy.console.lookup.Lookup;

@MybatisMapper
public interface VersionMapper {

	public List<Version> queryVersionList(@Param("lookup")Lookup lookup, @Param("table")String table);

	public int queryCount(@Param("table")String table);

	public void delete(@Param("id")int id, @Param("table")String table);

	public void insert(@Param("entity")Version entity,@Param("table") String table);

	public Version findVersion(@Param("id")int id, @Param("table")String table);

	public void update(@Param("entity")Version entity,@Param("table") String table);

}
