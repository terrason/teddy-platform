package com.hyl.teddy.console.dish;

import java.util.Collections;
import java.util.List;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hyl.teddy.console.attachment.AttachmentService;
import com.hyl.teddy.console.dish.mybaties.DishManagerMapper;
import com.hyl.teddy.console.entity.Dish;
import com.hyl.teddy.console.favoraties.mybatis.FavoratiesMapper;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;

@Service
public class DishManagerService {

    @Resource
    private DishManagerMapper dishManagerMapper;

    @Resource
    private AttachmentService attrchmentService;
    
    @Resource
    private FavoratiesMapper favoratiesMapper;

    /**
     * 分页查询
     *
     * @param lookup
     * @return
     */
    public Pager<Dish> queryDishList(Lookup lookup) {
        Pager<Dish> pager = new Pager<Dish>();
        int total = dishManagerMapper.countDish(lookup);
        pager.setPage(lookup.getPage());
        pager.setSize(lookup.getSize());
        pager.setTotal(total);
        List<Dish> entiters = pager.isOverflowed() ? Collections.EMPTY_LIST : dishManagerMapper.selectDishList(lookup);
        pager.setElements(entiters);
        return pager;
    }

    /**
     * 关闭菜品
     */
    public void closeDish(int id) {
        dishManagerMapper.closeDish(id);

    }

    /**
     * 打开菜品
     */
    public void openDish(int id) {
        dishManagerMapper.openDish(id);

    }

    /**
     * 删除菜品
     *
     * @param id
     */
    @Transactional
    public void removeDish(int id) {
        Dish dish = findDishById(id);
        if (dish != null) {
            attrchmentService.changeAttachmentTemporary(dish.getImage(), true);
        }
        dishManagerMapper.removeDish(id);
        favoratiesMapper.removeFavoraties(id, 2);

    }

    /**
     * 批量删除
     */
    @Transactional
    public void removeDishes(int[] pks) {
        String attachmentIds = queryAttachments(pks);
        attrchmentService.changeAttachmentTemporary(attachmentIds, true);
        dishManagerMapper.removeDishes(pks);
        favoratiesMapper.batchRemoveFavoraties(pks, 2);
                
    }

    public String queryAttachments(int... id) {
        List<Dish> list = dishManagerMapper.queryAttachments(id);
        StringBuilder pks = new StringBuilder();
        for (Dish dish : list) {
            pks.append(",").append(dish.getImage());
        }
        if (pks.length() > 0) {
            pks.deleteCharAt(0);
        }
        return pks.toString();
    }

    /**
     * 根据id查询菜品
     *
     * @param id
     * @return
     */
    public Dish findDishById(int id) {
        return dishManagerMapper.findDishById(id);
    }

    /**
     * 修改菜品
     *
     * @param entity
     */
    public boolean updateDish(Dish entity) {
        try {
            Dish old = findDishById(entity.getId());
            //当图标改变时
            if (entity.getImage() != old.getImage()) {
                attrchmentService.changeAttachmentTemporary(old.getImage(), true);
                attrchmentService.changeAttachmentTemporary(entity.getImage(), true);
            }
            dishManagerMapper.updateDish(entity);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    /**
     * 新增菜品
     *
     * @param entity
     */
    @Transactional
    public boolean createDish(Dish entity) {
        try {
            dishManagerMapper.createDish(entity);
            attrchmentService.changeAttachmentTemporary(entity.getImage(), true);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    /**
     * 批量关闭菜品
     */
    public boolean closeDishes(int[] pks) {
        try {
            dishManagerMapper.closeDishes(pks);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    /**
     * 批量打开菜品
     */
    public boolean openDishes(int[] pks) {
        try {
            dishManagerMapper.openDishes(pks);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    /**
     * 根据餐厅ID查询dish
     *
     * @param id
     * @return
     */
    public List<Dish> queryAllDishesByRestaurantId(int id) {
        return dishManagerMapper.queryAllDishesByRestaurantId(id);
    }

    /**
     * 查询dish
     *
     * @param id
     * @return
     */
    public List<Dish> queryAllDishes() {
        return dishManagerMapper.queryAllDishes();
    }

    /**
     * 设置餐厅热门菜
     *
     * @param hotDishIds 菜品ID列表
     * @param id 餐厅ID
     */
    public void setRestaurantHots(int[] hotDishIds, int id) {
        dishManagerMapper.clearRestaurantHots(id);
        dishManagerMapper.updateDishHot(hotDishIds);
    }

}
