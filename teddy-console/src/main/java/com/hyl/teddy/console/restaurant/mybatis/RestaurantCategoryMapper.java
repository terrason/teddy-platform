package com.hyl.teddy.console.restaurant.mybatis;

import com.hyl.teddy.console.MybatisMapper;
import com.hyl.teddy.console.entity.DishCategory;
import com.hyl.teddy.console.entity.RestaurantCategory;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 餐厅类别
 *
 * @author Administrator
 */
@MybatisMapper
public interface RestaurantCategoryMapper {

    /**
     * 餐厅类别列表
     *
     * @param params
     * @return
     */
    public List<RestaurantCategory> queryRestaurantCategoryList();
    /**
     * 删除
     * @param id 
     */
    public void deleteCategory(@Param("id") int id);
    
    public void deleteCategoryByPks(@Param("pks") int[] pks);

    public RestaurantCategory findCategoryById(@Param("id") int id);

    public void updateCategory(RestaurantCategory entity);

    public void createCategory(RestaurantCategory entity);

    public List<RestaurantCategory> queryAllCategories(@Param("id") int id);
    
    public int queryAttachment(@Param("id") int id);
    
    public List<RestaurantCategory> queryAttachments(@Param("pks") int[] pks);
}
