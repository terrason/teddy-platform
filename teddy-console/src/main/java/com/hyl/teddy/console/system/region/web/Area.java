package com.hyl.teddy.console.system.region.web;

public class Area {
	private int id;
	private String areaName;//区域名称
	/**
	 * 存放x-y,x-y,...
	 */
	private String xy;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getXy() {
		return xy;
	}
	public void setXy(String xy) {
		this.xy = xy;
	}
	
}
