package com.hyl.teddy.console.dish.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hyl.teddy.console.AbstractController;
import com.hyl.teddy.console.Remind;
import com.hyl.teddy.console.dish.DishCategoryService;
import com.hyl.teddy.console.dish.DishLookup;
import com.hyl.teddy.console.dish.DishManagerService;
import com.hyl.teddy.console.entity.Dish;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;
import com.hyl.teddy.console.restaurant.RestaurantService;

@Controller
@RequestMapping
public class DishMamagerController extends AbstractController {

	@Resource
	private DishManagerService dishManagerService;
	
	@Resource
	private DishCategoryService dishCategoryService; 
	
	@Resource
	private RestaurantService restaurantService;

	/**
	 * 进入菜品 主页面列表
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/dish/manager/", method = RequestMethod.GET)
	public String home(Model model) {
		Pager<Dish> pager = dishManagerService.queryDishList(getLookup());
		model.addAttribute("pager", pager);
		model.addAttribute("restaurants",restaurantService.queryAllRestaurant());
		model.addAttribute("dishCategories", dishCategoryService.queryAllCategories());
		getLookup().setTotal(pager.getTotal());
		return "dish/manager/list";
	}

	@Override
	protected Lookup instanceLookup() {
		// 封装查询条件
		return new DishLookup();
	}
	
	/**
	 *  接受用户提交的查询信息
	 * @param lookup
	 * @return
	 */
	@RequestMapping(value = "/dish/manager/", method = RequestMethod.POST)
	public String search(DishLookup lookup) {
		setLookup(lookup);
		return "redirect:/dish/manager/";
	}


	
	/**
	 *  关闭菜品
	 * @param id
	 * @param redirectAttrs
	 * @return
	 */
	@RequestMapping(value = "/dish/manager/{id}/close",method = RequestMethod.POST)
	public String close(@PathVariable int id,RedirectAttributes redirectAttrs) {
		if(id < 0) {
			throw new IllegalArgumentException("id 参数不合法");
		}
		dishManagerService.closeDish(id);
		redirectAttrs.addFlashAttribute(Remind.warning().appendMessage("该菜品被关闭！"));
		return "redirect:/dish/manager/";
	}
	
	/**
	 *  打开菜品
	 * @param id
	 * @param redirectAttrs
	 * @return
	 */
	@RequestMapping(value = "/dish/manager/{id}/open",method = RequestMethod.POST)
	public String open(@PathVariable int id,RedirectAttributes redirectAttrs) {
		if(id < 0) {
			throw new IllegalArgumentException("id 参数不合法");
		}
		dishManagerService.openDish(id);
		redirectAttrs.addFlashAttribute(Remind.warning().appendMessage("该菜品被打开！"));
		return "redirect:/dish/manager/";
	}
	
	

	
	/**
	 * 删除菜品
	 * @param id
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping( value ="dish/manager/{id}/remove", method = RequestMethod.POST)
	public String remove(@PathVariable int id, RedirectAttributes redirectAttributes) {
		if(id<= 0) {
			throw new IllegalArgumentException("id参数不合法");
		}
		dishManagerService.removeDish(id);
		redirectAttributes.addFlashAttribute(Remind.warning().appendMessage("成功删除"));
		return "redirect:/dish/manager/";
	}
	/**
	 * 批量删除菜品
	 * @param id
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping( value ="dish/manager/remove", method = RequestMethod.POST)
	public String removes(@RequestParam int[] pks, RedirectAttributes redirectAttributes) {
		if(pks.length <= 0) {
			throw new IllegalArgumentException("id参数不合法");
		}
		dishManagerService.removeDishes(pks);
		redirectAttributes.addFlashAttribute(Remind.warning().appendMessage("批量删除成功"));
		return "redirect:/dish/manager/";
	}
	
	
	/**
	 *   To add
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/dish/manager/0", method = RequestMethod.GET)
	public String input(Model model) {
		model.addAttribute("dishCategories",dishCategoryService.queryAllCategories());
		model.addAttribute("restaurants",restaurantService.queryAllRestaurant());
		Dish dish = new Dish();
		dish.setPrice(10.00);
		dish.setSold(100);
		model.addAttribute("entity", dish);
		return "dish/manager/form"; 
	}
	
	/**
	 * To edit
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/dish/manager/{id}", method = RequestMethod.GET)
	public String edit(@PathVariable int id, Model model) {
		if(id <=0 ){
			throw new IllegalArgumentException("id参数不合法");
		}
		Dish dish = dishManagerService.findDishById(id);
		model.addAttribute("entity", dish);
		model.addAttribute("restaurants",restaurantService.queryAllRestaurant());
		model.addAttribute("dishCategories", dishCategoryService.queryAllCategories());
		return 	"dish/manager/form";
	}
	
	/**
	 *  保存菜品
	 * @param id
	 * @param entity
	 * @param redirectAttrs
	 * @return
	 */
	@RequestMapping( value = "/dish/manager/{id}", method = RequestMethod.POST)
	public String save(@PathVariable int id, Dish entity, RedirectAttributes redirectAttrs) {
		entity.setId(id);
		if(id > 0) {
			if(dishManagerService.updateDish(entity)){
				redirectAttrs.addFlashAttribute(Remind.success().appendMessage("修改成功。"));
			}else {
				redirectAttrs.addFlashAttribute(Remind.warning().appendMessage("修改失败。"));
			}
			
		}else if (id == 0 ) {
			if(dishManagerService.createDish(entity)){
				redirectAttrs.addFlashAttribute(Remind.success().appendMessage("创建成功。"));
			}else {
				redirectAttrs.addFlashAttribute(Remind.warning().appendMessage("创建失败。"));
			}
			
		}else {
            throw new IllegalArgumentException("信息不合法");
		}
		return "redirect:/dish/manager/";
	}
	
	
	/**
	 *  批量关闭菜品
	 * @param id
	 * @param redirectAttrs
	 * @return
	 *//*
	@RequestMapping(value = "/dish/manager/close",method = RequestMethod.POST)
	public String closePks(@RequestParam int[] pks,RedirectAttributes redirectAttrs) {
		dishManagerService.closeDishes(pks);
		redirectAttrs.addFlashAttribute(Remind.warning().appendMessage(pks.length + "道菜品被关闭！"));
		return "redirect:/dish/manager/";
	}
	
	*//**
	 *  批量打开菜品
	 * @param id
	 * @param redirectAttrs
	 * @return
	 *//*
	@RequestMapping(value = "/dish/manager/open",method = RequestMethod.POST)
	public String openPks(@RequestParam int[] pks,RedirectAttributes redirectAttrs) {
		dishManagerService.openDishes(pks);
		redirectAttrs.addFlashAttribute(Remind.warning().appendMessage(pks.length + "道菜品均被打开！"));
		return "redirect:/dish/manager/";
	}
*/
}
