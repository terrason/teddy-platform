package com.hyl.teddy.console.system.feedback.web;

import com.hyl.teddy.console.AbstractController;
import com.hyl.teddy.console.Remind;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;
import com.hyl.teddy.console.entity.Feedback;
import com.hyl.teddy.console.system.feedback.FeedbackLookup;
import com.hyl.teddy.console.system.feedback.FeedbackService;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class FeedbackController extends AbstractController {

    @Resource
    private FeedbackService feedbackService;

    /**
     * 跳转到列表页面.
     */
    @RequestMapping(value = "/system/feedback/", method = RequestMethod.GET)
    public String home(Model model) {
        Pager<Feedback> pager = feedbackService.queryFeedbackList(getLookup());
        model.addAttribute("pager", pager);

        getLookup().setTotal(pager.getTotal());
        return "system/feedback/list";
    }

    @Override
    protected Lookup instanceLookup() {
        return new FeedbackLookup();
    }

    /**
     * 接收用户提交的查询信息.
     */
    @RequestMapping(value = "/system/feedback/", method = RequestMethod.POST)
    public String search(FeedbackLookup lookup) {
        setLookup(lookup);
        return "redirect:/system/feedback/";
    }

    /**
     * 跳转到查看页面.
     */
    @RequestMapping(value = "/system/feedback/{id}", method = RequestMethod.GET)
    public String view(@PathVariable int id, Model model) {
        if (id <= 0) {
            throw new IllegalArgumentException("id 参数不合法");
        }
        Feedback entity = feedbackService.findFeedbackById(id);
        model.addAttribute("entity", entity);
        model.addAttribute("content", feedbackService.selectReplyById(id));

        feedbackService.saveFeedbackStatus(true, id);
        return "system/feedback/view";
    }
    /**
     * 回复
     * @param id
     * @param content
     * @return 
     */
    @RequestMapping(value = "/system/feedback/{id}", method = RequestMethod.POST)
    public String save(@PathVariable int id,  String content,RedirectAttributes redirectAttrs){
        if(feedbackService.reply(id, content)){
            redirectAttrs.addFlashAttribute(Remind.success().appendMessage("回复成功！"));
        }else{
            redirectAttrs.addFlashAttribute(Remind.warning().appendMessage("回复失败！"));
        }
        return "redirect:/system/feedback/";
    }

    @RequestMapping(value = "/system/feedback/{id}/remove", method = RequestMethod.POST)
    public String remove(@PathVariable int id, RedirectAttributes redirectAttrs) {
        if (id <= 0) {
            throw new IllegalArgumentException("id 参数不合法");
        }
        feedbackService.removeFeedback(id);
        redirectAttrs.addFlashAttribute(Remind.warning().appendMessage("客户反馈信息已删除"));
        return "redirect:/system/feedback/";
    }

    @RequestMapping(value = "/system/feedback/remove", method = RequestMethod.POST)
    public String remove(@RequestParam int[] pks, RedirectAttributes redirectAttrs) {
        feedbackService.removeFeedbackByPks(pks);
        redirectAttrs.addFlashAttribute(Remind.warning().appendMessage(pks.length + "个客户反馈信息成功删除"));
        return "redirect:/system/feedback/";
    }
}
