package com.hyl.teddy.console.entity;

import java.util.Date;

import org.springframework.format.annotation.NumberFormat;

/**
 * 订单状态变动记录表 payment_times
 * @author Administrator
 *
 */
public class PaymentTimes {
	private int id;
	private int paymentId;//订单id
	private int status;//订单状态
	private Date createTime;//创建时间
	private String description;//订单状态描述
	private int costChange;
	
	@NumberFormat(pattern="#.##")
	public double getCostChange() {
		return 0.01*costChange;
	}
	public void setCostChange(int costChange) {
		this.costChange = costChange;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(int paymentId) {
		this.paymentId = paymentId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	//订单状态 0-未付款 1-支付失败 2-未指派 3-配餐中 4- 缺失菜品 5-配餐完成 6-配送中 8-已完成 9- 已取消
  	public String getOrderStatus() {
  		switch (status) {
  		case 0:
  			return "未付款";
  		case 1:
  			return "支付失败";
  		case 2:
  			return "未指派";
  		case 3:
  			return "配餐中";
  		case 4:
  			return "缺失菜品";
  		case 5:
  			return "配餐完成";
  		case 6:
  			return "配送中";
  		case 8:
  			return "已完成";
  		case 9:
  			return "已取消";	

  		}
  				
  		return "";
  	}
}
