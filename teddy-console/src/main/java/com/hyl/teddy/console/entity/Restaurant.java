package com.hyl.teddy.console.entity;

import java.util.Calendar;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 餐厅表 restaurant
 *
 * @author Administrator
 *
 */
public class Restaurant {

    private int id;
    private int categoryId;//分类
    private int[] categoryPks;
    private int[] dishPks;
    private String categoryName;
    private String name;//名称
    private Integer icon;//图标
    private String images;//所有图片存放在附件表，此处存放附件ID，多个用","分隔'
    private int price;//餐厅价位
    private String promotion;//促销信息
    @DateTimeFormat(pattern = "HH:mm")
    private Date openTime;//营业时间
    @DateTimeFormat(pattern = "HH:mm")
    private Date closeTime;//歇业时间
    private String description;//描述
    private String location;//地点
    private String tel;
    private int star;//星级
    private boolean enabled;//
    private boolean hotDefined;//是否自定义热菜

    private int packageFee;//打包费  （单位分）
    private int delivery; //送餐费 单位分  
    private int sold;//销量   

    public int getStatus() {
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH);
        int day = now.get(Calendar.DAY_OF_MONTH);
        Calendar open = Calendar.getInstance();
        open.setTime(openTime);
        open.set(Calendar.YEAR, year);
        open.set(Calendar.MONTH, month);
        open.set(Calendar.DAY_OF_MONTH, day);
        Calendar close = Calendar.getInstance();
        close.setTime(closeTime);
        close.set(Calendar.YEAR, year);
        close.set(Calendar.MONTH, month);
        close.set(Calendar.DAY_OF_MONTH, day);
        return enabled ? (now.after(open) && now.before(close) ? 1 : 2) : 0;//1--营业中  2--休息中  0--已关闭
    }

    public String getStatusName() {
        switch (getStatus()) {
            case 1:
                return "营业中";
            case 2:
                return "休息中";
            case 0:
                return "已关闭";
        }
        return null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIcon() {
        return icon;
    }

    public void setIcon(Integer icon) {
        this.icon = icon;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getPromotion() {
        return promotion;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }

    public Date getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Date openTime) {
        this.openTime = openTime;
    }

    public Date getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(Date closeTime) {
        this.closeTime = closeTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public boolean isHotDefined() {
        return hotDefined;
    }

    public void setHotDefined(boolean hotDefined) {
        this.hotDefined = hotDefined;
    }

    public int getPackageFee() {
        return packageFee;
    }

    public void setPackageFee(int packageFee) {
        this.packageFee = packageFee;
    }

    public int getDelivery() {
        return delivery;
    }

    public void setDelivery(int delivery) {
        this.delivery = delivery;
    }

    public int getSold() {
        return sold;
    }

    public void setSold(int sold) {
        this.sold = sold;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int[] getCategoryPks() {
        return categoryPks;
    }

    public void setCategoryPks(int[] categoryPks) {
        this.categoryPks = categoryPks;
    }

    public int[] getDishPks() {
        return dishPks;
    }

    public void setDishPks(int[] dishPks) {
        this.dishPks = dishPks;
    }

}
