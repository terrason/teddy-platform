package com.hyl.teddy.console.payment.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hyl.teddy.console.AbstractController;
import com.hyl.teddy.console.Remind;
import com.hyl.teddy.console.entity.Employee;
import com.hyl.teddy.console.entity.Payment;
import com.hyl.teddy.console.lookup.Lookup;
import com.hyl.teddy.console.pager.Pager;
import com.hyl.teddy.console.payment.GlobalPaymentService;
import com.hyl.teddy.console.payment.PaymentUnallotLookup;
import com.hyl.teddy.console.payment.PaymentUnallotService;
import com.hyl.teddy.console.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 未分配订单
 *
 * @author Administrator
 *
 */
@Controller
public class PaymentUnallotController extends AbstractController {

    private final Logger logger = LoggerFactory.getLogger(PaymentUnallotController.class);
    @Resource
    private PaymentUnallotService unallotService;
    @Resource
    private GlobalPaymentService globalPaymentService;
    @Resource
    private UserService userService;

    private final String ALLOT_DELIVER = "分配取菜员";
    private final String ALLOT_COURIER = "分配配送员";

    /**
     * 需分配送员的订单列表
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/payment/unallot/courier", method = RequestMethod.GET)
    public String courierHome(Model model) {
        Pager<PaymentUnallot> pager = unallotService.queryCourierPaymentList(getLookup());
        model.addAttribute("pager", pager);
        model.addAttribute("region", userService.findAllRegion());
        getLookup().setTotal(pager.getTotal());

        return "payment/unallot/courier/list";
    }

    /**
     * 需分配取菜员的订单列表
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/payment/unallot/deliver", method = RequestMethod.GET)
    public String deliverHome(Model model) {
        Pager<PaymentUnallot> pager = unallotService.queryDeliverPaymentList(getLookup());
        model.addAttribute("pager", pager);
        getLookup().setTotal(pager.getTotal());

        return "payment/unallot/deliver/list";
    }

    @Override
    protected Lookup instanceLookup() {
        return new PaymentUnallotLookup();
    }

    /**
     * search 配送员页面
     */
    @RequestMapping(value = "/payment/unallot/courier", method = RequestMethod.POST)
    public String searchCourier(PaymentUnallotLookup lookup) {
        setLookup(lookup);
        return "redirect:/payment/unallot/courier/";
    }

    /**
     * search 取菜员页面
     */
    @RequestMapping(value = "/payment/unallot/deliver", method = RequestMethod.POST)
    public String searchDeliver(PaymentUnallotLookup lookup) {
        setLookup(lookup);
        return "redirect:/payment/unallot/deliver/";
    }

    /**
     * GO分配取菜员页面
     *
     * @param orderno
     * @param redirectAttr
     * @return
     */
    @RequestMapping(value = "/payment/unallot/deliver/{orderno}/allotdeliver", method = RequestMethod.GET)
    public String allotDeliver(@PathVariable String orderno, Model model) {
        List<Employee> employees = unallotService.findDelivers();

        Payment payment = globalPaymentService.getLocalPayment(orderno);
        model.addAttribute("employees", employees);
        model.addAttribute("flag", ALLOT_DELIVER);
        model.addAttribute("payment", payment);
        return "payment/unallot/deliver/form";
    }

    /**
     * 保存取菜员
     *
     * @param orderno
     * @param employeeId
     * @param flag
     * @return
     */
    @RequestMapping(value = "/payment/unallot/deliver/{orderno}/allotdeliver", method = RequestMethod.POST)
    public String saveDeliver(@PathVariable String orderno, @RequestParam int employeeId, String flag, RedirectAttributes redirectAttrs) {
        if (employeeId <= 0) {
            redirectAttrs.addFlashAttribute(Remind.success().appendMessage("未选择取菜员"));
        } else {
            if (unallotService.saveDeliver(orderno, employeeId)) {
                redirectAttrs.addFlashAttribute(Remind.success().appendMessage("成功分配取菜员"));
            } else {
                redirectAttrs.addFlashAttribute(Remind.danger().appendMessage("分配取菜员失败"));
            }

        }
        return "redirect:/payment/unallot/deliver/";
    }

    /**
     * 批量分配取菜员
     *
     * @param orderno
     * @param redirectAttr
     * @return
     */
    @RequestMapping(value = "/payment/unallot/deliver/allotdeliver", method = RequestMethod.GET)
    public String allotDelivers(@RequestParam String[] pks, Model model) {
        List<Employee> employees = unallotService.findDelivers();

        //Payment payment = globalPaymentService.getLocalPayment(orderno);
        model.addAttribute("employees", employees);
        model.addAttribute("flag", ALLOT_DELIVER);
        model.addAttribute("pks", pks);
        //model.addAttribute("payment", payment);
        return "payment/unallot/deliver/form";
    }

    /**
     * 保存取菜员
     *
     * @param employeeId
     * @return
     */
    @RequestMapping(value = "/payment/unallot/deliver/allotdeliver", method = RequestMethod.POST)
    public String saveDelivers(@RequestParam String[] pks, @RequestParam int employeeId, RedirectAttributes redirectAttrs) {
        if (pks.length <= 0) {
            throw new IllegalArgumentException("orderno参数不合法");
        }
        if (employeeId <= 0) {
            redirectAttrs.addFlashAttribute(Remind.success().appendMessage("未选择取菜员"));
        }
        for (String orderno : pks) {
            try {
                unallotService.saveDeliver(orderno, employeeId);
                Thread.sleep(200);
            } catch (InterruptedException ex) {
                logger.warn("未知错误", ex);
            }
        }
        redirectAttrs.addFlashAttribute(Remind.success().appendMessage("成功分配取菜员"));
        return "redirect:/payment/unallot/deliver/";
    }

    /**
     * GO分配配送员页面
     *
     * @param orderno
     * @param redirectAttr
     * @return
     */
    @RequestMapping(value = "/payment/unallot/courier/{orderno}/allotcourier", method = RequestMethod.GET)
    public String allotCourier(@PathVariable String orderno, Model model) {
        List<Employee> employees = unallotService.findCouriers(orderno);
        Payment payment = globalPaymentService.getLocalPayment(orderno);
        model.addAttribute("employees", employees);
        model.addAttribute("flag", ALLOT_COURIER);
        model.addAttribute("payment", payment);
        return "payment/unallot/courier/form";
    }

    /**
     * 保存配送员
     *
     * @param orderno
     * @param employeeId
     * @param flag
     * @return
     */
    @RequestMapping(value = "/payment/unallot/courier/{orderno}/allotcourier", method = RequestMethod.POST)
    public String saveCourier(@PathVariable String orderno, @RequestParam int employeeId, String flag, RedirectAttributes redirectAttrs) {
        if (employeeId <= 0) {
            redirectAttrs.addFlashAttribute(Remind.success().appendMessage("未选择配送员"));
        } else {
            if (unallotService.saveCourier(orderno, employeeId)) {

            } else {
                redirectAttrs.addFlashAttribute(Remind.danger().appendMessage("分配分配员失败"));
            }

        }
        return "redirect:/payment/unallot/courier/";
    }

    /**
     * 批量分配取菜员
     *
     * @param orderno
     * @param redirectAttr
     * @return
     */
    @RequestMapping(value = "/payment/unallot/courier/allotdeliver", method = RequestMethod.GET)
    public String allotCouriers(@RequestParam String[] pks, Model model) {
         List<Employee> employees=null;
        if (pks.length > 0) {
            employees = unallotService.findCouriers(pks[0]);
        }
        //Payment payment = globalPaymentService.getLocalPayment(orderno);
        model.addAttribute("employees", employees);
        model.addAttribute("flag", ALLOT_DELIVER);
        model.addAttribute("pks", pks);
        //model.addAttribute("payment", payment);
        return "payment/unallot/courier/form";
    }

    /**
     * 保存取菜员
     *
     * @param employeeId
     * @return
     */
    @RequestMapping(value = "/payment/unallot/courier/allotdeliver", method = RequestMethod.POST)
    public String saveCouriers(@RequestParam String[] pks, @RequestParam int employeeId, RedirectAttributes redirectAttrs) {
        if (pks.length <= 0) {
            throw new IllegalArgumentException("orderno参数不合法");
        }
        if (employeeId <= 0) {
            redirectAttrs.addFlashAttribute(Remind.success().appendMessage("未选择取菜员"));
        }
        for (String orderno : pks) {
            try {
                unallotService.saveDeliver(orderno, employeeId);
                Thread.sleep(200);
            } catch (InterruptedException ex) {
                logger.warn("未知错误", ex);
            }
        }
        redirectAttrs.addFlashAttribute(Remind.success().appendMessage("成功分配取菜员"));
        return "redirect:/payment/unallot/courier/";
    }

}
