package com.hyl.teddy.console.entity;

/**
 * 用户收藏 cust_favoraties
 *
 * @author Administrator
 *
 */
public class CustFavoraties {

    private int id;
    private int customerId;//用户id
    private int type;//类别 1-店铺收藏         2-菜品收藏
    private int targetId;//类别是1 则是店铺id ，2是菜品id
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getTargetId() {
        return targetId;
    }

    public void setTargetId(int targetId) {
        this.targetId = targetId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
