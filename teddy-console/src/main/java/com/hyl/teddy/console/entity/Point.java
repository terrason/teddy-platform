package com.hyl.teddy.console.entity;

/**
 * 区域顶点表 region_point
 *
 * @author Administrator
 *
 */
public class Point {

    private int id;
    /**
     * 区域id
     */
    private int regionId;
    
    private String regionName;

	/**
     * 经度. 参与计算
     */
    public int x;
    /**
     * 经度带小数字符串.
     */
    private String longitude;
    /**
     * 纬度. 参与计算
     */
    public int y;
    /**
     * 纬度带小数字符串.
     */
    private String latitude;
    
    public Point(){}
    public Point (int x, int y) {
    	this.x = x;
    	this.y = y;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 经度带小数字符串.
     *
     * @return the longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * 经度带小数字符串.
     *
     * @param longitude the longitude to set
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * 纬度带小数字符串.
     *
     * @return the latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * 纬度带小数字符串.
     *
     * @param latitude the latitude to set
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getRegionId() {
		return regionId;
	}
	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}
	public String getRegionName() {
		return regionName;
	}
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
    
	
}
