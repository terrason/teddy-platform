package com.hyl.teddy.console;

import com.hyl.teddy.console.lookup.CommonLookup;
import com.hyl.teddy.console.lookup.Lookup;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.WebUtils;

@Controller
public abstract class AbstractController {

    public static final String SESSIONKEY_PRINCIPAL = "principal";
    public static final String SESSIONKEY_SUFFIX_LOOKUP = "lookup";

    public static final String VIEW_LOGIN = "login";
    public static final String VIEW_INFO = "info";
    public static final String VIEW_INDEX = "index";

    @Value("#{settings['url.apis']}")
    private String api;
    @Value("#{settings['url.timeout']}")
    private int timeout;

    protected ApiConnection api(String url) {
        Connection connect = Jsoup.connect(api + url).ignoreContentType(true).timeout(timeout);
        return new ApiConnection(connect);
    }

    /**
     * 获取当前用户主体对象.
     *
     * @return 当前用户主体对象
     * @throws IllegalStateException 当前线程不是web请求抛出此异常.
     */
    protected Principal getPrincipal() throws IllegalStateException {
        return getSessionAttribute(SESSIONKEY_PRINCIPAL);
    }

    /**
     * 获取用户最后一次在当前模块（Controller）提交的查询信息. 不会返回{@code null}.
     *
     * @return 用户最后一次在当前模块提交的查询信息
     * @throws IllegalStateException 当前线程不是web请求抛出此异常.
     */
    protected Lookup getLookup() throws IllegalStateException {
        Lookup lookup = getSessionAttribute(getClass().getName() + "." + SESSIONKEY_SUFFIX_LOOKUP);
        if (lookup == null) {
            lookup = instanceLookup();
            setSessionAttribute(getClass().getName() + "." + SESSIONKEY_SUFFIX_LOOKUP, lookup);
        }
        setSessionAttribute(SESSIONKEY_SUFFIX_LOOKUP, lookup);
        return lookup;
    }

    /**
     * 实例化查询信息. 在通用查询条件不满足时，子类应覆盖本方法提供具体查询信息实现类。
     *
     * @return 实例化查询信息
     */
    protected Lookup instanceLookup() {
        return new CommonLookup();
    }

    /**
     * 保存用户提交的查询信息.
     *
     * @param lookup 查询信息
     * @throws IllegalStateException 当前线程不是web请求抛出此异常.
     */
    protected void setLookup(Lookup lookup) throws IllegalStateException {
        setSessionAttribute(getClass().getName() + "." + SESSIONKEY_SUFFIX_LOOKUP, lookup);
    }

    /**
     * 获取当前Session中对象. 若当前无{@code session}则返回{@code null}且不会创建{@code session}.
     *
     * @param <T> 保存的对象类型.
     * @param key 对象保存键值.
     * @return 保存的对象或{@code null}.
     * @throws IllegalStateException 当前线程不是web请求抛出此异常.
     */
    protected <T> T getSessionAttribute(String key) throws IllegalStateException {
        return (T) WebUtils.getSessionAttribute(currentRequest(), key);
    }

    /**
     * 获取当前Session中对象. 若当前无{@code session}则创建一个.
     *
     * @param <T> 保存的对象类型.
     * @param key 对象保存键值.
     * @param clazz 保存的对象类型.
     * @return 保存的对象.
     * @throws IllegalStateException 当前线程不是web请求抛出此异常.
     */
    protected <T> T getOrCreateSessionAttribute(String key, Class<T> clazz) throws IllegalStateException {
        HttpSession session = currentRequest().getSession();
        return (T) WebUtils.getOrCreateSessionAttribute(session, key, clazz);
    }

    /**
     * 保存变量到当前Session. 若保存的对象为{@code null}，则移除该键值所保存的对象。
     *
     * @param key 键值.
     * @param obj 要保存的对象或{@code null}.
     * @throws IllegalStateException 当前线程不是web请求抛出此异常.
     */
    protected void setSessionAttribute(String key, Object obj) throws IllegalStateException {
        WebUtils.setSessionAttribute(currentRequest(), key, obj);
    }

    /**
     * 获取当前Request对象.
     *
     * @return 当前Request对象或{@code null}.
     * @throws IllegalStateException 当前线程不是web请求抛出此异常.
     */
    protected HttpServletRequest currentRequest() throws IllegalStateException {
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (attrs == null) {
            throw new IllegalStateException("当前线程中不存在 Request 上下文");
        }
        return attrs.getRequest();
    }

    /**
     * 获取当前session对象. 若当前线程不是web请求或当前尚未创建{@code session}则返回{@code null}.
     *
     * @return 当前session对象或{@code null}.
     */
    protected HttpSession currentSession() {
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (attrs == null) {
            return null;
        }
        return attrs.getRequest().getSession(false);
    }
}
