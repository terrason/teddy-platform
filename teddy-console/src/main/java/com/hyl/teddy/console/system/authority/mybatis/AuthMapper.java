package com.hyl.teddy.console.system.authority.mybatis;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.hyl.teddy.console.MybatisMapper;
import com.hyl.teddy.console.system.authority.Menu;

@MybatisMapper
public interface AuthMapper {

    public List<Menu> selectPrincipalMenus(@Param("principal") int principal);

    public List<Menu> selectSuperMenus();
    
    public List<Menu> selectWuyeMenus();

}
