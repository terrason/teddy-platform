/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.console;

import com.hyl.teddy.console.system.authority.Menu;
import com.hyl.teddy.console.system.user.UserInfo;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Principal implements Serializable{

    public static final int SUPER = 9;
    public static final int WUYE = 8;
    private final Logger logger = LoggerFactory.getLogger(Principal.class);

    private List<Menu> menus;
    private final Set<Menu> location = new TreeSet<Menu>();
    private final Map<String, Menu> menuUrlIndex = new HashMap<String, Menu>();

    private final UserInfo userInfo;

    public Principal(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public List<Menu> getMenus() {
        return menus;
    }

    public void setMenus(List<Menu> menus) {
        this.menus = menus;
        for (Menu menu : menus) {
            indexMenuUrl(menu);
        }
    }

    private void indexMenuUrl(Menu menu) {
        if (StringUtils.isNotBlank(menu.getUrl()) && !menu.getUrl().startsWith("javascript:")) {
            menuUrlIndex.put(menu.getUrl(), menu);
        }
        if (menu.getMenus() != null) {
            for (Menu m : menu.getMenus()) {
                indexMenuUrl(m);
            }
        }
    }

    public Set<Menu> getLocation() {
        return location;
    }

    public void setLocation(String url) {
        Menu menu = menuUrlIndex.get(url);
        if (menu != null) {
            location.clear();
            addMenuToLocation(menu);
        }
        logger.debug("location changed to {}", location);
    }

    private void addMenuToLocation(Menu menu) {
        location.add(menu);
        if (menu.getParent() != null) {
            addMenuToLocation(menu.getParent());
        }
    }

    public List getTasks() {
        return null;
    }

    public List getNotifications() {
        return null;
    }

    public List getMessages() {
        return null;
    }

    public String getUsername() {
        return userInfo.getUsername();
    }

    public String getRealName() {
        return userInfo.getRealName();
    }

    public void setPassword(String password) {
        userInfo.setPassword(password);
    }

    public int getStatus() {
        return userInfo.getStatus();
    }

    public int getType() {
        return userInfo.getType();
    }

    public String getSkin() {
        return userInfo.getSkin();
    }

    public void setSkin(String skin) {
        userInfo.setSkin(skin);
    }

    public boolean isNavbarFixed() {
        return userInfo.isNavbarFixed();
    }

    public void setNavbarFixed(boolean navbarFixed) {
        userInfo.setNavbarFixed(navbarFixed);
    }

    public boolean isMenuFixed() {
        return userInfo.isMenuFixed();
    }

    public void setMenuFixed(boolean menuFixed) {
        userInfo.setMenuFixed(menuFixed);
    }

    public boolean isBreadcrumbFixed() {
        return userInfo.isBreadcrumbFixed();
    }

    public void setBreadcrumbFixed(boolean breadcrumbFixed) {
        userInfo.setBreadcrumbFixed(breadcrumbFixed);
    }

    public boolean isPetty() {
        return userInfo.isPetty();
    }

    public void setPetty(boolean petty) {
        userInfo.setPetty(petty);
    }

    public int getUserId() {
        return userInfo.getId();
    }
}
