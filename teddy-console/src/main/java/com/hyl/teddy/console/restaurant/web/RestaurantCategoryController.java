package com.hyl.teddy.console.restaurant.web;

import com.hyl.teddy.console.Remind;
import com.hyl.teddy.console.attachment.AttachmentService;
import com.hyl.teddy.console.entity.RestaurantCategory;
import com.hyl.teddy.console.pager.Pager;
import com.hyl.teddy.console.restaurant.RestaurantCategoryService;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * 餐厅类别
 *
 * @author Administrator
 */
@Controller
@RequestMapping
public class RestaurantCategoryController {

    @Resource
    private RestaurantCategoryService categoryService;
    @Resource
    private AttachmentService attachmentService;

    @RequestMapping(value = "/restaurant/category/", method = RequestMethod.GET)
    public String home(Model model) {
        Pager<RestaurantCategory> pager
                = categoryService.queryRestaurantCategoryList();
        model.addAttribute("pager", pager);
        return "/restaurant/category/list";
    }

    /**
     * 删除分类
     *
     * @param id
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = "/restaurant/category/{id}/remove", method = RequestMethod.POST)
    public String remove(@PathVariable int id, RedirectAttributes redirectAttributes) {
        if (id <= 0) {
            throw new IllegalArgumentException("id参数不合法");
        }
        categoryService.removeCategory(id);
        redirectAttributes.addFlashAttribute(Remind.warning().appendMessage("成功删除"));
        return "redirect:/restaurant/category/";
    }
    /**
     * 删除所选择的种类信息
     *
     * @param pks
     * @param redirectAttrs
     * @return
     */
    @RequestMapping(value = "/restaurant/category/remove", method = RequestMethod.POST)
    public String removeByPks(@RequestParam int[] pks, RedirectAttributes redirectAttrs) {
        categoryService.removeCategoryByPks(pks);
        redirectAttrs.addFlashAttribute(Remind.warning().appendMessage(pks.length + "个种类成功删除！"));
        return "redirect:/restaurant/category/";
    }

    /**
     * To add
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/restaurant/category/0", method = RequestMethod.GET)
    public String input(Model model) {
        return "restaurant/category/form";
    }

    /**
     * To edit
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/restaurant/category/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable int id, Model model) {
        if (id <= 0) {
            throw new IllegalArgumentException("id参数不合法");
        }
        RestaurantCategory entity = categoryService.findCategoryById(id);
        model.addAttribute("entity", entity);
        return "restaurant/category/form";
    }

    /**
     * 保存分类
     *
     * @param id
     * @param entity
     * @param redirectAttrs
     * @return
     */
    @RequestMapping(value = "/restaurant/category/{id}", method = RequestMethod.POST)
    public String save(@PathVariable int id, RestaurantCategory entity, RedirectAttributes redirectAttrs) {
        entity.setId(id);
        if (id > 0) {
            RestaurantCategory old = categoryService.findCategoryById(id);
            if (categoryService.updateCategory(entity)) {
                attachmentService.changeAttachmentTemporary(old.getIcon(), true);
                attachmentService.changeAttachmentTemporary(entity.getIcon(), false);
                redirectAttrs.addFlashAttribute(Remind.success().appendMessage("修改成功。"));
            } else {
                redirectAttrs.addFlashAttribute(Remind.warning().appendMessage("修改失败。"));
            }

        } else if (id == 0) {
            if (categoryService.createCategory(entity)) {
                attachmentService.changeAttachmentTemporary(entity.getIcon(), false);
                redirectAttrs.addFlashAttribute(Remind.success().appendMessage("创建成功。"));
            } else {
                redirectAttrs.addFlashAttribute(Remind.warning().appendMessage("创建失败。"));
            }

        } else {
            throw new IllegalArgumentException("信息不合法");
        }
        return "redirect:/restaurant/category/";
    }
}
