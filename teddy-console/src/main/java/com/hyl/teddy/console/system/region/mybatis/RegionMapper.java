package com.hyl.teddy.console.system.region.mybatis;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.hyl.teddy.console.MybatisMapper;
import com.hyl.teddy.console.entity.Employee;
import com.hyl.teddy.console.entity.Point;
import com.hyl.teddy.console.entity.Region;
import com.hyl.teddy.console.entity.RestaurantPromotion;
import com.hyl.teddy.console.lookup.Lookup;

@MybatisMapper
public interface RegionMapper {

	public List<Region> selectlist(Lookup lookup);

	public List<Point> seletcPoints();

	public int countList(Lookup lookup);

	public List<Point> findPointsById(@Param("id")int id);

	public void insertRegion(Region r);

	public void insertPoints(List<Point> list);

	public void updateRegion(@Param("id")int id, @Param("name")String name);

	public void deleteOldPoint(@Param("id")int id);

	public void deleteRegion(@Param("id")int id);

	public List<Region> selectAllRegion();

}
