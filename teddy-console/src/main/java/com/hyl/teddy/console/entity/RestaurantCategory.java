package com.hyl.teddy.console.entity;
/**
 * 餐厅种类 restarant_category
 * @author Administrator
 *
 */
public class RestaurantCategory {
	private int id;
	private String name;
        private String checked;
        private Integer icon;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

    public Integer getIcon() {
        return icon;
    }

    public void setIcon(Integer icon) {
        this.icon = icon;
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }
	
}
