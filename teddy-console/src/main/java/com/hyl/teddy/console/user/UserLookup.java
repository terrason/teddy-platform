package com.hyl.teddy.console.user;

import com.hyl.teddy.console.restaurant.*;
import com.hyl.teddy.console.lookup.Lookup;

/**
 *
 * @author Administrator
 */
public class UserLookup extends Lookup {

    private String nickname;
    private String mobile;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
