$(document).ready(function () {
    var $btnRestaurantLoadmore = $("#btn-restaurant-loadmore");
    var $restaurantContainer = $("#restaurant-container");
    var $restaurantTemplate = $(".hidden-template", $restaurantContainer);
    var $emptyMessage = $("#help-norestaurant-message", $restaurantContainer);
    $restaurantContainer.on("loading.restaurant.teddy", function () {
        var $this = $(this);
        var data = {
            keyword: $this.data("keyword"),
            category: $this.data("category"),
            orderby: $this.data("orderby"),
            start: $this.data("start"),
            size: $this.data("size")
        };
        $.ajax($this.data("loadUrl"), {
            data: data,
            beforeSend: function () {
                if ($this.hasClass("loading")) {
                    return false;
                }
                $this.addClass("loading");
                $btnRestaurantLoadmore.prop("disabled", true);
                return true;
            }
        }).done(function (restaurants) {
            if (restaurants.length === 0) {
                $this.trigger("end.restaurant.teddy");
                return;
            }
            $emptyMessage.remove();
            $.each(restaurants, function (i, restaurant) {
                var $restaurant = $restaurantTemplate.clone();
                $restaurant.removeClass("hidden-template");
                $restaurant.attr("id", "restaurant-" + restaurant.id || 0);
                $this.append($restaurant);
                $('[data-bind="icon"]', $restaurant).attr("src", restaurant.icon || "");
                $('[data-bind="name"]', $restaurant).text(restaurant.name || "");
                //显示星评
                var $starContainer = $('[data-bind="star"]', $restaurant);
                var halfStar = Math.floor(restaurant.star / 2);
                var isInt = restaurant.star % 2 === 0;
                var i = 0;
                for (i = 0; i < halfStar; i++) {
                    $starContainer.append('<i class="fa fa-star"></i>');
                }
                if (!isInt) {
                    $starContainer.append('<i class="fa fa-star-half-o"></i>');
                    i++;
                }
                for (; i < 5; i++) {
                    $starContainer.append('<i class="fa fa-star-o"></i>');
                }
                //显示优惠标签
                var $tagContainer = $('[data-bind="tag"]', $restaurant);
                if (restaurant.tag) {
                    $.each(restaurant.tag, function (i, tag) {
                        var $tag = $('<span class="badge"></span>');
                        $tagContainer.append($tag);
                        switch (tag) {
                            case "WIN_CASH" :
                                $tag.text("返");
                                break;
                            case "VIP_DISCOUNT" :
                                $tag.text("VIP");
                                break;
                            case "COST_CASH" :
                                $tag.text("券");
                                break;
                            case "COST_SCORE" :
                                $tag.text("积");
                                break;
                            case "FREE_POSTAGE" :
                                $tag.text("免");
                                break;
                            default:
                                $tag.text(tag);
                        }
                        $tag.addClass(tag);
                    });
                } else {
                    $tagContainer.append('<span class="badge EMPTY">&nbsp;</span>');
                }
                //显示消费水平
                $('[data-bind="price"]', $restaurant).text("人均 " + restaurant.price + " 元");
                //显示活动说明
                var $desc = $('[data-bind="promotion"]', $restaurant);
                var desc = restaurant.promotion || "";
                if (desc.length > 0) {
                    $desc.attr("title", desc);
                    $desc.text(desc.substr(0, 44));
                    if (desc.length > 44) {
                        $desc.append("...");
                    }
                } else {
                    $desc.remove();
                }
                //点餐超链接
                var $archor = $('[data-bind="archor"]', $restaurant);
                if (restaurant.enabled) {
                    $archor.attr("href", ctx + "/restaurant/" + restaurant.id).addClass("btn-info");
                } else {
                    $archor.addClass("btn-default")
                }
            });
            $this.data("start", data.start + restaurants.length);
            if (restaurants.length < data.size) {
                $this.trigger("end.restaurant.teddy");
            }
        }).fail(function (xhr, status, ex) {
            var errmsg;
            if (!xhr.responseJSON) {
                errmsg = xhr.responseText.contains("<html") ? ex : xhr.responseText;
            } else {
                errmsg = xhr.responseJSON.message || xhr.responseJSON || ex;
            }
            $("<div>" + errmsg + "</div>").bPopup();
        }).always(function () {
            $this.removeClass("loading");
            $btnRestaurantLoadmore.prop("disabled", false);
            $(".ajax-spinner", $this).remove().appendTo($this);
        });
    }).on("end.restaurant.teddy", function () {
        $btnRestaurantLoadmore.remove();
    });
    $btnRestaurantLoadmore.click(function () {
        $restaurantContainer.trigger("loading.restaurant.teddy");
    }).click();
});