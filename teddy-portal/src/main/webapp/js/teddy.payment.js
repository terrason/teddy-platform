(function ($) {
    $.widget("hylapp.payment", {
        "version": "1.0-SNAPSHOT",
        "options": {
            restaurant: undefined,
            container: ".shopcart-item-container",
            synchronize: ctx + "/ajax/shopcart",
            loadCashes: ctx + "/ajax/profile/cash"
        },
        "bindings": {},
        "template": undefined,
        "container": undefined,
        "restaurant": undefined,
        "shopcart": {
            remove: function (key) {
                delete this[key];
            },
            toArray: function () {
                var arr = [];
                for (var key in this) {
                    var v = this[key];
                    if (v.id) {
                        var item = $.extend({}, v);
                        delete item.element;
                        arr.push(item);
                    }
                }
                return arr;
            }
        },
        "changed": false,
        "synchornized": true,
        "_create": function () {
            var widget = this;
            widget.restaurant = widget.options.restaurant || widget.element.data("restaurant");
            if (!widget.restaurant) {
                throw "unknown restaurant";
            }

            widget.bindings.totalCount = $('[data-bind="totalCount"]', widget.element);
            widget.bindings.totalFee = $('[data-bind="totalFee"]', widget.element);
            widget.bindings.submit = $('[data-bind="submit"]', widget.element);
            widget.bindings.paid = $('[data-bind="paid"]', widget.element);
            widget.bindings.payMoreOrNot = $('[data-bind="payMoreOrNot"]', widget.element);
            widget.bindings.moreFee = $('[data-bind="moreFee"]', widget.element);
            widget.bindings.address = $('[data-bind="address"]', widget.element);
            if (!widget.bindings.address.length) {
                widget.bindings.address = $('[name="address"]', widget.element);
            }
            widget.bindings.memo = $('[data-bind="memo"]', widget.element);
            widget.bindings.getCash = function () {
                var cash = parseFloat($('[data-bind="cash"]', widget.element).vald());
                if (isNaN(cash)) {
                    return 0;
                }
                return cash;
            };
            widget.bindings.getScore = function () {
                var score = parseFloat($('[data-bind="score"]:checked', widget.element).vald());
                if (isNaN(score)) {
                    return 0;
                }
                return score;
            };

            widget.container = $(widget.element.data("container") || widget.options.container, widget.element);
            if (!widget.container.length) {
                throw "unknown container";
            }
            widget.template = $(".hidden-template", widget.container);
            widget.load($(".shopcart-item", widget.container));
            widget.initPromotions();

            widget._bindGlobalEvent();
            widget.display();

            widget.timer = $.timer(function () {
                widget.synchornize();
            }, 10000, true);
        },
        initPromotions: function () {
            var widget = this;
            $.each(this.options.promotion, function (i, promotion) {
                var tag = promotion.tag;
                switch (tag) {
                    case "COST_CASH":
                        var $cash = $('<select class="select select-info select-sm" data-bind="cash"></select>');
                        var $group = $('<span class="form-group"></span>');
                        $group.append($cash);
                        $("#promotion-" + tag, widget.container).find(".todo-name").append($group);
                        $.ajax(widget.options.loadCashes).done(function (cashes) {
                            $.each(cashes, function (i, cash) {
                                $('<option value="' + cash.value + '">' + cash.name + '</option>').appendTo($cash);
                            });
                            if ($cash.children().length) {
                                $cash.children()[0].selected = "selected";
                                $cash.prepend('<option value="0">不使用代金券</option>');
                            } else {
                                $cash.prepend('<option value="0">您已没有可用代金券</option>');
                            }
                            $cash.select2();
                            $cash.on("change." + widget.widgetFullName, function () {
                                widget.changed = true;
                                widget.display();
                            }).change();
                        }).fail(function () {
                            $cash.prepend('<option value="0" selected="selected">您已没有可用代金券</option>');
                            $cash.select2();
                        });
                        ;
                        break;
                    case "COST_SCORE":
                        var $score = $('<input type="checkbox" data-on-color="info" data-bind="score"/>');
                        var $label = $('<label>&nbsp;使用积分抵现</label>');
                        var $group = $('<span class="form-group"></span>');
                        $score.attr("value", widget.options.principal.score).attr("id", "lbl-COSTSCORE-" + widget.widgetFullName);
                        $group.append($score).append($label);
                        $("#promotion-" + tag, widget.container).find(".todo-name").append($group);
                        var availableScoreCost = widget.options.principal.score / promotion.params[1];
                        if (availableScoreCost < 1) {
                            $label.html('<i class="fa fa-frown-o text-warning"></i> 您的积分余额不足');
                            $score.remove();
                        } else {
                            $label.attr("for", $score.attr("id"));
                        }
                        $score.bootstrapSwitch();

                        $score.on("switchChange.bootstrapSwitch", function (event, state) {
                            $score.prop("checked", state);
                            widget.changed = true;
                            widget.display();
                        });
                        break;
                }
            });
        },
        _destroy: function () {
            this._unbindEvent();
            this.timer.stop();
        },
        "_bindGlobalEvent": function () {
            var widget = this;
            this.bindings.submit.on("click." + widget.widgetFullName, function (e) {
                if (!isAuthenticated()) {
                    e.preventDefault();
                    $("#btn-show-login").click();
                    $nav.on($nojs.EVENT_AUTHENTICATION_DONE, function () {
                        widget.bindings.submit.click();
                    });
                    return;
                }
                var success = widget.synchornize(false);
                if (!success) {
                    e.preventDefault();
                    return;
                }

                widget._trigger("beforeSubmit", e, widget);
                if (e.isDefaultPrevented()) {
                    return;
                }
                var request = widget.request();
                var $cost = $("[name=cost]", widget.element);
                if (!$cost.length) {
                    $cost = $('<input type="hidden" name="cost" />');
                    $cost.appendTo(widget.element);
                }
                $cost.val(request.cost);
                var $goods = $("[name=goods]", widget.element);
                if (!$goods.length) {
                    $goods = $('<input type="hidden" name="goods" />');
                    $goods.appendTo(widget.element);
                }
                $goods.val(request.goods);
                var $restaurant = $("[name=restaurant]", widget.element);
                if (!$restaurant.length) {
                    $restaurant = $('<input type="hidden" name="restaurant" />');
                    $restaurant.appendTo(widget.element);
                }
                $restaurant.val(widget.restaurant);
                widget.element.submit();
            });
            $(document).on("click." + widget.widgetFullName, "[data-shopcart]", function () {
                var $this = $(this);
                var func = $this.data("shopcart");
                widget[func] && widget[func]($this.closest("li"));
            });
            $(window).on("unload." + widget.widgetFullName, function () {
                widget.synchornize();
            });
        },
        "_bindEvent": function (key, item) {
            var widget = this;
            item.element.on("click." + widget.widgetFullName, "button.close", function () {
                widget.remove(key);
            });
            item.element.on("change." + widget.widgetFullName, '[data-bind="count"]', function () {
                if (this.value <= 0) {
                    widget.remove(key);
                } else {
                    item.count = parseInt(this.value);
                    widget.changed = true;
                    widget.synchornized = false;
                    widget.display();
                }
            });
        },
        "_unbindEvent": function () {
            var namespace = "." + this.widgetFullName;
            if (arguments.length) {//解除某个条目的所有事件
                var item = arguments[0];
                item && item.element && item.element.off
                        && item.element.off(namespace);
            } else {//解除所有购物车相关事件
                $(document).off(namespace);
                $(window).off(namespace);
                this.bindings.submit.off(namespace);
                $.each(this.shopcart, function (key, item) {
                    item && item.element && item.element.off
                            && item.element.off(namespace);
                });
            }
        },
        _createItemDom: function (identity, item) {
            var $item = this.template.clone();
            $.each(item, function (key, value) {
                $('[data-bind="' + key + '"]', $item).vald(value);
            });
            $('[data-bind="identity"]', $item).vald(identity);
            //隐藏不存在的图标
            if (!item.icon) {
                $('[data-bind="icon"]', $item).remove();
            }
            //调用 bootstrap formhelper number-input 插件
            if ($.fn.bfhnumber) {
                var $count = $('[data-bind="count"]', $item);
                $count.addClass("bfh-number").bfhnumber($count.data());
            }

            $item.appendTo(this.container).removeClass("hidden-template").addClass("shopcart-item");
            item.element = $item;
        },
        _renderPromotion: function (tag, enabled, discount, value) {
            var $promotion = $("#promotion-" + tag, this.element)[enabled ? "removeClass" : "addClass"]("muted");
            var $discount = $('[data-bind="promotionDiscount"]', $promotion);
            if (isNaN(discount)) {
                $discount.closest(".shopcart-promotion-discount").remove();
                $discount.remove();
            } else {
                $discount.vald(discount);
            }
            if (!isNaN(value)) {
                $(".form-group", $promotion).find(":input[data-bind]").val(value);
            }
        },
        "request": function () {
            var goods = {
                principal: this.options.principal.id,
                address: this.bindings.address.vald(),
                restaurant: this.restaurant,
                memo: this.bindings.memo.vald(),
                cash: this.bindings.getCash(),
                score: this.bindings.getScore(),
                dishes: this.shopcart.toArray()
            };
            return {
                goods: JSON.stringify(goods, null, 4),
                cost: this.bindings.totalFee.vald()
            };
        },
        discount: function (promotion, primeCost) {
            switch (promotion.tag) {
                case "WIN_CASH": // 返还代金券  [50,1,5] 意为 满50元送1张5元代金券
                    return {enable: true, discount: 0};
                case "VIP_DISCOUNT":// 会员优惠，百分比打折
                    if (this.options.principal.level === 2) {
                        return {enable: true, discount: -1.0 * (100 - promotion.params[0]) / 100 * primeCost};
                    } else {
                        return {enable: false, discount: 0};
                    }
                case "COST_CASH":// 可使用代金券,该优惠不接受参数，用户只能选择一张代金券
                    return {enable: true, discount: -this.bindings.getCash()};
                case "COST_SCORE"://满{0}元可用{1}积分抵1元现金
                    if (primeCost >= promotion.params[0] && promotion.params[1] > 0) {
                        var discount = -1.0 * this.bindings.getScore() / promotion.params[1];
                        var rtnVal = {enable: true, discount: discount};
                        if (discount * -1.0 > primeCost) {
                            rtnVal.value = primeCost * promotion.params[1];
                            rtnVal.discount = -primeCost;
                        }
                        return rtnVal;
                    } else {
                        return {enable: false, discount: 0};
                    }
                case "FREE_POSTAGE": //免邮费该优惠不接受参数，用户订单免配送费
                    if (primeCost >= promotion.params[0]) {
                        return {enable: true, discount: -this.options.delivery};
                    } else {
                        return {enable: false, discount: 0};
                    }
                default:
                    return {enable: false, discount: 0};
            }
        },
        /**
         * 重新计算数据并显示. 重复调用时间内没有数据发生改变时不会重新计算。
         */
        display: function () {
            if (!this.changed) {
                return this.bindings.totalFee.vald();
            }
            this.changed = false;
            var totalCount = 0;
            var totalFee = 0;
            $.each(this.shopcart, function (key, item) {
                if (item && !$.isFunction(item)) {
                    totalCount += item.count;
                    var priceTimes = item.price * item.count;
                    totalFee += priceTimes;
                    $('[data-bind="priceTimes"]', item.element).vald(priceTimes);
                }
            });
            this.bindings.totalCount.vald(totalCount);

            if (totalFee <= 0) {
                totalFee = 1;
            } else {
                totalFee += this.options.packageFee;
                var widget = this;
                var primeCost = totalFee;
                totalFee += this.options.delivery;
                $.each(this.options.promotion, function (i, promotion) {
                    var discount = widget.discount(promotion, primeCost);
                    totalFee += discount.discount;
                    if (totalFee <= 0) {
                        totalFee = 1;
                    }
                    widget._renderPromotion(promotion.tag, discount.enable, discount.discount, discount.value);
                });
            }
            this.bindings.totalFee.vald(totalFee);
            return totalFee;
        },
        /**
         * 加载购物条目. 本方法不会计算、显示总价。
         */
        load: function ($items) {
            var widget = this;
            $items.each(function () {
                var $li = $(this);
                var item = {
                    "id": $('[data-bind="id"]', $li).vald(),
                    "identity": parseInt($('[data-bind="identity"]', $li).vald()),
                    "name": $('[data-bind="name"]', $li).vald(),
                    "count": parseInt($('[data-bind="count"]', $li).vald()),
                    "status": parseInt($('[data-bind="status"]', $li).vald()),
                    "icon": $('[ data-bind="icon"]', $li).vald(),
                    "price": parseFloat($('[data-bind="price"]', $li).vald()),
                    "element": $li
                };

                var key = item.identity ? ("identity-" + item.identity) : ("dish-" + item.id);
                widget.shopcart[key] = item;
                widget._bindEvent(key, item);
            });
            widget.changed = true;
        },
        /**
         * 加入购物车.
         */
        add: function ($dom) {
            var item = {
                "id": $('[data-bind="id"]', $dom).vald(),
                "name": $('[data-bind="name"]', $dom).vald(),
                "count": 1,
                "status": 0,
                "icon": $('[ data-bind="icon"]', $dom).vald(),
                "price": parseFloat($('[data-bind="price"]', $dom).vald())
            };
            var shopcartItem = this.shopcart["dish-" + item.id];
            if (shopcartItem) {
                shopcartItem.count++;
                $('[data-bind="count"]', shopcartItem.element).vald(shopcartItem.count);
            } else {
                var key = "dish-" + item.id;
                this.shopcart[key] = item;
                this._createItemDom(key, item);
                this._bindEvent(key, item);
            }
            this.changed = true;
            this.synchornized = false;

            this.display();
        },
        remove: function (key) {
            var item = this.shopcart[key];
            this._unbindEvent(item);
            item.element.remove();
            this.shopcart.remove(key);
            this.changed = true;
            this.synchornized = false;

            this.display();
        },
        synchornize: function (async) {
            if (this.synchornized) {
                return true;
            }
            this.synchornized = true;
            var widget = this;
            var sc = {};
            $.extend(true, sc, this.shopcart);
            $.each(sc, function () {
                delete this.element;
            });
            $.ajax(this.options.synchronize || this.element.data("synchronize"), {
                type: "post",
                async: async,
                data: {
                    restaurant: this.restaurant,
                    shopcart: JSON.stringify(sc, null, 4)
                }
            }).done(function (data) {
                console.log("shopcart synchornize successfull");
            }).fail(function (xhr, status, ex) {
                widget.synchornized = false;
                var errmsg;
                if (!xhr.responseJSON) {
                    errmsg = xhr.responseText.contains("<html") ? ex : xhr.responseText;
                } else {
                    errmsg = xhr.responseJSON.message || xhr.responseJSON || ex;
                }
                widget.element.showAjaxError(errmsg);
            });
            return this.synchornized;
        }
    });
})(jQuery);