var console = console || {
    log: function () {
        return false;
    },
    trace: function () {
        return false;
    },
    debug: function () {
        return false;
    },
    info: function () {
        return false;
    },
    warn: function () {
        return false;
    },
    error: function () {
        return false;
    }
};
$.fn.vald = function () {
    if (arguments.length) {
        _setValue(this, arguments[0]);
        return null;
    } else {
        return _getValue(this);
    }
    function _getValue($j) {
        if ($j.is(":input")) {
            return $j.val();
        } else if ($j.is("img")) {
            return $j.attr("src");
        } else {
            return $j.html();
        }
    }
    function _setValue($j, value) {
        if ($j.is(":input")) {
            $j.val(value);
        } else if ($j.is("img")) {
            $j.attr("src", value);
        } else {
            $j.html(value);
        }
    }
};
(function (window, $) {
    var $application = function (context) {
        if ($.onApplicationStart && $.isFunction($.onApplicationStart)) {
            $.onApplicationStart();
        }
        for (var i in event.before) {
            $.isFunction(event.before[i]) && event.before[i].call($nojs, context);
        }
        for (var name in $application) {
            var app = $application[name];
            if ($.isFunction(app.enable) ? app.enable() : app.enable === true) {
                app.init(context);
            }
        }
        for (var i in event.ready) {
            $.isFunction(event.ready[i]) && event.ready[i].call($nojs, context);
        }
    };
    var $nojs = $application;
    $nojs.EVENT_POPUP_OPEN = "$nojs.popup.open";
    $nojs.EVENT_POPUP_CLOSE = "$nojs.popup.close";
    $nojs.EVENT_AUTHENTICATION_NAMESPACE = "$nojs.authentication";
    $nojs.EVENT_AUTHENTICATION_START = "$nojs.authentication.start";
    $nojs.EVENT_AUTHENTICATION_TERMINATE = "$nojs.authentication.terminate";
    $nojs.EVENT_AUTHENTICATION_CANCEL = "$nojs.authentication.cancel";
    $nojs.EVENT_AUTHENTICATION_DONE = "$nojs.authentication.done";
    $nojs.EVENT_AUTHENTICATION_FAIL = "$nojs.authentication.fail";
    $nojs.AJAX_DONE = "done.ajax.$nojs";

    var event = {
        before: [],
        ready: []
    };
    $nojs.before = function (func) {
        event.before.push(func);
    };
    $nojs.ready = function (func) {
        event.ready.push(func);
    };

    $application.statistical = {
        enable: function () {
            return !!$.fn.statistic;
        },
        selector: ".statistical",
        init: function (context) {
            $(this.selector, context).statistic();
        }
    };
    $nojs.pagination = {
        enable: function () {
            return !!$.fn.pager;
        },
        selector: ".pager-list",
        init: function (context) {
            $(this.selector, context).pager();
        }
    };
    $nojs.wysiwygEditor = {
        enable: function () {
            return !!$.fn.ace_wysiwyg;
        },
        selector: ".wysiwyg-editor",
        options: {
            toolbar: [
                {name: "font", title: "字体"},
                null,
                {name: "fontSize", title: "字号"},
                null,
                {name: "bold", className: "btn-info", title: "粗体"},
                {name: "italic", className: "btn-info", title: "斜体"},
                {name: "strikethrough", className: "btn-info", title: "删除线"},
                {name: "underline", className: "btn-info", title: "下划线"},
                null,
                {name: "insertunorderedlist", className: "btn-success", title: "无序列表"},
                {name: "insertorderedlist", className: "btn-success", title: "有序列表"},
                {name: "outdent", className: "btn-purple", title: "增加缩进"},
                {name: "indent", className: "btn-purple", title: "减少缩进"},
                null,
                {name: "justifyleft", className: "btn-primary", title: "左对齐"},
                {name: "justifycenter", className: "btn-primary", title: "居中"},
                {name: "justifyright", className: "btn-primary", title: "右对齐"},
                {name: "justifyfull", className: "btn-inverse", title: "两端对齐"},
                null,
                {name: "createLink", className: "btn-pink", title: "加入超链接"},
                {name: "unlink", className: "btn-pink", title: "移除超链接"},
                null,
                {name: "insertImage", className: "btn-success", title: "插入图片"},
                null,
                {name: "foreColor", title: "颜色"},
                null,
                {name: "undo", className: "btn-grey", title: "撤销"},
                {name: "redo", className: "btn-grey", title: "重做"}
            ]
        },
        init: function (context) {
            var module = this;
            $(module.selector, context).each(function () {
                var $editor = $(this);
                var opt = $.extend({}, module.options, $editor.data());
                $editor.ace_wysiwyg(opt);

                var $form = $editor.closest("form");
                var $input = $("[name=" + opt.bindInput + "]", $form);
                $form.submit(function () {
                    $input.val($editor.cleanHtml());
                });
            });
        }
    };


    $nojs.WdatePicker = {
        enable: function () {
            return !!window.WdatePicker;         //jquery.ui
        },
        selector: "input.Wdate",
        init: function (context) {
            $(this.selector, context).each(function (i, dt) {
                var $input = $(dt);
                var option = $input.data();
                $input.click(function () {
                    WdatePicker.call(dt, option);
                });
            });
        }
    };

    $nojs.datetimepicker = {
        enable: function () {
            return !!$.fn.datetimepicker;
        },
        options: {
            autoclose: true,
            language: "zh-CN"
        },
        selector: "input[type=datetime]",
        initRange: function (option, $input, context) {
            function _bindDateTarget(opt) {
                var method = {
                    "startDateTarget": "setStartDate",
                    "endDateTarget": "setEndDate"
                };
                if (option[opt] === 'now') {
                    $input.datetimepicker(method[opt], new Date());
                    return;
                }
                var $target = $(option[opt], context);
                if (!$target.length) {
                    $target = $("[name=" + option[opt] + "]", context);
                }
                $target.on("changeDate", function (ev) {
                    $input.datetimepicker(method[opt], this.value);
                });
            }
            if (!option.startDateTarget && !option.endDateTarget) {
                return;
            }
            var $form = $input.closest("form");
            if ($form.length) {
                context = $form;
            }

            if (option.startDateTarget) {
                _bindDateTarget("startDateTarget");
            }
            if (option.endDateTarget) {
                _bindDateTarget("endDateTarget");
            }
        },
        init: function (context) {
            var module = this;
            $(this.selector, context).each(function (i, dt) {
                var $input = $(dt);
                var option = $.extend({}, module.options, $input.data());

                $input.datetimepicker(option);
                module.initRange(option, $input, context);

                $input.next("[class*=icon-]").click(function () {
                    $input.focus();
                });
            });
        }
    };
    $nojs.timepicker = $.extend(true, {}, $nojs.datetimepicker, {
        options: {
            format: "HH:ii",
            startView: 1,
            maxView: 1
        },
        selector: "input[type=time]"
    });
    $nojs.smscode = {
        enable: function () {
            return !!$.timer;
        },
        selector: "button[data-smscode]",
        init: function (context) {
            var thisModule = this;
            //短信验证码
            $(document).on("click", thisModule.selector, function () {
                var $this = $(this);
                var originalCaption = $this.text();
                var $caption = $("<span></span>");
                var remain = 60;
                var $remain = $("<span></span>").text(remain);
                $caption.append($remain).append(" 秒后才可重新获取");
                $this.html($caption);
                var timer = $.timer(function () {
                    if (remain > 0) {
                        $remain.text(--remain);
                    } else {
                        timer.stop();
                        $(thisModule.selector, context).prop("disabled", false);
                        $this.text(originalCaption);
                    }
                }, 1000, true);
                $(thisModule.selector, context).prop("disabled", true);
            });
        }
    };
    $nojs.nailing = {
        enable: function () {
            return !!$.fn.nails;
        },
        selector: ".nailing",
        init: function (context) {
            $(this.selector, context).each(function () {
                var $nailing = $(this);
                $nailing.nails($nailing.data());
            });
        }
    };
    $nojs.alternate = {//隔行变色
        enable: true,
        selector: "table.alternate",
        option: {
            alterClass: "hilight",
            row: "tr:even"
        },
        init: function (context) {
            var module = this;
            $(this.selector, context).each(function () {
                var $table = $(this);
                var opt = $.extend({}, module.option, $table.data());
                $(opt.row, $table).addClass(opt.alterClass);
            });
        }
    };
    $nojs.hoverable = {//hover变色
        enable: true,
        selector: ".hoverable,.tab1 tr",
        defaultHoverClass: "highlight",
        init: function (context) {
            var module = this;
            $(this.selector, context).mouseenter(function () {
                $(this).addClass($(this).data("hoverClass") || module.defaultHoverClass);
            }).mouseleave(function () {
                $(this).removeClass($(this).data("hoverClass") || module.defaultHoverClass);
            });
        }
    };
    $nojs.activable = {//按下变色
        enable: true,
        selector: ".activable",
        defaultActiveClass: "activated",
        init: function (context) {
            var module = this;
            $(this.selector, context).mousedown(function () {
                var $this = $(this);
                var activeClass = $this.data("activeClass") || module.defaultActiveClass;
                $this.addClass(activeClass);
            }).mouseup(function () {
                var $this = $(this);
                var activeClass = $this.data("activeClass") || module.defaultActiveClass;
                $this.removeClass(activeClass);
            });
        }
    };
    $nojs.collapsible = {//折叠还示
        enable: true,
        context: null,
        selector: {
            context: ".collapsible",
            controller: "[for]"
        },
        collapsedClass: "collapsed", //+折叠
        event: ["$nojs-collapsible-beforeExpand", "$nojs-collapsible-afterExpand", "$nojs-collapsible-beforeCollapses", "$nojs-collapsible-afterCollapses"],
        init: function (context) {
            this.context = context;
            var module = this;
            $(module.selector.context, context).each(function (i, thisContext) {
                $(module.selector.controller, thisContext).click(function () {
                    var $controller = $(this);
                    var $content = $("#" + $controller.attr("for"), thisContext);
                    var $thisContext = $(thisContext);
                    var collapsed = $thisContext.hasClass(module.collapsedClass);
                    if (collapsed) {//expand
                        $thisContext.trigger(module.event[0], module);
                        $thisContext.removeClass(module.collapsedClass);
                        $content.show();
                        $thisContext.trigger(module.event[1], module);
                    } else {//collapses
                        $thisContext.trigger(module.event[2], module);
                        $content.hide();
                        $thisContext.addClass(module.collapsedClass);
                        $thisContext.trigger(module.event[3], module);
                    }
                });
            });
        }
    };
    $nojs.sortable = {//排序模块
        enable: function () {
            return !!$.fn.sortable;
        },
        selector: {
            main: ".sortable",
            item: ".sortable-item",
            up: ".sortable-up",
            down: ".sortable-down",
            top: ".sortable-top",
            bottom: ".sortable-bottom"
        },
        option: {
            cursor: "move",
            placeholder: "list-group-item ui-state-highlight"
        },
        init: function (context) {
            var module = this;
            $(module.selector.main, context).each(function () {
                var $sortable = $(this);
                var opt = $.extend(module.option, $sortable.data());
                $sortable.sortable(opt);

                $(module.selector.up, $sortable).click(function () {
                    var $row = $(this).parents(module.selector.item);
                    var $prev = $row.prev();
                    $prev.before($row);
                });
                $(module.selector.down, $sortable).click(function () {
                    var $row = $(this).parents(module.selector.item);
                    var $next = $row.next();
                    $next.after($row);
                });
                $(module.selector.top, $sortable).click(function () {
                    var $row = $(this).parents(module.selector.item);
                    $sortable.prepend($row);
                });
                $(module.selector.bottom, $sortable).click(function () {
                    var $row = $(this).parents(module.selector.item);
                    $sortable.append($row);
                });
            });
        }
    };
    $nojs.imgsource = {//更换img源
        enable: function () {
            return !!window.srx;
        },
        selector: "img[data-src]",
        init: function (context) {
            var module = this;
            $(module.selector, context).each(function () {
                var $img = $(this);
                var src = $img.attr("data-src");
                if (src) {
                    if (/^(http|https|ftp):\/\/.*/.test(src)) {
                        $img.attr("src", src);
                    } else {
                        $img.attr("src", window.srx + src);
                    }
                }
            });
        }
    };
    $nojs.attachment = {//更换img源
        enable: function () {
            return !!window.srx;
        },
        selector: "img[data-attachment]",
        init: function (context) {
            var module = this;
            $(module.selector, context).each(function () {
                var $img = $(this);
                var src = $img.attr("data-attachment");
                if (src) {
                    if (/^(http|https|ftp):\/\/.*/.test(src)) {
                        $img.attr("src", src);
                    } else if (src === "0") {
                    } else {
                        $img.attr("src", ctx + "/ajax/upfile/" + src);
                    }
                }
            });
        }
    };
    $nojs.ahref = {//更换img源
        enable: function () {
            return !!window.srx;
        },
        selector: "a[data-href]",
        init: function (context) {
            var module = this;
            $(module.selector, context).each(function () {
                var $img = $(this);
                var src = $img.attr("data-href");
                if (src) {
                    if (/^(http|https|ftp):\/\/.*/.test(src)) {
                        $img.attr("href", src);
                    } else {
                        $img.attr("href", window.srx + src);
                    }
                }
            });
        }
    };
    $nojs.slider = {//滑块
        enable: function () {
            return !!$.fn.slider;
        },
        selector: ".slider",
        option: {
            slide: function (event, ui) {
                var $slider = $(this);
                var display = $slider.data("nojs.slider.display");
                if ($.isArray(display)) {
                    display[0].text(ui.values[0]);
                    display[1].text(ui.values[1]);
                } else if (display) {
                    display.text(ui.value);
                }

                var val = $slider.data("nojs.slider.val");
                if ($.isArray(val)) {
                    val[0].val(ui.values[0]);
                    val[1].val(ui.values[1]);
                } else if (val) {
                    val.val(ui.value);
                }
            }
        },
        init: function (context) {
            var module = this;
            $(module.selector, context).each(function () {
                var $slider = $(this);
                var opt = $.extend({}, module.option, $slider.data());
                if (opt.range) {
                    var displayMinSelector = $slider.data("displayMin");
                    var displayMaxSelector = $slider.data("displayMax");
                    var display = [$(displayMinSelector, context), $(displayMaxSelector, context)];
                    $slider.data("nojs.slider.display", display);

                    var valueMinSelector = $slider.data("valMin");
                    var valueMaxSelector = $slider.data("valMax");
                    var val = [$(valueMinSelector, context), $(valueMaxSelector, context)];
                    $slider.data("nojs.slider.val", val);

                    opt.values = [
                        val[0].val() || opt.min,
                        val[1].val() || opt.max
                    ];
                } else {
                    var displaySelector = $slider.data("display");
                    var display = $(displaySelector, context);
                    $slider.data("nojs.slider.display", display);

                    var valueSelector = $slider.data("val");
                    var val = $(valueSelector, context);
                    $slider.data("nojs.slider.val", val);

                    opt.value = val;
                }
                $slider.slider(opt);

                var display = $slider.data("nojs.slider.display");
                if ($.isArray(display)) {
                    display[0].text(opt.values[0]);
                    display[1].text(opt.values[1]);
                } else if (display) {
                    display.text(opt.value);
                }
            });
        }
    };
    $nojs.selectable = {//选择效果
        enable: function () {
            return !!$.fn.selectable;
        },
        selector: ".selectable",
        option: {},
        init: function (context) {
            $(this.selector, context).selectable();
        }
    };
    $nojs.moveable = {
        enable: false,
        selector: ".moveable",
        option: {
            target: "tr",
            move: "top"
        },
        init: function (context) {
            var opt = this.option;
            $(this.selector, context).click(function () {
                var $this = $(this);
                var option = $.extend({}, opt, $this.data());
                var $target = $this.parents(option.target);
                var $container = $target.parent();
                switch (option.move) {
                    case "top" :
                        $container.prepend($target);
                        break;
                    case "bottom":
                        $container.append($target);
                        break;
                }
            });
        }
    };
    $nojs.checkboxBonds = {
        enable: true,
        chkAll: "#chkAll",
        pks: "[name=pks]:checkbox:not(:disabled)",
        //pks: "[name=pks]:checkbox",
        render: ":button[data-confirm-require=pks]",
        init: function (context) {
            var $pks = $(this.pks, context);
            var $chkAll = $(this.chkAll, context);
            var $render = $(this.render, context);
            $pks.click(function () {
                $chkAll.prop("checked", $pks.length === $pks.filter(":checked").length);
                $render.prop("disabled", $pks.filter(":checked").length === 0);
            });
            $chkAll.click(function () {
                $pks.prop("checked", $(this).prop("checked"));
                $render.prop("disabled", $pks.filter(":checked").length === 0);
                $pks.change();
            });
            $render.prop("disabled", $pks.filter(":checked").length === 0);
        }
    };
    $nojs.checkboxTogether = {
        enable: true,
        selector: "input:checkbox[data-member]",
        $member: function ($leader, context) {
            var memberName = $leader.data("member");
            return $("input:checkbox[name=" + memberName + "]:not(:disabled)", context);
        },
        render: function ($leader, $member, context) {
            var $render = $leader.data("$render");
            if ($render === false) {
                return;
            }
            if (!$render) {
                var memberName = $leader.data("member");
                $render = $(":button[data-checkbox-require=" + memberName + "]");
                if ($render.length === 0) {
                    $leader.data("$render", false);
                    return;
                } else {
                    $leader.data("$render", $render);
                }
            }
            $render.prop("disabled", $member.filter(":checked").length === 0);
        },
        init: function (context) {
            var module = this;
            $(module.selector, context).each(function () {
                var $leader = $(this);
                var $member = module.$member($leader, context);
                $leader.change(function () {
                    $member.prop("checked", $leader.prop("checked"));
                    module.render($leader, $member, context);
                });
                $member.change(function () {
                    $leader.prop("checked", $member.length === $member.filter(":checked").length);
                    module.render($leader, $member, context);
                });
                module.render($leader, $member, context);
            });
        }
    };
    $nojs.validation = {
        enable: function () {
            return !!$.fn.validate;
        },
        selector: "form.validate",
        init: function (context) {
            var $form = $(this.selector, context);
            var option = {
                ignore: ".novalidate",
                highlight: function (element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'label',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    var $element = $(element);
                    var $formGroup = $element.closest(".form-group");
                    if ($formGroup.length) {
                        $formGroup.append(error);
                    } else {
                        $element.parent().append(error);
                    }
                }
            };
            $form.each(function () {
                var $form = $(this);
                var validation = $form.validate($.extend(option, $form.data()));
            });
        }
    };
    $nojs.readonly = {
        enable: true,
        selector: ".readonly",
        init: function (context) {
            var $readonlyContext = $(this.selector, context);
            $readonlyContext.find(":input").prop("readonly", true);
            $readonlyContext.find("textarea").each(function () {
                var $textarea = $(this);
                if ($textarea.hasClass("editor")) {
                    var text = $textarea.text();
                    $textarea.after(text);
                    $textarea.hide();
                } else {
                    $textarea.prop("readonly", true);
                }
            });
            $readonlyContext.find(":radio,:checkbox,select").prop("disabled", true);
            $readonlyContext.find(":button:not(.cancel)").hide();
        }
    };
    $nojs.arrayDictionary = {
        enable: function () {
            return !!window.dictionaryData;
        },
        selector: ".dictionary",
        event: "$nojs-dictionary-initialize",
        regex: {
            key: /([a-zA-Z0-9]+)\(([a-zA-Z0-9.]+)\->([a-zA-Z0-9.]+)\)/,
            parent: /(^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*)))\->([a-zA-Z0-9.]+)+/
        },
        $template: $("<option></option>"),
        _resolveRelation: function (obj, path) {
            if (!obj) {
                return null;
            }
            var first = path.shift();
            var value = obj[first];
            if (path.length === 0) {
                return value;
            } else {
                return this._resolveRelation(value, path);
            }
        },
        init: function (context) {
            var module = this;
            $(this.selector, context).on(this.event, function (event) {
                var $dictionary = $(this);
                var key = $dictionary.data("dictKey");
                var value = $dictionary.data("value");

                var arr = module.regex.key.exec(key);
                if (!arr || arr.length !== 4) {
                    return;
                }
                var dictKey = arr[1];
                var dictName = arr[2];
                var dictValue = arr[3];
                var dictionaryList = dictionaryData[dictKey];

                if (!dictKey || !dictionaryList) {
                    return;
                }

                if ($dictionary.is(":not(select)")) {
                    var val = value || $dictionary.text();
                    $.each(dictionaryList, function (i, obj) {
                        if (obj[dictName] == val) {
                            if ($dictionary.is("input")) {
                                $dictionary.val(obj[dictValue]);
                            } else {
                                $dictionary.text(obj[dictValue]);
                            }
                            return false;
                        }
                    });
                    return;
                }

                var $select = $dictionary;
                var parent = $dictionary.data("dictParent");
                if (parent) {
                    var parr = module.regex.parent.exec(parent);
                    if (!parr || parr.length !== 5) {
                        return;
                    }
                    var parentSelector = parr[1];
                    var dictionaryExpression = parr[4];
                    var dictionaryPath = dictionaryExpression.split(/\./);

                    var $parentSelect = $(parentSelector);
                    $parentSelect.change(function (event) {
                        var $trigger = $(this);
                        var parentValue = $trigger.val();

                        $select.empty();
                        var defaultOptionText = $select.data("default");
                        if (defaultOptionText) {
                            module.$template.clone()
                                    .text(defaultOptionText)
                                    .attr("value", "")
                                    .appendTo($select);
                        }
//			if(!parentValue){// 0==""==false
//			    return;
//			}
                        $.each(dictionaryList, function (i, obj) {
                            var relation = module._resolveRelation(obj, dictionaryPath.slice(0));
                            if (relation == parentValue) {
                                var $option = module.$template.clone();
                                $option.text(obj[dictValue]);
                                $option.attr("value", obj[dictName]);
                                $select.append($option);
                            }
                        });
                        $select.trigger("$nojs-select-initialize", event);
                    });

                } else {
                    $select.empty();
                    var defaultOptionText = $select.data("default");
                    if (defaultOptionText) {
                        module.$template.clone()
                                .text(defaultOptionText)
                                .attr("value", "")
                                .appendTo($select);
                    }
                    $.each(dictionaryList, function (i, obj) {
                        var $option = module.$template.clone();
                        $option.text(obj[dictValue]);
                        $option.attr("value", obj[dictName]);
                        $select.append($option);
                    });
                }

                $nojs.ready(function () {
                    $dictionary.change();
                });
            }).trigger(this.event);
        }
    };
    $nojs.selectValueInitialization = {//select值绑定
        enable: true,
        selector: "select",
        event: "$nojs-select-initialize",
        init: function (context) {
            $(this.selector, context).on(this.event, function (event) {
                var $select = $(this);
                var value = $select.data("value");
                if (value !== undefined) {
                    $select.children("option:selected").prop("selected", false);
                    $select.children("option[value=" + value + "]").prop("selected", true);
                }
                var autofire = $select.data("autofire");
                if (autofire) {
                    $nojs.ready(function () {
                        $select.change();
                    });
                }
            }).trigger(this.event);
        }
    };
    $nojs.radioGroupValueInitialization = {////radio值绑定
        enable: true,
        selector: ".radio-group",
        event: "$nojs-radio-initialize",
        init: function (context) {
            $(this.selector, context).on(this.event, function (event) {
                var $group = $(this);
                var value = $group.data("value");
                if (value !== undefined) {
                    $group.children(":radio:checked").prop("checked", false);
                    $group.children(":radio[value=" + value + "]").prop("checked", true);
                }
                var autofire = $group.data("autofire");
                if (autofire) {
                    $group.change();
                }
            }).trigger(this.event);
        }
    };
    $nojs.dropdownValueBind = {
        enable: function () {
            return !!$.fn.dropdown;
        },
        selector: "[data-toggle=dropdown][data-value]",
        event: "change.$nojs-dropdown",
        init: function (context) {
            var module = this;
            $(module.selector, context).each(function () {
                var $trigger = $(this);
                var value = $trigger.attr("data-value");
                if (value) {
                    var $value = $trigger.next().find("[data-value='" + value + "']");
                    if ($value.length) {
                        module.setText($trigger, $value.text());
                    }
                    module.setValue($trigger, value);
                }

                $trigger.next().find("[data-value]").click(function () {
                    var $this = $(this);
                    module.setText($trigger, $this.text());
                    module.setValue($trigger, $this.attr("data-value"));
                });
            });
        },
        setText: function ($trigger, text) {
            $(":text", $trigger).val(text);
            $(".display", $trigger).text(text);
        },
        setValue: function ($trigger, value) {
            var $value = $("input:hidden", $trigger);

            var oldValue = $value.val();
            $("input:hidden", $trigger).val(value || "");
            $trigger.attr("data-value", value);
            $trigger.trigger("change", oldValue);
        }
    };
    $nojs.dropdown={
        enable:true,
        init:function(context){
            $(".dropdown-menu [data-value]").click(function(){
                var $this=$(this);
                var value=$this.data("value");
                $this.closest(".dropdown-menu").parent().find("[data-bind=text]").vald($this.html());
                $this.closest(".dropdown-menu").parent().find("[data-bind=value]").vald(value);
            });
        }
    };
    $nojs.trimText = {
        enable: true,
        selector: ":text:not([data-skip-trim])",
        init: function (context) {
            $(this.selector, context).change(function () {
                this.value = $.trim(this.value);
            });
        }
    };
    $nojs.resetLookup = {//重置表单
        enable: true,
        selector: {
            reset: ".reset",
            control: ":input",
            form: "form.lookup"
        },
        init: function (context) {
            var $lookupForm = $(this.selector.form, context);
            var controlSelector = this.selector.control;
            $(this.selector.reset, $lookupForm).click(function () {
                $(controlSelector, $lookupForm).val("");
                $lookupForm.submit();
            });
        }
    };
    $nojs.action = {//模拟表单提交和标签
        enable: true,
        init: function (context) {
            for (var name in this) {
                var module = this[name];
                if (module && module.init) {
                    module.init(context);
                }
            }
        },
        addModule: function (name, module) {
            if (module && module.init) {
                this[name] = module;
            }
        },
        removeModule: function (name) {
            this[name] = undefined;
        },
        ajax: {
            selector: ".action-ajax",
            init: function (context) {
                var thisModule = this;
                $(thisModule.selector, context).click(function () {
                    var $button = $(this);
                    var confirmRequire = $button.data("confirmRequire");
                    if (confirmRequire) {
                        var $requiremence = $(confirmRequire, context);
                        if ($requiremence.length === 0) {
                            window.alert($button.data("confirmRequireMessage") || "未满足操作条件");
                            return;
                        }
                    }
                    var confirm = $button.data("confirm");
                    if (confirm) {
                        if (typeof (confirm) === "boolean") {
                            confirm = "您确定要执行此项操作吗？";
                        } else {
                            confirm += "\n\n   您确定要执行此项操作吗？";
                        }
                        if (!window.confirm(confirm)) {
                            return;
                        }
                    }
                    var href = $button.data("href") || $button.data("get");
                    if (!/^\//.test(href)) {

                        var index = window.location.href.indexOf("?");
                        var location = window.location.href.substring(0, index);
                        href = location + href;
                    }

                    var $params = $(".action-param", $button);
                    if ($params.length) {
                        href = href + "?1=1";
                        $params.each(function (i, paramSpan) {
                            var $param = $(paramSpan);
                            href += "&" + $param.data("key") + "=" + encodeURIComponent($param.data("value"));
                        });
                    } else {
                        var paramData = $button.data();
                        href += "?" + $.param(paramData, true);
                    }
                    $.ajax(href, $button.data()).done(function (data) {
                        $button.trigger($nojs.AJAX_DONE, data);
                    });
                });
            }
        },
        get: {
            selector: ".action-get",
            init: function (context) {
                $(this.selector, context).click(function () {
                    var $button = $(this);
                    var confirmRequire = $button.data("confirmRequire");
                    if (confirmRequire) {
                        var $requiremence = $(confirmRequire, context);
                        if ($requiremence.length === 0) {
                            window.alert($button.data("confirmRequireMessage") || "未满足操作条件");
                            return;
                        }
                    }
                    var confirm = $button.data("confirm");
                    if (confirm) {
                        if (typeof (confirm) === "boolean") {
                            confirm = "您确定要执行此项操作吗？";
                        } else {
                            confirm += "\n\n   您确定要执行此项操作吗？";
                        }
                        if (!window.confirm(confirm)) {
                            return;
                        }
                    }
                    var href = $button.data("href") || $button.data("get");
                    if (!/^\//.test(href)) {

                        var index = window.location.href.indexOf("?");
                        var location = window.location.href.substring(0, index);
                        href = location + href;
                    }

                    var $params = $(".action-param", $button);
                    if ($params.length) {
                        href = href + "?1=1";
                        $params.each(function (i, paramSpan) {
                            var $param = $(paramSpan);
                            href += "&" + $param.data("key") + "=" + encodeURIComponent($param.data("value"));
                        });
                    } else {
                        var paramData = $button.data();
                        href += "?" + $.param(paramData, true);
                    }

                    var formSelector = $button.data("form");
                    if (!formSelector) {
                        window.location = href;
                    } else {
                        var $form;
                        if ($.type(formSelector) === "string") {
                            $form = $(formSelector, context);
                        } else {
                            $form = $button.parents("form");
                        }
                        if (!$form.length) {
                            console.error("找不到绑定的表单");
                            return;
                        }
                        $form.attr("method", "get");
                        $form.attr("action", href);
                        $form.submit();
                    }
                });
            }
        },
        post: {
            selector: ".action-post",
            init: function (context) {
                $(this.selector, context).click(function () {
                    var $this = $(this);
                    var confirmRequire = $this.data("confirmRequire");
                    if (confirmRequire) {
                        var $requiremence = $(confirmRequire, context);
                        if ($requiremence.length === 0) {
                            window.alert($this.data("confirmRequireMessage") || "未满足操作条件");
                            return;
                        }
                    }
                    var confirm = $this.data("confirm");
                    if (confirm) {
                        if (typeof (confirm) === "boolean") {
                            confirm = "您确定要执行此项操作吗？";
                        } else {
                            confirm += "\n\n   您确定要执行此项操作吗？";
                        }
                        if (!window.confirm(confirm)) {
                            return;
                        }
                    }

                    var $form = $this.parents("form");
                    var formSelector = $this.data("form");
                    if (formSelector) {
                        $form = $(formSelector, context);
                    }
                    if (!$form.length) {
                        $form = $('<form method="post"></form>');
                        $this.parent().append($form);
                    }
                    var post = $this.data("post") || $this.data("href");
                    if (post) {
                        $form.attr("action", post);
                    }
                    $form.attr("method", "post");
                    var data = $this.data();
                    $.each(data, function (key, value) {
                        if (value) {
                            var $input = $("<input type=\"hidden\" class=\"help\"/>");
                            $form.append($input);
                            $input.attr("name", key);
                            $input.val(value);
                        }
                    });
                    $form.submit();
                    $(".help:hidden", $form).remove();
                });
            }
        },
        back: {
            selector: ".action-back",
            init: function (context) {
                $(this.selector, context).click(function () {
                    window.history.back();
                });
            }
        }
    };
    $nojs.dialog = {//依赖artdialog-5.x
        enable: function () {
            return !!$.dialog;
        },
        selector: ".dialog",
        event: {
            remoteContentReady: "application.dialog.remote.ready"
        },
        ok: function () {
            var $form = $("form", this.dom.content);
            $form.find("button[type=submit]").click();
            return false;
        },
        cancel: function () {
            return true;
        },
        ready: function () {
            if ($.bootstrapIE6) {
                $.bootstrapIE6(this.dom.content);
            }
            $application(this.dom.content);
        },
        init: function (context) {
            var thisModule = this;
            $(thisModule.selector, context).click(function () {
                var $actionButton = $(this);
                var data = $actionButton.data();
                var option = $.extend({
                    lock: true,
                    okValue: "保存",
                    ok: thisModule.ok,
                    cancelValue: "取消",
                    cancel: thisModule.cancel
                }, data);
                if (data.readonly) {
                    option.ok = undefined;
                }
                var dialog = $.dialog(option);
                $.ajax({
                    url: option.url,
                    dataType: "html",
                    cache: false,
                    data: data
                }).done(function (data, textStatus, jqXHR) {
                    var $dialog = $(data);
                    dialog.content($dialog[0]);
                    var $form = $dialog.is("form") ? $dialog : $("form", $dialog);
                    var action = $form.attr("action");
                    if (!action || action === "")
                        $form.attr("action", option.dialogUrl || option.url);
                    thisModule.ready.call(dialog);
                    $actionButton.trigger(thisModule.event.remoteContentReady, {module: thisModule, dialog: dialog, $content: $dialog, $form: $form});
                });
            });
        }
    };
    $nojs.closeable = {
        enable: true,
        init: function (context) {
            $(document).on("click", "button.close", function () {
                var $this = $(this);
                var removeContext = function () {
                    $this.closest("[data-closeable]").remove();
                };
                if ($this.hasClass("action-ajax")) {
                    $this.one($nojs.AJAX_DONE, removeContext);
                } else {
                    removeContext();
                }
            });
        }
    };

    function _replaceMetacharator(metastring) {
        if (metastring && metastring.length)
            return metastring.replace(/[.]/, "\\.");
        else {
            return "";
        }
    }
    //Copy from jQuery.
    window.$application = window.$nojs = $application;
    if (typeof define === "function" && define.amd && define.amd.$nojs) {
        define("$nojs", [], function () {
            return $nojs;
        });
    }
})(window, jQuery);

$(document).ready(function () {

    $.fn.extend({
        pushData: function (name, value) {
            var data = this.data(name);
            if (!data) {
                data = new Array();
                this.data(name, data);
            }
            $.isArray(data) && (value || value === false) && data.push(value);
        }
    });

    window.$nojs(document);
});