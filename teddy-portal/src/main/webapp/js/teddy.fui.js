$(document).ready(function () {
    // Focus state for append/prepend inputs
    $(document).on('focus', '.form-control', function () {
        $(this).closest('.input-group, .form-group').addClass('focus');
    }).on('blur', '.form-control', function () {
        $(this).closest('.input-group, .form-group').removeClass('focus');
    });

    $(document).on("click.bs.dropdown.data-api", ".shotcart-container", function (e) {
        e.stopPropagation();
    });

    $('[data-toggle="switch"]').bootstrapSwitch();

    $('[data-toggle="select"]').select2();
    $('[data-toggle=tooltip]').tooltip();
});
