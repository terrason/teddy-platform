(function ($) {
    $.widget("hylapp.shopcart", {
        "version": "1.0-SNAPSHOT",
        "options": {
            restaurant: undefined,
            container: ".shopcart-item-container",
            synchronize: ctx + "/ajax/shopcart"
        },
        "bindings": {},
        "template": undefined,
        "container": undefined,
        "restaurant": undefined,
        "shopcart": {
            remove: function (key) {
                this[key] = undefined;
                try {
                    delete this[key]
                } catch (e) {
                }
            }
        },
        "changed": false,
        "synchornized": true,
        "_create": function () {
            this.restaurant = this.options.restaurant || this.element.data("restaurant");
            if (!this.restaurant) {
                throw "unknown restaurant";
            }

            this.bindings.totalCount = $('[data-bind="totalCount"]', this.element);
            this.bindings.totalFee = $('[data-bind="totalFee"]', this.element);
            this.bindings.submit = $('[data-bind="submit"]', this.element);

            this.container = $(this.element.data("container") || this.options.container, this.element);
            if (!this.container.length) {
                throw "unknown container";
            }
            this.template = $(".hidden-template", this.container);
            this.load($(".shopcart-item", this.container));

            this._bindGlobalEvent();
            this.display();

            var widget = this;
            this.timer = $.timer(function () {
                widget.synchornize();
            }, 10000, true);
        },
        _destroy: function () {
            this._unbindEvent();
            this.timer.stop();
        },
        "_bindGlobalEvent": function () {
            var widget = this;
            this.bindings.submit.on("click." + widget.widgetFullName, function (e) {
                if (!isAuthenticated()) {
                    e.preventDefault();
                    $("#btn-show-login").click();
                    $nav.one($nojs.EVENT_AUTHENTICATION_DONE, function () {
                        widget.bindings.submit.click();
                    });
                    return;
                }
                var success = widget.synchornize(false);
                if (!success) {
                    e.preventDefault();
                }
            });
            $(document).on("click." + widget.widgetFullName, "[data-shopcart]", function () {
                var $this = $(this);
                var func = $this.data("shopcart");
                widget[func] && widget[func]($this.closest("li"));
            });
            $(window).on("unload." + widget.widgetFullName, function () {
                widget.synchornize();
            });
        },
        "_bindEvent": function (key, item) {
            var widget = this;
            item.element.on("click." + widget.widgetFullName, "button.close", function () {
                widget.remove(key);
            });
            item.element.on("change." + widget.widgetFullName, '[data-bind="count"]', function () {
                if (this.value <= 0) {
                    widget.remove(key);
                } else {
                    item.count = parseInt(this.value);
                    widget.changed = true;
                    widget.synchornized = false;
                    widget.display();
                }
            });
        },
        "_unbindEvent": function () {
            var namespace = "." + this.widgetFullName;
            if (arguments.length) {//解除某个条目的所有事件
                var item = arguments[0];
                item && item.element && item.element.off
                        && item.element.off(namespace);
            } else {//解除所有购物车相关事件
                $(document).off(namespace);
                $(window).off(namespace);
                this.bindings.submit.off(namespace);
                $.each(this.shopcart, function (key, item) {
                    item && item.element && item.element.off
                            && item.element.off(namespace);
                });
            }
        },
        _createItemDom: function (identity, item) {
            var $item = this.template.clone();
            $.each(item, function (key, value) {
                $('[data-bind="' + key + '"]', $item).vald(value);
            });
            $('[data-bind="identity"]', $item).vald(identity);
            //隐藏不存在的图标
            if (!item.icon) {
                $('[data-bind="icon"]', $item).remove();
            }
            //调用 bootstrap formhelper number-input 插件
            if ($.fn.bfhnumber) {
                var $count = $('[data-bind="count"]', $item);
                $count.addClass("bfh-number").bfhnumber($count.data());
            }

            $item.appendTo(this.container).removeClass("hidden-template").addClass("shopcart-item");
            item.element = $item;
        },
        /**
         * 重新计算数据并显示. 重复调用时间内没有数据发生改变时不会重新计算。
         */
        display: function () {
            if (!this.changed) {
                return;
            }
            this.changed=false;
            var totalCount = 0;
            var totalFee = 0;
            $.each(this.shopcart, function (key, item) {
                if (item && !$.isFunction(item)) {
                    totalCount += item.count;
                    var priceTimes = item.price * item.count;
                    totalFee += priceTimes;
                    $('[ data-bind="priceTimes"]', item.element).vald(priceTimes);
                }
            });
            this.bindings.totalCount.vald(totalCount);
            this.bindings.totalFee.vald(totalFee);
        },
        /**
         * 加载购物条目. 本方法不会计算、显示总价。
         */
        load: function ($items) {
            var widget = this;
            $items.each(function () {
                var $li = $(this);
                var item = {
                    "id": $('[data-bind="id"]', $li).vald(),
                    "identity": parseInt($('[data-bind="identity"]', $li).vald()),
                    "name": $('[data-bind="name"]', $li).vald(),
                    "count": parseInt($('[data-bind="count"]', $li).vald()),
                    "status": parseInt($('[data-bind="status"]', $li).vald()),
                    "icon": $('[ data-bind="icon"]', $li).vald(),
                    "price": parseFloat($('[data-bind="price"]', $li).vald()),
                    "element": $li
                };

                var key = item.identity ? ("identity-" + item.identity) : ("dish-" + item.id);
                widget.shopcart[key] = item;
                widget._bindEvent(key, item);
            });
            widget.changed = true;
        },
        /**
         * 加入购物车.
         */
        add: function ($dom) {
            var item = {
                "id": $('[data-bind="id"]', $dom).vald(),
                "name": $('[data-bind="name"]', $dom).vald(),
                "count": 1,
                "status": 0,
                "icon": $('[ data-bind="icon"]', $dom).vald(),
                "price": parseFloat($('[data-bind="price"]', $dom).vald())
            };
            var shopcartItem = this.shopcart["dish-" + item.id];
            if (shopcartItem) {
                shopcartItem.count++;
                $('[data-bind="count"]', shopcartItem.element).vald(shopcartItem.count);
            } else {
                var key = "dish-" + item.id;
                this.shopcart[key] = item;
                this._createItemDom(key, item);
                this._bindEvent(key, item);
            }
            this.changed = true;
            this.synchornized = false;

            this.display();
        },
        remove: function (key) {
            var item = this.shopcart[key];
            this._unbindEvent(item);
            item.element.remove();
            this.shopcart.remove(key);
            this.changed = true;
            this.synchornized = false;

            this.display();
        },
        synchornize: function (async) {
            if (this.synchornized) {
                return true;
            }
            this.synchornized = true;
            var widget = this;
            var sc = {};
            $.extend(true, sc, this.shopcart);
            $.each(sc, function () {
                delete this.element;
            });
            $.ajax(this.options.synchronize || this.element.data("synchronize"), {
                type: "post",
                async: async,
                data: {
                    restaurant: this.restaurant,
                    shopcart: JSON.stringify(sc, null, 4)
                }
            }).done(function (data) {
                console.log("shopcart synchornize successfull");
            }).fail(function (xhr, status, ex) {
                widget.synchornized = false;
                var errmsg;
                if (!xhr.responseJSON) {
                    errmsg = xhr.responseText.contains("<html") ? ex : xhr.responseText;
                } else {
                    errmsg = xhr.responseJSON.message || xhr.responseJSON || ex;
                }
                widget.element.showAjaxError(errmsg);
            });
            return this.synchornized;
        }
    });
})(jQuery);