/**
 * 处理登录、注册逻辑
 * export event:
 *       $nojs.EVENT_AUTHENTICATION_DONE
 *       $nojs.EVENT_AUTHENTICATION_FAIL
 **/
var COMMON_ERROR_DISPLAY = "help-input-validate-msg";
var AUTHC_SUCCESS_CLASS = "authenticated";
$.fn.extend({
    "showAjaxError": function () {
        var $form = this;
        var name;
        var message;
        if (arguments.length === 1) {
            name = COMMON_ERROR_DISPLAY;
            message = arguments[0];
        } else if (arguments.length === 2) {
            name = arguments[0];
            message = arguments[1];
        } else {
            return;
        }
        var validator = $form.data("validator");
        if (validator) {
            var error = {};
            error[name] = message;
            validator.showErrors(error);
        }
    },
    "closeOwnerDialog": function () {
        var $form = this;
        $form.closest(".authc").modal('hide');
    }
});
var $nav;
$(document).ready(function () {
    $nav = $("#navbar-teddy");
    var $authForm = $(".authc-form");

    var _loadPrincipal = function () {
        $.ajax(ctx + "/ajax/authc/principal", {cache: false}).done(_setAuthentication);
    };
    function _setAuthentication(principal) {
        if (principal && principal.id) {
            if (!$nav.hasClass(AUTHC_SUCCESS_CLASS)) {
                $nav.addClass(AUTHC_SUCCESS_CLASS);
                $nav.trigger($nojs.EVENT_AUTHENTICATION_DONE, principal);
            }
        } else {
            if ($nav.hasClass(AUTHC_SUCCESS_CLASS)) {
                $nav.removeClass(AUTHC_SUCCESS_CLASS);
                $nav.trigger($nojs.EVENT_AUTHENTICATION_FAIL);
            }
        }
    }


    $authForm.ajaxForm({
        cache: false,
        type: "post",
        beforeSubmit: function (arr, $form, options) {
            if($form.data("reauthenticate")){
                $nav.removeClass(AUTHC_SUCCESS_CLASS);
            }
            return $form.data("validator").form();
        },
        success: function (data, status, xhr, $form) {
            $form.trigger($nojs.AJAX_DONE,data);
            _setAuthentication(data);
            $form.closeOwnerDialog();
        },
        error: function (xhr, status, ex, $form) {
            var errmsg;
            if (!xhr.responseJSON) {
                errmsg = xhr.responseText.contains("<html") ? ex : xhr.responseText;
            } else {
                errmsg = xhr.responseJSON.message || xhr.responseJSON || ex;
            }
            $form.showAjaxError(errmsg);
        }
    });


    $.timer(function () {
        _loadPrincipal();
    }, 60000, true);

    $("button[data-smscode]").click(function () {
        var $trigger = $(this);
        var url = $trigger.data("smscode");
        if (!url) {
            return;
        }
        var mobile = getMobile($trigger);
        $.ajax(url, {
            cache: false,
            data: {
                mobile: mobile
            }
        }).done(function (data) {
            setToken($trigger, data);
        });

        function getMobile($trigger) {
            var mobileId = $trigger.data("mobile");
            if (!mobileId) {
                return null;
            }
            var $mobile = $("#" + mobileId);
            if (!$mobile.length) {
                $mobile = $("[name=" + mobileId + "]", $trigger.closest("form"));
            }
            if (!$mobile.length) {
                return null;
            }
            return $mobile.val();
        }

        function setToken($trigger, token) {
            var tokenId = $trigger.data("token");
            if (tokenId) {
                var $token = $("#" + tokenId);
                if (!$token.length) {
                    $token = $("[name=" + tokenId + "]", $trigger.closest("form"));
                }
                $token.val(token);
            }
        }
    });

    $nav.on($nojs.EVENT_AUTHENTICATION_DONE, function (event, principal) {
        $("img[data-bind=avatar]", this).attr("src", principal.avatar);
        $("[data-bind=vipLevel]", this).text(principal.vipLevel);
        $("[data-bind=score]", this).text(principal.score);
        $("[data-bind=nickname]", this).text(principal.nickname);
    }).on($nojs.EVENT_AUTHENTICATION_FAIL, function (event) {
        $("img[data-bind=avatar]", this).attr("src", "");
        $("span[data-bind=vipLevel]", this).text("");
        $("span[data-bind=score]", this).text(0);
        $("[data-bind=nickname]", this).text("游客");
    });
});

function isAuthenticated() {
    return $nav.hasClass(AUTHC_SUCCESS_CLASS);
}
