<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/WEB-INF/jspf/head.jspf"%>
        <title><spring:message code="application.title" /></title>
    </head>
    <body>
        <%@include file="/WEB-INF/jspf/nav.jspf"%>
        <form class="container" id="payment" data-restaurant="${id}" action="${ctx}/buy">
            <div class="row">
                <div class="col-xs-12 col-md-9 col-md-offset-2" id="address-selection">
                    <input type="hidden" name="address" data-bind="value" value="${shopping.address.id}" />
                    <button class="btn btn-inverse btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="col-xs-9 text-left" data-bind="text">${shopping.address.name}&nbsp;&nbsp;${shopping.address.mobile}<br /><span class="text-muted">${shopping.address.location}</span></span>
                        <span class="col-xs-3 text-right"><span class="caret" style="margin-top:1em;"></span></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel" style="margin-left:15px;">
                        <c:forEach items="${shopping.addresses}" var="cur">
                            <li>
                                <a href="javascript:;" data-value="${cur.id}">
                                    ${cur.name}&nbsp;&nbsp;${cur.mobile}<br /><span class="text-muted">${cur.location}</span>
                                </a>
                            </li>
                        </c:forEach>
                    </ul>
                </div>
            </div>
            <div class="row" style="margin-bottom:2em;margin-top:1em;">
                <div class="col-xs-12 col-md-9 col-md-offset-2 text-right text-info">
                    <a href="${ctx}/profile/address">编辑我的送餐地址</a>
                </div>
            </div>
            <div class="row affix-shopcart">
                <div class="col-xs-12 col-md-9 col-md-offset-2">
                    <div class="todo">
                        <div class="todo-search" style="cursor:default;">
                            &nbsp;&nbsp;<i class="fa fa-shopping-cart"></i>我的菜单
                        </div>
                        <ul class="shopcart-item-container">
                            <c:forEach items="${shopcart[id].items}" var="entity">
                                <c:set value="${entity.value}" var="cur"></c:set>
                                <li class="shopcart-item ${cur.statusClass}">
                                    <div class="todo-icon">
                                        <c:if test="${not empty cur.icon}">
                                            <img data-bind="icon" src="${cur.icon}"/>
                                        </c:if>
                                    </div>
                                    <div class="todo-content">
                                        <c:if test="${cur.status==0}">
                                            <button type="button" class="close">&times;</button>
                                        </c:if>
                                        <h4 class="todo-name" data-bind="name">${cur.name}</h4>
                                        <input type="hidden" data-bind="id" value="${cur.dishId}"/>
                                        <input type="hidden" data-bind="identity" value="${cur.id}"/>
                                        <input type="hidden" data-bind="status" value="${cur.status}"/>
                                        <span class="text-warning">￥ <span data-bind="price">${cur.price}</span></span>&nbsp;&nbsp;×&nbsp;
                                        <div class="form-inline">
                                            <div class="input-group input-group-xs">
                                                <input class="form-control bfh-number" data-bind="count" type="text" value="${cur.count}"/>
                                            </div>
                                        </div>
                                        &nbsp;= <span class="text-warning">￥ </span><span class="text-warning" data-bind="priceTimes"></span>
                                    </div>
                                </li>
                            </c:forEach>
                            <li class="shopcart-promotion">
                                <div class="todo-icon fa fa-gift"></div>
                                <div class="todo-content">
                                    <h4 class="todo-name">打包费</h4>
                                    <span class="shopcart-promotion-discount text-warning">￥ ${shopping.packageFee}</span>
                                </div>
                            </li>
                            <li class="shopcart-promotion">
                                <div class="todo-icon fa fa-truck"></div>
                                <div class="todo-content">
                                    <h4 class="todo-name">配送费</h4>
                                    <span class="shopcart-promotion-discount text-warning">￥ ${shopping.delivery}</span>
                                </div>
                            </li>
                            <c:forEach items="${shopping.promotion}" var="cur">
                                <li class="shopcart-promotion" id="promotion-${cur.tag}">
                                    <div class="todo-icon"><span class="badge ${cur.tag}">${cur.name}</span></div>
                                    <div class="todo-content">
                                        <h4 class="todo-name">${cur}</h4>
                                        <span class="shopcart-promotion-discount">
                                            <span class="discount-money text-warning">￥ <span data-bind="promotionDiscount">-20</span></span>
                                        </span>
                                    </div>
                                </li>
                            </c:forEach>
                            <li class="shopcart-promotion amount">
                                <div class="todo-icon fa fa-calculator"></div>
                                <div class="todo-content">
                                    <h4 class="todo-name">
                                        总计 <span class="text-danger ${shopcart[id].paid gt 0 ? 'text-del':''}">￥ <span data-bind="totalFee">0.0</span></span>
                                        <c:if test="${shopcart[id].paid gt 0}">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;您已支付 <span class="text-danger">￥ <span data-bind="paid">${shopcart[id].paid}</span></span>
                                            ,<span data-bind="payMoreOrNot"><%--还需支付/将退款--%></span> <span class="text-danger">￥ <span data-bind="moreFee"></span></span>
                                            </c:if>
                                    </h4>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <hr />
                    <div class="form-group">
                        <label class="control-label" for="memo">备注：</label>
                        <textarea class="form-control" id="memo" data-bind="memo" value="${shopcart[id].memo}"></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary btn-block" type="button" data-bind="submit">提交订单</button>
                    </div>
                </div>
            </div>
        </form>
        <script type="text/javascript">
            var configuration =${shoppingConfiguration};
            $(document).ready(function () {
                var $addressSelection = $("#address-selection");
                var $addressDropdownMenu = $(".dropdown-menu", $addressSelection);
                var $addressToggler = $(".dropdown-toggle", $addressSelection);
                $addressSelection.on("show.bs.dropdown", function () {
                    $addressDropdownMenu.width($addressToggler.width());
                });
                $("#payment").payment(configuration);
            });
        </script>
        <script type="text/javascript" src="${ctx}/js/teddy.payment.js"></script>
        <%@include file="/WEB-INF/jspf/footer.jspf" %>
    </body>
</html>