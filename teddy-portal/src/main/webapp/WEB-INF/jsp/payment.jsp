<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/WEB-INF/jspf/head.jspf"%>
        <title><spring:message code="application.title" /></title>
        <link rel="stylesheet" href="${ctx}/css/timeline.min.css" />
    </head>
    <body>
        <%@include file="/WEB-INF/jspf/nav.jspf"%>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="list-group">
                        <a class="list-group-item" href="${ctx}/profile/info"><span class="fa fa-user fa-fw"></span>个人资料</a>
                        <a class="list-group-item" href="${ctx}/profile/cash"><span class="fa fa-ticket fa-fw"></span>我的代金券</a>
                        <a class="list-group-item" href="${ctx}/profile/address"><span class="fa fa-map-marker fa-fw"></span>我的送餐地址</a>
                        <a class="list-group-item active" href="${ctx}/profile/payment"><span class="fa fa-credit-card fa-fw"></span>我的订单</a>
                        <a class="list-group-item" href="${ctx}/profile/favorate"><span class="fa fa-heart fa-fw"></span>我的收藏</a>
                    </div>
                </div>
                <div class="col-md-9">
                    <ul class="nav nav-tabs nav-justified" role="tablist">
                        <li role="presentation" class="${param.timeline}active"><a href="#">Home</a></li>
                        <li role="presentation"><a href="#">Profile</a></li>
                    </ul>
                    <div class="timeline">
                        <div class="timeline-block">
                            My Content 1!
                        </div>
                        <div class="timeline-block">
                            My Content 2!
                        </div>
                        <div class="timeline-block">
                            My Content 3!
                        </div>
                        <div class="timeline-block">
                            My Content 3!
                        </div>
                        <div class="timeline-block">
                            My Content 3!
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.timeline').timeline();
            });
        </script>
        <script type="text/javascript" src="${ctx}/js/timeline.js"></script>
        <%@include file="/WEB-INF/jspf/footer.jspf" %>
    </body>
</html>