<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/WEB-INF/jspf/head.jspf"%>
        <title><spring:message code="application.title" /></title>
    </head>
    <body>
        <%@include file="/WEB-INF/jspf/nav.jspf"%>
        <div class="row">
            <div class="col-xs-12 col-md-12">恭喜您获得${cash}元代金券</div>
        </div>
        <%@include file="/WEB-INF/jspf/footer.jspf" %>
    </body>
</html>