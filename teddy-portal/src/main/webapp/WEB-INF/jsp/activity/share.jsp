<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/WEB-INF/jspf/head.jspf"%>
        <title><spring:message code="application.title" /></title>
        <style type="text/css">
            .btn-act{position:absolute;width:32.8%;height:7.65845070422535%;left:33.6%;z-index:999;background-position:0 0;background-size:100% 100%;border:0px none;background-color: transparent;}
            #btn-receive{background-image:url('${ctx}/img/activity/share/receive.png');bottom:27.43838028169014%;}
            #btn-download{background-image:url('${ctx}/img/activity/share/donwload.png');bottom:13.80897887323944%;}
        </style>
    </head>
    <body>
        <%@include file="/WEB-INF/jspf/nav.jspf"%>

        <div class="container" style='margin-top:-30px'>
            <div class="row">
                <div class="col-xs-12 text-center" style="padding-left: 0px;padding-right: 0px;">
                    <input type="hidden" id="orderno" name="orderno" value="${orderno}">
                    <img src="${ctx}/img/activity/share/rule.jpg" class="img-responsive" style='margin-left:auto;margin-right:auto;'/>
                    <button class="btn-act" id="btn-receive"></button>
                    <a  class="btn-act" id="btn-download" href="http://download.tdxmeal.com"></a>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#btn-receive").click(function (e) {
                    if (!isAuthenticated()) {
                        e.preventDefault();
                        $("#btn-show-login").click();
                        var $this = $(this);
                        $nav.one($nojs.EVENT_AUTHENTICATION_DONE, function () {
                            $this.click();
                        });
                        return;
                    }

                    $.ajax("${ctx}/activity/mainshare", {
                        data: {
                            orderno: $("#orderno").val()
                        }
                    }).done(function (data) {
                        alert(data);
                    }).fail(function (xhr, status, ex) {
                        var errmsg;
                        if (!xhr.responseJSON) {
                            errmsg = xhr.responseText.contains("<html") ? ex : xhr.responseText;
                        } else {
                            errmsg = xhr.responseJSON.message || xhr.responseJSON || ex;
                        }
                        $("<div class='popup'>" + errmsg + "</div>").bPopup();
                    });
                });
            });
        </script>
    </body>
</html>