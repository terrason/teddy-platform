<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8"/>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
        <title><spring:message code="application.title"/></title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="error-container">
                        <div class="well">
                            <h1 class="grey lighter smaller">
                                <span class="blue bigger-125">
                                    <i class="icon-random"></i>
                                    ${empty code ? 500 : code}
                                </span>
                                对不起，出错啦！ {{(&gt;_&lt;)}}
                            </h1>

                            <hr />
                            <p>您访问的页面出错，服务器返回错误信息：</p>
                            <pre class="alert alert-danger">${exception.message}</pre>
                            <hr />
                            <h3 class="lighter smaller">
                                But we are working
                                <i class="icon-wrench icon-animated-wrench bigger-125"></i>
                                on it!
                            </h3>

                            <div class="space">
                                <!--
                                Failed URL: ${url}
                                Exception:  ${exception.message}
                                <c:forEach items="${exception.stackTrace}" var="cur">
                                    ${cur}
                                </c:forEach>
                                -->
                            </div>

                            <div>
                                <h4 class="lighter smaller">Meanwhile, try one of the following:</h4>

                                <ul class="list-unstyled spaced inline bigger-110 margin-15">
                                    <li>
                                        <i class="icon-hand-right blue"></i>
                                        Read the faq
                                    </li>

                                    <li>
                                        <i class="icon-hand-right blue"></i>
                                        Give us more info on how this specific error occurred!
                                    </li>
                                </ul>
                            </div>

                            <hr />
                            <div class="space"></div>

                            <div class="center">
                                <a href="javascript:window.history.back();" class="btn btn-default">
                                    <i class="icon-arrow-left"></i>
                                    后腿
                                </a>

                                <a href="${ctx}/" class="btn btn-primary">
                                    <i class="icon-dashboard"></i>
                                    首页
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
