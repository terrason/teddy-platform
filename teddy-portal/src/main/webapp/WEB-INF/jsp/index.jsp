<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/WEB-INF/jspf/head.jspf"%>
        <title><spring:message code="application.title" /></title>
    </head>
    <body>
        <%@include file="/WEB-INF/jspf/nav.jspf"%>

        <div class="container">
            <c:if test="${empty searchable}">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="btn-group" id="orderby-btn-group">
                            <a class="btn btn-info ${empty param.orderby or (param.orderby eq 1) ? 'active':''}" href="${ctx}/home?actvtn=${param.actvtn}&orderby=1"><span class="fa fa-comments"></span>&nbsp;评价最好</a>
                            <a class="btn btn-info ${param.orderby eq 2 ? 'active':''}" href="${ctx}/home?actvtn=${param.actvtn}&orderby=2"><span class="fa fa-jpy"></span>&nbsp;消费水平</a>
                        </div>
                    </div>
                </div>
            </c:if>
            <div class="row" id="restaurant-container"
                 data-keyword="${param.keyword}"
                 data-category="${param.actvtn}"
                 data-orderby="${param.orderby}"
                 data-start="0"
                 data-size="24"
                 data-load-url="${ctx}/ajax${searchable}/restaurant">
                <i class="ajax-spinner fa fa-spinner fa-spin"></i>
                <c:if test="${empty restaurants}">
                    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3" id="help-norestaurant-message">
                        <p class="text-warning">亲，没有这家餐厅哦！</p>
                    </div>
                </c:if>
                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 hidden-template">
                    <div class="tile">
                        <a data-bind="archor"  href="javascript:;">
                            <div class="tile_img">
                                <img class="tile-image" data-bind="icon" alt=""/>
                                <div class="tile_imgp" data-bind="promotion"></div>
                            </div>
                            <div class="tile_btm">
                                <h3 class="tile-title" data-bind="name"></h3>
                                <div class="tile-title text-warning" data-bind="star"></div>
                                <span class="tile-title xs"  data-bind="price"></span>
                                <span class="tile-title xs"  data-bind="tag" style="margin-left:8px;"></span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button class="btn btn-primary btn-block" id="btn-restaurant-loadmore" autocomplete="off">加载更多餐厅</button>
                </div>
            </div>
        </div>
        <%@include file="/WEB-INF/jspf/footer.jspf" %>
        <script src="${ctx}/js/teddy.restaurant.js"></script>
    </body>
</html>