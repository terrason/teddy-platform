<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/WEB-INF/jspf/head.jspf"%>
        <title><spring:message code="application.title" /></title>
    </head>
    <body data-spy="scroll"  data-target=".col-category">
        <%@include file="/WEB-INF/jspf/nav.jspf"%>
        <div class="container" id="jumbotron">
            <div class="jumbotron">
                <h1>${entity.name}</h1>
                <c:if test="${not empty entity.promotion}">
                    <p>${entity.promotion}</p>
                </c:if>
            </div>
        </div>
        <div class="container">
            <div class="row teddy-restaurant">
                <div class="col-xs-3 col-sm-2 col-category">
                    <ul class="nav nav-pills nav-stacked" id="nav-dish-category" role="tablist">
                        <c:forEach items="${entity.category}" var="cur" varStatus="status">
                            <li class="${status.first ? 'active':''}" role="presentation">
                                <a href="#category-${cur.id}">${cur.name}</a>
                            </li>
                        </c:forEach>
                    </ul>
                </div>
                <div class="col-xs-9 col-sm-10 col-dishes">
                    <div id="dish-category">
                        <c:forEach items="${entity.category}" var="category">
                            <div class="category-section" id="category-${category.id}">
                                <div class="category-name">${category.name}</div>
                                <ul class="list-unstyled">
                                    <c:forEach items="${category.dishes}" var="cur" varStatus="status">
                                        <li class="row dish-item hoverable" id="dish-${cur.id}">
                                            <div class="dish-body col-sm-12 clearfix">
                                                <c:if test="${not empty cur.image}">
                                                    <a class="dish-icon media-middle media-left" href="#">
                                                        <img src="${cur.image}" data-bind="icon" alt=""/>
                                                    </a>
                                                </c:if>
                                                <div class="media-body">
                                                    <h6 class="media-heading" data-bind="name">${cur.name}</h6>
                                                    <div class="media-description">
                                                        <span class="dish-sold">已售${cur.sold}份</span>&nbsp;&nbsp;<span class="dish-price">￥ <span data-bind="price">${cur.price}</span> /份</span>
                                                    </div>
                                                    <input type="hidden" data-bind="id" value="${cur.id}"/>
                                                </div>
                                                <div class="media-body media-middle media-right">
                                                    <button class="btn btn-primary help-noedit" type="button" data-shopcart="add"><i class="fa fa-cutlery"></i>&nbsp;加入菜单</button>
                                                </div>
                                            </div>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>
        <form class="container affix-shopcart" id="shopcart-container" action="${ctx}/restaurant/${id}/pay" data-restaurant="${id}">
            <div class="row">
                <div class="col-xs-9 col-xs-offset-3 col-sm-10 col-sm-offset-2 col-shopcart">
                    <div class="todo">
                        <div class="todo-search collapsed" data-toggle="collapse" data-target=".shopcart-item-container" aria-expanded="false">
                            <div class="row">
                                <div class="col-sm-4">
                                    <span class="caret"></span>&nbsp;&nbsp;<i class="fa fa-shopping-cart"></i>我的菜单 <span class="badge" data-bind="totalCount">0</span>
                                </div>
                                <div class="col-sm-4">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;总价 <span class="text-danger">￥ <span data-bind="totalFee">0.0</span></span>
                                </div>
                                <div class="text-right col-sm-4">
                                    <button class="btn btn-inverse" type="submit" data-bind="submit"><i class="fa fa-gavel"></i>结账</button>
                                </div>
                            </div>
                        </div>
                        <ul class="collapse shopcart-item-container">
                            <li class="hidden-template">
                                <div class="todo-icon"><img data-bind="icon" /></div>
                                <div class="todo-content">
                                    <button type="button" class="close">&times;</button>
                                    <h4 class="todo-name" data-bind="name"></h4>
                                    <input type="hidden" data-bind="id"/>
                                    <input type="hidden" data-bind="identity"/>
                                    <input type="hidden" data-bind="status"/>
                                    <span class="text-warning">￥ <span data-bind="price"></span></span>&nbsp;&nbsp;×&nbsp;
                                    <div class="form-inline">
                                        <div class="input-group input-group-xs">
                                            <input class="form-control" data-bind="count" type="text" value="1"/>
                                        </div>
                                    </div>
                                    &nbsp;= <span class="text-warning">￥ </span><span class="text-warning" data-bind="priceTimes"></span>
                                </div>
                            </li>
                            <c:forEach items="${shopcart[id].items}" var="entity">
                                <c:set value="${entity.value}" var="cur"></c:set>
                                <li class="shopcart-item ${cur.statusClass}">
                                    <div class="todo-icon">
                                        <c:if test="${not empty cur.icon}">
                                            <img data-bind="icon" src="${cur.icon}"/>
                                        </c:if>
                                    </div>
                                    <div class="todo-content">
                                        <c:if test="${cur.status==0}">
                                            <button type="button" class="close">&times;</button>
                                        </c:if>
                                        <h4 class="todo-name" data-bind="name">${cur.name}</h4>
                                        <input type="hidden" data-bind="id" value="${cur.dishId}"/>
                                        <input type="hidden" data-bind="identity" value="${cur.id}"/>
                                        <input type="hidden" data-bind="status" value="${cur.status}"/>
                                        <span class="text-warning">￥ <span data-bind="price">${cur.price}</span></span>&nbsp;&nbsp;×&nbsp;
                                        <div class="form-inline">
                                            <div class="input-group input-group-xs">
                                                <input class="form-control bfh-number" data-bind="count" type="text" value="${cur.count}"/>
                                            </div>
                                        </div>
                                        &nbsp;= <span class="text-warning">￥ </span><span class="text-warning" data-bind="priceTimes"></span>
                                    </div>
                                </li>
                            </c:forEach>
                        </ul>
                    </div>
                </div>
            </div>
        </form>
        <script type="text/javascript" src="${ctx}/js/teddy.shopcart.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                var $shopcartContainer = $("#shopcart-container");
                var $footer = $("footer");
                var $shopcartItemContainer = $(".shopcart-item-container");
                var $shopcartHead = $(".todo-search");
                $shopcartItemContainer.on("show.bs.collapse", function () {
                    $shopcartHead.removeClass("collapsed");
                }).on("hidde.bs.collapse", function () {
                    $shopcartHead.addClass("collapsed");
                }).on("dblclick", ".shopcart-item", function () {
                    $shopcartItemContainer.collapse("toggle");
                }).on("dblclick", ".bfh-number-btn", function (e) {
                    e.stopPropagation();
                });
                ;

                var $navbar = $("#navbar-teddy");
                var $jumbotron = $("#jumbotron");
                $("#nav-dish-category").affix({
                    offset: {
                        top: $navbar.height() + 30 + $jumbotron.height(),
                        bottom: function () {
                            return $footer.height() + 62;
                        }
                    }
                });
                $shopcartContainer.affix({
                    offset: {
                        bottom: function () {
                            return $footer.height() + 62;
                        }
                    },
                    helpme: true
                }).on("affix-bottom.bs.affix", function () {
                    $shopcartItemContainer.collapse('show');

                }).on("affix.bs.affix", function () {
                    $shopcartItemContainer.removeClass("in");
                    $shopcartHead.addClass("collapsed");
                });
                $shopcartContainer.shopcart();
            });
        </script>
        <%@include file="/WEB-INF/jspf/footer.jspf" %>
    </body>
</html>