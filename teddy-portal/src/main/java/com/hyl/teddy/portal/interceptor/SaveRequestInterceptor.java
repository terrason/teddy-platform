/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.portal.interceptor;

import com.hyl.teddy.portal.auth.Location;
import com.hyl.teddy.portal.restaurant.Category;
import com.hyl.teddy.portal.system.Configuration;
import com.hyl.teddy.portal.util.ContextUtils;
import com.hyl.teddy.portal.util.Utils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * 保存最后一次GET的请求.
 *
 * @author terrason
 */
public class SaveRequestInterceptor extends HandlerInterceptorAdapter {

    public static final String USER_SESSION_KEY = "principal";
    public static final String CONFIGURATION_SESSION_KEY = "configuration";
    @Autowired
    private ContextUtils util;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Location current = null;
        if ("GET".equalsIgnoreCase(request.getMethod())) {
            current = Utils.saveRequest();
        }

        Configuration configuration = util.getSessionAttribute(CONFIGURATION_SESSION_KEY);
        if (configuration == null) {
            configuration = util.api("/system").get(Configuration.class);
            util.setSessionAttribute("configuration", configuration);
        }
        if (current != null) {
            for (Category module : configuration.getModule()) {
                if (current.getActivation().contains(String.valueOf(module.getId()))) {
                    current.setModule(module);
                    break;
                }
            }
        }
        return true;
    }

}
