package com.hyl.teddy.portal;

import java.io.IOException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController extends AbstractController {

    /**
     * 跳转到首页.
     */
    @RequestMapping("/")
    public String root(Model model) throws IOException {
        return "redirect:/home";
    }

    @RequestMapping("/home")
    public String home(Model model) throws IOException {
        return "index";
    }
}
