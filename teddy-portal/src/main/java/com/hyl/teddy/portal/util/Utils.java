package com.hyl.teddy.portal.util;

import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.codec.binary.Base64;
import com.hyl.teddy.portal.UnauthenticationException;
import com.hyl.teddy.portal.auth.Location;
import com.hyl.teddy.portal.auth.Principal;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 *
 * @author lip
 */
public class Utils {

    private static final Logger logger = LoggerFactory.getLogger(Utils.class);

    public static final String SESSIONKEY_PRINCIPAL = "principal";
    public static final String LAST_PAGE = "com.hyl.teddy.portal.web.lastPage";
    public static final String CURRENT_KEY = "current";
    public static final String REDIRECT_HOME = "/";
    public static final String ACTIVATION_PARAM_NAME = "actvtn";

    private static final Base64 base64 = new Base64(true);

    public static Base64 getBase64() {
        return base64;
    }

    /**
     * 获取当前Request对象.
     *
     * @return 当前Request对象或{@code null}.
     * @throws IllegalStateException 当前线程不是web请求抛出此异常.
     */
    public static HttpServletRequest currentRequest() throws IllegalStateException {
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (attrs == null) {
            throw new IllegalStateException("当前线程中不存在 Request 上下文");
        }
        return attrs.getRequest();
    }

    /**
     * 获取当前session对象. 若当前线程不是web请求或当前尚未创建{@code session}则返回{@code null}.
     *
     * @return 当前session对象或{@code null}.
     */
    public static HttpSession currentSession() {
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (attrs == null) {
            return null;
        }
        return attrs.getRequest().getSession(false);
    }

    public static String getRequestUrl(HttpServletRequest request) {
        String contextPath = request.getContextPath();
        String requestURI = request.getRequestURI();
        int i = requestURI.indexOf("?");
        if (i < 0) {
            i = requestURI.length();
        }
        return requestURI.substring(contextPath.length(), i);
    }

    public static Location saveRequest() {
        HttpServletRequest request = currentRequest();
        Location current = Utils.hashRequestPage(request);
        request.getSession().setAttribute(LAST_PAGE, current.getUri());
        request.getSession().setAttribute(CURRENT_KEY, current);
        logger.debug("save request for {}", request.getRequestURI());
        return current;
    }

    public static String retrieveSavedRequest() {
        HttpSession session = currentSession();
        if (session == null) {
            return REDIRECT_HOME;
        }
        String HashedlastPage = (String) session.getAttribute(LAST_PAGE);
        if (HashedlastPage == null) {
            return REDIRECT_HOME;
        } else {
            return retrieve(HashedlastPage);
        }
    }

    public static String retrieve(String targetPage) {
        byte[] decode = base64.decode(targetPage);
        try {
            String requestUri = new String(decode, "UTF-8");
            int i = requestUri.indexOf("/", 1);
            return requestUri.substring(i);
        } catch (UnsupportedEncodingException ex) {
            //this does not happen
            return null;
        }
    }

    private static Location hashRequestPage(HttpServletRequest request) {
        String reqUri = request.getRequestURI();
        String query = request.getQueryString();
        Location location = new Location();
        if (query != null) {
            reqUri += "?" + query;
        }
        try {
            location.setUri(base64.encodeAsString(reqUri.getBytes("UTF-8")));
            location.setActivation(request.getParameterValues(ACTIVATION_PARAM_NAME));
            return location;
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException("编码错误", ex);
        }
    }

    public static Principal getPrincipal() {
        HttpSession session = currentSession();
        if (session == null) {
            throw new UnauthenticationException();
        }
        Object principal = session.getAttribute(SESSIONKEY_PRINCIPAL);
        if (principal == null) {
            throw new UnauthenticationException();
        }
        return (Principal) principal;
    }

    private static ObjectMapper objectMapper=new ObjectMapper();
    public static ObjectMapper json(){
        return objectMapper;
    }
}
