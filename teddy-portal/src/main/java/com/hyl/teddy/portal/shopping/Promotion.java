/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.portal.shopping;

/**
 * 优惠政策
 *
 * @author Administrator
 */
public interface Promotion {

    /**
     * 优惠政策名称. 用来唯一标识优惠政策。
     */
    public String getName();

    /**
     * 优惠政策标签字符. 例如： {@code 返} {@code 券} 等。
     */
    public String getTag();

    /**
     * 优惠参数.
     */
    public int[] getParams();

    /**
     * 生成该优惠政策的字符串描述.
     */
    public String toString();
}
