package com.hyl.teddy.portal.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;
import com.hyl.teddy.portal.auth.Principal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthenticationInterceptor extends HandlerInterceptorAdapter {

    public static final String USER_SESSION_KEY = "principal";

    private final Logger logger = LoggerFactory.getLogger(AuthenticationInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        logger.debug("apply authc interceptor");

        Principal principal = (Principal) WebUtils.getSessionAttribute(request, USER_SESSION_KEY);
        if (principal != null) {
            return true;
        } else {
            request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
            return false;
        }
    }

}
