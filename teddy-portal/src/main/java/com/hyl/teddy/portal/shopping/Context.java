/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.portal.shopping;

import com.hyl.teddy.portal.auth.Principal;
import com.hyl.teddy.portal.profile.Address;
import com.hyl.teddy.portal.shopping.impl.DefaultPromotion;
import java.util.List;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * @author terrason
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Context {

    private int packageFee;
    private int delivery;
    private List<DefaultPromotion> promotion;
    private Address address;
    private Principal principal;
    private List<Address> addresses;

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public int getPackageFee() {
        return packageFee;
    }

    public void setPackageFee(int packageFee) {
        this.packageFee = packageFee;
    }

    public int getDelivery() {
        return delivery;
    }

    public void setDelivery(int delivery) {
        this.delivery = delivery;
    }

    public List<DefaultPromotion> getPromotion() {
        return promotion;
    }

    public void setPromotion(List<DefaultPromotion> promotion) {
        this.promotion = promotion;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Principal getPrincipal() {
        return principal;
    }

    public void setPrincipal(Principal principal) {
        this.principal = principal;
    }

}
