/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.portal.system;

import com.hyl.teddy.portal.AbstractResponse;

/**
 *
 * @author terrason
 */
public class SystemResponse extends AbstractResponse {

    private Configuration data;

    @Override
    public Configuration getData() {
        return data;
    }

    public void setData(Configuration data) {
        this.data = data;
    }

}
