package com.hyl.teddy.portal.util;

import org.codehaus.jackson.map.ObjectMapper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Value;

import com.hyl.teddy.portal.AbstractResponse;
import java.io.IOException;
import org.jsoup.Connection;
import org.springframework.stereotype.Service;

@Service
public class CacheEvictor {

    @Value("#{settings['cache']}")
    private String host;

    public void evictCache(String cache, Object key) {
        try {
            System.out.println("EvictCacheForApis");
            Connection connect = Jsoup.connect(host + cache);
            if (key != null) {
                connect.data("key", key.toString());
            }
            Document document = connect.ignoreContentType(true).timeout(30000).post();
            String text = document.body().text();
            AbstractResponse b = new ObjectMapper().readValue(text, AbstractResponse.class);
            if (b.getCode() != 0) {
                throw new RuntimeException(b.getMessage());
            }
        } catch (IOException ex) {
            throw new RuntimeException("清空缓存接口调用失败", ex);
        }
    }
}
