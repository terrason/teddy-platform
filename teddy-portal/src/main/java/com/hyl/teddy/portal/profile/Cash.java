/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.portal.profile;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 *
 * @author terrason
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Cash {
    private int value;
    private String name;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
