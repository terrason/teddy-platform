/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.portal.system;

import com.hyl.teddy.portal.restaurant.Category;
import java.util.List;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 *
 * @author terrason
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Configuration {

    private String contact;
    private boolean displayComment;
    private List<Category> module;

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public boolean isDisplayComment() {
        return displayComment;
    }

    public void setDisplayComment(boolean displayComment) {
        this.displayComment = displayComment;
    }

    public List<Category> getModule() {
        return module;
    }

    public void setModule(List<Category> module) {
        this.module = module;
    }
}
