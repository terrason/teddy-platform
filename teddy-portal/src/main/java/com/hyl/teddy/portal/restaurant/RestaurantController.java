/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.portal.restaurant;

import com.hyl.teddy.portal.AbstractController;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author terrason
 */
@Controller
public class RestaurantController extends AbstractController {

    @RequestMapping("/restaurant")
    public String index(Model model) {
        model.addAttribute("searchable", "/search");
        return "index";
    }

    @RequestMapping("/restaurant/{id}")
    public String restaurant(@PathVariable int id, Model model) {
        Restaurant restaurant = api("/dish").data("restaurant", id).get(Restaurant.class);
        model.addAttribute("entity", restaurant);
        return "restaurant";
    }

    @RequestMapping(value = "/ajax/restaurant", produces = "text/json;charset=UTF-8")
    @ResponseBody
    public List<Restaurant> list(@RequestParam(defaultValue = "0") int category,
            @RequestParam(defaultValue = "1") int delivery,
            @RequestParam(defaultValue = "1") int orderby,
            @RequestParam(defaultValue = "0") int start,
            @RequestParam(defaultValue = "24") int size) {
        return api("/restaurant")
                .data("category", String.valueOf(category))
                .data("delivery", String.valueOf(delivery))
                .data("orderby", String.valueOf(orderby))
                .data("start", String.valueOf(start))
                .data("size", String.valueOf(size))
                .list(Restaurant.class);
    }

    @RequestMapping(value = "/ajax/search/restaurant", produces = "text/json;charset=UTF-8")
    @ResponseBody
    public List<Restaurant> search(@RequestParam String keyword,
            @RequestParam(defaultValue = "0") int start,
            @RequestParam(defaultValue = "24") int size) {
        return api("/search/restaurant")
                .data("keyword", keyword)
                .data("start", String.valueOf(start))
                .data("size", String.valueOf(size))
                .list(Restaurant.class);
    }
}
