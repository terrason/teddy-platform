/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.portal.util;

import com.hyl.teddy.portal.AbstractController;
import com.hyl.teddy.portal.ApiConnection;
import com.hyl.teddy.portal.UnauthenticationException;
import com.hyl.teddy.portal.auth.Principal;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;

/**
 *
 * @author terrason
 */
@Controller
public class ContextUtils extends AbstractController {

    @Override
    public HttpSession currentSession() {
        return super.currentSession(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public HttpServletRequest currentRequest() throws IllegalStateException {
        return super.currentRequest(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setSessionAttribute(String key, Object obj) throws IllegalStateException {
        super.setSessionAttribute(key, obj); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <T> T getOrCreateSessionAttribute(String key, Class<T> clazz) throws IllegalStateException {
        return super.getOrCreateSessionAttribute(key, clazz); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <T> T getSessionAttribute(String key) throws IllegalStateException {
        return super.getSessionAttribute(key); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getPrincipalId() throws UnauthenticationException {
        return super.getPrincipalId(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Principal getPrincipal() throws IllegalStateException {
        return super.getPrincipal(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ApiConnection api(String url) {
        return super.api(url); //To change body of generated methods, choose Tools | Templates.
    }
}
