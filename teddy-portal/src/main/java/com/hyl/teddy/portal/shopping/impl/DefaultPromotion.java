/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.portal.shopping.impl;

import com.hyl.teddy.portal.shopping.Promotion;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import org.apache.commons.lang3.ArrayUtils;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 *
 * @author terrason
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DefaultPromotion implements Promotion {

    private String tag;
    private int[] params;

    public String getName() {
        if ("WIN_CASH".equals(tag)) {
            return "返";
        }
        if ("VIP_DISCOUNT".equals(tag)) {
            return "VIP";
        }
        if ("COST_CASH".equals(tag)) {
            return "券";
        }
        if ("COST_SCORE".equals(tag)) {
            return "积";
        }
        if ("FREE_POSTAGE".equals(tag)) {
            return "免";
        }
        return null;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int[] getParams() {
        return params;
    }

    public void setParams(int[] params) {
        this.params = Arrays.copyOf(params, 4);
    }

    @Override
    public String toString() {
        if ("WIN_CASH".equals(tag)) {
            return "满" + params[0] + "元送" + params[1] + "张" + params[2] + "元代金券";
        }
        if ("VIP_DISCOUNT".equals(tag)) {
            NumberFormat formattor = new DecimalFormat("0.#");
            return "会员优惠" + formattor.format(0.1 * params[0]) + "折 ";
        }
        if ("COST_CASH".equals(tag)) {
            return "可使用一张代金券";
        }
        if ("COST_SCORE".equals(tag)) {
            return "满" + params[0] + "元可用积分抵现";
        }
        if ("FREE_POSTAGE".equals(tag)) {
            return "满" + params[0] + "元免邮费 ";
        }
        return null;
    }

}
