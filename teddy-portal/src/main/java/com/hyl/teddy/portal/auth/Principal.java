/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.portal.auth;

/**
 *
 * @author terrason
 */
public class Principal {

    private int id;
    /**
     * 手机号码.
     */
    private String mobile;
    /**
     * 昵称.
     */
    private String nickname;
    /**
     * 积分.
     */
    private int score;
    /**
     * 头像.
     */
    private String avatar;
    /**
     * 注册时间.
     */
    private String regTime;
    /**
     * 是否已有送餐地址.
     */
    private boolean maiden;
    /**
     * 等级 0-初级会员 1-高级会员 2-VIP.
     */
    private int level;

    public String getVipLevel() {
        switch (level) {
            case 0:
                return "初级会员";
            case 1:
                return "高级会员";
            case 2:
                return "VIP会员";
        }
        return null;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 手机号码.
     *
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 手机号码.
     *
     * @param mobile the mobile to set
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 昵称.
     *
     * @return the nickname
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * 昵称.
     *
     * @param nickname the nickname to set
     */
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    /**
     * 积分.
     *
     * @return the score
     */
    public int getScore() {
        return score;
    }

    /**
     * 积分.
     *
     * @param score the score to set
     */
    public void setScore(int score) {
        this.score = score;
    }

    /**
     * 头像.
     *
     * @return the avatar
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * 头像.
     *
     * @param avatar the avatar to set
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * 注册时间.
     *
     * @return the regTime
     */
    public String getRegTime() {
        return regTime;
    }

    /**
     * 注册时间.
     *
     * @param regTime the regTime to set
     */
    public void setRegTime(String regTime) {
        this.regTime = regTime;
    }

    /**
     * 是否已有送餐地址.
     *
     * @return the maiden
     */
    public boolean isMaiden() {
        return maiden;
    }

    /**
     * 是否已有送餐地址.
     *
     * @param maiden the maiden to set
     */
    public void setMaiden(boolean maiden) {
        this.maiden = maiden;
    }

    /**
     * 等级 0-初级会员 1-高级会员 2-VIP.
     *
     * @return the level
     */
    public int getLevel() {
        return level;
    }

    /**
     * 等级 0-初级会员 1-高级会员 2-VIP.
     *
     * @param level the level to set
     */
    public void setLevel(int level) {
        this.level = level;
    }
}
