/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.portal.profile;

import com.hyl.teddy.portal.AbstractController;
import com.hyl.teddy.portal.ApiConnection;
import com.hyl.teddy.portal.UnauthenticationException;
import com.hyl.teddy.portal.auth.Principal;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author terrason
 */
@Controller
public class ProfileController extends AbstractController {

    @RequestMapping(value = "/ajax/profile/passwd", method = RequestMethod.POST, produces = "text/json;charset=UTF-8")
    @ResponseBody
    public Principal passwd(@RequestParam(required = false) String pwdold, @RequestParam String pwdnew) throws UnauthenticationException {
        ApiConnection api = api("/profile/passwd").data("principal", String.valueOf(getPrincipalId()), "pwdnew", pwdnew);
        if (pwdold != null) {
            api.data("pwdold", pwdold);
        }
        api.post(null);
        Principal principal = getPrincipal();
        principal.setLevel(1);
        return principal;
    }

    @RequestMapping("/ajax/profile/cash")
    @ResponseBody
    public List<Cash> cash() {
        return api("/profile/cash").data("principal", getPrincipalId()).list(Cash.class);
    }
}
