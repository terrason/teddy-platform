/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.portal.activity.share;

import com.hyl.teddy.portal.AbstractController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Administrator
 */
@Controller
public class ShareController extends AbstractController {

    @RequestMapping("/activity/share/{orderno}")
    public String index(@PathVariable String orderno, Model model) {
        model.addAttribute("orderno", orderno);
        return "activity/share";
    }

    @RequestMapping(value = "/activity/mainshare", produces = "text/json;charset=UTF-8")
    @ResponseBody
    public String mainshare(@RequestParam String orderno, Model model) {
        int cash = api("/receive/share")
                .data("principal", getPrincipal().getId())
                .data("orderno", orderno)
                .get(Integer.class);
        model.addAttribute("cash", cash);
        return "恭喜您,获得了"+cash+"元代金券!";
    }
}
