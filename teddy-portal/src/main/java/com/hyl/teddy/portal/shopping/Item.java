/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.portal.shopping;

import java.io.Serializable;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 菜品订购明细.
 *
 * @author terrason
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Item implements Serializable{

    @JsonProperty("identity")
    private int id;
    @JsonProperty("id")
    private int dishId;//菜品id
    private String name;//菜品名称
    private double price;//价格.
    private int count;//数量
    private int status;//状态 0初始 1 取菜 2 退菜
    private int dishCategoryId;
    private String dishCategoryName;
    private String icon;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDishId() {
        return dishId;
    }

    public void setDishId(int dishId) {
        this.dishId = dishId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getStatusClass() {
        switch (status) {
            case 1:
                return "todo-done";
            case 2:
            case 3:
                return "todo-deny";
            default:
                return "";
        }
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDishCategoryId() {
        return dishCategoryId;
    }

    public void setDishCategoryId(int dishCategoryId) {
        this.dishCategoryId = dishCategoryId;
    }

    public String getDishCategoryName() {
        return dishCategoryName;
    }

    public void setDishCategoryName(String dishCategoryName) {
        this.dishCategoryName = dishCategoryName;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

}
