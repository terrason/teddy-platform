package com.hyl.teddy.portal;

import com.hyl.teddy.portal.util.Utils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

@ControllerAdvice
public class GlobalExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    public static final String DEFAULT_ERROR_VIEW = "500";

    @ExceptionHandler
    public ModelAndView defaultErrorHandler(HttpServletRequest request, HttpServletResponse response, Exception ex) throws Exception {
        if (request.getHeader("accept").contains("json")
                || (request.getHeader("X-Requested-With") != null
                && request.getHeader("X-Requested-With").contains("XMLHttpRequest"))) {
            logger.error(ex.getMessage(), ex);
            response.setStatus(500);
            response.setContentType("text/json;charset=UTF-8");
            request.setAttribute(WebUtils.ERROR_STATUS_CODE_ATTRIBUTE, 500);
            BaseResponse r = new BaseResponse();
            r.setCode(500);
            r.setMessage(ex.getMessage());
            Utils.json().writeValue(response.getWriter(), r);
            return null;
        }
        ModelAndView mav = createModelAndView(request, ex);
        mav.setViewName(DEFAULT_ERROR_VIEW);

        return mav;
    }

    private ModelAndView createModelAndView(HttpServletRequest req, Exception ex) throws Exception {
        // If the exception is annotated with @ResponseStatus rethrow it and let
        // the framework handle it.
        // AnnotationUtils is a Spring Framework utility class.
        if (AnnotationUtils.findAnnotation(ex.getClass(), ResponseStatus.class) != null) {
            throw ex;
        }

        // Otherwise setup and send the user to a default error-view.
        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", ex);
        mav.addObject("url", req.getRequestURL());
        return mav;
    }

    @ExceptionHandler(HttpStatusCodeException.class)
    public ResponseEntity httpStatusCodeErrorHandler(HttpServletRequest request, HttpStatusCodeException ex) throws Exception {
        logger.error(ex.getMessage(), ex);
        if (request.getHeader("accept").contains("json")
                || (request.getHeader("X-Requested-With") != null
                && request.getHeader("X-Requested-With").contains("XMLHttpRequest"))) {
            return new ResponseEntity(ex.getStatusText(), ex.getResponseHeaders(), ex.getStatusCode());
        }
        return new ResponseEntity(ex.getStatusText().getBytes("UTF-8"), ex.getResponseHeaders(), ex.getStatusCode());
    }
}
