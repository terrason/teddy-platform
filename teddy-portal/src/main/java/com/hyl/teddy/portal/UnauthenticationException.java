/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.portal;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * 需要重新登录的异常.
 *
 * @author terrason
 */
@ResponseStatus(value=HttpStatus.UNAUTHORIZED)
public class UnauthenticationException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    

}
