/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.portal.auth;

import com.hyl.teddy.portal.AbstractController;
import static com.hyl.teddy.portal.AbstractController.SESSIONKEY_PRINCIPAL;
import com.hyl.teddy.portal.util.Utils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author terrason
 */
@Controller
public class AuthController extends AbstractController {

    @RequestMapping(value = "/ajax/authc/smscode", produces = "text/json;charset=UTF-8")
    @ResponseBody
    public String smscode(@RequestParam String mobile) {
        return api("/auth/smscode").data("mobile", mobile).get(String.class);
    }

    @RequestMapping(value = "/ajax/authc/principal", produces = "text/json;charset=UTF-8")
    @ResponseBody
    public Principal principal() {
        return getSessionAttribute(SESSIONKEY_PRINCIPAL);
    }

    @RequestMapping(value = "/ajax/authc/login", params = "password", method = RequestMethod.POST, produces = "text/json;charset=UTF-8")
    @ResponseBody
    public Principal login(@RequestParam String mobile, @RequestParam String password) {
        Principal principal = api("/auth/login").data("mobile", mobile, "password", password, "pushKey", "").post(Principal.class);
        setSessionAttribute(SESSIONKEY_PRINCIPAL, principal);
        return principal;
    }

    @RequestMapping(value = "/ajax/authc/login", params = "!password", method = RequestMethod.POST, produces = "text/json;charset=UTF-8")
    @ResponseBody
    public Principal smslogin(@RequestParam String mobile, @RequestParam String token, @RequestParam String smscode) {
        Principal principal = api("/auth/login").data("mobile", mobile, "token", token, "smscode", smscode, "pushKey", "").post(Principal.class);
        setSessionAttribute(SESSIONKEY_PRINCIPAL, principal);
        return principal;
    }

    @RequestMapping(value = "/ajax/authc/forgot", method = RequestMethod.POST, produces = "text/json;charset=UTF-8")
    @ResponseBody
    public Principal forgot(@RequestParam String mobile,
            @RequestParam String token,
            @RequestParam String smscode,
            @RequestParam String password) {
        Principal principal = api("/auth/passwd").data("mobile", mobile, "token", token, "smscode", smscode, "password", password, "pushKey", "").post(Principal.class);
        setSessionAttribute(SESSIONKEY_PRINCIPAL, principal);
        return principal;
    }

    @RequestMapping(value = "/ajax/authc/regist", method = RequestMethod.POST, produces = "text/json;charset=UTF-8")
    @ResponseBody
    public Principal regist(@RequestParam String mobile,
            @RequestParam String token,
            @RequestParam String smscode,
            @RequestParam String password) {
        Principal principal = api("/auth/register").data("mobile", mobile, "token", token, "smscode", smscode, "password", password, "pushKey", "").post(Principal.class);
        setSessionAttribute(SESSIONKEY_PRINCIPAL, principal);
        return principal;
    }

    @RequestMapping("/authc/logout")
    public String logout() {
        setSessionAttribute(SESSIONKEY_PRINCIPAL, null);
        return "redirect:" + Utils.retrieveSavedRequest();
    }
}
