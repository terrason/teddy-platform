/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.portal;

import com.hyl.teddy.portal.util.Utils;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.type.JavaType;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

/**
 *
 * @author terrason
 */
public class ApiConnection {

    private final Logger logger = LoggerFactory.getLogger(ApiConnection.class);

    private final Connection connection;

    public ApiConnection(Connection connection) {
        this.connection = connection;
    }

    public ApiConnection url(URL url) {
        connection.url(url);
        return this;
    }

    public ApiConnection url(String url) {
        connection.url(url);
        return this;
    }

    public ApiConnection userAgent(String userAgent) {
        connection.userAgent(userAgent);
        return this;
    }

    public ApiConnection timeout(int millis) {
        connection.timeout(millis);
        return this;
    }

    public ApiConnection maxBodySize(int bytes) {
        connection.maxBodySize(bytes);
        return this;
    }

    public ApiConnection referrer(String referrer) {
        connection.referrer(referrer);
        return this;
    }

    public ApiConnection followRedirects(boolean followRedirects) {
        connection.followRedirects(followRedirects);
        return this;
    }

    public ApiConnection method(Connection.Method method) {
        connection.method(method);
        return this;
    }

    public ApiConnection ignoreHttpErrors(boolean ignoreHttpErrors) {
        connection.ignoreHttpErrors(ignoreHttpErrors);
        return this;
    }

    public ApiConnection ignoreContentType(boolean ignoreContentType) {
        connection.ignoreContentType(ignoreContentType);
        return this;
    }

    public ApiConnection data(String key, Object value) {
        connection.data(key, value == null ? "" : value.toString());
        return this;
    }

    public ApiConnection data(Collection<Connection.KeyVal> data) {
        connection.data(data);
        return this;
    }

    public ApiConnection data(Map<String, String> data) {
        connection.data(data);
        return this;
    }

    public ApiConnection data(String... keyvals) {
        connection.data(keyvals);
        return this;
    }

    public ApiConnection header(String name, String value) {
        connection.header(name, value);
        return this;
    }

    public ApiConnection cookie(String name, String value) {
        connection.cookie(name, value);
        return this;
    }

    public ApiConnection cookies(Map<String, String> cookies) {
        connection.cookies(cookies);
        return this;
    }

    public ApiConnection parser(Parser parser) {
        connection.parser(parser);
        return this;
    }

    public <T> T get(Class<T> rtnType) throws CommonRuntimeException {
        try {
            logger.debug("GET {}", connection.request().url());
            Document document = get();
            String text = document.body().text();
            logger.debug(text);
            BaseResponse response = Utils.json().readValue(text, BaseResponse.class);
            if (response.getCode() != 0) {
                throw new CommonRuntimeException(HttpStatus.valueOf(response.getCode()), response.getMessage());
            }
            String segment = text.split("\"data\":")[1];
            String data = segment.substring(0, segment.length() - 1);
            return Utils.json().readValue(data, rtnType);
        } catch (IOException ex) {
            throw new CommonRuntimeException(HttpStatus.GATEWAY_TIMEOUT, "接口调用失败", ex);
        }
    }

    public <T> List<T> list(Class<T> rtnType) throws CommonRuntimeException {
        try {
            logger.debug("GET {}", connection.request().url());
            Document document = get();
            String text = document.body().text();
            logger.debug(text);
            BaseResponse response = Utils.json().readValue(text, BaseResponse.class);
            if (response.getCode() != 0) {
                throw new CommonRuntimeException(HttpStatus.valueOf(response.getCode()), response.getMessage());
            }
            String segment = text.split("\"data\":")[1];
            String data = segment.substring(0, segment.length() - 1);
            JavaType parametricType = Utils.json().getTypeFactory().constructParametricType(ArrayList.class, rtnType);
            ArrayList<T> list = new ArrayList<T>();
            return Utils.json().readValue(data, parametricType);
        } catch (IOException ex) {
            throw new CommonRuntimeException(HttpStatus.GATEWAY_TIMEOUT, "接口调用失败", ex);
        }
    }

    public Document get() throws IOException {
        return connection.get();
    }

    public <T> T post(Class<T> rtnType) throws CommonRuntimeException {
        try {
            logger.debug("POST {}", connection.request().url());
            Document document = post();
            String text = document.body().text();
            logger.debug(text);
            BaseResponse response = Utils.json().readValue(text, BaseResponse.class);
            if (response.getCode() != 0) {
                throw new CommonRuntimeException(HttpStatus.valueOf(response.getCode()), response.getMessage());
            }
            if (rtnType == null) {
                return null;
            }
            String segment = text.split("\"data\":")[1];
            String data = segment.substring(0, segment.length() - 1);
            return Utils.json().readValue(data, rtnType);
        } catch (IOException ex) {
            throw new IORuntimeException("接口调用失败", ex);
        }
    }

    public Document post() throws IOException {
        return connection.post();
    }

    public Connection.Response execute() throws IOException {
        return connection.execute();
    }

    public Connection.Request request() {
        return connection.request();
    }

    public Connection request(Connection.Request request) {
        return connection.request(request);
    }

    public Connection.Response response() {
        return connection.response();
    }

    public Connection response(Connection.Response response) {
        return connection.response(response);
    }

}
