/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.portal.profile;

import com.hyl.teddy.portal.AbstractController;
import java.io.IOException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author terrason
 */
@Controller
public class PaymentController extends AbstractController {

    @RequestMapping("/profile/payment/{orderno}")
    public String buy(@PathVariable String orderno, Model ui) throws IOException {

        return "payment";
    }
}
