/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.portal.restaurant;

import java.util.List;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 *
 * @author terrason
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Restaurant {

    /**
     * 餐厅ID.
     */
    private int id;
    /**
     * 餐厅名称.
     */
    private String name;
    /**
     * 餐厅图标.
     */
    private String icon;
    /**
     * 餐厅星评.
     */
    private int star;
    /**
     * 餐厅活动说明.
     */
    private String promotion;
    /**
     * 人均消费.
     */
    private float price;
    /**
     * 销量.
     */
    private int sold;
    /**
     * 优惠标签.
     */
    private String[] tag;
    /**
     * 是否营业中.
     */
    private boolean enabled;

    /**
     * 菜品种类.
     */
    private List<DishCategory> category;

    /**
     * 餐厅ID.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * 餐厅ID.
     *
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 餐厅名称.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * 餐厅名称.
     *
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 餐厅图标.
     *
     * @return the icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     * 餐厅图标.
     *
     * @param icon the icon to set
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * 餐厅星评.
     *
     * @return the star
     */
    public int getStar() {
        return star;
    }

    /**
     * 餐厅星评.
     *
     * @param star the star to set
     */
    public void setStar(int star) {
        this.star = star;
    }

    /**
     * 餐厅活动说明.
     *
     * @return the promotion
     */
    public String getPromotion() {
        return promotion;
    }

    public void setNotice(String notice) {
        promotion = notice;
    }

    /**
     * 餐厅活动说明.
     *
     * @param promotion the promotion to set
     */
    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }

    /**
     * 人均消费.
     *
     * @return the price
     */
    public float getPrice() {
        return price;
    }

    /**
     * 人均消费.
     *
     * @param price the price to set
     */
    public void setPrice(float price) {
        this.price = price;
    }

    /**
     * 销量.
     *
     * @return the sold
     */
    public int getSold() {
        return sold;
    }

    /**
     * 销量.
     *
     * @param sold the sold to set
     */
    public void setSold(int sold) {
        this.sold = sold;
    }

    /**
     * 优惠标签.
     *
     * @return the tag
     */
    public String[] getTag() {
        return tag;
    }

    /**
     * 优惠标签.
     *
     * @param tag the tag to set
     */
    public void setTag(String[] tag) {
        this.tag = tag;
    }

    /**
     * 是否营业中.
     *
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * 是否营业中.
     *
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * 菜品种类.
     *
     * @return the category
     */
    public List<DishCategory> getCategory() {
        return category;
    }

    /**
     * 菜品种类.
     *
     * @param category the category to set
     */
    public void setCategory(List<DishCategory> category) {
        this.category = category;
    }
}
