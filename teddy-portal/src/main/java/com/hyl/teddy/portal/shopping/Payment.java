/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.portal.shopping;

import com.hyl.teddy.portal.profile.Address;
import com.hyl.teddy.portal.shopping.impl.DefaultPromotion;
import java.util.ArrayList;

/**
 *
 * @author terrason
 */
public class Payment {

    private String orderno;//订单号
    private String courierNumber;
    private String courierName;
    private String courierContact;
    private Address address;
    private boolean vip;
    private int restaurantId;//餐厅
    private String restaurantImg;
    private String restaurantName;
    private String restaurantLocation;
    private String restaurantContact;
    private int shared;//分享后所得代金券，未分享为-1
    /**
     * 订单状态 0-未付款 1-支付失败 2-未指派 3-配餐中 4- 缺失菜品 5-配餐完成 6-配送中 8-已完成 9- 已取消
     */
    private int status;
    private String statusDesc;//订单状态描述
    private String memo;//客户订餐备注
    private int cash;
    private int score;//此次订单可产生积分
    private String createTime;//创建时间
    private int star;//评价星级
    private String comment;//内容
    private boolean editable;//是否可编辑，用户客户加菜
    private ArrayList<Item> goods;
    private ArrayList<DefaultPromotion> promotions;
    private double cost;//订单当前实际费用
    private double primeCost;
    private double packageFee;
    private double delivery;
    private String readyTime;//取菜确认时间
    private String assignTime;//后台分配配送员时间（配送员接单时间）

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public double getPrimeCost() {
        return primeCost;
    }

    public void setPrimeCost(double primeCost) {
        this.primeCost = primeCost;
    }

    public double getPackageFee() {
        return packageFee;
    }

    public void setPackageFee(double packageFee) {
        this.packageFee = packageFee;
    }

    public double getDelivery() {
        return delivery;
    }

    public void setDelivery(double delivery) {
        this.delivery = delivery;
    }

    public String getCourierNumber() {
        return courierNumber;
    }

    public void setCourierNumber(String courierNumber) {
        this.courierNumber = courierNumber;
    }

    public String getCourierName() {
        return courierName;
    }

    public void setCourierName(String courierName) {
        this.courierName = courierName;
    }

    public String getCourierContact() {
        return courierContact;
    }

    public void setCourierContact(String courierContact) {
        this.courierContact = courierContact;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public boolean isVip() {
        return vip;
    }

    public void setVip(boolean vip) {
        this.vip = vip;
    }

    public int getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(int restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getRestaurantImg() {
        return restaurantImg;
    }

    public void setRestaurantImg(String restaurantImg) {
        this.restaurantImg = restaurantImg;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getRestaurantLocation() {
        return restaurantLocation;
    }

    public void setRestaurantLocation(String restaurantLocation) {
        this.restaurantLocation = restaurantLocation;
    }

    public String getRestaurantContact() {
        return restaurantContact;
    }

    public void setRestaurantContact(String restaurantContact) {
        this.restaurantContact = restaurantContact;
    }

    public int getShared() {
        return shared;
    }

    public void setShared(int shared) {
        this.shared = shared;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public int getCash() {
        return cash;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public ArrayList<Item> getGoods() {
        return goods;
    }

    public void setGoods(ArrayList<Item> goods) {
        this.goods = goods;
    }

    public ArrayList<DefaultPromotion> getPromotions() {
        return promotions;
    }

    public void setPromotions(ArrayList<DefaultPromotion> promotions) {
        this.promotions = promotions;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getReadyTime() {
        return readyTime;
    }

    public void setReadyTime(String readyTime) {
        this.readyTime = readyTime;
    }

    public String getAssignTime() {
        return assignTime;
    }

    public void setAssignTime(String assignTime) {
        this.assignTime = assignTime;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (Item item : goods) {
            builder.append(item.getName()).append("×").append(item.getCount()).append("，");
        }
        if (builder.length() > 0) {
            builder.deleteCharAt(builder.length() - 1);
        }
        return builder.toString();
    }
}
