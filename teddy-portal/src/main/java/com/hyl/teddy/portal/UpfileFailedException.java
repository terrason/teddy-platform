/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.portal;

/**
 *
 * @author Administrator
 */
public class UpfileFailedException extends Exception {

    public UpfileFailedException() {
        super("文件上传失败");
    }

    public UpfileFailedException(String string) {
        super("文件上传失败：" + string);
    }

    public UpfileFailedException(String string, Throwable thrwbl) {
        super("文件上传失败：" + string, thrwbl);
    }

    public UpfileFailedException(Throwable thrwbl) {
        super("文件上传失败", thrwbl);
    }

}
