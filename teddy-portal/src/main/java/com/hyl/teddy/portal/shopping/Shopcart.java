/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.portal.shopping;

import java.io.Serializable;
import java.util.Map;

/**
 *
 * @author terrason
 */
public final class Shopcart implements Serializable {

    /**
     * 店铺ID.
     */
    private int restaurant;
    /**
     * 购物详情.
     */
    private Map<String, Item> items;
    /**
     * 已付款. 加菜有效.
     */
    private double paid;
    /**
     * 备注. 加菜有效.
     */
    private String memo;

    /**
     * 店铺ID.
     *
     * @return the restaurant
     */
    public int getRestaurant() {
        return restaurant;
    }

    /**
     * 店铺ID.
     *
     * @param restaurant the restaurant to set
     */
    public void setRestaurant(int restaurant) {
        this.restaurant = restaurant;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * 购物详情.
     *
     * @return the items
     */
    public Map<String, Item> getItems() {
        return items;
    }

    /**
     * 购物详情.
     *
     * @param items the items to set
     */
    public void setItems(Map<String, Item> items) {
        this.items = items;
    }

    /**
     * 已付款.
     *
     * @return the paid
     */
    public double getPaid() {
        return paid;
    }

    /**
     * 已付款.
     *
     * @param paid the paid to set
     */
    public void setPaid(double paid) {
        this.paid = paid;
    }

}
