/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.portal.auth;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author terrason
 */
public class Location {

    private String uri;
    private final Set<String> activation = new HashSet<>();
    private Object module;

    public Object getModule() {
        return module;
    }

    public void setModule(Object module) {
        this.module = module;
    }

    /**
     * @return the uri
     */
    public String getUri() {
        return uri;
    }

    /**
     * @param uri the uri to set
     */
    public void setUri(String uri) {
        this.uri = uri;
    }

    /**
     * @return the activation
     */
    public Set<String> getActivation() {
        return activation;
    }

    public void setActivation(String[] s) {
        activation.clear();
        if (s != null) {
            for (int i = 0; i < s.length; i++) {
                activation.add(s[i]);
            }
        }
    }

    @Override
    public String toString() {
        return uri;
    }
}
