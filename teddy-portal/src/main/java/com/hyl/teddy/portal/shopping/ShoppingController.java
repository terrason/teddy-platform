/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.portal.shopping;

import com.hyl.teddy.portal.AbstractController;
import com.hyl.teddy.portal.profile.Address;
import com.hyl.teddy.portal.util.Utils;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author terrason
 */
@Controller
public class ShoppingController extends AbstractController {

    private final Logger logger = LoggerFactory.getLogger(ShoppingController.class);
    @Value("#{settings['url.alipay.return']}")
    private String returnUrl;

    @RequestMapping(value = "/ajax/shopcart", produces = "text/json;charset=UTF-8")
    @ResponseBody
    public int synchronize(@RequestParam int restaurant, @RequestParam String shopcart) throws IOException {
        Map<Integer, Shopcart> scs = getOrCreateSessionAttribute("shopcart", HashMap.class);
        Shopcart sc = scs.get(restaurant);
        if (sc == null) {
            sc = new Shopcart();
            scs.put(restaurant, sc);
        }
        sc.setRestaurant(restaurant);
        if ("[]".equals(shopcart)) {
            sc.setItems(Collections.EMPTY_MAP);
            return 0;
        }
        LinkedHashMap<String, Item> items = Utils.json().readValue(shopcart, new TypeReference<LinkedHashMap<String, Item>>() {
        });

        sc.setItems(items);
        return 0;
    }

    @RequestMapping(value = "/restaurant/{id}/pay", method = RequestMethod.GET)
    public String readyForPay(@PathVariable int id, Model ui) throws IOException {
        Context context = api("/buy")
                .data("restaurant", id)
                .data("principal", getPrincipalId())
                .get(Context.class);
        List<Address> addresses = api("/profile/address")
                .data("principal", getPrincipalId())
                .list(Address.class);
        context.setAddresses(addresses);
        ui.addAttribute("shopping", context);
        ui.addAttribute("shoppingConfiguration", Utils.json().writeValueAsString(context));
        return "pay";
    }

    @RequestMapping("/alipayCallback")
    public String alipayCallback(@RequestParam("out_trade_no") String orderno,
            @RequestParam("trade_no") String alipayno,
            @RequestParam("trade_status") String status,
            @RequestParam("total_fee") double fee,
            Map params) throws IOException {
        logger.debug("支付宝返回结果...\n订单号：{}\n支付宝交易号：{}\n状态：{}", new Object[]{orderno, alipayno, status});
        //TODO 验证是否是支付宝发出的请求

        api("/alipayCallback")
                .data("out_trade_no", orderno)
                .data("trade_no", alipayno)
                .data("trade_status", status)
                .data("total_fee", fee)
                .get();

        return "redirect:/profile/payment/" + orderno;
    }

    @RequestMapping("/buy")
    public String buy(@RequestParam String goods, @RequestParam double cost, @RequestParam int restaurant, Model ui) throws IOException {
        Payment payment = api("/buy")
                .data("goods", goods)
                .data("cost", cost)
                .post(Payment.class);
        HashMap<String, String> data = api("/inner/create-pay")
                .data("orderno", payment.getOrderno())
                //                .data("fee", payment.getCost())
                .data("fee", "0.02")
                .data("subject", payment.getRestaurantName())
                .data("body", payment.toString())
                .data("returnUrl", returnUrl)
                .get(HashMap.class);
        ui.addAttribute("data", data);
        Map<Integer, Shopcart> scs = getSessionAttribute("shopcart");
        if (scs != null) {
            scs.remove(restaurant);
        }
        return "buy";
    }
}
