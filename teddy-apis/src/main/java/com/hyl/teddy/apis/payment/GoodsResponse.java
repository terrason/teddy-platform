package com.hyl.teddy.apis.payment;

import com.hyl.teddy.apis.entity.PaymentGoods;
import java.util.List;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * 订单详情里的菜品集合
 *
 * @author Administrator
 *
 */
public class GoodsResponse {

    private int principal;//当前登陆用户id
    private int address;//送餐地址id
    private int restaurant;//餐厅id
    private String memo;//备注
    private List<PaymentGoods> dishes;
    private int cash;//客户使用的代金券面值，没有写0
    private int score;//客户消费抵押积分，没有写0

    @JsonIgnore
    public boolean isEmpty() {
        return dishes == null || dishes.isEmpty();
    }

    public int getPrincipal() {
        return principal;
    }

    public void setPrincipal(int principal) {
        this.principal = principal;
    }

    public int getAddress() {
        return address;
    }

    public void setAddress(int address) {
        this.address = address;
    }

    public int getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(int restaurant) {
        this.restaurant = restaurant;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public List<PaymentGoods> getDishes() {
        return dishes;
    }

    public void setDishes(List<PaymentGoods> dishes) {
        this.dishes = dishes;
    }

    public int getCash() {
        return cash;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

}
