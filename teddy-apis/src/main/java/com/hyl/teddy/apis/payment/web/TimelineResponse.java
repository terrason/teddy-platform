package com.hyl.teddy.apis.payment.web;

import com.hyl.teddy.apis.payment.TimeStep;
import java.util.List;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * 订单状态Response
 *
 * @author Administrator
 *
 */
public class TimelineResponse {

    @JsonIgnore
    private int id;
    private String createTime;//订餐时间
    /**
     * 1-待支付 2-已下单 3-已接单 4-配送中 5-已完成 6-已取消
     */
    private int status; //
    private String statusText;//订单状态描述
    private String statusDesc;//订单描述
    private String statusTime;//订单最后时刻 取自TimeSteps.get(0).getCreateTime();
    private String courierNumber; //配送员编号
    private String courierMobile;//配送员手机
    private List<TimeStep> steps;

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getStatusTime() {
        return statusTime;
    }

    public void setStatusTime(String statusTime) {
        this.statusTime = statusTime;
    }

    public String getCourierNumber() {
        return courierNumber;
    }

    public void setCourierNumber(String courierNumber) {
        this.courierNumber = courierNumber;
    }

    public String getCourierMobile() {
        return courierMobile;
    }

    public void setCourierMobile(String courierMobile) {
        this.courierMobile = courierMobile;
    }

    public List<TimeStep> getSteps() {
        return steps;
    }

    public void setSteps(List<TimeStep> steps) {
        this.steps = steps;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
