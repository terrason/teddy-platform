/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.asynchronous.jdbc;

import com.hyl.teddy.apis.CommonRuntimeException;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.stereotype.Service;

/**
 * 异步数据库操作.
 *
 * @author Administrator
 */
@Service
public class AsyncDao extends Thread {

    private final Logger logger = LoggerFactory.getLogger(AsyncDao.class);
    private final BlockingQueue<Update> sharedQueue = new LinkedBlockingQueue<>();
//    private final Pattern pattern = Pattern.compile("(update\\s+[\\p{Alnum}_`]+\\s+set\\s+([\\p{Alnum}_`]+)\\s*=\\s*\\2\\s*\\[+-]\\s*)(1)(\\s+where.+)");

    @Resource
    private TransactionalExecutor transactionalExecutor;

    @Override
    public void run() {
        while (true) {
            try {
                Update sql = sharedQueue.take();
                if (sql.isBatched()) {
                    transactionalExecutor.serializableExecute(sql.getSql(), sql.getBatch());
                } else if (sql.isMultiple()) {
                    transactionalExecutor.serializableExecute(sql.getSql(), sql.toArgs());
                } else {
                    transactionalExecutor.serializableExecute(sql.getSql(), sql.toArgs().get(0));
                }
            } catch (Exception ex) {
                logger.error("忽略数据库错误:" + ex.getMessage(), ex);
            }
        }
    }

    /**
     * 异步批量执行SQL语句.
     */
    public void update(String sql, List<Object[]> parameters) {
        Update update = new Update(sql);
        for (Object[] params : parameters) {
            update.add(params);
        }
        try {
            sharedQueue.put(update);
            notifyNewPut();
        } catch (InterruptedException ex) {
            throw new CommonRuntimeException(HttpStatus.EXPECTATION_FAILED, "加入异步SQL语句出错", ex);
        }
    }

    /**
     * 异步批量更新数据库.
     */
    public void update(String sql, BatchPreparedStatementSetter batchPreparedStatementSetter) {
        Update update = new Update(sql);
        update.setBatch(batchPreparedStatementSetter);
        try {
            sharedQueue.put(update);
            notifyNewPut();
        } catch (InterruptedException ex) {
            throw new CommonRuntimeException(HttpStatus.EXPECTATION_FAILED, "加入异步SQL语句出错", ex);
        }
    }

    /**
     * 异步更新数据库.
     */
    public void update(String sql, Object... parameters) {
        try {
            sharedQueue.put(new Update(sql).add(parameters));
            notifyNewPut();
        } catch (InterruptedException ex) {
            throw new CommonRuntimeException(HttpStatus.EXPECTATION_FAILED, "加入异步SQL语句出错", ex);
        }
    }

    private void notifyNewPut() {
        if (!isAlive()) {
            setDaemon(true);
            start();
        }
    }
}
