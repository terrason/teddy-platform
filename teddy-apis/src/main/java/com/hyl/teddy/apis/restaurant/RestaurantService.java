package com.hyl.teddy.apis.restaurant;

import com.hyl.teddy.apis.entity.Payment;
import com.hyl.teddy.apis.entity.PaymentGoods;
import com.hyl.teddy.apis.entity.Restaurant;
import com.hyl.teddy.apis.payment.PaymentListenerAdaptor;
import com.hyl.teddy.apis.payment.PaymentStatusChangeEvent;
import com.hyl.teddy.apis.profile.FavoritesService;
import com.hyl.teddy.apis.promotion.Promotion;
import com.hyl.teddy.apis.restaurant.web.RestaurantListResponse;
import com.hyl.teddy.apis.restaurant.dao.RestaurantDao;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.annotation.Resource;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * 餐厅Service
 *
 * @author Administrator
 */
@Service
public class RestaurantService extends PaymentListenerAdaptor {

    @Resource
    private RestaurantDao dao;

    @Resource
    private FavoritesService favoritesService;
    @Resource
    private CacheManager cacheManager;

    private final Set<Integer> dirtyForStarCalculation = new HashSet<Integer>();

    /**
     * 餐厅列表
     *
     * @param category
     * @param orderby
     * @param start
     * @param size
     * @return
     */
    public List<RestaurantListResponse> restaurantList(int category, int orderby, int start, int size) {
        return dao.restaurantList(category, orderby, start, size);
    }

    /**
     * 餐厅搜索
     *
     * @param keyword
     * @param start
     * @param size
     * @return
     */
    public List<RestaurantListResponse> searchRestaurantList(String keyword, int start, int size) {
        return dao.searchRestaurantList(keyword, start, size);
    }

    public Restaurant getRestaurant(int id) {
        return dao.selectRestaurant(id);
    }

    /**
     * 餐厅详情
     *
     * @param id 餐厅ID
     * @param principal 用户ID
     * @return
     */
    public Restaurant queryRestaurant(int id, int principal) {
        Restaurant restaurant = getRestaurant(id);
        restaurant.setFavored(favoritesService.isCustomerFavoredRestaurant(principal, id));
        return restaurant;
    }

    /**
     * 查询某店铺的优惠信息.
     *
     * @param restaurantId 店铺ID
     * @return 优惠信息
     */
    public List<Promotion> queryPromotionByRestaurant(int restaurantId) {
        return dao.selectPromotionByRestaurant(restaurantId);
    }

    /**
     * 增加店铺及菜品销量
     *
     * @param restaurantId 店铺ID
     * @param goodList 订单菜品明细
     */
    public void increaseSalesVolume(int restaurantId, List<PaymentGoods> goodList) {
        int volume = dao.updateDishesSalesVolume(goodList);
        dao.updateRestaurantSold(restaurantId, volume);
        Cache cache = cacheManager.getCache("restaurant");
        if (cache == null) {
            return;
        }
        synchronized (cache) {
            Cache.ValueWrapper valueWrapper = cache.get(restaurantId);
            if (valueWrapper == null) {
                return;
            }
            Restaurant entity = (Restaurant) valueWrapper.get();
            entity.setSold(entity.getSold() + volume);
            cache.put(restaurantId, entity);
        }
    }

    /**
     * 在监听到订单状态改变为5时，更新餐厅及菜品销量.
     */
    @Override
    public void fireStatusChanged(PaymentStatusChangeEvent event) {
        Payment payment = event.getPayment();
        if (payment.getStatus() == 5) {
            logger.debug("监听到订单状态改变为5，更新餐厅及菜品销量...");
            List<PaymentGoods> goods = payment.getGoods();
            if (goods != null && goods.size() > 0) {
                int restaurantId = payment.getRestaurantId();
                //存dish表的id 和count
                List<PaymentGoods> goodList = new ArrayList<PaymentGoods>();
                for (PaymentGoods g : goods) {
                    if (g.getStatus() < 2) {
                        goodList.add(g);
                    }
                }
                increaseSalesVolume(restaurantId, goodList);
            }
            logger.debug("销量更新结束======>");
        }
    }

    @Override
    public int getPriority() {
        return 230;
    }

    public void calcStar(int restaurantId) {
        dirtyForStarCalculation.add(restaurantId);
    }

    @Scheduled(cron = "0 0/30 * * * ?")
    public void starCalculation() {
        logger.debug("店铺星评统计任务启动...");
        Iterator<Integer> iterator = dirtyForStarCalculation.iterator();
        while (iterator.hasNext()) {
            Integer next = iterator.next();
            dao.updateStarCalculation(next);
            iterator.remove();
        }

        logger.debug("店铺星评统计任务结束 >>>>");
    }
}
