/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.payment;

import com.hyl.teddy.apis.AbstractComponent;

/**
 *
 * @author terrason
 */
public abstract class PaymentListenerAdaptor extends AbstractComponent implements PaymentListener {

    @Override
    public int compareTo(PaymentListener t) {
        return this.getPriority() - t.getPriority();
    }

}
