package com.hyl.teddy.apis.profile.dao;

import com.hyl.teddy.apis.entity.Cash;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.hyl.teddy.apis.entity.Customer;
import com.hyl.teddy.apis.entity.CustAddress;
import com.hyl.teddy.apis.profile.MessageResponse;
import java.util.Date;
import java.util.Map;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.RowCallbackHandler;

@Repository
public class CustomerDao {

    @Resource
    private JdbcTemplate jdbcTemplate;

    public Integer selectCustomer(String mobile) {
        String sql = "select id from customer where mobile=?";

        List<Integer> customer = jdbcTemplate.query(sql, new RowMapper<Integer>() {
            @Override
            public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getInt(1);
            }
        }, mobile);
        return customer.isEmpty() ? null : customer.get(0);
    }

    @Cacheable(value = "customer", key = "#principal", unless = "#result == null")
    public Customer selectCustomer(int principal) {
        String sql = "select c.id, c.password,c.push_key,c.mobile,c.nickname,c.score,c.score_highest,c.avatar,c.disabled,count(addr.id)=0 as maiden from customer c left join cust_address addr on c.id=addr.customer_id where c.id=? group by c.id";

        List<Customer> customers = jdbcTemplate.query(sql, new RowMapper<Customer>() {
            @Override
            public Customer mapRow(ResultSet rs, int rowNum) throws SQLException {
                Customer cust = new Customer();
                cust.setId(rs.getInt("id"));
                cust.setMobile(rs.getString("mobile"));
                cust.setPassword(rs.getString("password"));
                cust.setNickname(rs.getString("nickname"));
                cust.setScore(rs.getInt("score"));

                cust.setScoreHighest(rs.getInt("score_highest"));
                cust.setAvatar(rs.getString("avatar"));
                cust.setMaiden(rs.getBoolean("maiden"));
                cust.setDisabled(rs.getBoolean("disabled"));
                cust.setPushKey(rs.getString("push_key"));
                return cust;
            }
        }, principal);
        if (customers.isEmpty()) {
            return null;
        }
        final Customer customer = customers.get(0);

        jdbcTemplate.query("select cash,`count` from cust_cash where customer_id=?", new RowCallbackHandler() {

            @Override
            public void processRow(ResultSet rs) throws SQLException {
                int cash = rs.getInt("cash");
                int volume = rs.getInt("count");
                customer.increaseCash(cash, volume);
            }
        }, principal);
        return customer;
    }

    public int insertCustomer(final Customer customer) {
        final String sql = "insert into customer(mobile,password,nickname,score_highest,score,avatar,reg_time,push_key,disabled)values(?,?,?,?,?,?,now(),?,0)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
                PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, customer.getMobile());
                ps.setString(2, customer.getPassword());
                ps.setString(3, customer.getNickname());
                ps.setInt(4, customer.getScoreHighest());
                ps.setInt(5, customer.getScore());
                ps.setString(6, customer.getAvatar());
                ps.setString(7, customer.getPushKey());
                return ps;
            }
        }, keyHolder);
        return keyHolder.getKey().intValue();
    }

    public Map<Integer, Cash> selectCustomerCashes(int principal) {
        return null;
    }

    /**
     * 更新客户信息.
     *
     * @param customer
     */
    @CacheEvict(value = "customer", key = "#customer.id")
    public void updateCustomer(Customer customer) {
        jdbcTemplate.update("update customer set nickname=?,password=?,avatar=?,push_key=?  where id=?",
                customer.getNickname(),
                customer.getPassword(),
                customer.getAvatar(),
                customer.getPushKey(),
                customer.getId());
    }

    public CustAddress selectDefaultAddress(final int principal) {
        String sql = " SELECT a.* FROM cust_address a WHERE a.customer_id = ? and a.`default` = 1 ";
        List<CustAddress> list = jdbcTemplate.query(sql, new RowMapper<CustAddress>() {

            @Override
            public CustAddress mapRow(ResultSet rs, int rowNum) throws SQLException {
                CustAddress a = new CustAddress();
                a.setCustomerId(principal);
                a.setDefaultAddress(rs.getBoolean("default"));
                a.setId(rs.getInt("id"));
                a.setLongitudeValue(rs.getInt("longitude"));
                a.setLatitudeValue(rs.getInt("latitude"));
                a.setLocation(rs.getString("location"));
                a.setName(rs.getString("name"));
                a.setMobile(rs.getString("mobile"));
                a.setRegionId(rs.getInt("region_id"));
                return a;
            }

        }, principal);
        return list.isEmpty() ? null : list.get(0);
    }

    public List<MessageResponse> selectCustomerMessage(int id, int start, int size) {
        return jdbcTemplate.query("select id,title,create_time from cust_message where customer_id = ? limit ?,?", new RowMapper<MessageResponse>() {

            @Override
            public MessageResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
                MessageResponse m = new MessageResponse();
                m.setId(rs.getInt("id"));
                m.setTitle(rs.getString("title"));
                Date createDate = rs.getDate("create_time");
                if (createDate != null) {
                    m.setCreateTime(DateFormatUtils.format(createDate, "yyyy-MM-dd"));
                }

                return m;
            }

        }, id, start, size);
    }

    /**
     * 增加用户积分. 不加经验值.
     *
     * @param id 用户ID
     * @param score 要增加的积分
     */
    public void updateOnlyScore4Increase(int id, int score) {
        jdbcTemplate.update("update customer set score=score+? where id=?", score, id);
    }

    /**
     * 增加用户积分. 同时增加经验值.
     *
     * @param id 用户ID
     * @param score 要增加的积分
     */
    public void updateScore4Increase(int id, int score) {
        jdbcTemplate.update("update customer set score=score+?,score_highest=score_highest+? where id=?", score, score, id);
    }

    /**
     * 减少用户积分. 不会减少经验值.
     *
     * @param id 用户ID
     * @param score 要减少的积分
     */
    public void updateScore4Decrease(int id, int score) {
        jdbcTemplate.update("update customer set score=score-? where id=?", score, id);
    }

    /**
     * 减少用户代金券. 一次减少一张.
     *
     * @param principal 用户ID
     * @param cash 要减少的代金券面值
     */
    public void updateCustomerCash4Decrease(int principal, int cash, int count) {
        jdbcTemplate.update("update cust_cash set `count`=`count`-? where customer_id=? and cash=?", count, principal, cash);
    }

    /**
     * 增加用户代金券. 一次增加一张.
     *
     * @param principal 用户ID
     * @param cash 要增加的代金券面值
     */
    public void updateCustomerCash4Increase(int principal, int cash, int count) {
        int c = jdbcTemplate.update("update cust_cash set `count`=`count`+? where customer_id=? and cash=?", count, principal, cash);
        if (c == 0) {
            jdbcTemplate.update("insert into cust_cash(customer_id,cash,`count`)values(?,?,?)", principal, cash, count);
        }
    }
}
