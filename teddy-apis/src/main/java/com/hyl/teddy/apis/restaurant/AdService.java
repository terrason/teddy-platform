package com.hyl.teddy.apis.restaurant;

import com.hyl.teddy.apis.entity.Advertisement;
import com.hyl.teddy.apis.restaurant.dao.AdDao;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * 餐厅广告位Service
 *
 * @author Administrator
 */
@Service
public class AdService {

    @Resource
    private AdDao dao;

    public List<Advertisement> queryRestaurantAds() {
        return dao.selectRestaurantAds();
    }

    public List<Advertisement> queryHomeAds() {
        return dao.selectHomeAds();
    }
}
