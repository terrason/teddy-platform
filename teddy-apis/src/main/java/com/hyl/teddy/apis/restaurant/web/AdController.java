package com.hyl.teddy.apis.restaurant.web;

import com.hyl.teddy.apis.BaseResponse;
import com.hyl.teddy.apis.entity.Advertisement;
import com.hyl.teddy.apis.restaurant.AdService;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 餐厅广告位
 * @author Administrator
 */
@Controller
@RequestMapping
public class AdController {
    @Resource
    private AdService adService;
    
    @RequestMapping(value = "/restaurant/ad",method=RequestMethod.GET)
    @ResponseBody
    public BaseResponse adList(){
        BaseResponse response = new BaseResponse();
        List<Advertisement> a = adService.queryRestaurantAds();
        response.setData(a);
        return response;
    }
}
