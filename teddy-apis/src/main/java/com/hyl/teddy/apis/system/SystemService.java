package com.hyl.teddy.apis.system;

import com.hyl.teddy.apis.attachment.AttachmentService;
import com.hyl.teddy.apis.restaurant.AdService;
import com.hyl.teddy.apis.restaurant.CategoryService;
import com.hyl.teddy.apis.system.dao.SystemDao;

import java.util.*;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

/**
 * 客户Service
 *
 * @author Messi
 */
@Service
public class SystemService {

    @Resource
    private SystemDao dao;
    @Resource
    private AdService adService;
    @Resource
    private CategoryService categoryService;
    @Resource
    private AttachmentService attachmentService;

    public Map<String, String> getSystemConfiguration() {
        return dao.selectStaticResource();
    }

    /**
     * 熊仔电话.
     */
    public String getServiceTelephone() {
        return getSystemConfiguration().get("service.tel");
    }

    /**
     * 关于我们.
     */
    public String getAboutus() {
        return getSystemConfiguration().get("service.aboutus");
    }

    /**
     * 技术支持.
     */
    public String getSupport() {
        return getSystemConfiguration().get("service.support");
    }

    /**
     * 许可协议.
     */
    public String getLicense() {
        return getSystemConfiguration().get("service.license");
    }

    /**
     * 代金券使用规则.
     */
    public String getCashrule() {
        return getSystemConfiguration().get("service.cash.rule");
    }

    /**
     * 客户是否可以填写评价内容.
     */
    public boolean isCommentWithContent() {
        return Boolean.parseBoolean(getSystemConfiguration().get("config.comment.withContent"));
    }

    /**
     * 送积分规则. 多少元送1积分
     */
    public int getScorePerMoney() {
        return Integer.parseInt(getSystemConfiguration().get("config.score.scorePerMoney"));
    }

    /**
     * 是否自动分配取菜员.
     */
    public boolean isDeliverAutoAssignable() {
        return Boolean.parseBoolean(getSystemConfiguration().get("config.deliver.autoAssignable"));
    }

    /**
     * 是否自动分配配送员.
     */
    public boolean isCourierAutoAssignable() {
        return Boolean.parseBoolean(getSystemConfiguration().get("config.courier.autoAssignable"));
    }

    /**
     * 客户VIP的积分阙值.
     */
    public int getVipExperience() {
        return Integer.parseInt(getSystemConfiguration().get("config.vip.experience"));
    }
    /**
     * 分享订单获得代金券面值
     * @return 
     */
    public int getSharePayment(){
        return Integer.parseInt(getSystemConfiguration().get("share.payment"));
    }
    /**
     * 领取分享获得代金券面值
     * @return 
     */
    public int getShareReceive(){
        return Integer.parseInt(getSystemConfiguration().get("share.receive"));
    }
    /**
     * 版本
     *
     * @param mobile
     * @param password
     * @param pushKey
     * @return
     */
    public VersionCustomer getVersionForUpgrade(int code) {
        VersionCustomer version = dao.selectVersionForUpgrade(code);
        if (version != null) {
            version.setDownloadUrl(attachmentService.toResourceUrl(version.getDownloadId()));
        }
        return version;
    }

    public SystemResponse queryHomeResponse() {
        SystemResponse sr = new SystemResponse();
        sr.setContact(getServiceTelephone());
        sr.setDisplayComment(isCommentWithContent());
        sr.setAdvertise(adService.queryHomeAds());
        sr.setModule(categoryService.categoryList());
        return sr;
    }
}
