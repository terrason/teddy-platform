package com.hyl.teddy.apis.profile.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.hyl.teddy.apis.profile.FeedbackResponse;

@Repository
public class FeedbackDao {
	@Resource
	private JdbcTemplate jdbc;
	
	/**
	 * 根据用户id 获得反馈信息
	 * @param custId
	 * @return
	 */
	public List<FeedbackResponse> findFeedbackByCustId(int custId) {
		String sql = " SELECT a.id AS id, a.content AS content, a.create_time AS  createTime ,b.content AS replyContent, b.create_time AS replyTime " +
				"FROM feedback a LEFT JOIN reply b ON a.id = b.feedback_id " +
				"WHERE a.customer_id = ? ORDER BY a.create_time; ";
		return jdbc.query(sql, new RowMapper<FeedbackResponse>(){

			@Override
			public FeedbackResponse mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				FeedbackResponse f = new FeedbackResponse();
				f.setId(rs.getInt("id"));
				f.setContent(rs.getString("content"));
				Date createDate = rs.getDate("createTime");
				Date replyDate = rs.getDate("replyTime");
				if(createDate != null) {
					f.setCreateTime(DateFormatUtils.format(createDate,"yyyy-MM-dd"));
				}
				if(replyDate != null) {
					f.setReplyTime(DateFormatUtils.format(replyDate,"yyyy-MM-dd"));
				}
				f.setReplyContent(rs.getString("replyContent"));
				return f;
			}
			
		}, custId);
	}

	/**
	 *  新增意见反馈
	 * @param principal
	 * @param content
	 */
	public int addFeedback(int customerId, String content) {
		String sql = " INSERT INTO feedback(content,create_time,`read`,customer_id) VALUES(?,NOW(),0,?) ";
		return jdbc.update(sql, content, customerId);
		
	}
}
