/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.jpush;

import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Message;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.Notification;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author terrason
 */
public class PushClient {

    private final String title;
    private final String appkey;
    private final String secret;
    private final boolean production;
    private final int liveTime;
    private final Map<String, Object> extras = new LinkedHashMap<String, Object>();
    private final Collection<String> to = new ArrayList<String>();
    private final Logger logger = LoggerFactory.getLogger(PushClient.class);

    public PushClient(String title, String appkey, String secret, boolean production, int liveTime) {
        this.title = title;
        this.appkey = appkey;
        this.secret = secret;
        this.production = production;
        this.liveTime = liveTime;
    }

    public PushClient data(String key, Object value) {
        extras.put(key, value);
        return this;
    }

    public PushClient to(String registerId) {
        to.add(registerId);
        return this;
    }

    public boolean push() {
        JPushClient jpush = new JPushClient(secret, appkey, liveTime);
        Message.Builder message = Message.newBuilder().setMsgContent("extraMsg");
        for (Map.Entry<String, Object> entrySet : extras.entrySet()) {
            String key = entrySet.getKey();
            Object value = entrySet.getValue();
            if (value == null) {
            } else if (value instanceof Boolean) {
                message.addExtra(key, (Boolean) value);
            } else if (value instanceof Number) {
                message.addExtra(key, (Number) value);
            } else if (value instanceof String) {
                message.addExtra(key, (String) value);
            } else {
                message.addExtra(key, value.toString());
            }
        }
        try {
            Audience audience;
            if (to.isEmpty()) {
                audience = Audience.all();
            } else {
                audience = Audience.registrationId(to);
            }
            PushPayload pushPayload = PushPayload
                    .newBuilder()
                    .setPlatform(Platform.all())
                    .setAudience(audience)
                    .setNotification(
                            Notification.newBuilder().setAlert(title).build())
                    .setMessage(message.build())
                    .setOptions(
                            Options.newBuilder().setApnsProduction(production)
                            .setSendno(Integer.parseInt((new Date().getTime() / 1000) + "")).build()).build();
            logger.debug("jpush:{}", pushPayload);
            PushResult result = jpush.sendPush(pushPayload);
            if (!result.isResultOK()) {
                logger.warn("推送失败");
            }
            return result.isResultOK();
        } catch (Exception ex) {
            logger.warn("推送失败", ex);
            return false;
        }
    }
}
