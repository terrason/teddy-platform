/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.restaurant.thumbnails;

import com.hyl.teddy.apis.thumbnails.ThumbSize;
import com.hyl.teddy.apis.thumbnails.impl.AttachmentThumbnailsGenerator;
import java.awt.Dimension;
import org.springframework.stereotype.Service;

/**
 * 餐厅广告位缩略图
 *
 * @author Administrator
 */
@Service("resturantAdvertisement")
public class AdvertisementForResturant extends AttachmentThumbnailsGenerator {

    public String toThumbmailsMD(int resource) {
        return toThumbmails(resource, ThumbSize.MD);
    }

    @Override
    protected Dimension size(ThumbSize pattern) {
        switch (pattern) {
            case MD:
                return new Dimension(606, 150);//606*150
            default:
                return null;
        }
    }
}
