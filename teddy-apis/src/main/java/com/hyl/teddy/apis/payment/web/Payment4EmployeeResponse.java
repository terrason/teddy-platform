/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.payment.web;

import com.hyl.teddy.apis.entity.Payment;
import java.util.Collection;

/**
 *
 * @author terrason
 */
public class Payment4EmployeeResponse {

    /**
     * 订单数.
     */
    private int total;
    /**
     * 取消订单数.
     */
    private int cancel;

    /**
     * 订单列表.
     */
    private Collection<Payment> elements;

    /**
     * 订单数.
     * @return the total
     */
    public int getTotal() {
        return total;
    }

    /**
     * 订单数.
     * @param total the total to set
     */
    public void setTotal(int total) {
        this.total = total;
    }

    /**
     * 取消订单数.
     * @return the cancel
     */
    public int getCancel() {
        return cancel;
    }

    /**
     * 取消订单数.
     * @param cancel the cancel to set
     */
    public void setCancel(int cancel) {
        this.cancel = cancel;
    }

    /**
     * 订单列表.
     * @return the elements
     */
    public Collection<Payment> getElements() {
        return elements;
    }

    /**
     * 订单列表.
     * @param elements the elements to set
     */
    public void setElements(Collection<Payment> elements) {
        this.elements = elements;
    }
}
