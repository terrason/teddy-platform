package com.hyl.teddy.apis.restaurant.web;

import com.hyl.teddy.apis.AbstractComponent;
import com.hyl.teddy.apis.BaseResponse;
import com.hyl.teddy.apis.entity.Restaurant;
import com.hyl.teddy.apis.promotion.Promotion;
import com.hyl.teddy.apis.restaurant.RestaurantService;
import com.hyl.teddy.apis.restaurant.thumbnails.RestaurantImage;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 餐厅
 *
 * @author Administrator
 */
@Controller
@RequestMapping
public class RestaurantController extends AbstractComponent {

    @Resource
    private RestaurantService rs;
    @Resource
    private RestaurantImage restaurantImage;

    /**
     * 餐厅列表
     *
     * @param category 类别
     * @param orderby 排序
     * @param start 起始页
     * @param size 页长
     * @return
     */
    @RequestMapping(value = "/restaurant", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse restaurantList(@RequestParam(defaultValue = "0") int category, @RequestParam(defaultValue = "1") int orderby, @RequestParam(defaultValue = "0") int start, @RequestParam(defaultValue = "10") int size) {
        BaseResponse response = new BaseResponse();
        if (orderby > 2 || orderby < 1) {
            orderby = 1;
        }
        List<RestaurantListResponse> restaurantResponse = rs.restaurantList(category, orderby, start, size);
        response.setData(restaurantResponse);
        return response;
    }

    /**
     * 餐厅搜索
     *
     * @param keyword
     * @param start
     * @param size
     * @return
     */
    @RequestMapping(value = "/search/restaurant", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse searchRestaurantList(@RequestParam String keyword, @RequestParam(defaultValue = "0") int start, @RequestParam(defaultValue = "10") int size) {
        BaseResponse response = new BaseResponse();
        List<RestaurantListResponse> RestaurantResponse = rs.searchRestaurantList(keyword, start, size);
        response.setData(RestaurantResponse);
        return response;
    }

    /**
     * 餐厅详情.
     */
    @RequestMapping(value = "/restaurant/{id}", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse queryRestaurant(@PathVariable int id, @RequestParam(defaultValue = "0") int principal) {
        BaseResponse response = new BaseResponse();
        Restaurant restaurant = rs.queryRestaurant(id, principal);
        List<Promotion> queryPromotionByRestaurant = rs.queryPromotionByRestaurant(id);
        if (!queryPromotionByRestaurant.isEmpty()) {
            List<String> tags = new ArrayList<>();
            for (Promotion promotion : queryPromotionByRestaurant) {
                tags.add(promotion.name());
            }
            restaurant.setTag(tags);
        }
        response.setData(restaurant);
        return response;
    }

}
