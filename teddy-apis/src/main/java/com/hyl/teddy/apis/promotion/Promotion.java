/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.promotion;

import java.util.ArrayList;
import java.util.List;

/**
 * 优惠政策
 *
 * @author Administrator
 */
public enum Promotion {

    WIN_CASH, VIP_DISCOUNT, COST_CASH, COST_SCORE, FREE_POSTAGE;

    private int[] params;
    private final List<PromotionDiscountHandler> promotionDiscountHandlers = new ArrayList<PromotionDiscountHandler>();

    public int[] getParams() {
        return params;
    }

    public void setParams(int[] params) {
        this.params = params;
    }

    /**
     * 设置优惠费用.
     *
     * @param money 常为负值. 单位：分
     */
    public void setDiscount(int money) {
        for (PromotionDiscountHandler handler : promotionDiscountHandlers) {
            handler.setMoney(money);
        }
    }

    public void addHandler(PromotionDiscountHandler handler) {
        promotionDiscountHandlers.add(handler);
    }
}
