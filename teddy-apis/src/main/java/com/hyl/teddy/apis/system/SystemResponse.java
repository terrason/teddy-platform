package com.hyl.teddy.apis.system;

import com.hyl.teddy.apis.entity.Advertisement;
import com.hyl.teddy.apis.entity.RestaurantCategory;
import java.util.Collection;
import java.util.List;

/**
 * 广告位 advertisement
 *
 * @author Administrator
 *
 */
public class SystemResponse {

    private String contact;
    private boolean displayComment;
    private List<RestaurantCategory> module;
    private List<Advertisement> advertise;

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public boolean getDisplayComment() {
        return displayComment;
    }

    public void setDisplayComment(boolean displayComment) {
        this.displayComment = displayComment;
    }

    public Collection<RestaurantCategory> getModule() {
        return module;
    }

    public void setModule(List<RestaurantCategory> module) {
        this.module = module;
    }

    public List<Advertisement> getAdvertise() {
        return advertise;
    }

    public void setAdvertise(List<Advertisement> advertise) {
        this.advertise = advertise;
    }

}
