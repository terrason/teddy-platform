/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.payment;

import com.hyl.teddy.apis.CommonRuntimeException;
import com.hyl.teddy.apis.entity.Payment;
import com.hyl.teddy.apis.payment.dao.AlipayDao;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

/**
 *
 * @author terrason
 */
@Service
public class AlipayService {

    private final Logger logger = LoggerFactory.getLogger(AlipayService.class);
    @Resource
    private AlipayDao alipayDao;

    @Value("#{settings['alipay.test']}")
    private boolean trial;
    @Value("#{settings['alipay.pay.notify']}")
    private String payCallback;
    @Value("#{settings['alipay.url']}")
    private String url;
    @Value("#{settings['alipay.seller_email']}")
    private String teddyAccount;
    @Value("#{settings['alipay.seller_user_id']}")
    private String teddyPid;
    @Value("#{settings['alipay.certificate']}")
    private String certificate;

    public Endorser createRefund(String id, String method, String notifyUrl, Payment payment, double fee, String description, Date refundDate) {
        if (trial) {
            fee = 0.01;
        }
        Endorser endorser = new Endorser()
                .data("service", "refund_fastpay_by_platform_pwd")
                .data("partner", teddyPid)
                .data("_input_charset", "UTF-8")
                .data("notify_url", notifyUrl)
                .data("seller_user_id", teddyPid)
                .data("refund_date", DateFormatUtils.format(refundDate, "yyyy-MM-dd hh:mm:ss"))
                .data("batch_no", id)
                .data("batch_num", "1")
                .data("detail_data", payment.getAlipayNo() + "^" + fee + "^" + description);
        alipayDao.insertRefundRequest(id, method, payment.getOrderno(), endorser.get("refund_date"), endorser.get("detail_data"));
        return endorser;
    }

    public Endorser createPay(String orderno, String subject, String body, String returnUrl, double fee) {
        return new Endorser()
                .data("service", "create_direct_pay_by_user")
                .data("partner", teddyPid)
                .data("_input_charset", "utf-8")
                .data("payment_type", "1")
                .data("notify_url", payCallback)
                .data("return_url", returnUrl)
                .data("seller_id", teddyPid)
                .data("out_trade_no", orderno)
                .data("subject", subject)
                .data("total_fee", String.valueOf(fee))
                .data("body", body)
                .data("show_url", "http://www.tdxmeal.com");
    }

    public void createRefundResponse(String id, int successNum, String resultDetails) {
        alipayDao.insertRefundResponse(id, successNum, resultDetails);
    }

    public void updateRefundStatus(AlipayRefundRequest request, boolean success) {
        alipayDao.updateRefundRequestStatus(request.getId(), success, request.getOrderno());
    }

    public void savePaid(String orderno,String alipayno,double fee){
        alipayDao.insertPaid(orderno,alipayno,fee);
    }
    public AlipayRefundRequest getRefundRequest(String id) {
        String orderno = alipayDao.selectRequestOrderno(id);
        AlipayRefundRequest request = alipayDao.selectPaymentRequest(orderno);
        if (request == null) {
            request = alipayDao.selectRequest(id);
        }
        return request;
    }

    public boolean isOccupied(String orderno) {
        return alipayDao.selectPaymentRequest(orderno) != null;
    }

    public class Endorser {

        private final Map<String, String> actors = new TreeMap<String, String>();

        public Endorser data(String key, String value) {
            actors.put(key, value);
            return this;
        }

        public String endorse() {
            StringBuilder builder = new StringBuilder();
            for (Map.Entry<String, String> entrySet : actors.entrySet()) {
                String key = entrySet.getKey();
                String value = entrySet.getValue();
                if (StringUtils.isNotBlank(value)) {
                    builder.append("&").append(key).append("=").append(value);
                }
            }

            if (builder.length() > 0) {
                builder.deleteCharAt(0);
            }

            builder.append(certificate);
            logger.debug("签名信息明文：{}", builder);
            try {
                return DigestUtils.md5DigestAsHex(builder.toString().getBytes("UTF-8"));
            } catch (UnsupportedEncodingException ex) {
                logger.error("签名时出错", ex);
                throw new CommonRuntimeException(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        public Map<String, String> getActors() {
            return new HashMap(actors);
        }

        public Map<String, String> toParameters() {
            Map<String, String> params = getActors();
            params.put("sign", endorse());
            params.put("sign_type", "MD5");
            return params;
        }

        public String get(String key) {
            return actors.get(key);
        }
    }
}
