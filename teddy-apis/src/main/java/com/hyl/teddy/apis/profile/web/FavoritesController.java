package com.hyl.teddy.apis.profile.web;

import java.util.List;

import javax.annotation.Resource;
import javax.naming.AuthenticationException;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hyl.teddy.apis.BaseResponse;
import com.hyl.teddy.apis.entity.Customer;
import com.hyl.teddy.apis.profile.CustomerService;
import com.hyl.teddy.apis.profile.FavoritesDishResponse;
import com.hyl.teddy.apis.profile.FavoritesService;
import com.hyl.teddy.apis.payment.PaymentService;
import com.hyl.teddy.apis.restaurant.web.RestaurantListResponse;
import org.springframework.http.HttpStatus;

/**
 * teddy个人中心接口 16 我的收藏（餐厅） 17 我的收藏（菜品） 18 添加到收藏
 *
 * 19 取消收藏
 *
 * @author Administrator
 *
 */
@Controller
public class FavoritesController {

    @Resource
    private FavoritesService service;
    @Resource
    private MessageSource mss;

    @Resource
    private PaymentService orderService;
    @Resource
    private CustomerService customerService;

    /**
     * 16 我的收藏（餐厅）
     *
     * @param principal 当前登陆用户id
     * @return
     * @throws AuthenticationException
     */
    @RequestMapping(value = "/profile/favorite/restaurant", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse favoritesRestaurant(
            @RequestParam(defaultValue = "0") int principal)
            throws AuthenticationException {
        if (principal == 0) {
            throw new AuthenticationException();
        }
        BaseResponse b = new BaseResponse();
        List<RestaurantListResponse> list = service
                .findRestaurantByCustId(principal);
        b.setData(list);
        return b;
    }

    /**
     * 17 我的收藏（菜品）
     *
     * @param principal 当前登陆用户id
     * @return
     * @throws AuthenticationException
     */
    @RequestMapping(value = "/profile/favorite/dish", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse favoritesDish(
            @RequestParam(defaultValue = "0") int principal)
            throws AuthenticationException {
        if (principal == 0) {
            throw new AuthenticationException();
        }
        BaseResponse b = new BaseResponse();
        List<FavoritesDishResponse> list = service.findDisHByCustId(principal);
        b.setData(list);
        return b;
    }

    /**
     * 18 1添加到收藏 — 餐厅
     *
     * @param principal 当前用户id
     * @param restaurant 餐厅id
     * @return
     * @throws AuthenticationException
     */
    @RequestMapping(value = "/profile/favorite", method = RequestMethod.POST, params = "restaurant")
    @ResponseBody
    public BaseResponse addFavoriteForRestaurant(
            @RequestParam(defaultValue = "0") int principal,
            @RequestParam int restaurant) throws AuthenticationException {
        return addFavorite(principal, 1, restaurant);
    }

    /**
     * 18 2添加到收藏 — 菜品
     *
     * @param principal 当前用户id
     * @param restaurant 菜品id
     * @return
     * @throws AuthenticationException
     */
    @RequestMapping(value = "/profile/favorite", method = RequestMethod.POST, params = "dish")
    @ResponseBody
    public BaseResponse addFavoriteForDish(
            @RequestParam(defaultValue = "0") int principal,
            @RequestParam int dish) throws AuthenticationException {
        return addFavorite(principal, 2, dish);
    }

    /**
     * 19 1 取消收藏 餐厅
     *
     * @param principal
     * @param restaurant
     * @return
     * @throws AuthenticationException
     */
    @RequestMapping(value = "/profile/unfavorite", method = RequestMethod.POST, params = "restaurant")
    @ResponseBody
    public BaseResponse removeFavoriteForRestaurant(
            @RequestParam(defaultValue = "0") int principal,
            @RequestParam int restaurant) throws AuthenticationException {
        return removeFavorite(principal, 1, restaurant);
    }

    /**
     * 19 2 取消收藏 菜品
     *
     * @param principal
     * @param dish
     * @return
     * @throws AuthenticationException
     */
    @RequestMapping(value = "/profile/unfavorite", method = RequestMethod.POST, params = "dish")
    @ResponseBody
    public BaseResponse removeFavoriteForDish(
            @RequestParam(defaultValue = "0") int principal,
            @RequestParam int dish) throws AuthenticationException {
        return removeFavorite(principal, 2, dish);
    }

    /**
     * 收藏餐厅或菜品的逻辑
     *
     * @param customerId 用户id
     * @param type 1 餐厅 2 菜品
     * @param target 餐厅或者菜品id
     * @return
     * @throws AuthenticationException
     */
    public BaseResponse addFavorite(int customerId, int type, int target)
            throws AuthenticationException {
        if (customerId == 0) {
            throw new AuthenticationException();
        }
        BaseResponse b = new BaseResponse();
        Customer customer = customerService.getCustomer(customerId);
        // 用户是否存在
        if (customer == null) {
            b.setCode(HttpStatus.NOT_FOUND.value());
            b.setMessage(mss.getMessage("error.default", null, null));
            return b;
        }
        // 判断菜品是否收藏过
        if (service.existFavorite(customerId, type, target)) {
            b.setCode(HttpStatus.ALREADY_REPORTED.value());
            b.setMessage(mss.getMessage("favorite.addFailed", null, null));
            return b;
        }
        service.addFavorite(customerId, type, target);
        return b;

    }

    /**
     *
     * 取消收藏餐厅或菜品的逻辑
     *
     * @param customerId 用户id
     * @param type 1 餐厅 2 菜品
     * @param target 餐厅或者菜品id
     * @return
     * @throws AuthenticationException
     */
    public BaseResponse removeFavorite(int customerId, int type, int target)
            throws AuthenticationException {
        if (customerId == 0) {
            throw new AuthenticationException();
        }
        BaseResponse b = new BaseResponse();
        
        // 用户是否存在
        Customer customer = customerService.getCustomer(customerId);
        if (customer == null) {
            b.setCode(HttpStatus.NOT_FOUND.value());
            b.setMessage(mss.getMessage("error.default", null, null));
            return b;
        }
        // 判断菜品是否收藏过 没收藏过则提示 favorite.deleteFailed
        if (!service.existFavorite(customerId, type, target)) {
            b.setCode(208);
            b.setMessage(mss.getMessage("favorite.deleteFailed", null, null));
            return b;
        }
        service.removeFavorite(customerId, type, target);
        return b;

    }

}
