package com.hyl.teddy.apis.restaurant.dao;

import com.hyl.teddy.apis.attachment.AttachmentService;
import com.hyl.teddy.apis.entity.RestaurantCategory;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * 餐厅分类列表DAO
 *
 * @author Administrator
 */
@Repository
public class CategoryDao {

    @Resource
    private JdbcTemplate jdbcTemplate;
    @Resource
    private AttachmentService attachmentService;

    public List<RestaurantCategory> categoryList() {
        return jdbcTemplate.query("select rc.id,rc.`name`,rc.icon from restaurant_category rc", new RowMapper<RestaurantCategory>() {

            @Override
            public RestaurantCategory mapRow(ResultSet rs, int i) throws SQLException {
                RestaurantCategory rc = new RestaurantCategory();
                rc.setId(rs.getInt("id"));
                rc.setName(rs.getString("name"));
                rc.setIcon(attachmentService.toResourceUrl(rs.getInt("icon")));
                return rc;
            }

        });
    }
}
