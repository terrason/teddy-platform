/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.thumbnails;

/**
 *
 * @author Administrator
 */
public enum ThumbSize {

    /**
     * 极小型.
     */
    XS,
    /**
     * 小型.
     */
    SM,
    /**
     * 普通型.
     */
    MD,
    /**
     * 大型.
     */
    LG,
    /**
     * 极大型.
     */
    XL;

    private String lowercase;

    @Override
    public String toString() {
        if (lowercase == null) {
            lowercase = name().toLowerCase();
        }
        return lowercase;
    }
}
