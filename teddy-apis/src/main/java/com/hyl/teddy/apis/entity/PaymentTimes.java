package com.hyl.teddy.apis.entity;
/**
 * 订单状态变动记录表 payment_times
 * @author Administrator
 *
 */
public class PaymentTimes {
	private int id;
	private int paymentId;//订单id
	private int status;//订单状态
	private String createTime;//创建时间
	private String description;//订单状态描述
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(int paymentId) {
		this.paymentId = paymentId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
}
