/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.jpush;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * 泰迪熊配送员推送
 *
 * @author terrason
 */
@Service
public class CourierJpushService implements JpushService {

    @Value("#{settings['jpush.courier_appkey']}")
    private String appkey;
    @Value("#{settings['jpush.courier_mastersecret']}")
    private String secret;
    @Value("#{settings['jpush.apns_production']}")
    private boolean production;
    @Value("#{settings['jpush.time_to_live']}")
    private int liveTime;

    @Override
    public PushClient create(String title) {
        return new PushClient(title, appkey, secret, production, liveTime);
    }

}
