package com.hyl.teddy.apis.entity;

/**
 * 区域表 region
 *
 * @author Administrator
 *
 */
public class Region {

    private int id;
    /**
     * 区域名称.
     */
    private String name;
    /**
     * 区域各顶点.
     */
    private Point[] points;

    /**
     * 判断区域是否包含点.
     */
    public boolean contains(Point p) {
        int i, j;
        boolean inside=false, redo=true;
        if(points.length<3){
        	return false;
        }

        for (i = 0; i < points.length; ++i) {
            if (points[i].x == p.x && points[i].y == p.y) {// 是否在顶点上
                return true;
            }
        }

        while (redo) {
            redo = false;
            inside = false;
            for (i = 0, j = points.length - 1; i < points.length; j = i, i++) {
                if ((points[i].y < p.y && p.y < points[j].y)
                        || (points[j].y < p.y && p.y < points[i].y)) {
                    if (p.x <= points[i].x || p.x <= points[j].x) {
                        double _x = (p.y - points[i].y) * (points[j].x - points[i].x) / (points[j].y - points[i].y) + points[i].x;
                        if (p.x < _x) { // 在线的左侧
                            inside = !inside;
                        } else if (p.x == _x) { // 在线上
                            inside = true;
                            break;
                        }
                    }
                } else if (p.y == points[i].y) {
                    if (p.x < points[i].x) // 交点在顶点上
                    {
                        redo = true;
                        break;
                    }
                } else if (points[i].y == points[j].y && // 在水平的边界线上
                        p.y == points[i].y
                        && ((points[i].x < p.x && p.x < points[j].x)
                        || (points[j].x < p.x && p.x < points[i].x))) {
                    inside = true;
                    break;
                }
            }
        }

        return inside;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 区域名称.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * 区域名称.
     *
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 区域各顶点.
     *
     * @return the points
     */
    public Point[] getPoints() {
        return points;
    }

    /**
     * 区域各顶点.
     *
     * @param points the points to set
     */
    public void setPoints(Point[] points) {
        this.points = points;
    }

}
