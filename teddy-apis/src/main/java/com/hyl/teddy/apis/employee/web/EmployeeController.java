package com.hyl.teddy.apis.employee.web;

import com.hyl.teddy.apis.AbstractComponent;
import com.hyl.teddy.apis.BaseResponse;
import com.hyl.teddy.apis.employee.Employee;
import com.hyl.teddy.apis.employee.EmployeeService;

import javax.annotation.Resource;
import javax.naming.AuthenticationException;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 餐厅
 *
 * @author Administrator
 */
@Controller
@RequestMapping
public class EmployeeController extends AbstractComponent {

    @Resource
    private EmployeeService employeeService;

    /**
     * 用户登录.
     */
    @RequestMapping(value = "/employee/login", method = RequestMethod.POST)
    @ResponseBody
    public BaseResponse employeeLogin(@RequestParam String username,
            @RequestParam String password,
            @RequestParam(defaultValue = "") String pushKey) {
        BaseResponse response = new BaseResponse();
        Employee employee = employeeService.getEmployee(username);
        if (employee == null) {
            response.setCode(HttpStatus.NOT_FOUND.value());
            response.setMessage(messageSource.getMessage("authentication.failed.username", null, null));
            return response;
        }
        if (!password.equals(employee.getPassword())) {
            response.setCode(HttpStatus.FAILED_DEPENDENCY.value());
            response.setMessage(messageSource.getMessage("authentication.failed.password", null, null));
            return response;
        }
        login(employee, pushKey);
        response.setData(employee);
        return response;
    }

    private void login(Employee emp, String pushKey) {
        if (!pushKey.equals(emp.getPushKey())) {
            emp.setPushKey(pushKey);
            employeeService.updatePushkey(emp);

        }
    }

    /**
     * 修改密码.
     */
    @RequestMapping(value = "/employee/passwd", method = RequestMethod.POST)
    @ResponseBody
    public BaseResponse updatePwd(@RequestParam int principal,
            @RequestParam String pwdold,
            @RequestParam String pwdnew) throws AuthenticationException {
        BaseResponse response = new BaseResponse();
        Employee employee = employeeService.getEmployee(principal);
        if (employee == null) {
            throw new AuthenticationException();
        }
        if (!pwdold.equals(employee.getPassword())) {
            response.setCode(HttpStatus.FAILED_DEPENDENCY.value());
            response.setMessage(messageSource.getMessage("authentication.failed.password", null, null));
            return response;
        }
        employeeService.updatePwd(principal, pwdnew);
        return response;
    }

    /**
     * 更新坐标
     *
     * @param principal
     * @param longitude
     * @param latitude
     * @return
     */
    @RequestMapping(value = "/coordinate/update")
    @ResponseBody
    public BaseResponse locate(@RequestParam int principal,
            @RequestParam String longitude,
            @RequestParam String latitude) {

        BaseResponse response = new BaseResponse();
        int i = employeeService.updatePositiopn(principal, longitude, latitude);
        if (i < 1) {
            response.setCode(HttpStatus.NOT_FOUND.value());
            response.setMessage(messageSource.getMessage("error.default", null, null));
        }
        return response;
    }

}
