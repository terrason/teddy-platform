package com.hyl.teddy.apis.restaurant.web;

/**
 * 菜品详情
 *
 * @author Administrator
 */
public class DishResponse {

    private int id;
    private String name;//菜名
    /**
     * 当前用户是否收藏过该菜品。 true--已收藏；false--未收藏。若未传入登录用户ID，这里始终返回 false .
     */
    private boolean favored;
    private String image;//菜品图片  附件表id
    private double price; //价位
    private int sold;//销量
    private String description;//描述
    private boolean enabled;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isFavored() {
        return favored;
    }

    public void setFavored(boolean favored) {
        this.favored = favored;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getSold() {
        return sold;
    }

    public void setSold(int sold) {
        this.sold = sold;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "DishResponse{" + "id=" + id + ", name=" + name + ", favored=" + favored + ", image=" + image + ", price=" + price + ", sold=" + sold + ", description=" + description + ", enabled=" + enabled + '}';
    }
    
}
