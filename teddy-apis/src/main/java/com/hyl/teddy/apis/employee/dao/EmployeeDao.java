package com.hyl.teddy.apis.employee.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.hyl.teddy.apis.employee.Employee;
import com.hyl.teddy.apis.employee.Version;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.RowCallbackHandler;

@Repository
public class EmployeeDao {

    @Resource
    private JdbcTemplate jdbcTemplate;

    public Employee selectEmployee(String username) {
        String sql = "select c.id,c.username,c.password,c.name,c.mobile,c.push_key,c.type from employee c where c.username=?";

        List<Employee> customer = jdbcTemplate.query(sql, new RowMapper<Employee>() {
            @Override
            public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
                Employee entity = new Employee();
                entity.setId(rs.getInt("id"));
                entity.setMobile(rs.getString("mobile"));
                entity.setUsername(rs.getString("username"));
                entity.setPassword(rs.getString("password"));
                entity.setName(rs.getString("name"));
                entity.setType(rs.getInt("type"));
                entity.setPushKey(rs.getString("push_key"));
                return entity;
            }
        }, username);
        return customer.isEmpty() ? null : customer.get(0);
    }

    /**
     * 修改密码
     *
     * @param principal
     * @param psdnew
     * @return
     */
    public int updatePassword(int principal, String psdnew) {
        return jdbcTemplate.update("update employee set password=? where id=?", psdnew, principal);
    }

    public void updatepushKey(Employee emp) {
        jdbcTemplate.update("update employee set push_key=?  where id=?",
                emp.getPushKey(),
                emp.getId());
    }

    /**
     * 版本更新（取菜员）.
     */
    public Version selectUpgradeVersionForDeliver(int code) {
        String sql = "select vc.version,vc.log, vc.download_url from version_deliver vc where vc.code>? order by vc.code desc limit 1";
        List<Version> list = jdbcTemplate.query(sql, new RowMapper<Version>() {
            @Override
            public Version mapRow(ResultSet rs, int rowNum) throws SQLException {
                Version cust = new Version();

                cust.setVersion(rs.getString("version"));
                cust.setLog(rs.getString("log"));
                cust.setDowlloadUrl(rs.getString("download_url"));
                return cust;
            }

        }, code);
        return list.isEmpty() ? null : list.get(0);
    }

    /**
     * 版本更新（配菜员）.
     */
    public Version selectUpgradeVersionForCourier(int code) {
        String sql = "select vc.version,vc.log, vc.download_url from version_courier vc where vc.code>? order by vc.code desc limit 1";
        List<Version> list = jdbcTemplate.query(sql, new RowMapper<Version>() {
            @Override
            public Version mapRow(ResultSet rs, int rowNum) throws SQLException {
                Version cust = new Version();

                cust.setVersion(rs.getString("version"));
                cust.setLog(rs.getString("log"));
                cust.setDowlloadUrl(rs.getString("download_url"));
                return cust;
            }

        }, code);
        return list.isEmpty() ? null : list.get(0);

    }

    /**
     * 配送员地址更新
     *
     * @param principal
     * @param status
     * @return
     */
    public int updatePosition(int principal, String longitude, String latitude) {
        return jdbcTemplate.update("update employee set longitude=?,latitude=? where id=?",
                Double.parseDouble(longitude) * 1000000,
                Double.parseDouble(latitude) * 1000000,
                principal);
    }

    @Cacheable("employees")
    public Map<Integer, Employee> selectAvailableEmployees() {
        final Map<Integer, Employee> employees = new HashMap<>();
        Date now = new Date();
        Date timeBegin = DateUtils.truncate(now, Calendar.DATE);
        Date timeEnd = DateUtils.addDays(timeBegin, 1);
        jdbcTemplate.query("select e.id,e.username,e.password,e.`name`,e.`type`,e.mobile,e.push_key,count(g.id) taskWeight\n"
                + "from employee e\n"
                + "left join payment p on (p.status between 3 and 4) and e.id=p.deliver_id\n"
                + "left join payment_goods g on p.id=g.payment_id\n"
                + "where e.`type`=1 and e.status<9\n"
                + "group by e.id", new RowCallbackHandler() {

                    @Override
                    public void processRow(ResultSet rs) throws SQLException {
                        Employee entity = new Employee();
                        entity.setId(rs.getInt("id"));
                        entity.setUsername(rs.getString("username"));
                        entity.setPassword(rs.getString("password"));
                        entity.setName(rs.getString("name"));
                        entity.setType(rs.getInt("type"));
                        entity.setMobile(rs.getString("mobile"));
                        entity.setPushKey(rs.getString("push_key"));
                        entity.setTaskWeight(rs.getInt("taskWeight"));
                        employees.put(entity.getId(), entity);
                    }
                }, (Object[]) null);
        jdbcTemplate.query("select deliver_id,orderno from payment where (create_time between ? and ?) and (status between 3 and 4)", new RowCallbackHandler() {

            @Override
            public void processRow(ResultSet rs) throws SQLException {
                int deliverId = rs.getInt(1);
                String orderno = rs.getString(2);
                Employee deliver = employees.get(deliverId);
                if (deliver != null) {
                    deliver.addWork(orderno);
                }
            }
        }, timeBegin, timeEnd);
        jdbcTemplate.query("select e.id,e.username,e.password,e.`name`,e.`type`,e.region_id,e.mobile,e.push_key,count(g.id) taskWeight\n"
                + "from employee e\n"
                + "left join payment p on p.status<8 and e.id=p.courier_id\n"
                + "left join payment_goods g on p.id=g.payment_id\n"
                + "where e.`type`=2 and e.status<9\n"
                + "group by e.id", new RowCallbackHandler() {

                    @Override
                    public void processRow(ResultSet rs) throws SQLException {
                        Employee entity = new Employee();
                        entity.setId(rs.getInt("id"));
                        entity.setUsername(rs.getString("username"));
                        entity.setPassword(rs.getString("password"));
                        entity.setName(rs.getString("name"));
                        entity.setType(rs.getInt("type"));
                        entity.setRegionId(rs.getInt("region_id"));
                        entity.setMobile(rs.getString("mobile"));
                        entity.setPushKey(rs.getString("push_key"));
                        entity.setTaskWeight(rs.getInt("taskWeight"));
                        employees.put(entity.getId(), entity);
                    }
                }, (Object[]) null);
        jdbcTemplate.query("select courier_id,orderno from payment where (create_time between ? and ?) and status=6", new RowCallbackHandler() {

            @Override
            public void processRow(ResultSet rs) throws SQLException {
                int courierId = rs.getInt(1);
                String orderno = rs.getString(2);
                Employee courier = employees.get(courierId);
                if (courier != null) {
                    courier.addWork(orderno);
                }
            }
        }, timeBegin, timeEnd);
        jdbcTemplate.query("select id,username,password,`name`,`type`,mobile,push_key from employee where `type`=3 and status<9", new RowCallbackHandler() {

            @Override
            public void processRow(ResultSet rs) throws SQLException {
                Employee entity = new Employee();
                entity.setId(rs.getInt("id"));
                entity.setUsername(rs.getString("username"));
                entity.setPassword(rs.getString("password"));
                entity.setName(rs.getString("name"));
                entity.setType(rs.getInt("type"));
                entity.setMobile(rs.getString("mobile"));
                entity.setPushKey(rs.getString("push_key"));
                employees.put(entity.getId(), entity);
            }
        }, (Object[]) null);
        return employees;
    }

    public void updateStatus(int id, int status) {
        jdbcTemplate.update("update employee set status=? where id=?", status, id);
    }
}
