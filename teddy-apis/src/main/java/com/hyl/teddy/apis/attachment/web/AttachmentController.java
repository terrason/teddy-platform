/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.attachment.web;

import com.hyl.teddy.apis.AbstractComponent;
import com.hyl.teddy.apis.attachment.AttachmentService;
import com.hyl.teddy.apis.attachment.thumbnails.DimensionThumbnails;
import java.awt.Dimension;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author terrason
 */
@Controller
public class AttachmentController extends AbstractComponent {

    private static final String COMMON_BROKEN_IMAGE_REDIREDTION = "/static/avatar/0.png";

    @Resource
    private AttachmentService attachmentService;
    @Resource
    private DimensionThumbnails dimensionThumbnails;

    @RequestMapping(value = "/attachment/{id}", method = RequestMethod.GET)
    public String view(@PathVariable int id, @RequestParam(required = false) Integer width, @RequestParam(required = false) Integer height) {
        String value = attachmentService.view(id);
        String redirectUrl;
        if (width == null || height == null) {
            redirectUrl = attachmentService.toResourceUrl(value);
        } else {
            redirectUrl = dimensionThumbnails.toThumbmails(value, new Dimension(width, height));
        }
        return "redirect:" + redirectUrl;
    }

    @RequestMapping(value = "/attachment/null", method = RequestMethod.GET)
    public String broken(@RequestParam(required = false) Integer width, @RequestParam(required = false) Integer height) {
        String value = COMMON_BROKEN_IMAGE_REDIREDTION;
        String redirectUrl;
        if (width == null || height == null) {
            redirectUrl = attachmentService.toResourceUrl(value);
        } else {
            redirectUrl = dimensionThumbnails.toThumbmails(value, new Dimension(width, height));
        }
        return "redirect:" + redirectUrl;
    }
}
