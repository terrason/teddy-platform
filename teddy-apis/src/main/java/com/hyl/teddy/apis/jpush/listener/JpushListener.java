package com.hyl.teddy.apis.jpush.listener;

import javax.annotation.Resource;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import com.hyl.teddy.apis.employee.Employee;
import com.hyl.teddy.apis.employee.EmployeeService;
import com.hyl.teddy.apis.entity.Customer;
import com.hyl.teddy.apis.entity.Payment;
import com.hyl.teddy.apis.jpush.CourierJpushService;
import com.hyl.teddy.apis.jpush.CustomerJpushService;
import com.hyl.teddy.apis.jpush.DeliverJpushService;
import com.hyl.teddy.apis.payment.PaymentListenerAdaptor;
import com.hyl.teddy.apis.payment.PaymentStatusChangeEvent;
import com.hyl.teddy.apis.profile.CustomerService;

/**
 * 极光推送监听
 *
 * @author VIC
 */
@Service
public class JpushListener extends PaymentListenerAdaptor {

    @Resource
    private CustomerJpushService customerJpushServier;

    @Resource
    private CourierJpushService courierJpushService;

    @Resource
    private DeliverJpushService deliverJpushService;

    @Resource
    private MessageSource messageSource;

    @Resource
    private EmployeeService employeeService;

    @Resource
    private CustomerService customerService;

    /**
     * 推送功能标识 0-活动
     */
    public static final Integer MODULE_ACITIVITY = 0;
    /**
     * 推送功能标识 1-取菜员取菜
     */
    public static final Integer MODULE_DELIVER_TAKE = 1;
    /**
     * 推送功能标识 2-配送员配送
     */
    public static final Integer MODULE_COURIER_TAKE = 2;
    /**
     * 推送功能标识 3-通知用户缺菜
     */
    public static final Integer MODULE_MISSING_DISH = 3;
    /**
     * 推送功能标识 4-通知用户完成订单
     */
    public static final Integer MODULE_ORDER_FINISH = 1;

    @Override
    public void fireStatusChanged(PaymentStatusChangeEvent event) {
        Payment payment = event.getPayment();
        String orderno = payment.getOrderno();
        int status = event.getPayment().getStatus();
        if (status == 3) {
            logger.debug("监听到订单状态改变为{}，通知取菜员取菜...", status);

            Employee employee = employeeService.getEmployee(payment.getDeliverId());
            if (employee != null) {
                deliverJpushService.create(messageSource.getMessage("push.deliver.take", new Object[]{orderno}, null))
                        .data("module", MODULE_DELIVER_TAKE)
                        .data("orderno", orderno)
                        .to(employee.getPushKey())
                        .push();
            }
            logger.debug("通知取菜员取菜结束============>");
        }
        if (status == 4) {
            logger.debug("监听到订单状态改变为{}，通知用户缺失菜品...", status);
            Customer customer = customerService.getCustomer(payment.getCustomerId());
            if (customer != null) {
                customerJpushServier.create(messageSource.getMessage("push.missing.dish", new Object[]{orderno}, null, null))
                        .data("module", MODULE_MISSING_DISH)
                        .data("orderno", orderno)
                        .to(customer.getPushKey())
                        .push();
            }
            logger.debug("通知用户缺失菜品结束============>");
        }
        if (status == 5) {
            logger.debug("监听到订单状态改变为{}，通知配送员配送", status);
            Employee employee = employeeService.getEmployee(payment.getCourierId());
            if (employee != null) {
                courierJpushService.create(messageSource.getMessage("push.courier.take", new Object[]{orderno}, null))
                        .data("module", MODULE_COURIER_TAKE)
                        .data("orderno", orderno)
                        .to(employee.getPushKey())
                        .push();
            }
            logger.debug("通知配送员配送结束============>");

        }
        if (status == 8) {
            logger.debug("监听到订单状态改变为{}，通知用户订单完成...", status);
            Customer customer = customerService.getCustomer(payment.getCustomerId());
            if (customer != null) {
                customerJpushServier.create(messageSource.getMessage("push.order.finish", new Object[]{orderno}, null, null))
                        .data("module", MODULE_ORDER_FINISH)
                        .data("orderno", payment.getOrderno())
                        .to(customer.getPushKey())
                        .push();
            }
            logger.debug("通知用户订单完成结束============>");
        }

    }

    @Override
    public int getPriority() {
        return 1732;
    }

}
