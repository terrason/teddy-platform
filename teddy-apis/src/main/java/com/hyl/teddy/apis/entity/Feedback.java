package com.hyl.teddy.apis.entity;
/**
 * 反馈意见 feedback
 * @author Administrator
 *
 */
public class Feedback {
	private int id;
	private String content;//反馈意见内容
	private String createTime;//反馈时间
	private int read;//是否已读 1-已读 0-新提交未阅读',
	private int customerId;//提交用户id.
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public int getRead() {
		return read;
	}
	public void setRead(int read) {
		this.read = read;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	
	
}
