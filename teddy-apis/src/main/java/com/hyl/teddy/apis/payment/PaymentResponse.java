package com.hyl.teddy.apis.payment;

import java.util.List;

import com.hyl.teddy.apis.entity.Payment;
import com.hyl.teddy.apis.entity.PaymentGoods;
import com.hyl.teddy.apis.profile.AddressResponse;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.springframework.format.annotation.NumberFormat;

/**
 * 订单详情
 *
 * @author Administrator
 *
 */
public class PaymentResponse {

    @JsonIgnore
    private int identification;//订单编号
    @JsonProperty("orderno")
    private String id;//订单号 orderno
    private String createTime;
    private AddressResponse address;//用户送餐地址
    private int restaurantId;//餐厅id
    private String restaurantName;//餐厅名称
    private double packageFee;//打包费 
    private double delivery;//送餐费
    private boolean vip;
    private List<PromotionResponse> promotion;//优惠标签
    private boolean editable;////是否可编辑，用于客户加菜
    private GoodsResponse goods;//菜品详情

    private double cost;//订单费用 （元）保留一位小数

    public PaymentResponse() {
    }

    public PaymentResponse(Payment p) {
        this.identification = p.getId();
        this.id = p.getOrderno();
        this.createTime = p.getCreateTime();
        this.address = new AddressResponse(p.getAddress());
        this.restaurantId = p.getRestaurantId();
        this.restaurantName = p.getRestaurantName();
        this.packageFee = p.getPackageFee();
        this.delivery = p.getDelivery();
        this.promotion = p.getPromotions();
        this.vip = p.isVip();
        this.editable = p.isEditable();
        this.goods = findGoods(p);
    }

    /**
     * 设置菜品详情
     *
     * @param p 订单
     * @return
     */
    private GoodsResponse findGoods(Payment p) {
        List<PaymentGoods> paymentGoods = p.getGoods();
        GoodsResponse response = new GoodsResponse();
        response.setAddress(p.getAddressId());
        response.setCash(p.getCash());
        response.setMemo(p.getMemo());
        response.setPrincipal(p.getCustomerId());
        response.setRestaurant(p.getRestaurantId());
        response.setScore(p.getScore());
        response.setDishes(paymentGoods);
        return response;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isVip() {
        return vip;
    }

    public void setVip(boolean vip) {
        this.vip = vip;
    }

    public int getIdentification() {
        return identification;
    }

    public void setIdentification(int identification) {
        this.identification = identification;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public AddressResponse getAddress() {
        return address;
    }

    public void setAddress(AddressResponse address) {
        this.address = address;
    }

    public int getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(int restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public double getPackageFee() {
        return packageFee;
    }

    public void setPackageFee(double packageFee) {
        this.packageFee = packageFee;
    }

    public double getDelivery() {
        return delivery;
    }

    public void setDelivery(double delivery) {
        this.delivery = delivery;
    }

    public List<PromotionResponse> getPromotion() {
        return promotion;
    }

    public void setPromotion(List<PromotionResponse> promotion) {
        this.promotion = promotion;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public GoodsResponse getGoods() {
        return goods;
    }

    public void setGoods(GoodsResponse goods) {
        this.goods = goods;
    }

    @NumberFormat(pattern = "#.##")
    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

}
