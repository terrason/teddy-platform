/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis;

/**
 * 短信验证码发送异常.
 *
 * @author Administrator
 */
public class SmsException extends Exception {

    public SmsException() {
    }

    public SmsException(String string) {
        super(string);
    }

    public SmsException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public SmsException(Throwable thrwbl) {
        super(thrwbl);
    }
}
