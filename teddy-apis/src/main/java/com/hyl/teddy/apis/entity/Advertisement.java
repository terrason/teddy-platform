package com.hyl.teddy.apis.entity;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * 广告位  advertisement
 * @author Administrator
 *
 */
public class Advertisement {
	private int id;
        @JsonIgnore
	private String category;//类型
	private String image;// 附件表id(图片) attachment表
	private String url;//广告跳转地址
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
}
