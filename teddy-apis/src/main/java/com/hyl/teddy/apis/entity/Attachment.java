package com.hyl.teddy.apis.entity;
/**
 *   附件表    attachment
 * @author Administrator
 *
 */
public class Attachment {
	private int id;
	private String file;//绝对路径
	private String	path;// 相对路径
	private String createTime;//创建时间
	private boolean temporary;//是否临时
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public boolean isTemporary() {
		return temporary;
	}
	public void setTemporary(boolean temporary) {
		this.temporary = temporary;
	}
	
	
}
