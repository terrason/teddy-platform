/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.payment;

import com.hyl.teddy.apis.entity.Payment;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author terrason
 */
public class PaymentStatusChangeEvent {

    private final int originalStatus;
    private final Payment payment;
    private final PaymentService paymentService;
    private final List<Runnable> doneCallbacks = new LinkedList<Runnable>();

    public PaymentStatusChangeEvent(Payment payment, int originalStatus, PaymentService paymentService) {
        this.payment = payment;
        this.originalStatus = originalStatus;
        this.paymentService = paymentService;
    }

    /**
     * 事件传播完时由事件源调用.
     */
    protected void done() {
        for (Runnable doneCallback : doneCallbacks) {
            doneCallback.run();
        }
    }

    /**
     * 添加事件结束回调. 这些回调会在时间传播完后调用。
     *
     * @param callback 回调
     */
    public void done(Runnable callback) {
        doneCallbacks.add(callback);
    }

    /**
     * 在事件结束后，将订单跳转到另一个状态.
     *
     * @param status 新的状态
     */
    public void gotoStatusAfterFinished(final int status) {
        done(new Runnable() {

            @Override
            public void run() {
                paymentService.firePaymentStatusChanged(payment, status);
            }
        });
    }

    public Payment getPayment() {
        return payment;
    }

    public PaymentService getPaymentService() {
        return paymentService;
    }

    public int getOriginalStatus() {
        return originalStatus;
    }
}
