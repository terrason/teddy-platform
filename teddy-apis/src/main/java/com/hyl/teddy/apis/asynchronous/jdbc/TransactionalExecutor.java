/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.asynchronous.jdbc;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service
public class TransactionalExecutor {

    @Resource
    private JdbcTemplate jdbcTemplate;

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void serializableExecute(String sql, List<Object[]> parameters) {
        jdbcTemplate.batchUpdate(sql, parameters);
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void serializableExecute(String sql, BatchPreparedStatementSetter batchPreparedStatementSetter) {
        jdbcTemplate.batchUpdate(sql, batchPreparedStatementSetter);
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void serializableExecute(String sql, Object[] parameters) {
        jdbcTemplate.update(sql, parameters);
    }
}
