package com.hyl.teddy.apis.restaurant.dao;

import com.hyl.teddy.apis.entity.Advertisement;
import com.hyl.teddy.apis.restaurant.thumbnails.AdvertisementForResturant;
import com.hyl.teddy.apis.system.thumbnails.AdvertisementForHome;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * 餐厅广告位
 *
 * @author Administrator
 */
@Repository
public class AdDao {

    @Resource
    private JdbcTemplate jdbcTemplate;
    @Resource
    private AdvertisementForResturant advertisementForResturant;
    @Resource
    private AdvertisementForHome advertisementForHome;

    public List<Advertisement> selectRestaurantAds() {
        return jdbcTemplate.query("select id,image,url from advertisement where category=2", new RowMapper<Advertisement>() {

            @Override
            public Advertisement mapRow(ResultSet rs, int i) throws SQLException {
                Advertisement a = new Advertisement();
                a.setId(rs.getInt("id"));
                a.setImage(advertisementForResturant.toThumbmailsMD(rs.getInt("image")));
                a.setUrl(rs.getString("url"));
                return a;
            }
        }, (Object[]) null);
    }

    public List<Advertisement> selectHomeAds() {
        return jdbcTemplate.query("select id,image,url from advertisement where category=1", new RowMapper<Advertisement>() {

            @Override
            public Advertisement mapRow(ResultSet rs, int i) throws SQLException {
                Advertisement a = new Advertisement();
                a.setId(rs.getInt("id"));
                a.setImage(advertisementForHome.toThumbmailsMD(rs.getInt("image")));
                a.setUrl(rs.getString("url"));
                return a;
            }
        }, (Object[]) null);
    }
}
