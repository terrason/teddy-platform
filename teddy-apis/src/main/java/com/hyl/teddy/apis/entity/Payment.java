package com.hyl.teddy.apis.entity;

import com.hyl.teddy.apis.CommonRuntimeException;
import com.hyl.teddy.apis.payment.PromotionResponse;

import java.util.ArrayList;
import java.util.List;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.http.HttpStatus;

/**
 * 订单 payment
 *
 * @author Administrator
 *
 */
public class Payment {

    @JsonIgnore
    private int id;
    private String orderno;//订单号
    @JsonIgnore
    private int deliverId;//配送方式id
    @JsonIgnore
    private int courierId;//配送员id
    private String courierNumber;
    private String courierName;
    private String courierContact;
    @JsonIgnore
    private int addressId;//地址
    private CustAddress address;
    @JsonIgnore
    private int customerId;//用户id
    private boolean vip;
    @JsonIgnore
    private int regionId;//区域
    private int restaurantId;//餐厅
    private String restaurantImg;
    private String restaurantName;
    private String restaurantLocation;
    private String restaurantContact;
    @JsonIgnore
    private int packageFeeValue;//打包费  （单位分）
    @JsonIgnore
    private int deliveryValue; //送餐费 单位分  
    private int shared;//分享后所得代金券，未分享为-1
    /**
     * 订单状态 0-未付款 1-支付失败 2-未指派 3-配餐中 4- 缺失菜品 5-配餐完成 6-配送中 8-已完成 9- 已取消
     */
    private int status;
    private String statusDesc;//订单状态描述
    private String memo;//客户订餐备注
    private int cash;
    private int score;//此次订单可产生积分
    private String createTime;//创建时间
    private int star;//评价星级
    private String comment;//内容
    private boolean editable;//是否可编辑，用户客户加菜
    private List<PaymentGoods> goods = new ArrayList<PaymentGoods>();
    private List<PromotionResponse> promotions = new ArrayList<PromotionResponse>();
    @JsonIgnore
    private int paid;//实际费用 支付时候发生变化，单位：分。客户端可用来对账
    private double cost;//订单当前实际费用

    private String readyTime;//取菜确认时间
    private String assignTime;//后台分配配送员时间（配送员接单时间）
    @JsonIgnore
    private String alipayNo;

    /**
     * 订单原价：总费用 单位分
     */
    public int primeCost() {
        int primeCost = 0;
        for (PaymentGoods g : goods) {
            if (g.getStatus() < 2) {	//状态不属于退菜时 参与计算
                primeCost += g.getPriceValue() * g.getCount();
            }
        }
        if (primeCost > 0) {
            primeCost += packageFeeValue;
        }
        // primeCost +=  delivery; //配送费不算入本金
        return primeCost;
    }

    @NumberFormat(pattern = "#.##")
    public double getPrimeCost() {
        return 0.01 * primeCost();
    }

    @NumberFormat(pattern = "#.##")
    public double getPackageFee() {
        return 1.0 * packageFeeValue / 100;
    }

    @NumberFormat(pattern = "#.##")
    public double getDelivery() {
        return 1.0 * deliveryValue / 100;
    }

    @NumberFormat(pattern = "#.##")
    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void addGoods(PaymentGoods item) {
        this.goods.add(item);
    }

    public void addPromotion(PromotionResponse item) {
        promotions.add(item);
    }

    /**
     * 取菜任务量.
     */
    @JsonIgnore
    public int getDeliverWeight() {
        return goods.size();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public int getDeliverId() {
        return deliverId;
    }

    public void setDeliverId(int deliverId) {
        this.deliverId = deliverId;
    }

    public int getCourierId() {
        return courierId;
    }

    public void setCourierId(int courierId) {
        this.courierId = courierId;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public CustAddress getAddress() {
        return address;
    }

    public void setAddress(CustAddress address) {
        this.address = address;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getRegionId() {
        return regionId;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    public int getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(int restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getRestaurantImg() {
        return restaurantImg;
    }

    public void setRestaurantImg(String restaurantImg) {
        this.restaurantImg = restaurantImg;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getRestaurantLocation() {
        return restaurantLocation;
    }

    public void setRestaurantLocation(String restaurantLocation) {
        this.restaurantLocation = restaurantLocation;
    }

    public int getPackageFeeValue() {
        return packageFeeValue;
    }

    public void setPackageFeeValue(int packageFeeValue) {
        this.packageFeeValue = packageFeeValue;
    }

    public int getDeliveryValue() {
        return deliveryValue;
    }

    public void setDeliveryValue(int deliveryValue) {
        this.deliveryValue = deliveryValue;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public int getCash() {
        return cash;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public List<PaymentGoods> getGoods() {
        return goods;
    }

    public void setGoods(List<PaymentGoods> goods) {
        this.goods = goods;
    }

    public List<PromotionResponse> getPromotions() {
        return promotions;
    }

    public void setPromotions(List<PromotionResponse> promotions) {
        this.promotions = promotions;
    }

    public int getPaid() {
        return paid;
    }

    public void setPaid(int paid) {
        this.paid = paid;
    }

    public String getReadyTime() {
        return readyTime;
    }

    public void setReadyTime(String readyTime) {
        this.readyTime = readyTime;
    }

    public String getAssignTime() {
        return assignTime;
    }

    public void setAssignTime(String assignTime) {
        this.assignTime = assignTime;
    }

    public boolean isVip() {
        return vip;
    }

    public void setVip(boolean vip) {
        this.vip = vip;
    }

    public String getCourierNumber() {
        return courierNumber;
    }

    public void setCourierNumber(String courierNumber) {
        this.courierNumber = courierNumber;
    }

    public String getCourierName() {
        return courierName;
    }

    public void setCourierName(String courierName) {
        this.courierName = courierName;
    }

    public String getCourierContact() {
        return courierContact;
    }

    public void setCourierContact(String courierContact) {
        this.courierContact = courierContact;
    }

    public String getRestaurantContact() {
        return restaurantContact;
    }

    public void setRestaurantContact(String restaurantContact) {
        this.restaurantContact = restaurantContact;
    }

    public String getAlipayNo() {
        return alipayNo;
    }

    public void setAlipayNo(String alipayNo) {
        this.alipayNo = alipayNo;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Payment other = (Payment) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    public int getShared() {
        return shared;
    }

    public void setShared(int shared) {
        this.shared = shared;
    }

}
