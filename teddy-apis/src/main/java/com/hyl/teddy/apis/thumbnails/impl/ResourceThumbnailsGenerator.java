/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.thumbnails.impl;

import com.hyl.teddy.apis.attachment.AttachmentService;
import com.hyl.teddy.apis.attachment.PathInfo;
import com.hyl.teddy.apis.thumbnails.ThumbSize;
import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public abstract class ResourceThumbnailsGenerator {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    @Resource
    private AttachmentService attachmentService;

    /**
     * 生成缩略图的具体大小.
     */
    protected abstract Dimension size(ThumbSize pattern);

    public String toThumbmails(String resource, ThumbSize pattern) {
        if (StringUtils.isBlank(resource)) {
            logger.debug("原样返回空资源");
            return resource;
        }
        PathInfo attachment = attachmentService.parseAttachment(resource);
        ResourceRename rename = new ResourceRename(pattern);
        String newname = rename.apply(attachment.getUrl(), null);
        if (!attachment.isLocalResource()) {
            logger.debug("原样返回网络资源:{}", attachment.getUrl());
            return attachment.getUrl();
        }

        if (!attachment.isFileExists()) {
            logger.debug("附件文件不存在：{}", attachment.getFile());
            return attachmentService.getResourceHost() + newname;
        }

        String thumbFileName = rename.apply(attachment.getFile().getAbsolutePath(), null);
        File thumbFile = new File(thumbFileName);
        if (thumbFile.exists()) {
            logger.debug("缩略图已存在，不再重新创建：{}", thumbFile);
            return attachmentService.getResourceHost() + newname;
        }

        try {
            Dimension dimension = size(pattern);
            if (dimension == null) {
                return attachmentService.getResourceHost() + attachment.getUrl();
            }
            Thumbnails.of(attachment.getFile())
                    .size(dimension.width, dimension.height)
                    .toFile(thumbFile);
            logger.debug("创建缩略图文件：{}", thumbFile.getAbsoluteFile());
        } catch (IOException ex) {
            logger.warn("生成缩略图出错", ex);
        }

        return attachmentService.getResourceHost() + newname;
    }

    public List<Map<ThumbSize, String>> toMultipleThumbmails(String resources, ThumbSize... pattern) {
        if (StringUtils.isBlank(resources)) {
            return Collections.EMPTY_LIST;
        }
        List<Map<ThumbSize, String>> list = new ArrayList<>();
        String[] resourceArray = resources.split(",");
        for (String resource : resourceArray) {
            Map<ThumbSize, String> photo = new HashMap<>();
            for (ThumbSize size : pattern) {
                photo.put(size, toThumbmails(resource, size));
            }
            list.add(photo);
        }
        return list;
    }

}
