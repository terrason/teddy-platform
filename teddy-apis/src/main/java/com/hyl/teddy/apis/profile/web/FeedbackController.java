package com.hyl.teddy.apis.profile.web;

import java.util.List;

import javax.annotation.Resource;
import javax.naming.AuthenticationException;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hyl.teddy.apis.BaseResponse;
import com.hyl.teddy.apis.entity.Customer;
import com.hyl.teddy.apis.profile.CustomerService;
import com.hyl.teddy.apis.profile.FeedbackResponse;
import com.hyl.teddy.apis.profile.FeedbackService;
import org.springframework.http.HttpStatus;

/**
 * TEDDY 个人中心接口 19 查看意见反馈历史 20 意见反馈
 *
 * @author Administrator
 *
 */
@Controller
public class FeedbackController {

    @Resource
    private MessageSource mss;

    @Resource
    private FeedbackService service;

    @Resource
    private CustomerService customerService;

    /**
     * 19 查看意见反馈历史
     *
     * @param principal 当前登陆用户id
     * @return
     * @throws AuthenticationException
     */
    @RequestMapping(value = "/profile/feedback", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse feedbackHistory(@RequestParam(defaultValue = "0") int principal) throws AuthenticationException {
        if (principal == 0) {
            throw new AuthenticationException();
        }
        BaseResponse b = new BaseResponse();
        Customer customer = customerService.getCustomer(principal);
        if (customer == null) {
            b.setCode(HttpStatus.NOT_FOUND.value());
            b.setMessage(mss.getMessage("error.default", null, null));
            return b;
        }
        List<FeedbackResponse> list = service.findFeedbackByCustId(principal);
        b.setData(list);
        return b;
    }

    /**
     *
     * @param principal 当前用户id
     * @param content 反馈内容
     * @return
     * @throws AuthenticationException
     */
    @RequestMapping(value = "/profile/feedback", method = RequestMethod.POST)
    @ResponseBody
    public BaseResponse AddFeedback(
            @RequestParam(defaultValue = "0") int principal, String content)
            throws AuthenticationException {
        if (principal == 0) {
            throw new AuthenticationException();
        }
        BaseResponse b = new BaseResponse();
        service.addFeedback(principal, content);
        return b;
    }

}
