package com.hyl.teddy.apis.restaurant.dao;

import com.hyl.teddy.apis.entity.Dish;
import com.hyl.teddy.apis.restaurant.SearchDish;
import com.hyl.teddy.apis.restaurant.thumbnails.DishImg;
import com.hyl.teddy.apis.restaurant.web.DishCategoryResponse;
import com.hyl.teddy.apis.restaurant.web.DishResponse;
import com.hyl.teddy.apis.restaurant.web.RestaurantDishResponse;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * 菜品DAO
 *
 * @author Administrator
 */
@Repository
public class DishDao {

    @Resource
    private JdbcTemplate jdbcTemplate;
    @Resource
    private DishImg dishImg;

    /**
     * 餐厅菜品ID
     *
     * @param restaurant 餐厅ID
     * @return
     */
    public RestaurantDishResponse queryRestaurantDish(final int restaurant) {
        List<RestaurantDishResponse> list = jdbcTemplate.query("select id,`name`,promotion from restaurant where id=?", new RowMapper<RestaurantDishResponse>() {

            @Override
            public RestaurantDishResponse mapRow(ResultSet rs, int i) throws SQLException {
                RestaurantDishResponse dr = new RestaurantDishResponse();
                dr.setId(rs.getInt("id"));
                dr.setName(rs.getString("name"));
                dr.setNotice(rs.getString("promotion"));
                final Map<Integer, DishCategoryResponse> categories = new LinkedHashMap<>();
                dr.setCategory(categories.values());

                jdbcTemplate.query("select id,`name` from dish_category", new RowCallbackHandler() {

                    @Override
                    public void processRow(ResultSet rs) throws SQLException {

                        DishCategoryResponse dcr = new DishCategoryResponse();
                        int id = rs.getInt("id");
                        dcr.setId(id);
                        dcr.setName(rs.getString("name"));
                        categories.put(id, dcr);
                    }
                }, (Object[]) null);
                jdbcTemplate.query("select id,`name`,image,category_id,price,sold,enabled from dish where restaurant_id=? order by sold desc", new RowCallbackHandler() {

                    @Override
                    public void processRow(ResultSet rs) throws SQLException {
                        Dish d = new Dish();
                        d.setId(rs.getInt("id"));
                        d.setName(rs.getString("name"));
                        d.setImage(dishImg.toThumbmailsMD(rs.getInt("image")));
                        d.setCategoryId(rs.getInt("category_id"));
                        d.setPrice(0.01 * rs.getDouble("price"));
                        d.setSold(rs.getInt("sold"));
                        d.setEnabled(rs.getBoolean("enabled"));
                        DishCategoryResponse category = categories.get(d.getCategoryId());
                        if (category != null) {
                            category.addDish(d);
                        }
                    }
                }, restaurant);

                Iterator<Map.Entry<Integer, DishCategoryResponse>> iterator = categories.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry<Integer, DishCategoryResponse> next = iterator.next();
                    if (next.getValue().getDishes().isEmpty()) {
                        iterator.remove();
                    }
                }
                return dr;
            }
        }, restaurant);
        return list.isEmpty() ? null : list.get(0);
    }

    /**
     * 菜品全局搜索
     *
     * @param keyword 搜索关键字
     * @return
     */
    public List<SearchDish> searchDish(String keyword) {
        return jdbcTemplate.query("select d.id,d.`name`,d.restaurant_id,d.image,d.category_id,d.price,d.sold,r.`name` restaurantName\n"
                + ",(d.enabled and r.enabled and (date_format(now(),'%H:%i') between r.open_time and r.close_time)) as enabled\n"
                + "from dish d\n"
                + "left join restaurant r on r.id=d.restaurant_id\n"
                + "where d.`name` like ?\n"
                + "order by d.sold desc", new RowMapper<SearchDish>() {

                    @Override
                    public SearchDish mapRow(ResultSet rs, int i) throws SQLException {
                        SearchDish sd = new SearchDish();
                        sd.setId(rs.getInt("id"));
                        sd.setName(rs.getString("name"));
                        sd.setRestaurantName(rs.getString("restaurantName"));
                        sd.setRestaurantId(rs.getInt("restaurant_id"));
                        sd.setImage(dishImg.toThumbmailsMD(rs.getInt("image")));
                        sd.setPrice(rs.getDouble("price") / 100.0);
                        sd.setSold(rs.getInt("sold"));
                        sd.setEnabled(rs.getBoolean("enabled"));
                        return sd;
                    }
                }, "%" + keyword + "%");
    }

    /**
     * 餐厅菜品搜索
     *
     * @param restaurant 餐厅ID
     * @param keyword 搜索关键字
     * @return
     */
    public List<Dish> dishSearch(int restaurant, String keyword) {
        return jdbcTemplate.query("select d.id,d.`name`,d.image,d.category_id,d.price,d.sold,c.`name` categoryName,d.enabled from dish d "
                + "left join dish_category c on d.category_id=c.id "
                + "where d.restaurant_id=? and d.`name` like ? "
                + "order by sold desc", new RowMapper<Dish>() {

                    @Override
                    public Dish mapRow(ResultSet rs, int i) throws SQLException {
                        Dish d = new Dish();
                        d.setId(rs.getInt("id"));
                        d.setName(rs.getString("name"));
                        d.setImage(dishImg.toThumbmailsMD(rs.getInt("image")));
                        d.setPrice(rs.getDouble("price") / 100.0);
                        d.setSold(rs.getInt("sold"));
                        d.setCategoryId(rs.getInt("category_id"));
                        d.setCategoryName(rs.getString("categoryName"));
                        d.setEnabled(rs.getBoolean("enabled"));
                        return d;
                    }
                }, restaurant, "%" + keyword + "%");
    }

    /**
     * 菜品详情
     *
     * @param id
     * @param principal
     * @return
     */
    public DishResponse selectDishWithPrincipal(int id) {
        final DishResponse dr = new DishResponse();
        return jdbcTemplate.queryForObject("select id,`name`,image,price,sold,description,enabled from dish where id=?", new RowMapper<DishResponse>() {

            @Override
            public DishResponse mapRow(ResultSet rs, int i) throws SQLException {
                dr.setId(rs.getInt("id"));
                dr.setName(rs.getString("name"));
                dr.setImage(dishImg.toThumbmailsLG(rs.getInt("image")));
                dr.setPrice(rs.getDouble("price") / 100.0);
                dr.setSold(rs.getInt("sold"));
                dr.setDescription(rs.getString("description"));
                dr.setEnabled(rs.getBoolean("enabled"));
                return dr;
            }
        }, id);
    }

    public void updateDishStatus(int id, boolean enabled) {
        jdbcTemplate.update("update dish set enabled=? where id=?", enabled, id);
    }
}
