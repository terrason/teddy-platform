package com.hyl.teddy.apis.payment;

import com.hyl.teddy.apis.promotion.Promotion;
import com.hyl.teddy.apis.promotion.PromotionDiscountHandler;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.format.annotation.NumberFormat;

/**
 * 订单详情中的优惠标签
 *
 * @author Administrator
 *
 */
public class PromotionResponse implements PromotionDiscountHandler {
    @JsonIgnore
    private int id;
    private String tag;
    private int[] params;
    @JsonIgnore
    private int discount;

    public PromotionResponse() {
    }

    public PromotionResponse(Promotion promotion) {
        tag = promotion.name();
        params = promotion.getParams();
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int[] getParams() {
        return params;
    }

    public void setParams(int[] params) {
        this.params = params;
    }

    public Promotion toPromotion() {
        Promotion promotion = Promotion.valueOf(tag);
        promotion.setParams(params);
        promotion.addHandler(this);
        return promotion;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    @NumberFormat(pattern = "#.#")
    public double getMoney() {
        return 1.0 * discount / 100;
    }

    @Override
    public void setMoney(int money) {
        setDiscount(money);
    }

}
