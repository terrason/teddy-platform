/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.payment.web;

import org.springframework.format.annotation.NumberFormat;

/**
 *
 * @author terrason
 */
public class ExtraGoodsResponse {
    private double fee;
    private String orderno;

    @NumberFormat(pattern = "#.##")
    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    @Override
    public String toString() {
        return "ExtraGoodsResponse{" + "fee=" + fee + ", orderno=" + orderno + '}';
    }
}
