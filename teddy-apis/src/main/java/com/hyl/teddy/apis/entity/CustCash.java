package com.hyl.teddy.apis.entity;
/**
 * 客户代金券 cust_cach
 * @author Administrator
 *
 */
public class CustCash {
	private int id;
	private int  customerId;//用户id
	private int cashId;//代金券id
	private int count;//代金券数量
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public int getCashId() {
		return cashId;
	}
	public void setCashId(int cashId) {
		this.cashId = cashId;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
}
