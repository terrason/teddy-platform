package com.hyl.teddy.apis.profile;

import com.hyl.teddy.apis.entity.Restaurant;

/**
 * 我的收藏 —餐厅
 * @author Administrator
 *
 */
public class FavoriteRestaurantResponse {
	private int id;
	private String name;
	private String icon;
	private int star;//每2分表示一颗星，例如9表示4个半星。
	private String promotion;
	private int price;//价格
	private int sold;//销量??
	private String createTime;
	private String[] tag = new String[]{"WIN_CASH","VIP_DISCOUNT","COST_CASH","COST_SCORE","FREE_POSTAGE","DEDUCT"};
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public int getStar() {
		return star;
	}
	public void setStar(int star) {
		this.star = star;
	}
	public String getPromotion() {
		return promotion;
	}
	public void setPromotion(String promotion) {
		this.promotion = promotion;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getSold() {
		return sold;
	}
	public void setSold(int sold) {
		this.sold = sold;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String[] getTag() {
		return tag;
	}
	public void setTag(String[] tag) {
		this.tag = tag;
	}
	
	
	
}
