/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.attachment.thumbnails;

import com.hyl.teddy.apis.CommonRuntimeException;
import com.hyl.teddy.apis.IORuntimeException;
import com.hyl.teddy.apis.asynchronous.operation.AsyncOperation;
import com.hyl.teddy.apis.attachment.AttachmentService;
import com.hyl.teddy.apis.attachment.PathInfo;
import com.hyl.teddy.apis.thumbnails.impl.ResourceRename;
import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import javax.annotation.Resource;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 *
 * @author terrason
 */
@Service
public class DimensionThumbnails {

    private final Logger logger = LoggerFactory.getLogger(DimensionThumbnails.class);
    @Resource
    private AttachmentService attachmentService;
    @Resource
    private AsyncOperation asyncOperation;

    public String toThumbmails(String resource, final Dimension dimension) {
        if (StringUtils.isBlank(resource)) {
            logger.debug("原样返回空资源");
            return resource;
        }

        final PathInfo attachment = attachmentService.parseAttachment(resource);
        if (!attachment.isLocalResource()) {
            logger.debug("原样返回网络资源:{}", attachment.getUrl());
            return attachment.getUrl();
        }

        if (!attachment.isFileExists()) {
            logger.debug("附件文件不存在：{}", attachment.getFile());
            return attachmentService.toBrokenUrl(dimension);
        }
        if (dimension == null) {
            logger.debug("没有缩放大小，返回原始资源！");
            return attachmentService.getResourceHost() + attachment.getUrl();
        }
        String suffix = dimension.width + "x" + dimension.height;
        ResourceRename rename = new ResourceRename(suffix);
        String newname = rename.apply(attachment.getUrl(), null);
        final String thumbFileName = rename.apply(attachment.getFile().getAbsolutePath(), null);

        asyncOperation.put(new Runnable() {

            @Override
            public void run() {
                try {
                    final File thumbFile = new File(thumbFileName);
                    if (thumbFile.exists()) {
                        logger.debug("缩略图已存在，不再重新创建：{}", thumbFile);
                        return;
                    }
                    Thumbnails.of(attachment.getFile())
                            .size(dimension.width, dimension.height)
                            .toFile(thumbFile);
                    logger.debug("创建缩略图文件：{}", thumbFile.getAbsoluteFile());
                } catch (IOException ex) {
                    throw new IORuntimeException("生成缩略图出错", ex);
                }
            }
        });
        return attachmentService.getResourceHost() + newname;
    }
}
