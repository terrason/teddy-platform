package com.hyl.teddy.apis.system;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * 个人中心 用户信息表 customer
 *
 * @author Administrator
 *
 */
public class VersionCustomer {

    @JsonIgnore
    private int id;
    @JsonIgnore
    private int code;
    private String version;// 版本
    private String log;// 更新日志
    @JsonIgnore
    private int downloadId;
    private String downloadUrl;//下载地址
    private String downloadTimes;// 下载次数

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public int getDownloadId() {
        return downloadId;
    }

    public void setDownloadId(int downloadId) {
        this.downloadId = downloadId;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getDownloadTimes() {
        return downloadTimes;
    }

    public void setDownloadTimes(String downloadTimes) {
        this.downloadTimes = downloadTimes;
    }

}
