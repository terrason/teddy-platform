/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.payment;

/**
 * 订单状态监听器.
 *
 * @author terrason
 */
public interface PaymentListener extends Comparable<PaymentListener> {

    /**
     * 订单状态变化后执行的业务代码.
     *
     * @param event 事件对象 包含状态变化后的订单对象
     */
    public void fireStatusChanged(PaymentStatusChangeEvent event);

    /**
     * 监听器优先级. 值越小优先级越高。
     */
    public int getPriority();
}
