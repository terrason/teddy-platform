package com.hyl.teddy.apis.profile.web;

import java.util.List;
import javax.annotation.Resource;
import javax.naming.AuthenticationException;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.hyl.teddy.apis.BaseResponse;
import com.hyl.teddy.apis.entity.Customer;
import com.hyl.teddy.apis.profile.CashResponse;
import com.hyl.teddy.apis.profile.CustomerService;
import com.hyl.teddy.apis.profile.MessageResponse;
import java.util.ArrayList;
import java.util.Map;

/**
 * teddy个人中心 接口 我的积分 我的代金券 我的信息
 *
 * @author Administrator
 *
 */
@Controller
public class OwnController {

    @Resource
    private CustomerService customerService;

    @Resource
    private MessageSource messageSource;

    /**
     * 1 我的积分
     *
     * @param id 当前用户id
     * @return
     * @throws AuthenticationException
     */
    @RequestMapping(value = "/profile/score", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse findScore(@RequestParam(defaultValue = "0") int principal) throws AuthenticationException {
        if (principal == 0) {
            throw new AuthenticationException();
        }
        Customer customer = customerService.getCustomer(principal);
        if (customer == null) {
            throw new AuthenticationException();
        }
        return new BaseResponse(customer.getScore());
    }

    /**
     * 2 我的代金券
     *
     * @param id 当前用户id
     * @return
     * @throws AuthenticationException
     */
    @RequestMapping(value = "/profile/cash", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse findCash(@RequestParam(defaultValue = "0") int principal) throws AuthenticationException {
        if (principal == 0) {
            throw new AuthenticationException();
        }
        Customer customer = customerService.getCustomer(principal);
        if (customer == null) {
            throw new AuthenticationException();
        }

        List<CashResponse> list = new ArrayList<>();
        for (Map.Entry<Integer, Integer> cashEntity : customer.getCashes().entrySet()) {
            int cash = cashEntity.getKey();
            int volume = cashEntity.getValue();
            list.add(new CashResponse(cash, volume));
        }
        return new BaseResponse(list);
    }

    /**
     * 3 我的消息
     *
     * @param id 当前用户id
     * @return
     * @throws AuthenticationException
     */
    @RequestMapping(value = "/profile/message", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse findMessage(@RequestParam(defaultValue = "0") int principal,
            @RequestParam(defaultValue = "0") int start,
            @RequestParam(defaultValue = "10") int size) throws AuthenticationException {
        if (principal == 0) {
            throw new AuthenticationException();
        }
        BaseResponse b = new BaseResponse();
        List<MessageResponse> list = customerService.findCustomerMessages(principal, start, size);
        return new BaseResponse(list);
    }
}
