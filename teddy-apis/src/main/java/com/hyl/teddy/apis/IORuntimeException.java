/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis;

import java.io.File;

/**
 *
 * @author Administrator
 */
public class IORuntimeException extends RuntimeException {

    public IORuntimeException() {
    }

    public IORuntimeException(String string) {
        super(string);
    }

    public IORuntimeException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public IORuntimeException(Throwable thrwbl) {
        super(thrwbl);
    }

}
