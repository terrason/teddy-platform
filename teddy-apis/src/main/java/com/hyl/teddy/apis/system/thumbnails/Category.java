/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.system.thumbnails;

import com.hyl.teddy.apis.thumbnails.ThumbSize;
import com.hyl.teddy.apis.thumbnails.impl.AttachmentThumbnailsGenerator;
import java.awt.Dimension;
import org.springframework.stereotype.Service;

/**
 *
 * @author Administrator
 */
@Service
public class Category extends AttachmentThumbnailsGenerator {

    public String toThumbmailsMD(int resource) {
        return toThumbmails(resource, ThumbSize.MD);
    }

    @Override
    protected Dimension size(ThumbSize pattern) {
        switch (pattern) {
            case MD:
                return new Dimension(288, 288);//144*144
            default:
                return null;
        }
    }

}
