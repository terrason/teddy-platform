package com.hyl.teddy.apis.profile;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.hyl.teddy.apis.profile.dao.FeedbackDao;

@Service
public class FeedbackService {

	@Resource
	private FeedbackDao dao;

	public List<FeedbackResponse> findFeedbackByCustId(int principal) {
		return dao.findFeedbackByCustId(principal);
	}

	/**
	 * 新增意见反馈
	 * @param principal
	 * @param content
	 */
	public void addFeedback(int principal, String content) {
		dao.addFeedback(principal, content);
		
	}
}
