/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.profile.listener;

import com.hyl.teddy.apis.entity.Customer;
import com.hyl.teddy.apis.entity.Payment;
import com.hyl.teddy.apis.payment.PaymentListenerAdaptor;
import com.hyl.teddy.apis.payment.PaymentStatusChangeEvent;
import com.hyl.teddy.apis.profile.CustomerService;
import javax.annotation.Resource;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

/**
 * 订单状态监听器.
 *
 * <ul>
 * <li>用于订单支付成功时，扣除客户代金券或积分。</li>
 * <li>同时，在订单缺失菜品时，回退客户代金券和积分。</li>
 * </ul>
 *
 * @author terrason
 */
@Service
public class CustomerPaymentListener extends PaymentListenerAdaptor {

    @Resource
    private CustomerService customerService;
    @Resource
    private CacheManager cacheManager;

    @Override
    public void fireStatusChanged(PaymentStatusChangeEvent event) {
        final Payment payment = event.getPayment();
        if (payment.getStatus() == 2 && (event.getOriginalStatus() < 2 || payment.isEditable())) {
            logger.debug("监听到订单状态改变为2，更新客户积分及代金券...");
            final Customer customer = customerService.getCustomer(payment.getCustomerId());
            if (customer == null) {
                logger.debug("更新客户积分及代金券处理非正常结束：找不到指定的客户【id={}】！", payment.getCustomerId());
                return;
            }
            try {
                if (payment.getCash() > 0) {
                    customerService.costCash(customer, payment.getCash(), 1);
                    logger.debug("已扣除用户一张{}元代金券", payment.getCash(), 1);
                }
                if (payment.getScore() > 0) {
                    customerService.costScore(customer, payment.getScore());
                    logger.debug("已扣除用户{}积分", payment.getScore());
                }
            } catch (Exception ex) {
                customerService.evictCache(payment.getCustomerId());
                throw ex;
            }

            logger.debug("更新客户积分及代金券处理完成=========>");
        }
    }

    @Override
    public int getPriority() {
        return 500;
    }
}
