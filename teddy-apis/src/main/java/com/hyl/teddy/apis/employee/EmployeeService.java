package com.hyl.teddy.apis.employee;

import com.hyl.teddy.apis.CommonRuntimeException;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.hyl.teddy.apis.employee.dao.EmployeeDao;
import com.hyl.teddy.apis.entity.Payment;
import com.hyl.teddy.apis.payment.PaymentListenerAdaptor;
import com.hyl.teddy.apis.payment.PaymentService;
import com.hyl.teddy.apis.payment.PaymentStatusChangeEvent;
import com.hyl.teddy.apis.system.SystemService;
import java.util.Collection;
import java.util.Map;
import org.springframework.http.HttpStatus;

/**
 * 客户Service
 *
 * @author Messi
 */
@Service
public class EmployeeService extends PaymentListenerAdaptor {

    @Resource
    private EmployeeDao dao;
    @Resource
    private SystemService systemService;
    @Resource
    private PaymentService paymentService;

    public Employee getEmployee(String username) {
        return dao.selectEmployee(username);
    }

    public Employee getEmployee(int principal) {
        return getAvailableEmployees().get(principal);
    }

    public int updatePwd(int principal, String pwdnew) {
        int c = dao.updatePassword(principal, pwdnew);
        getEmployee(principal).setPassword(pwdnew);
        return c;
    }

    public int updatePositiopn(int principal, String longitude, String latitude) {
        return dao.updatePosition(principal, longitude, latitude);
    }

    public void updatePushkey(Employee emp) {
        dao.updatepushKey(emp);
    }

    /**
     * 版本更新（取菜员）.
     */
    public Version queryUpgradeVersionForDeliver(int code) {
        return dao.selectUpgradeVersionForDeliver(code);

    }

    /**
     * 版本更新（配菜员）.
     */
    public Version queryUpgradeVersionForCourier(int code) {
        return dao.selectUpgradeVersionForCourier(code);
    }

    public Map<Integer, Employee> getAvailableEmployees() {
        return dao.selectAvailableEmployees();
    }

    /**
     * 分配订单的配送员. 分配时不修改配送员状态.
     *
     * @param payment 订单
     * @param employeeId 配送员ID
     */
    public void assignCourierToPayment(Payment payment, int employeeId) {
        Employee employee = getAvailableEmployees().get(employeeId);
        if (employee == null) {
            throw new CommonRuntimeException(HttpStatus.NOT_FOUND, "mission.employee");
        }
        payment.setCourierId(employeeId);
        paymentService.updatePaymentCourier(employeeId, payment.getId());
    }

    /**
     * 分配订单的取菜员. 分配时不修改取菜员状态. 不修改订单状态。
     *
     * @param payment 订单
     * @param employeeId 取菜员ID
     */
    public void assignDeliverToPayment(Payment payment, int employeeId) {
        Employee employee = getAvailableEmployees().get(employeeId);
        if (employee == null) {
            throw new CommonRuntimeException(HttpStatus.NOT_FOUND, "mission.employee");
        }
        payment.setDeliverId(employeeId);
        paymentService.updatePaymentDeliver(employeeId, payment.getId());
    }

    /**
     * 自动分配配送员.
     */
    private void assignCourier(PaymentStatusChangeEvent event) {
        Payment payment = event.getPayment();
        if (systemService.isCourierAutoAssignable() && payment.getCourierId() == 0) {
            if (payment.getStatus() == 2) {
                logger.debug("监听到订单状态改变为2，正在分配配送员...");
                if (payment.getCourierId() != 0) {
                    logger.debug("自动分配配送员结束：加菜订单使用原配送员！=======>");
                    return;
                }
                Collection<Employee> employees = dao.selectAvailableEmployees().values();
                Employee idle = null;
                int weight = 9999999;
                for (Employee employee : employees) {
                    if (employee.getType() == 2 && employee.getRegionId() == payment.getRegionId() && employee.getTaskWeight() < weight) {
                        weight = employee.getTaskWeight();
                        idle = employee;
                    }
                }
                if (idle == null) {
                    logger.debug("自动分配配送员结束：无闲置配送员！======>");
                    return;
                }
                assignCourierToPayment(payment, idle.getId());
                idle.increaseTaskWeight(1);
                logger.debug("自动分配配送员结束======>");
            } else if (event.getOriginalStatus() == 6 && payment.getStatus() > 6) {
                logger.debug("监听到订单状态改变为{}，配送员降低任务量...", payment.getStatus());
                Employee deliver = getAvailableEmployees().get(payment.getCourierId());
                deliver.decreaseTaskWeight(1);
                logger.debug("配送员任务量已降低======>");
            }
        }
    }
/**
 * 自动分配取菜员.
 */
    private void assignDeliver(PaymentStatusChangeEvent event) {
        Payment payment = event.getPayment();
        if (systemService.isDeliverAutoAssignable()) {
            if (payment.getStatus() == 2) {
                logger.debug("监听到订单状态改变为2，正在分配取菜员...");
                if (payment.getDeliverId() != 0) {
                    logger.debug("加菜订单使用原取菜员！");
                } else {
                    Collection<Employee> employees = dao.selectAvailableEmployees().values();
                    Employee idle = null;
                    int weight = 9999999;
                    for (Employee employee : employees) {
                        if (employee.getType() == 1 && employee.getTaskWeight() < weight) {
                            weight = employee.getTaskWeight();
                            idle = employee;
                        }
                    }
                    if (idle == null) {
                        logger.debug("自动分配取菜员结束：无闲置取菜员！======>");
                        return;
                    }
                    assignDeliverToPayment(payment, idle.getId());
                    idle.increaseTaskWeight(payment.getDeliverWeight());
                }
                logger.debug("自动分配取菜员结束：订单状态即将变化到 3 ！======>");

                event.gotoStatusAfterFinished(3);
            } else if (payment.getStatus() == 5) {
                logger.debug("监听到订单状态改变为5，取菜员降低任务量...");
                Employee deliver = getAvailableEmployees().get(payment.getDeliverId());
                deliver.decreaseTaskWeight(payment.getDeliverWeight());
                logger.debug("取菜员任务量已降低======>");
            }
        }
    }

    /**
     * 在客户支付完成时，自动分配取菜员. 同时负责更新员工状态.
     */
    @Override
    public void fireStatusChanged(PaymentStatusChangeEvent event) {
        assignCourier(event);
        assignDeliver(event);
        Payment payment = event.getPayment();
        //设置员工状态
        if (payment.getStatus() == 3) {
            int deliverId = payment.getDeliverId();
            Employee deliver = getAvailableEmployees().get(deliverId);
            boolean idle = deliver.isIdle();
            deliver.addWork(payment.getOrderno());
            if (idle) {
                dao.updateStatus(deliverId, 1);
            }
        } else if (payment.getStatus() == 5) {
            int deliverId = payment.getDeliverId();
            Employee deliver = getAvailableEmployees().get(deliverId);
            deliver.removeWork(payment.getOrderno());
            if (deliver.isIdle()) {
                dao.updateStatus(deliverId, 0);
            }
        } else if (payment.getStatus() == 6) {
            int courierId = payment.getCourierId();
            Employee courier = getAvailableEmployees().get(courierId);
            boolean idle = courier.isIdle();
            courier.addWork(payment.getOrderno());
            if (idle) {
                dao.updateStatus(courierId, 1);
            }
        } else if (event.getOriginalStatus() == 6 && payment.getStatus() > 6) {
            int courierId = payment.getCourierId();
            Employee courier = getAvailableEmployees().get(courierId);
            courier.removeWork(payment.getOrderno());
            if (courier.isIdle()) {
                dao.updateStatus(courierId, 0);
            }
        }
    }

    /**
     * 该监听器涉及订单状态变化，必须在系统中所有*订单状态监听器*的执行顺序最后. 否则会导致严重错误！
     */
    @Override
    public int getPriority() {
        return 999999;
    }
}
