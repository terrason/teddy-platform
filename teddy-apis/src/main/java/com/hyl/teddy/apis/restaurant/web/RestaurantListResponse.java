package com.hyl.teddy.apis.restaurant.web;


/**
 * 餐厅列表接口返回值
 * @author Administrator
 */
public class RestaurantListResponse {
    /**
     * 餐厅ID
     */
    private int id;
    /**
     * 餐厅名
     */
    private String name;
    /**
     * 餐厅列表图标
     */
    private String icon;
    /**
     * 餐厅星级
     */
    private int star;
    /**
     * 促销信息
     */
    private String promotion;
    /**
     * 餐厅价位
     */
    private double price;
    /**
     * 餐厅销量
     */
    private int sold;
    
    private String[] tag;
    
    private boolean enabled;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public String getPromotion() {
        return promotion;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getSold() {
        return sold;
    }

    public void setSold(int sold) {
        this.sold = sold;
    }


    public String[] getTag() {
        return tag;
    }

    public void setTag(String[] tag) {
        this.tag = tag;
    }
    
}
