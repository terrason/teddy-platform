package com.hyl.teddy.apis.profile.thumbnails;

import com.hyl.teddy.apis.thumbnails.ThumbSize;
import com.hyl.teddy.apis.thumbnails.impl.ResourceThumbnailsGenerator;

import java.awt.Dimension;

import org.springframework.stereotype.Service;

/**
 *
 * @author Administrator
 */
@Service
public class Avatar extends ResourceThumbnailsGenerator {

    public String toThumbmailsMD(String resource) {
        return toThumbmails(resource, ThumbSize.MD);
    }

    public String toThumbmailsSM(String resource) {
        return toThumbmails(resource, ThumbSize.SM);
    }

    @Override
    protected Dimension size(ThumbSize pattern) {
        switch (pattern) {
            case MD:
                return new Dimension(356, 356);//178*178
            case SM:
                return new Dimension(200, 200);//100*100
            default:
                return null;
        }
    }
}
