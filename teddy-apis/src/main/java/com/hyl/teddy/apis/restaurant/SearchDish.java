package com.hyl.teddy.apis.restaurant;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * 菜品 dish
 *
 * @author Administrator
 *
 */
public class SearchDish {

    private int id;
    private int restaurantId;//餐厅id
    private String restaurantName;//餐厅名
    @JsonIgnore
    private int categoryId;//类别id
    @JsonIgnore
    private String categoryName;//类别名称
    private String name;//菜名
    // @JsonProperty("icon")
    private String image;//菜品图片  附件表id
    private double price; //价位
    private int sold;//销量
    @JsonIgnore
    private String description;//描述
    @JsonIgnore
    private boolean hot;//是否为热菜
    private boolean enabled;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(int restaurantId) {
        this.restaurantId = restaurantId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getSold() {
        return sold;
    }

    public void setSold(int sold) {
        this.sold = sold;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isHot() {
        return hot;
    }

    public void setHot(boolean hot) {
        this.hot = hot;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}
