package com.hyl.teddy.apis.payment;

import com.hyl.teddy.apis.entity.Customer;
import com.hyl.teddy.apis.profile.AddressResponse;
import java.util.List;

/**
 * 确认订单接口返回值
 *
 * @author Administrator
 */
public class BuyResponse {

    private double packageFee;
    private double delivery;
    private List<PromotionResponse> promotion;
    private AddressResponse address;
    private Customer principal;

    public double getPackageFee() {
        return packageFee;
    }

    public void setPackageFee(double packageFee) {
        this.packageFee = packageFee;
    }

    public double getDelivery() {
        return delivery;
    }

    public void setDelivery(double delivery) {
        this.delivery = delivery;
    }

    public List<PromotionResponse> getPromotion() {
        return promotion;
    }

    public void setPromotion(List<PromotionResponse> promotion) {
        this.promotion = promotion;
    }

    public AddressResponse getAddress() {
        return address;
    }

    public void setAddress(AddressResponse address) {
        this.address = address;
    }

    public Customer getPrincipal() {
        return principal;
    }

    public void setPrincipal(Customer principal) {
        this.principal = principal;
    }

    
}
