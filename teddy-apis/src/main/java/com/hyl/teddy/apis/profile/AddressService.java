package com.hyl.teddy.apis.profile;

import java.util.List;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.hyl.teddy.apis.entity.CustAddress;
import com.hyl.teddy.apis.entity.Customer;
import com.hyl.teddy.apis.entity.Point;
import com.hyl.teddy.apis.entity.Region;
import com.hyl.teddy.apis.profile.dao.AddressDao;
import java.util.Collection;
import javax.naming.AuthenticationException;

@Service
public class AddressService {

    @Resource
    private AddressDao dao;
    @Resource
    private CustomerService customerService;

    public List<DeliveryAddressResponse> findAddress(int principal) {
        return dao.findAddress(principal);
    }

    public int addAddress(CustAddress ad) throws AuthenticationException {
        Customer customer = customerService.getCustomer(ad.getCustomerId());
        if (customer == null) {
            throw new AuthenticationException();
        }
        int c = dao.addAddress(ad);
        dao.updateDefaultAddress(c, customer.getId());
        customer.setMaiden(false);
        return c;
    }

    /**
     * 获得送餐
     *
     * @param id 送餐地址id
     * @return
     */
    public CustAddress getAddress(int id) {
        return dao.findCustAddress(id);
    }

    public int updateAddress(CustAddress ca) {
        return dao.updateAddress(ca);

    }

    public int removeAddress(int id) {
        return dao.removeAddress(id);

    }

    /**
     *
     * @param id 送餐地址id
     * @param principal 用户id
     * @return
     */
    @Transactional
    public void putDefault(int id, int principal) {
        dao.updateDefaultAddress(id, principal);
    }

    /**
     * 根据坐标点获得所在区域
     *
     * @param p 顶点（x y）
     * @return 区域id
     */
    public int findRegionId(int x, int y) {
        Point p = new Point(x, y);
        Collection<Region> list = dao.findRegions();
        for (Region r : list) {
            if (r.contains(p)) {
                return r.getId();
            }
        }
        return 0;
    }
}
