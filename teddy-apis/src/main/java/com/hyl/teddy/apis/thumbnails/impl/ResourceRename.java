/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.thumbnails.impl;

import com.hyl.teddy.apis.thumbnails.ThumbSize;
import net.coobird.thumbnailator.ThumbnailParameter;
import net.coobird.thumbnailator.name.Rename;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Administrator
 */
public class ResourceRename extends Rename {

    private final String suffix;

    public ResourceRename(ThumbSize size) {
        this.suffix = size.name();
    }

    public ResourceRename(String suffix) {
        this.suffix = suffix;
    }

    @Override
    public String apply(String fileName, ThumbnailParameter param) {
        if (StringUtils.isNotBlank(suffix)) {
            return appendSuffix(fileName, "-" + suffix);
        }
        return fileName;
    }

}
