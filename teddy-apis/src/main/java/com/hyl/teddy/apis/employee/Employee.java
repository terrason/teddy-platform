package com.hyl.teddy.apis.employee;

import java.util.Collection;
import java.util.LinkedHashSet;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 个人中心 用户信息表 customer
 *
 * @author Administrator
 *
 */
public class Employee {

    private int id;// 员工ID
    @JsonProperty("account")
    private String username;// 工号
    private String name;// 姓名
    @JsonProperty("hotline")
    private String mobile; // 客服电话
    @JsonIgnore
    private String longitude;
    @JsonIgnore
    private String latitude;
    @JsonIgnore
    private String pushKey;
    @JsonIgnore
    private String password;
    private int type;
    @JsonIgnore
    private int regionId;
    @JsonIgnore
    private final Collection<String> works = new LinkedHashSet<String>();
    /**
     * 员工任务量.
     */
    @JsonIgnore
    private int taskWeight;

    public void addWork(String work) {
        works.add(work);
    }

    public void removeWork(String work) {
        works.remove(work);
    }

    @JsonIgnore
    public boolean isIdle() {
        return works.isEmpty();
    }

    public String getPushKey() {
        return pushKey;
    }

    public void setPushKey(String pushKey) {
        this.pushKey = pushKey;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    /**
     * 员工任务量.
     *
     * @return the taskWeight
     */
    public int getTaskWeight() {
        return taskWeight;
    }

    /**
     * 员工任务量.
     *
     * @param taskWeight the taskWeight to set
     */
    public void setTaskWeight(int taskWeight) {
        this.taskWeight = taskWeight;
    }

    public synchronized int increaseTaskWeight(int weight) {
        taskWeight += weight;
        return taskWeight;
    }

    public synchronized int decreaseTaskWeight(int weight) {
        taskWeight = weight > taskWeight ? 0 : taskWeight - weight;
        return taskWeight;
    }

    public int getRegionId() {
        return regionId;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }
}
