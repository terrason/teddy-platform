package com.hyl.teddy.apis.system.thumbnails;

import com.hyl.teddy.apis.thumbnails.ThumbSize;
import com.hyl.teddy.apis.thumbnails.impl.AttachmentThumbnailsGenerator;
import java.awt.Dimension;
import org.springframework.stereotype.Service;

/**
 *
 * @author Administrator
 */
@Service("systemAdvertisement")
public class AdvertisementForHome extends AttachmentThumbnailsGenerator {

    public String toThumbmailsMD(int resource) {
        return toThumbmails(resource, ThumbSize.MD);
    }

    @Override
    protected Dimension size(ThumbSize pattern) {
        switch (pattern) {
            case MD:
                return new Dimension(1280, 646);//640*323
            default:
                return null;
        }
    }

}
