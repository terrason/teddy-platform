/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.payment.listener;

import com.hyl.teddy.apis.entity.Payment;
import com.hyl.teddy.apis.entity.PaymentGoods;
import com.hyl.teddy.apis.payment.PaymentListenerAdaptor;
import com.hyl.teddy.apis.payment.PaymentStatusChangeEvent;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

/**
 * 在取菜员确认订单时，将订单明细状态改为1.
 *
 * @author terrason
 */
@Service
public class ChangeGoodsStatusListener extends PaymentListenerAdaptor {

    @Resource
    private JdbcTemplate jdbcTemplate;

    @Override
    public void fireStatusChanged(PaymentStatusChangeEvent event) {
        Payment payment = event.getPayment();
        if (payment.getStatus() == 4 || payment.getStatus() == 5) {
            logger.debug("监听到订单状态改变为{}，更新订单菜品已接单状态...", payment.getStatus());
            final List<PaymentGoods> goods = payment.getGoods();
            jdbcTemplate.batchUpdate("update payment_goods set status=? where id=?", new BatchPreparedStatementSetter() {

                @Override
                public void setValues(PreparedStatement ps, int i) throws SQLException {
                    PaymentGoods item = goods.get(i);
                    if (item.getStatus() == 0) {
                        item.setStatus(1);
                    } else if (item.getStatus() == 2) {
                        item.setStatus(3);
                    }
                    ps.setInt(1, item.getStatus());
                    ps.setInt(2, item.getId());
                }

                @Override
                public int getBatchSize() {
                    return goods.size();
                }
            });
            logger.debug("订单菜品状态更新完成==========>");
        }
    }

    @Override
    public int getPriority() {
        return 201;
    }

}
