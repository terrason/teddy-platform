/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.payment.dao;

import com.hyl.teddy.apis.payment.AlipayRefundRequest;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author terrason
 */
@Repository
public class AlipayDao {

    @Resource
    private JdbcTemplate jdbcTemplate;

    @CachePut(value = "alipay_refund_request", key = "#orderno")
    public AlipayRefundRequest insertRefundRequest(String id, String method, String orderno, String refundDate, String detail) {
        AlipayRefundRequest request = new AlipayRefundRequest();
        request.setId(id);
        request.setOrderno(orderno);
        request.setStatus(-1);
        jdbcTemplate.update("insert into alipay_refund_request(batch_no,`type`,orderno,refund_date,detail_data)values(?,?,?,?,?)", id, method, orderno, refundDate, detail);
        return request;
    }

    @CacheEvict(value = "alipay_refund_request", key = "#orderno")
    public void updateRefundRequestStatus(String id, boolean success, String orderno) {
        jdbcTemplate.update("update alipay_refund_request set status=? where batch_no=?", success ? 1 : 0, id);
    }

    public void insertRefundResponse(String id, int successNum, String resultDetails) {
        jdbcTemplate.update("insert into alipay_refund_response(batch_no,success_num,details)values(?,?,?)", id, successNum, resultDetails);
    }

    public AlipayRefundRequest selectRequest(String id) {
        List<AlipayRefundRequest> list = jdbcTemplate.query("select batch_no,orderno,status from alipay_refund_request where batch_no=?", new RowMapper<AlipayRefundRequest>() {

            @Override
            public AlipayRefundRequest mapRow(ResultSet rs, int rowNum) throws SQLException {
                AlipayRefundRequest entity = new AlipayRefundRequest();
                entity.setId(rs.getString(1));
                entity.setOrderno(rs.getString(2));
                entity.setStatus(rs.getInt(3));
                return entity;
            }
        }, id);
        return list.isEmpty() ? null : list.get(0);
    }

    public String selectRequestOrderno(String id) {
        List<String> list = jdbcTemplate.query("select orderno from alipay_refund_request where batch_no=?", new RowMapper<String>() {

            @Override
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getString(1);
            }
        }, id);
        return list.isEmpty() ? null : list.get(0);
    }

    @Cacheable(value = "alipay_refund_request", key = "#orderno", unless = "#result == null")
    public AlipayRefundRequest selectPaymentRequest(String orderno) {
        List<AlipayRefundRequest> list = jdbcTemplate.query("select batch_no,orderno,status from alipay_refund_request where orderno=? and status=0", new RowMapper<AlipayRefundRequest>() {

            @Override
            public AlipayRefundRequest mapRow(ResultSet rs, int rowNum) throws SQLException {
                AlipayRefundRequest entity = new AlipayRefundRequest();
                entity.setId(rs.getString(1));
                entity.setOrderno(rs.getString(2));
                entity.setStatus(rs.getInt(3));
                return entity;
            }
        }, orderno);
        return list.isEmpty() ? null : list.get(0);
    }

    public void insertPaid(String orderno, String alipayno, double fee) {
//        TODO
    }
}
