package com.hyl.teddy.apis.payment;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * 订单状态用
 * @author Administrator
 *
 */
public class TimeStep {
	private String time;// 时刻
	private String description;
	@JsonIgnore
	private int status;

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
