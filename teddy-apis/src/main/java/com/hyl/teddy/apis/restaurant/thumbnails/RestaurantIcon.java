package com.hyl.teddy.apis.restaurant.thumbnails;

import com.hyl.teddy.apis.thumbnails.ThumbSize;
import com.hyl.teddy.apis.thumbnails.impl.AttachmentThumbnailsGenerator;
import java.awt.Dimension;
import org.springframework.stereotype.Service;

/**
 *
 * @author Administrator
 */
@Service
public class RestaurantIcon extends AttachmentThumbnailsGenerator {

    public String toThumbmailsMD(int resource) {
        return toThumbmails(resource, ThumbSize.MD);
    }

    public String[] toThumbmailsMD(String resourceIds) {
        return toMultipleThumbmails(resourceIds, ThumbSize.MD).toArray(new String[0]);
    }

    @Override
    protected Dimension size(ThumbSize pattern) {
        switch (pattern) {
            case MD:
                return new Dimension(123, 123);//123*123
            default:
                return null;
        }
    }

}
