/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.employee;

/**
 *
 * @author terrason
 */
public class Version {

    /**
     * 版本.
     */
    private String version;
    /**
     * 更新日志.
     */
    private String log;
    /**
     * 下载地址.
     */
    private String dowlloadUrl;

    /**
     * 版本.
     *
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * 版本.
     *
     * @param version the version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * 更新日志.
     *
     * @return the log
     */
    public String getLog() {
        return log;
    }

    /**
     * 更新日志.
     *
     * @param log the log to set
     */
    public void setLog(String log) {
        this.log = log;
    }

    /**
     * 下载地址.
     *
     * @return the dowlloadUrl
     */
    public String getDowlloadUrl() {
        return dowlloadUrl;
    }

    /**
     * 下载地址.
     *
     * @param dowlloadUrl the dowlloadUrl to set
     */
    public void setDowlloadUrl(String dowlloadUrl) {
        this.dowlloadUrl = dowlloadUrl;
    }
}
