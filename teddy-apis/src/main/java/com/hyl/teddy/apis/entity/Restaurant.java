package com.hyl.teddy.apis.entity;

import com.hyl.teddy.apis.restaurant.CommentResponse;
import java.util.Collection;
import java.util.List;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * 餐厅表 restaurant
 *
 * @author Administrator
 *
 */
public class Restaurant {

    private int id;
    private String name;//名称
    private String icon;//图标
    private String[] image;//所有图片存放在附件表，此处存放附件ID，多个用","分隔'
    @JsonIgnore
    private String imageIds;
    private int price;//餐厅价位 单位：分
    private String promotion;//促销信息
    private String openTime;//营业时间
    private String closeTime;//歇业时间
    private String description;//描述
    private String location;//地点
    private String tel;
    private int star;//星级
    private boolean hotDefined;//是否自定义热菜

    private int packageFee;//打包费  （单位分）
    private int delivery; //送餐费 单位分  
    private int sold;//销量
    private List<CommentResponse> comments;
    /**
     * 热门菜.
     */
    private Collection<Dish> hot;

    private boolean favored;

    private List<String> tag;

    private boolean enabled;

    public String getBusinessHours() {
        return openTime + "-" + closeTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImageIds() {
        return imageIds;
    }

    public void setImageIds(String imageIds) {
        this.imageIds = imageIds;
    }

    public boolean isFavored() {
        return favored;
    }

    public void setFavored(boolean favored) {
        this.favored = favored;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String[] getImage() {
        return image;
    }

    public void setImage(String[] image) {
        this.image = image;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getPromotion() {
        return promotion;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public boolean isHotDefined() {
        return hotDefined;
    }

    public void setHotDefined(boolean hotDefined) {
        this.hotDefined = hotDefined;
    }

    public int getPackageFee() {
        return packageFee;
    }

    public void setPackageFee(int packageFee) {
        this.packageFee = packageFee;
    }

    public int getDelivery() {
        return delivery;
    }

    public void setDelivery(int delivery) {
        this.delivery = delivery;
    }

    public int getSold() {
        return sold;
    }

    public void setSold(int sold) {
        this.sold = sold;
    }

    public Collection<Dish> getHot() {
        return hot;
    }

    public void setHot(Collection<Dish> hot) {
        this.hot = hot;
    }

    public List<CommentResponse> getComments() {
        return comments;
    }

    public void setComments(List<CommentResponse> comments) {
        this.comments = comments;
    }

    public List<String> getTag() {
        return tag;
    }

    public void setTag(List<String> tag) {
        this.tag = tag;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}
