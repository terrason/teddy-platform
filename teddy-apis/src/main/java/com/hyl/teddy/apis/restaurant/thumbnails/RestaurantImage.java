/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.restaurant.thumbnails;

import com.hyl.teddy.apis.thumbnails.ThumbSize;
import com.hyl.teddy.apis.thumbnails.impl.AttachmentThumbnailsGenerator;
import java.awt.Dimension;
import org.springframework.stereotype.Service;

/**
 *
 * @author terrason
 */
@Service
public class RestaurantImage extends AttachmentThumbnailsGenerator {

    public String[] toThumbnailsMD(String ids) {
        return toMultipleThumbmails(ids, ThumbSize.MD).toArray(new String[0]);
    }

    @Override
    protected Dimension size(ThumbSize pattern) {
        switch (pattern) {
            case MD:
                return new Dimension(640, 298);
            default:
                return null;
        }
    }

}
