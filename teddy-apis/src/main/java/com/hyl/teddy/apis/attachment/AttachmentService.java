/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.attachment;

import com.hyl.teddy.apis.AbstractComponent;
import com.hyl.teddy.apis.AttachmentCleaner;
import com.hyl.teddy.apis.attachment.dao.AttachmentDao;
import com.hyl.teddy.apis.tools.CnToSpell;
import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author terrason
 */
@Service
public class AttachmentService extends AbstractComponent {

    private final Pattern schema = Pattern.compile("^(http|https|ftp)://.*");
    @Resource
    private AttachmentCleaner attachmentCleaner;
    @Resource
    private AttachmentDao attachmentDao;

    @Value("#{settings['resource.host']}")
    private String _resourceHost;
    @Value("#{settings['resource.path']}")
    private String _resourcePath;
    @Value("#{settings['url.attachment']}")
    private String attachmentUrlPrefix;

    /**
     * 判断某资源是否是外部资源.
     */
    public boolean isInternetResource(String resource) {
        return schema.matcher(resource).matches();
    }

    /**
     * 获取资源服务器地址.
     */
    public String getResourceHost() {
        if (StringUtils.isBlank(_resourceHost)) {
            HttpServletRequest request = currentRequest();
            return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        }
        if (_resourceHost.startsWith("..")) {
            HttpServletRequest request = currentRequest();
            return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + _resourceHost.substring(2);
        }
        if (_resourceHost.startsWith("./")) {
            HttpServletRequest request = currentRequest();
            return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + _resourceHost.substring(1);
        }
        return _resourceHost;
    }

    /**
     * 获取资源服务器文件路径.
     */
    public String getResourceFilepath() {
        if (StringUtils.isBlank(_resourcePath)) {
            HttpServletRequest request = currentRequest();
            return request.getServletContext().getRealPath("/");
        }
        if (_resourcePath.startsWith("." + File.separator)) {
            HttpServletRequest request = currentRequest();
            return request.getServletContext().getRealPath("/") + _resourcePath.substring(1);
        }

        if (_resourcePath.startsWith("..")) {
            HttpServletRequest request = currentRequest();
            String realPath = request.getServletContext().getRealPath("/");
            int index = realPath.lastIndexOf(File.separatorChar, realPath.length() - 2);
            String parentPath = realPath.substring(0, index);
            return parentPath + _resourcePath.substring(2);
        }
        return _resourcePath;
    }

    /**
     * 将数据库中保存的{@code 资源值}转化成资源URL.
     *
     * @param resource {@code 资源值}
     * @return 资源URL或{@code null}
     */
    public String toResourceUrl(String resource) {
        if (StringUtils.isBlank(resource)) {
            return null;
        }
        if (schema.matcher(resource).matches()) {
            return resource;
        } else {
            return getResourceHost() + resource;
        }
    }

    /**
     * 根据附件ID生成资源URL.
     * 生成的URL能被{@link com.hyl.teddy.apis.attachment.web.AttachmentController}解析。
     *
     * @param id 附件ID
     * @return 资源URL或{@code null}
     */
    public String toResourceUrl(int id) {
        if (id <= 0) {
            return null;
        }
        HttpServletRequest request = currentRequest();
        String htx = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        return htx + "/attachment/" + id;
    }

    /**
     * 生成通用错误资源URL重定向链接. 会生成`/attachment/null`.
     *
     * @param size 缩放大小
     * @return 资源URL
     */
    public String toBrokenUrl(Dimension size) {
        HttpServletRequest request = currentRequest();
        String htx = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        return htx + "/attachment/null?width=" + size.width + "&height=" + size.height;
    }

    /**
     * 根据附件ID生成资源URL.
     * 生成的URL能被{@link com.hyl.teddy.apis.attachment.web.AttachmentController}解析。
     *
     * @param id 附件ID
     * @param size 缩略图大小
     * @return 资源URL或{@code null}
     */
    public String toResourceUrl(int id, Dimension size) {
        if (size == null) {
            return toResourceUrl(id);
        }
        if (id <= 0) {
            return null;
        }
        if (StringUtils.isBlank(attachmentUrlPrefix)) {
            HttpServletRequest request = currentRequest();
            attachmentUrlPrefix = request.getScheme() + "://" + request.getRemoteHost() + ":" + request.getServerPort() + request.getContextPath() + "/attachment/";
        }
        return attachmentUrlPrefix + id + "?width=" + size.width + "&height=" + size.height;
    }

    /**
     * 将数据库中保存的多个{@code 资源值}转化成资源URL数组.
     *
     * @param resource 多个{@code 资源值}，用英文逗号（,）分隔.
     * @return 资源URL数组或空数组.
     */
    public String[] toMultipleResourceUrl(String resources) {
        if (StringUtils.isBlank(resources)) {
            return new String[0];
        }
        String[] split = resources.split(",");
        for (int i = 0; i < split.length; i++) {
            String resource = split[i];
            if (!schema.matcher(resource).matches()) {
                split[i] = getResourceHost() + resource;
            }
        }
        return split;
    }

    /**
     * 根据附件路径获取附件文件信息.
     *
     * @param url 文件路径. 数据库中保存的相对路径。
     * @return 附件文件信息
     */
    public PathInfo parseAttachment(String url) {
        if (url.startsWith(getResourceHost())) {
            int length = getResourceHost().length();
            String realUrl = url.substring(length);
            return parseAttachment(realUrl);
        } else if (schema.matcher(url).matches()) {
            return new PathInfo(null, url);
        } else {
            StringBuilder filePath = new StringBuilder();
            StringBuilder webpath = new StringBuilder();
            String filePathAndName = url.replace('/', File.separatorChar);

            filePath.append(getResourceFilepath());
            if (!filePathAndName.startsWith(File.separator)) {
                filePath.append(File.separatorChar);
                webpath.append("/");
            }
            filePath.append(filePathAndName);
            webpath.append(url);

            File file = new File(filePath.toString());
            File dictory = file.getParentFile();
            if (!dictory.exists()) {
                dictory.mkdirs();
            }
            return new PathInfo(file, webpath.toString());
        }
    }

    /**
     * 删除单个附件.
     * 相当于执行{@link #removeAttachment(java.lang.String, boolean) removeAttachment(url,false)}.
     *
     * @param url 单个{@code 资源值}
     */
    public void removeAttachment(String url) {
        if (StringUtils.isBlank(url)) {
            return;
        }
        File file = parseAttachment(url).getFile();
        attachmentCleaner.put(file);
    }

    /**
     * 立即删除附件.
     *
     * @param url 附件{@code 资源值}
     * @param immediate 是否理立即删除. 传入{@code false}时，会在另一个线程中处理删除逻辑。
     */
    public void removeAttachment(String url, boolean immediate) {
        if (StringUtils.isBlank(url)) {
            return;
        }
        File file = parseAttachment(url).getFile();
        if (immediate) {
            attachmentCleaner.delete(file);
        } else {
            attachmentCleaner.put(file);
        }
    }

    /**
     * 删除多个附件.
     *
     * @param urls 多个{@code 资源值}，使用英文逗号{@code (,)}分隔。
     */
    public void removeMultipleAttachment(String urls) {
        if (StringUtils.isBlank(urls)) {
            return;
        }
        for (String url : urls.split(",")) {
            removeAttachment(url);
        }
    }

    /**
     * 上传多张附件.
     */
    public String[] upfile(MultipartFile[] multipartFiles, String namespace) throws IOException {
        if (multipartFiles == null || multipartFiles.length == 0) {
            return null;
        }
        ArrayList<String> urls = new ArrayList<>();
        Set<String> filenames = new HashSet<>();
        String path = namespace.endsWith("/") ? namespace + UUID.randomUUID() + "/" : namespace + "/" + UUID.randomUUID() + "/";
        for (int i = 0; i < multipartFiles.length; i++) {
            MultipartFile multipartFile = multipartFiles[i];
            if (multipartFile == null || multipartFile.isEmpty()) {
                continue;
            }
            String filename = CnToSpell.getFullSpell(multipartFile.getOriginalFilename());
            if (filenames.contains(filename)) {
                filename += ("-" + i);
            }
            filenames.add(filename);

            String url = path + filename;
            PathInfo attachment = parseAttachment(url);
            File file = attachment.getFile();

            File directory = file.getParentFile();
            if (!directory.exists()) {
                directory.mkdirs();
            }
            InputStream inputStream = null;
            try {
                inputStream = multipartFile.getInputStream();
                file.createNewFile();
                FileUtils.copyInputStreamToFile(inputStream, file);
                urls.add(attachment.getUrl());
            } finally {
                try {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                } catch (IOException ex) {
                    logger.warn("未知的错误", ex);
                }
            }
        }
        return urls.toArray(new String[0]);
    }

    /**
     * 上传单个附件.
     */
    public String upfile(MultipartFile multipartFile, String namespace) throws UpfileFailedException {
        if (multipartFile == null || multipartFile.isEmpty()) {
            return null;
        }
        String filename = CnToSpell.getFullSpell(multipartFile.getOriginalFilename());
        String url = namespace.endsWith("/") ? namespace + UUID.randomUUID() + "/" + filename : namespace + "/" + UUID.randomUUID() + "/" + filename;
        PathInfo attachment = parseAttachment(url);
        File file = attachment.getFile();

        File directory = file.getParentFile();
        if (!directory.exists()) {
            directory.mkdirs();
        }
        InputStream inputStream = null;
        try {
            inputStream = multipartFile.getInputStream();
            file.createNewFile();
            FileUtils.copyInputStreamToFile(inputStream, file);
            return attachment.getUrl();
        } catch (IOException ex) {
            throw new UpfileFailedException("上传文件出错! namespace=" + namespace + ",file=" + multipartFile.getOriginalFilename());
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException ex) {
                logger.warn("未知的错误", ex);
            }
        }
    }

    /**
     * 创建临时的附件.
     *
     * @param attachment 附件信息
     * @return 新创建的附件ID
     */
    public int createTemporaryAttachment(PathInfo attachment) {
        return attachmentDao.insertAttachment(attachment);
    }

    /**
     * 修改附件状态.
     *
     * @param id 附件ID
     * @param temporary 修改后的状态（表示附件是否临时性的）
     */
    public void changeAttachmentTemporary(int id, boolean temporary) {
        attachmentDao.updateTemporary(id, temporary);
    }

    /**
     * 修改附件状态.
     *
     * @param ids 附件ID
     * @param temporary 修改后的状态（表示附件是否临时性的）
     */
    public void changeAttachmentTemporary(String ids, boolean temporary) {
        attachmentDao.updateTemporary(ids, temporary);
    }

    /**
     * 查看附件内容.
     *
     * @param id 附件ID
     * @return 附件相对路径
     */
    public String view(int id) {
        if (id <= 0) {
            return null;
        }
        return attachmentDao.selectPath(id);
    }
}
