package com.hyl.teddy.apis;

import com.hyl.teddy.apis.attachment.UpfileFailedException;
import java.util.Locale;
import javax.annotation.Resource;
import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GlobalExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @Resource
    private MessageSource messageSource;

    @ExceptionHandler(CommonRuntimeException.class)
    @ResponseBody
    public BaseResponse commonRuntimeExceptionHandler(HttpServletRequest req, Locale locale, CommonRuntimeException ex) throws Exception {
        BaseResponse response = new BaseResponse();
        response.setCode(ex.getStatusCode().value());
        response.setMessage(translateMessage(ex, locale));
        logger.error(response.getMessage(), ex);
        return response;
    }

    @ExceptionHandler(AuthenticationException.class)
    @ResponseBody
    public BaseResponse authenticationExceptionHandler(HttpServletRequest req, Locale locale, AuthenticationException ex) throws Exception {
        BaseResponse response = new BaseResponse();
        response.setCode(HttpStatus.UNAUTHORIZED.value());
        response.setMessage(messageSource.getMessage("error.unauthorized", null, locale));
        logger.error(ex.getMessage(), ex);
        return response;
    }

    @ExceptionHandler(UpfileFailedException.class)
    @ResponseBody
    public BaseResponse upfileFailedExceptionHandler(HttpServletRequest req, Locale locale, UpfileFailedException ex) throws Exception {
        BaseResponse response = new BaseResponse();
        response.setCode(HttpStatus.INSUFFICIENT_STORAGE.value());
        response.setMessage(messageSource.getMessage("error.default", null, locale));
        logger.error(ex.getMessage(), ex);
        return response;
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseBody
    public BaseResponse illegalArgumentExceptionHandler(HttpServletRequest req, Locale locale, IllegalArgumentException ex) throws Exception {
        BaseResponse response = new BaseResponse();
        response.setCode(HttpStatus.NOT_ACCEPTABLE.value());
        response.setMessage(messageSource.getMessage("error.argument", new Object[]{ex.getMessage()}, locale));
        logger.error(response.getMessage(), ex);
        return response;
    }

    @ExceptionHandler
    @ResponseBody
    public BaseResponse defaultErrorHandler(HttpServletRequest req, Locale locale, Exception ex) throws Exception {
        if (AnnotationUtils.findAnnotation(ex.getClass(), ResponseStatus.class) != null) {
            throw ex;
        }
        if (ex instanceof ServletRequestBindingException || (ex instanceof TypeMismatchException) || (ex instanceof HttpRequestMethodNotSupportedException)) {
            throw ex;
        }
        BaseResponse response = new BaseResponse();
        response.setCode(500);
        response.setMessage(messageSource.getMessage("error.default", null, locale));
        logger.error(ex.getMessage(), ex);
        return response;
    }

    private String translateMessage(CommonRuntimeException ex, Locale locale) {
        String statusText = ex.getStatusText();
        String[] texts = statusText.split("[|]");
        if (texts.length == 1) {
            try {
                return messageSource.getMessage(ex.getStatusText(), ex.getParams(), locale);
            } catch (NoSuchMessageException noSuchMessageException) {
                return ex.getStatusText();
            }
        } else {
            StringBuilder builder = new StringBuilder();
            for (String text : texts) {
                try {
                    builder.append(messageSource.getMessage(text, null, locale)).append("<br />");
                } catch (NoSuchMessageException noSuchMessageException) {
                    builder.append(text).append("<br />");
                }
            }
            return builder.toString();
        }
    }

}
