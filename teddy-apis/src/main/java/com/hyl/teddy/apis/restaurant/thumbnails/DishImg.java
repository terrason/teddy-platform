package com.hyl.teddy.apis.restaurant.thumbnails;

import com.hyl.teddy.apis.thumbnails.ThumbSize;
import com.hyl.teddy.apis.thumbnails.impl.AttachmentThumbnailsGenerator;
import java.awt.Dimension;
import org.springframework.stereotype.Service;

/**
 * 菜品图片缩略图
 *
 * @author Administrator
 */
@Service
public class DishImg extends AttachmentThumbnailsGenerator {

    public String toThumbmailsXS(int resource) {
        return toThumbmails(resource, ThumbSize.XS);
    }

    public String toThumbmailsMD(int resource) {
        return toThumbmails(resource, ThumbSize.MD);
    }

    public String toThumbmailsLG(int resource) {
        return toThumbmails(resource, ThumbSize.LG);
    }

    @Override
    public String toThumbmails(int resource, ThumbSize pattern) {
        return super.toThumbmails(resource, pattern);
    }

    @Override
    protected Dimension size(ThumbSize pattern) {
        switch (pattern) {
            case MD:
                return new Dimension(123, 123);//123*123
            case LG:
                return new Dimension(565, 485);
            case XS:
                return new Dimension(24, 24);
            default:
                return null;
        }

    }
}
