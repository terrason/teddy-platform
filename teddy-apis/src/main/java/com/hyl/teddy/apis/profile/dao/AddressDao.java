package com.hyl.teddy.apis.profile.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.hyl.teddy.apis.entity.CustAddress;
import com.hyl.teddy.apis.entity.Point;
import com.hyl.teddy.apis.entity.Region;
import com.hyl.teddy.apis.profile.DeliveryAddressResponse;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.RowCallbackHandler;

@Repository
public class AddressDao {

    @Resource
    private JdbcTemplate jdbcTemplate;

    public List<DeliveryAddressResponse> findAddress(final int principal) {
        List<DeliveryAddressResponse> list = jdbcTemplate.query("SELECT a.* FROM cust_address a WHERE a.customer_id = ?", new RowMapper<DeliveryAddressResponse>() {

            @Override
            public DeliveryAddressResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
                DeliveryAddressResponse a = new DeliveryAddressResponse();
                a.setId(rs.getInt("id"));
                a.setName(rs.getString("name"));
                a.setDefaultAddress(rs.getBoolean("default"));
                a.setMobile(rs.getString("mobile"));
                a.setLocation(rs.getString("location"));
                a.setLongitude(rs.getInt("longitude") * Math.pow(10, -6));
                a.setLatitude(rs.getInt("latitude") * Math.pow(10, -6));
                return a;
            }

        }, principal);
        return list;
    }

    public int addAddress(final CustAddress ad) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        final String sql = " INSERT INTO cust_address (customer_id, mobile, location, longitude, latitude, name, `default`) values(?,?,?,?,?,?,?)";
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setInt(1, ad.getCustomerId());
                ps.setString(2, ad.getMobile());
                ps.setString(3, ad.getLocation());
                ps.setInt(4, ad.getLongitudeValue());
                ps.setInt(5, ad.getLatitudeValue());
                ps.setString(6, ad.getName());
                ps.setBoolean(7, ad.isDefaultAddress());
                return ps;

            }

        }, keyHolder);
        return keyHolder.getKey().intValue();
    }

    /**
     * 获得送餐
     *
     * @param id 送餐地址id
     * @param principal 当前用户id
     * @return
     */
    @Cacheable(value = "address", key = "#id", unless = "#result == null")
    public CustAddress findCustAddress(int id) {
        try {
            return jdbcTemplate.queryForObject("select * from cust_address where id = ?", new RowMapper<CustAddress>() {

                @Override
                public CustAddress mapRow(ResultSet rs, int rowNum) throws SQLException {
                    CustAddress ca = new CustAddress();
                    ca.setId(rs.getInt("id"));
                    ca.setCustomerId(rs.getInt("customer_id"));
                    ca.setMobile(rs.getString("mobile"));
                    ca.setLocation(rs.getString("location"));
                    ca.setLongitude(String.format("%.6f", rs.getInt("longitude") * Math.pow(10, -6)));
                    ca.setLatitude(String.format("%.6f", rs.getInt("latitude") * Math.pow(10, -6)));
                    ca.setDefaultAddress(rs.getBoolean("default"));
                    ca.setLongitudeValue(rs.getInt("longitude"));
                    ca.setLatitudeValue(rs.getInt("latitude"));
                    ca.setRegionId(rs.getInt("region_id"));
                    return ca;
                }

            }, id);
        } catch (IncorrectResultSizeDataAccessException ex) {
            return null;
        }
    }

    @CacheEvict(value = "address", key = "#ca.id")
    public int updateAddress(CustAddress ca) {
        return jdbcTemplate.update("UPDATE cust_address SET mobile = ?, location = ?, longitude = ?, latitude = ?, region_id = ? ,name = ? WHERE id =? ",
                ca.getMobile(), ca.getLocation(), ca.getLongitudeValue(), ca.getLatitudeValue(), ca.getRegionId(), ca.getName(), ca.getId());
    }

    @CacheEvict(value = "address", key = "#id")
    public int removeAddress(int id) {
        return jdbcTemplate.update("DELETE from cust_address WHERE id = ?", id);
    }

    /**
     * 设置默认送餐地址
     *
     * @param id
     * @return
     */
    @CacheEvict("address")
    public void updateDefaultAddress(int id, int principal) {
        jdbcTemplate.update("update cust_address set `default`= 0 where customer_id=?", principal);
        jdbcTemplate.update("update cust_address set `default` = 1 where id = ?", id);
    }

    private class RegionPoints {

        private final ArrayList<Point> points = new ArrayList<Point>();

        public void addPoint(Point p) {
            points.add(p);
        }

        public Point[] toArray() {
            return points.toArray(new Point[points.size()]);
        }
    }

    /**
     * 获得所有区域
     *
     * @return
     */
    @Cacheable("regions")
    public Collection<Region> findRegions() {
        final Map<Integer, Region> regions = new LinkedHashMap<Integer, Region>();
        final Map<Integer, RegionPoints> points = new LinkedHashMap<Integer, RegionPoints>();
        jdbcTemplate.query("select * from region", new RowCallbackHandler() {

            @Override
            public void processRow(ResultSet rs) throws SQLException {
                Region r = new Region();
                r.setId(rs.getInt("id"));
                r.setName(rs.getString("name"));
                regions.put(r.getId(), r);
            }

        }, (Object[]) null);
        jdbcTemplate.query("select id, region_id, longitude,latitude from region_point", new RowCallbackHandler() {

            @Override
            public void processRow(ResultSet rs) throws SQLException {
                Point p = new Point();
                p.setId(rs.getInt("id"));
                p.setX(rs.getInt("longitude"));
                p.setY(rs.getInt("latitude"));
                int regionId = rs.getInt("region_id");
                RegionPoints rps = points.get(regionId);
                if (rps == null) {
                    rps = new RegionPoints();
                    points.put(regionId, rps);
                }
                rps.addPoint(p);
            }

        }, (Object[]) null);
        for (Map.Entry<Integer, Region> entrySet : regions.entrySet()) {
            Integer key = entrySet.getKey();
            Region value = entrySet.getValue();
            RegionPoints rps = points.get(key);
            if (rps != null) {
                value.setPoints(rps.toArray());
            }
        }
        return regions.values();
    }

}
