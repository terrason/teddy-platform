/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.restaurant;

/**
 *
 * @author Administrator
 */
public class CommentResponse {

    public CommentResponse() {
    }

    public CommentResponse(int id, String nickname, int star, String createTime, String comment) {
        this.id = id;
        this.nickname = nickname;
        this.star = star;
        this.createTime = createTime;
        this.comment = comment;
    }

    private int id;
    /**
     * 用户昵称
     */
    private String nickname;
    /**
     * 星级
     */
    private int star;
    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 评价内容
     */
    private String comment;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
