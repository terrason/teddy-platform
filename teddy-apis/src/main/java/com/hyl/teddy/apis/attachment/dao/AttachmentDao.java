/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.attachment.dao;

import com.hyl.teddy.apis.attachment.PathInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

/**
 *
 * @author terrason
 */
@Repository
public class AttachmentDao {

    @Resource
    private JdbcTemplate jdbcTemplate;

    public int insertAttachment(final PathInfo attachment) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
                PreparedStatement ps = conn.prepareStatement("insert into attachment(`file`,`path`,create_time,`temporary`)values(?,?,now(),1)", Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, attachment.getFile().getAbsolutePath());
                ps.setString(2, attachment.getUrl());
                return ps;
            }
        }, keyHolder);
        return keyHolder.getKey().intValue();
    }

    public void updateTemporary(int id, boolean temporary) {
        jdbcTemplate.update("update attachment set `temporary`=? where id=?", temporary, id);
    }

    public void updateTemporary(String ids, boolean temporary) {
        jdbcTemplate.update("update attachment set `temporary`=? where id in (" + ids + ")");
    }

    @Cacheable(value = "attachment", key = "#id")
    public String selectPath(int id) {
        List<String> list = jdbcTemplate.query("select `path` from attachment where id=?", new RowMapper<String>() {

            @Override
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getString(1);
            }
        }, id);
        return list.isEmpty() ? null : list.get(0);
    }
}
