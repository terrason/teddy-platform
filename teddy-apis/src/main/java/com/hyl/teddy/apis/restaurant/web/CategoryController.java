package com.hyl.teddy.apis.restaurant.web;

import com.hyl.teddy.apis.BaseResponse;
import com.hyl.teddy.apis.entity.RestaurantCategory;
import com.hyl.teddy.apis.restaurant.CategoryService;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 餐厅分类列表
 * @author Administrator
 */
@Controller
@RequestMapping
public class CategoryController {
    @Resource
    private CategoryService cs;
    @RequestMapping(value = "/restaurant/category",method=RequestMethod.GET)
    @ResponseBody
    public BaseResponse categoryList(){
        BaseResponse response = new BaseResponse();
        List<RestaurantCategory> restaurantCategory = cs.categoryList();
        RestaurantCategory rc = new RestaurantCategory();
        rc.setId(0);
        rc.setName("全部分类");
        restaurantCategory.add(rc);
        response.setData(restaurantCategory);
        return response;
    }
}
