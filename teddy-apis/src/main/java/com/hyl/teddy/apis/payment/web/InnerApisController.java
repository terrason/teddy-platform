/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.payment.web;

import com.hyl.teddy.apis.BaseResponse;
import com.hyl.teddy.apis.CommonRuntimeException;
import com.hyl.teddy.apis.employee.EmployeeService;
import com.hyl.teddy.apis.entity.Payment;
import com.hyl.teddy.apis.payment.AlipayService;
import com.hyl.teddy.apis.payment.PaymentService;
import com.hyl.teddy.apis.promotion.PromotionService;
import java.util.Date;
import javax.annotation.Resource;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author terrason
 */
@Controller
public class InnerApisController {

    @Resource
    private PromotionService promotionService;
    @Resource
    private PaymentService paymentService;
    @Resource
    private EmployeeService employeeService;
    @Resource
    private MessageSource messageSource;
    @Resource
    private AlipayService alipayService;

    @Value("#{settings['alipay.cancel.callback']}")
    private String cancelCallback;
    @Value("#{settings['alipay.giveup.callback']}")
    private String giveupCallback;
    @Value("#{settings['alipay.exchange.callback']}")
    private String exchangeCallback;
    private final Logger logger = LoggerFactory.getLogger(InnerApisController.class);
//
//    /**
//     * 换菜接口.
//     */
//    @RequestMapping("/inner/payment-exchange/{orderno}")
//    @ResponseBody
//    public BaseResponse exchange(@PathVariable String orderno) {
//        BaseResponse response = new BaseResponse();
//        logger.debug("订单换菜开始：释放订单加菜权限，回退客户积分及代金券...");
//        Payment payment = paymentService.getPayment(orderno);
//        if (payment == null) {
//            logger.debug("回退客户积分及代金券处理非正常结束：订单不存在【orderno={}】！", orderno);
//            response.setCode(HttpStatus.NOT_FOUND.value());
//            response.setMessage(messageSource.getMessage("mission.payment", null, null));
//            return response;
//        }
//        paymentService.exchange(payment);
//        logger.debug("订单换菜处理完成=========>");
//        return response;
//    }
//
//    /**
//     * 退菜接口.
//     */
//    @RequestMapping("/inner/payment-giveup/{orderno}")
//    @ResponseBody
//    public BaseResponse giveup(@PathVariable String orderno) {
//        BaseResponse response = new BaseResponse();
//        logger.debug("订单退菜开始...");
//        Payment payment = paymentService.getPayment(orderno);
//        if (payment == null) {
//            logger.debug("订单退菜处理非正常结束：订单不存在【orderno={}】！", orderno);
//            response.setCode(HttpStatus.NOT_FOUND.value());
//            response.setMessage(messageSource.getMessage("mission.payment", null, null));
//            return response;
//        }
//
//        paymentService.giveup(payment);
//        logger.debug("订单退菜处理完成=========>");
//        return response;
//    }
//
//    /**
//     * 取消订单接口.
//     */
//    @RequestMapping("/inner/payment-cancel/{orderno}")
//    @ResponseBody
//    public BaseResponse cancel(@PathVariable String orderno) {
//        BaseResponse response = new BaseResponse();
//        logger.debug("订单取消开始...");
//        Payment payment = paymentService.getPayment(orderno);
//        if (payment == null) {
//            logger.debug("订单取消处理非正常结束：订单不存在【orderno={}】！", orderno);
//            response.setCode(HttpStatus.NOT_FOUND.value());
//            response.setMessage(messageSource.getMessage("mission.payment", null, null));
//            return response;
//        }
//
//        paymentService.cancel(payment);
//        logger.debug("订单取消处理完成=========>");
//        return response;
//    }

    /**
     * 退订单明细接口. 不会修改餐厅菜品状态。
     */
    @RequestMapping("/inner/payment/{orderno}/dish/{id}")
    @ResponseBody
    public BaseResponse cancelItem(@PathVariable String orderno, @PathVariable int id) {
        paymentService.disableGoodsItem(orderno, id);
        return new BaseResponse();
    }

    /**
     * 分配取菜员接口.
     */
    @RequestMapping("/inner/payment/{orderno}/deliver/{employee}")
    @ResponseBody
    public BaseResponse assignDeliver(@PathVariable String orderno, @PathVariable int employee) {
        Payment payment = paymentService.getPayment(orderno);
        if (payment == null) {
            throw new CommonRuntimeException(HttpStatus.NOT_FOUND, "missing.payment");
        }
        employeeService.assignDeliverToPayment(payment, employee);
        paymentService.firePaymentStatusChanged(payment, 3);
        return new BaseResponse();
    }

    /**
     * 分配配送员接口.
     */
    @RequestMapping("/inner/payment/{orderno}/courier/{employee}")
    @ResponseBody
    public BaseResponse assignCourier(@PathVariable String orderno, @PathVariable int employee) {
        Payment payment = paymentService.getPayment(orderno);
        if (payment == null) {
            throw new CommonRuntimeException(HttpStatus.NOT_FOUND, "missing.payment");
        }
        employeeService.assignCourierToPayment(payment, employee);
        return new BaseResponse();
    }

    @RequestMapping("/inner/create-pay")
    @ResponseBody
    public BaseResponse createPayRequest(@RequestParam String orderno,
            @RequestParam String subject,
            @RequestParam String body,
            @RequestParam String returnUrl,
            @RequestParam double fee) {
        AlipayService.Endorser endorser = alipayService.createPay(orderno, subject, body, returnUrl, fee);
        return new BaseResponse(endorser.toParameters());
    }

    @RequestMapping("/inner/create-refund/exchange")
    @ResponseBody
    public BaseResponse createExchangeRefund(@RequestParam String orderno) {
        BaseResponse response = new BaseResponse();
        Payment payment = paymentService.getPayment(orderno);
        if (payment == null) {
            response.setCode(HttpStatus.NOT_FOUND.value());
            response.setMessage(messageSource.getMessage("mission.payment", null, null));
            return response;
        }
        String notifyUrl = exchangeCallback;
        int originalPaid = payment.getPaid();
        int actualCost = promotionService.actualCost(payment);
        double fee = (originalPaid - actualCost) * 0.01;

        Date now = new Date();
        String ds = DateFormatUtils.format(now, "yyyyMMdd");
        String id = ds + orderno + String.valueOf(now.getTime()).substring(6);

        AlipayService.Endorser endorser = alipayService.createRefund(id, "exchange", notifyUrl, payment, fee, "客户换菜", now);
        response.setData(endorser.toParameters());
        return response;
    }

    @RequestMapping("/inner/create-refund/giveup")
    @ResponseBody
    public BaseResponse createGiveupRefund(@RequestParam String orderno) {
        BaseResponse response = new BaseResponse();
        Payment payment = paymentService.getPayment(orderno);
        if (payment == null) {
            response.setCode(HttpStatus.NOT_FOUND.value());
            response.setMessage(messageSource.getMessage("mission.payment", null, null));
            return response;
        }
        String notifyUrl = giveupCallback;
        int originalPaid = payment.getPaid();
        int actualCost = promotionService.actualCost(payment);
        double fee = (originalPaid - actualCost) * 0.01;

        Date now = new Date();
        String ds = DateFormatUtils.format(now, "yyyyMMdd");
        String id = ds + orderno + String.valueOf(now.getTime()).substring(6);

        AlipayService.Endorser endorser = alipayService.createRefund(id, "giveup", notifyUrl, payment, fee, "客户退菜", now);
        response.setData(endorser.toParameters());
        return response;
    }

    @RequestMapping("/inner/create-refund/cancel")
    @ResponseBody
    public BaseResponse createCancelRefund(@RequestParam String orderno) {
        BaseResponse response = new BaseResponse();
        Payment payment = paymentService.getPayment(orderno);
        if (payment == null) {
            response.setCode(HttpStatus.NOT_FOUND.value());
            response.setMessage(messageSource.getMessage("mission.payment", null, null));
            return response;
        }
        String notifyUrl = cancelCallback;
        double fee = 0.01 * payment.getPaid();

        Date now = new Date();
        String ds = DateFormatUtils.format(now, "yyyyMMdd");
        String id = ds + orderno + String.valueOf(now.getTime()).substring(6);

        AlipayService.Endorser endorser = alipayService.createRefund(id, "cancel", notifyUrl, payment, fee, "取消订单", now);
        response.setData(endorser.toParameters());
        return response;
    }

    @RequestMapping("/inner/alipay-beats")
    @ResponseBody
    public BaseResponse beatAlipay(@RequestParam String orderno) {
        boolean occupied = alipayService.isOccupied(orderno);
        return new BaseResponse(occupied);
    }
}
