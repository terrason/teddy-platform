package com.hyl.teddy.apis.payment.web;

import org.springframework.format.annotation.NumberFormat;

/**
 * 个人中心——我的订单返回数据
 *
 * @author Administrator
 *
 */
public class BougthResponse {

    private String id;
    private String title;//餐厅名称
    private String content;//订单内容(菜名拼接而成)
    @NumberFormat(pattern = "#.#")
    private double cost;//应付款
    private String createTime;
    private int status;
    private String statusText;
    private boolean commented;//是否评价
    private String restaurantImg;//餐厅图片

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean isCommented() {
        return commented;
    }

    public void setCommented(boolean commented) {
        this.commented = commented;
    }

    public String getRestaurantImg() {
        return restaurantImg;
    }

    public void setRestaurantImg(String restaurantImg) {
        this.restaurantImg = restaurantImg;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

}
