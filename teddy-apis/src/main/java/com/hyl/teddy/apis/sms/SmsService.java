/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.sms;

import com.hyl.teddy.apis.SmsException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.MessageFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author Administrator
 */
@Service
public class SmsService {

    private final Logger logger = LoggerFactory.getLogger(SmsService.class);

    @Value("#{settings['sms.url']}")
    private String url;
    @Value("#{settings['sms.account']}")
    private String account;
    @Value("#{settings['sms.password']}")
    private String password;
    @Value("#{settings['sms.template']}")
    private String _template;
    @Value("#{settings['sms.debug']}")
    private boolean debug = true;

    private MessageFormat template;

    public void sendSmscode(String mobile, String code) throws SmsException, UnsupportedEncodingException, MalformedURLException {
        if (debug) {
            logger.debug("sms.debug=true 模式下不发送短信");
            return;
        }
        String message = getTemplate().format(new Object[]{code});
        logger.debug("发送短信：\n{}\n至 {}", message, mobile);

        String send_content = URLEncoder.encode(message.replaceAll("<br/>", " "), "GBK");//发送内容
        URL connection = new URL(url + "?CorpID=" + account + "&Pwd=" + password + "&Mobile=" + mobile + "&Content=" + send_content + "&Cell=&SendTime=");
        BufferedReader in = null;
        HttpURLConnection con = null;
        try {
            logger.debug("调用发短信接口： {}", connection);
            con = (HttpURLConnection) connection.openConnection();
            con.connect();
            boolean isError = con.getResponseCode() >= 400;
            InputStream is = isError ? con.getErrorStream() : con.getInputStream();
            in = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder();
            String t;
            while (null != (t = in.readLine())) {
                response.append(t).append("\n");
            }
            logger.debug("发送短信接口返回：\n{}", response);
            if (response.charAt(0) != '0') {
                throw new SmsException("短信验证码发送失败：" + response);
            }
        } catch (Exception ex) {
            throw new SmsException("发送短信时出现网络错误", ex);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ex) {
                    throw new IllegalStateException(ex);
                }
                if (con != null) {
                    con.disconnect();
                }
            }
        }
    }

    private MessageFormat getTemplate() {
        if (template == null) {
            template = new MessageFormat(_template);
        }
        return template;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setTemplate(String _template) {
        this._template = _template;
    }
}
