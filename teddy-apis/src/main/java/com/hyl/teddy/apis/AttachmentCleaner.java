/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis;

import java.io.File;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import javax.annotation.PostConstruct;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * 附件清洁工.
 *
 * @author Administrator
 */
@Service
public class AttachmentCleaner {

    private final Logger logger = LoggerFactory.getLogger(AttachmentCleaner.class);
    private final BlockingQueue<File> sharedQueue = new LinkedBlockingQueue<>();
    private final Thread cleaner = new Thread(new Runnable() {

        @Override
        public void run() {
            while (true) {
                try {
                    File file = sharedQueue.take();
                    delete(file);
                } catch (Exception ex) {
                    logger.warn("删除文件出错", ex);
                }
            }
        }
    });

    public void delete(File file) {
        if (file.isDirectory()) {
            FileUtils.deleteQuietly(file);
            logger.debug("删除目录：{}", file.getAbsoluteFile());
        } else {
            File directory = file.getParentFile();
            file.delete();
            logger.debug("删除文件：{}", file.getAbsoluteFile());

            if (directory.length() == 0) {
                directory.delete();
                logger.debug("删除空文件夹：{}", file.getAbsoluteFile());
            }
        }
    }

    @PostConstruct
    public void init() {
        cleaner.start();
    }

    /**
     * 放入需要删除的文件.
     *
     * @param file 需要删除的文件
     */
    public void put(File file) {
        try {
            sharedQueue.put(file);
        } catch (InterruptedException ex) {
            throw new IORuntimeException("加入待删除的文件失败：" + file.getAbsolutePath(), ex);
        }
    }

}
