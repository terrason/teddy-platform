/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.profile;

import com.hyl.teddy.apis.CommonRuntimeException;
import org.springframework.http.HttpStatus;

/**
 *
 * @author terrason
 */
public class InsufficientScoreException extends CommonRuntimeException {

    public InsufficientScoreException() {
        super(HttpStatus.BANDWIDTH_LIMIT_EXCEEDED, "validate.score.insufficient");
    }

}
