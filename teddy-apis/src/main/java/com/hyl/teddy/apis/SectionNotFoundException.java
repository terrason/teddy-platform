/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class SectionNotFoundException extends Exception {

    public SectionNotFoundException() {
    }

    public SectionNotFoundException(String string) {
        super(string);
    }

    public SectionNotFoundException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public SectionNotFoundException(Throwable thrwbl) {
        super(thrwbl);
    }

}
