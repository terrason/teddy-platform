/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis;

import javax.annotation.Resource;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 释放缓存接口.
 *
 * @author lip
 */
@Controller
public class CacheEvictionController {

    @Resource
    private CacheManager cacheManager;

    /**
     * 释放缓存.
     *
     * @param cache 缓存名称.
     * @param key 缓存键值.
     * @return 操作结果
     */
    @RequestMapping(value = "/cache/evict/{cache}")
    @ResponseBody
    public BaseResponse evicteCache(@PathVariable String cache, @RequestParam(required = false) String key) {
        BaseResponse response = new BaseResponse();
        Cache c = cacheManager.getCache(cache);
        if (c == null) {
            response.setCode(HttpStatus.NOT_FOUND.value());
            response.setMessage("当前不存在缓存：" + cache);
            return response;
        }
        if (StringUtils.hasText(key)) {
            try {
                int id = Integer.parseInt(key);
                c.evict(id);
            } catch (Exception ex) {
            }
            c.evict(key);
        } else {
            c.clear();
        }
        return response;
    }

}
