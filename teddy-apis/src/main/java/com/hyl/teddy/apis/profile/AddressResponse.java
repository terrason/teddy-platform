package com.hyl.teddy.apis.profile;

import com.hyl.teddy.apis.entity.CustAddress;
import org.springframework.format.annotation.NumberFormat;

public class AddressResponse {

    private int id;
    private String name;
    private boolean defaultAddress;
    private String mobile;
    private String location;
    @NumberFormat(pattern = "0.000000")
    private Double longitude;
    @NumberFormat(pattern = "0.000000")
    private Double latitude;

    public AddressResponse() {
    }

    /**
     * 构造函数：把CustAddress转化为AddressResponse
     *
     * @param address
     */
    public AddressResponse(CustAddress address) {
        this.id = address.getId();
        this.name = address.getName();
        this.defaultAddress = address.isDefaultAddress();
        this.mobile = address.getMobile();
        this.location = address.getLocation();
        this.longitude = 1.0 * address.getLongitudeValue() / 1000000;
        this.latitude = 1.0 * address.getLatitudeValue() / 1000000;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDefaultAddress() {
        return defaultAddress;
    }

    public void setDefaultAddress(boolean defaultAddress) {
        this.defaultAddress = defaultAddress;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

}
