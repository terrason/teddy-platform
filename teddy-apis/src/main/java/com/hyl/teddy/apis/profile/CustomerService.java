package com.hyl.teddy.apis.profile;

import com.hyl.teddy.apis.entity.Customer;
import com.hyl.teddy.apis.profile.dao.CustomerDao;
import com.hyl.teddy.apis.entity.CustAddress;
import com.hyl.teddy.apis.system.SystemService;

import java.util.*;

import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 客户Service
 *
 * @author Messi
 */
@Service
public class CustomerService {

    public static final String DEFAULT_ARVATAR = "/static/avatar/0.png";

    @Resource
    private CustomerDao dao;
    @Resource
    private SystemService systemService;
    @Resource
    private CacheManager cacheManager;

    private final Random random = new Random();

    /**
     * 根据手机号码获取客户信息.
     */
    public Customer getCustomer(String mobile) {
        Integer customerId = dao.selectCustomer(mobile);
        return customerId == null ? null : getCustomer(customerId);
    }
    /**
     * 释放指定ID的用户缓存.
     * @param customerId 用户ID
     */
    public void evictCache(int customerId) {
        Cache cache = cacheManager.getCache("customer");
        if (cache != null) {
            cache.evict(customerId);
        }
    }

    /**
     * 根据客户ID获取客户信息.
     */
    public Customer getCustomer(int principal) {
        Customer c = dao.selectCustomer(principal);
        if (c != null) {
            setCustomerLevel(c);
        }
        return c;
    }

    public boolean isVip(Customer customer) {
        setCustomerLevel(customer);
        return customer.getLevel() == 2;
    }

    public void setCustomerLevel(Customer customer) {
        if (StringUtils.isNotBlank(customer.getPassword())) {
            customer.setLevel(customer.getScoreHighest() < systemService.getVipExperience() ? 1 : 2);
        }
    }

    /**
     * 获取验证码
     *
     * @param token 验证码唯一票据
     * @return 验证码
     */
    @Cacheable(value = "smscode", key = "#token")
    public String getSmscode(String token) {
        return String.valueOf(random.nextInt(899999) + 100000);
    }

    public void saveCustomer(Customer customer) {
        if (customer.getId() > 0) {
            dao.updateCustomer(customer);
        } else {
            int id = dao.insertCustomer(customer);
            customer.setId(id);
        }
    }

    /**
     * 消耗客户代金券.
     *
     * @param customer 客户
     * @param cash 代金券面值
     * @param count 消耗代金券数量
     */
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void costCash(Customer customer, int cash, int count) {
        customer.decreaseCash(cash, count);
        dao.updateCustomerCash4Decrease(customer.getId(), cash, count);
    }

    /**
     * 客户赚取代金券
     *
     * @param customer 客户
     * @param cash 代金券面值
     * @param count 赚取代金券数量
     */
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void earnCash(Customer customer, int cash, int count) {
        customer.increaseCash(cash, count);
        dao.updateCustomerCash4Increase(customer.getId(), cash, count);
    }

    /**
     * 客户赚取积分。 会增加经验值
     *
     * @param customer 客户
     * @param score 积分
     */
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void earnScore(Customer customer, int score) {
        customer.increaseScore(score);
        customer.increaseExperience(score);
        dao.updateScore4Increase(customer.getId(), score);
    }

    /**
     * 回退客户积分. 不会增加经验值。
     *
     * @param customer 客户
     * @param score 积分
     */
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void rollbackScore(Customer customer, int score) {
        customer.increaseScore(score);
        dao.updateOnlyScore4Increase(customer.getId(), score);
    }

    /**
     * 客户消费积分
     *
     * @param customer 客户
     * @param score 积分
     */
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void costScore(Customer customer, int score) {
        customer.decreaseScore(score);
        dao.updateScore4Decrease(customer.getId(), score);
    }

    public CustAddress findDefaultAddress(int principal) {
        return dao.selectDefaultAddress(principal);
    }

    public List<MessageResponse> findCustomerMessages(int principal, int start, int size) {
        return dao.selectCustomerMessage(principal, start, size);
    }
}
