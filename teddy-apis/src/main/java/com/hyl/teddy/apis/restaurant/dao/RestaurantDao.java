package com.hyl.teddy.apis.restaurant.dao;

import com.hyl.teddy.apis.AbstractComponent;
import com.hyl.teddy.apis.entity.Dish;
import com.hyl.teddy.apis.entity.PaymentGoods;
import com.hyl.teddy.apis.entity.Restaurant;
import com.hyl.teddy.apis.promotion.Promotion;
import com.hyl.teddy.apis.restaurant.CommentResponse;
import com.hyl.teddy.apis.restaurant.thumbnails.RestaurantIcon;
import com.hyl.teddy.apis.restaurant.web.RestaurantListResponse;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.Resource;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * 餐厅DAO
 *
 * @author Administrator
 */
@Repository
public class RestaurantDao extends AbstractComponent {

    @Resource
    private JdbcTemplate jdbcTemplate;
    @Resource
    private RestaurantIcon restaurantIcon;

    @Cacheable(value = "restaurant", key = "#id", unless = "#result == null")
    public Restaurant selectRestaurant(int id) {
        List<Restaurant> list = jdbcTemplate.query("select id,`name`,icon,images,price,sold,package,delivery,promotion\n"
                + ",open_time,close_time,description,location,tel,star,hot_defined\n"
                + "from restaurant\n"
                + "where id=?", new RowMapper<Restaurant>() {

                    @Override
                    public Restaurant mapRow(ResultSet rs, int rowNum) throws SQLException {
                        Restaurant restaurant = new Restaurant();
                        restaurant.setId(rs.getInt("id"));
                        restaurant.setName(rs.getString("name"));
                        restaurant.setIcon(restaurantIcon.toThumbmailsMD(rs.getInt("icon")));
                        restaurant.setImageIds(rs.getString("images"));
                        restaurant.setImage(restaurantIcon.toThumbmailsMD(rs.getString("images")));
                        restaurant.setPrice(rs.getInt("price"));
                        restaurant.setSold(rs.getInt("sold"));
                        restaurant.setPackageFee(rs.getInt("package"));
                        restaurant.setDelivery(rs.getInt("delivery"));
                        restaurant.setPromotion(rs.getString("promotion"));
                        Time time = rs.getTime("open_time");
                        if (time != null) {
                            restaurant.setOpenTime(DateFormatUtils.format(time, "HH:mm"));
                        }
                        time = rs.getTime("close_time");
                        if (time != null) {
                            restaurant.setCloseTime(DateFormatUtils.format(time, "HH:mm"));
                        }
                        restaurant.setDescription(rs.getString("description"));
                        restaurant.setLocation(rs.getString("location"));
                        restaurant.setTel(rs.getString("tel"));
                        restaurant.setStar(rs.getInt("star"));
                        restaurant.setHotDefined(rs.getBoolean("hot_defined"));
                        return restaurant;
                    }
                }, id);
        if (list.isEmpty()) {
            return null;
        }
        Restaurant restaurant = list.get(0);
        //热门菜
        String hotDishSql = restaurant.isHotDefined()
                ? "select id,`name`,enabled from dish where restaurant_id=? and hot>0 order by hot limit 3"
                : "select id,`name`,enabled from dish where restaurant_id=? order by sold desc limit 3";
        List<Dish> dishes = jdbcTemplate.query(hotDishSql, new RowMapper<Dish>() {

            @Override
            public Dish mapRow(ResultSet rs, int rowNum) throws SQLException {
                Dish dish = new Dish();
                dish.setId(rs.getInt("id"));
                dish.setName(rs.getString("name"));
                dish.setEnabled(rs.getBoolean("enabled"));
                return dish;
            }
        }, id);
        restaurant.setHot(dishes);
        //评论
        List<CommentResponse> comments = jdbcTemplate.query("select c.id,c.nickname,p.star,p.comment,p.comment_time from payment p left join customer c on p.customer_id=c.id where p.restaurant_id=? and p.star>0 order by p.comment_time desc", new RowMapper<CommentResponse>() {

            @Override
            public CommentResponse mapRow(ResultSet rs, int i) throws SQLException {
                CommentResponse cr = new CommentResponse();
                cr.setId(rs.getInt("id"));
                cr.setNickname(rs.getString("nickname"));
                Timestamp timestamp = rs.getTimestamp("comment_time");
                if (timestamp != null) {
                    cr.setCreateTime(DateFormatUtils.format(timestamp, "yyyy-MM-dd"));
                }
                cr.setStar(rs.getInt("star"));
                cr.setComment(rs.getString("comment"));
                return cr;
            }
        }, id);
        restaurant.setComments(comments);
        return restaurant;
    }

    /**
     * 餐厅列表DAO
     *
     * @param category 类别
     * @param orderby 排序
     * @param start 起始页
     * @param size 页长
     * @return
     */
    public List<RestaurantListResponse> restaurantList(int category, int orderby, int start, int size) {
        String sql = "select r.id,r.`name`,r.icon,r.star,r.promotion,r.price,r.sold,group_concat(distinct rp.pattern) tag,"
                + "(r.enabled and (date_format(now(),'%H:%i') between r.open_time and r.close_time)) as enabled from restaurant r "
                + "left join restaurant_promotion rp on r.id=rp.restaurant_id left join rel_rest_category rrc on r.id= rrc.restaurant_id where 1 = 1 ";
        List<Object> param = new ArrayList<Object>();
        if (category > 0) {
            sql +="and rrc.category_id=? ";
            param.add(category);
        }
        sql += "and r.enabled = 1 ";
        param.add(start);
        param.add(size);
        sql += "group by r.id ";
        if (orderby == 1) {
            sql += "order by r.star desc ";
        } else if (orderby == 2) {
            sql += "order by r.price ";
        }
        sql += "limit ?,?";
        return jdbcTemplate.query(sql, new RowMapper<RestaurantListResponse>() {

            @Override
            public RestaurantListResponse mapRow(ResultSet rs, int i) throws SQLException {
                RestaurantListResponse rr = new RestaurantListResponse();
                rr.setId(rs.getInt("id"));
                rr.setName(rs.getString("name"));
                rr.setIcon(restaurantIcon.toThumbmailsMD(rs.getInt("icon")));
                rr.setStar(rs.getInt("star"));
                rr.setPromotion(rs.getString("promotion"));
                rr.setPrice(0.01 * rs.getDouble("price"));
                rr.setSold(rs.getInt("sold"));
                String tags = rs.getString("tag");
                if (tags == null) {
                    String[] tag = null;
                    rr.setTag(tag);
                } else {
                    String[] tag = tags.split(",");
                    rr.setTag(tag);
                }
                rr.setEnabled(rs.getBoolean("enabled"));
                return rr;
            }
        }, param.toArray());
    }

    /**
     * 全局搜索餐厅
     *
     * @param keyword 搜索关键字
     * @param start
     * @param size
     * @return
     */
    public List<RestaurantListResponse> searchRestaurantList(String keyword, int start, int size) {
        return jdbcTemplate.query("select r.id,r.`name`,r.icon,r.star,r.promotion,r.price,r.sold,group_concat(rp.pattern) tag,"
                + "(r.enabled and (date_format(now(),'%H:%i') between r.open_time and r.close_time)) as enabled from restaurant r "
                + "left join restaurant_promotion rp on r.id=rp.restaurant_id "
                + "where r.`name` like ? and r.enabled=1 "
                + "group by r.id "
                + "order by r.star desc limit ?,?", new RowMapper<RestaurantListResponse>() {

                    @Override
                    public RestaurantListResponse mapRow(ResultSet rs, int i) throws SQLException {
                        RestaurantListResponse rr = new RestaurantListResponse();
                        rr.setId(rs.getInt("id"));
                        rr.setName(rs.getString("name"));
                        rr.setIcon(restaurantIcon.toThumbmailsMD(rs.getInt("icon")));
                        rr.setStar(rs.getInt("star"));
                        rr.setPromotion(rs.getString("promotion"));
                        rr.setPrice(0.01 * rs.getDouble("price"));
                        rr.setSold(rs.getInt("sold"));
                        String tags = rs.getString("tag");
                        if (tags != null) {
                            String[] tag = tags.split(",");
                            rr.setTag(tag);
                        }
                        rr.setEnabled(rs.getBoolean("enabled"));
                        return rr;
                    }
                }, "%" + keyword + "%", start, size);
    }

    /**
     * 查询某店铺的优惠信息.
     *
     * @param restaurantId 店铺ID
     * @return 优惠信息
     */
    @Cacheable(value = "restaurant_promotions", key = "#restaurantId", unless = "#result.isEmpty()")
    public List<Promotion> selectPromotionByRestaurant(int restaurantId) {
        return jdbcTemplate.query("select rp.pattern,rp.param0,rp.param1,rp.param2,rp.param3 from restaurant_promotion rp left join restaurant r on rp.restaurant_id=r.id where r.id=?", new RowMapper<Promotion>() {

            @Override
            public Promotion mapRow(ResultSet rs, int i) throws SQLException {
                Promotion promotion = Promotion.valueOf(rs.getString("pattern"));
                promotion.setParams(new int[]{rs.getInt("param0"), rs.getInt("param1"), rs.getInt("param2"), rs.getInt("param3")});
                return promotion;
            }
        }, restaurantId);
    }

    public int updateDishesSalesVolume(final List<PaymentGoods> goodList) {
        final AtomicInteger volume = new AtomicInteger();
        jdbcTemplate.batchUpdate("update dish set sold = sold + ? where id = ?", new BatchPreparedStatementSetter() {

            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                PaymentGoods goodsItem = goodList.get(i);
                ps.setInt(1, goodsItem.getCount());
                ps.setInt(2, goodsItem.getDishId());
                volume.addAndGet(goodsItem.getCount());
            }

            @Override
            public int getBatchSize() {
                return goodList.size();
            }
        });
        return volume.get();
    }

    public void updateRestaurantSold(int restaurantId, int volume) {
        jdbcTemplate.update("update restaurant set sold = sold + ? where id = ?", volume, restaurantId);
    }

    public void updateStarCalculation(int restaurantId) {
        double avg = jdbcTemplate.queryForObject("select ifnull(avg(star),-1) from payment where  star>0 and restaurant_id=?", Double.class, restaurantId);
        if (avg >= 0) {
            jdbcTemplate.update("update restaurant set star=? where id=?", Math.round(avg), restaurantId);
        }
    }
}
