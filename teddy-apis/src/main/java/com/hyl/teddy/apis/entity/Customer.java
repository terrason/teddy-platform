package com.hyl.teddy.apis.entity;

import com.hyl.teddy.apis.profile.InsufficientCashException;
import com.hyl.teddy.apis.profile.InsufficientScoreException;
import java.util.HashMap;
import java.util.Map;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * 个人中心 用户信息表 customer
 *
 * @author Administrator
 *
 */
public class Customer {

    private int id;
    private String mobile;// 手机号码
    @JsonIgnore
    private String password;// 密码
    private String nickname;// 昵称
    @JsonIgnore
    private int scoreHighest;// 积分总计
    private int score;// 积分
    private String avatar;// 头像
    private String regTime;// 注册时间
    @JsonIgnore
    private String pushKey;// jpush机器号
    @JsonIgnore
    private boolean disabled;// 是否禁用 0 否 1 是
    private boolean maiden;//是否已有送餐地址
    private int level;//等级 0-初级会员 1-高级会员 2-VIP

    /**
     * 代金券. 以代金券面值为键，数量为值。
     */
    @JsonIgnore
    private final Map<Integer, Integer> cashes = new HashMap<>();

    /**
     * 减少积分.
     */
    public synchronized void decreaseScore(int score) {
        if (this.score < score) {
            throw new InsufficientScoreException();
        }
        this.score -= score;
    }

    /**
     * 增加积分. 不加经验值
     */
    public synchronized void increaseScore(int score) {
        this.score += score;
    }

    /**
     * 增加经验值
     */
    public synchronized void increaseExperience(int experience) {
        this.scoreHighest += experience;
    }

    public int getCashVolume(int cash) {
        Integer v = cashes.get(cash);
        return v == null ? 0 : v;
    }

    /**
     * 增加代金券.
     *
     * @param value 代金券面值
     * @param volume 数量
     */
    public void increaseCash(int value, int volume) {
        synchronized (cashes) {
            Integer v = cashes.get(value);
            if (v == null) {
                v = 0;
            }
            v += volume;
            cashes.put(value, v);
        }
    }

    /**
     * 减少代金券.
     *
     * @param value 代金券面值
     * @param volume 数量
     */
    public void decreaseCash(int value, int volume) {
        synchronized (cashes) {
            Integer v = cashes.get(value);
            if (v == null || v < volume) {
                throw new InsufficientCashException();
            }
            v -= volume;
            cashes.put(value, v);
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getScoreHighest() {
        return scoreHighest;
    }

    public void setScoreHighest(int scoreHighest) {
        this.scoreHighest = scoreHighest;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getRegTime() {
        return regTime;
    }

    public void setRegTime(String regTime) {
        this.regTime = regTime;
    }

    public String getPushKey() {
        return pushKey;
    }

    public void setPushKey(String pushKey) {
        this.pushKey = pushKey;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public boolean isMaiden() {
        return maiden;
    }

    public void setMaiden(boolean maiden) {
        this.maiden = maiden;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Map<Integer, Integer> getCashes() {
        return cashes;
    }

    @Override
    public Customer clone() throws CloneNotSupportedException {
        Customer customer = (Customer) super.clone();
        customer.id = id;
        customer.mobile = mobile;
        customer.password = password;
        customer.nickname = nickname;
        customer.scoreHighest = scoreHighest;
        customer.score = score;
        customer.avatar = avatar;
        customer.regTime = regTime;
        customer.pushKey = pushKey;
        customer.disabled = disabled;
        customer.maiden = maiden;
        customer.level = level;
        return customer;
    }
}
