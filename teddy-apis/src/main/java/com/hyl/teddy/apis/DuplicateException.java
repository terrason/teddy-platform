/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis;

/**
 *
 * @author Administrator
 */
public class DuplicateException extends Exception {

    private static final String prefix = "重复";

    public DuplicateException() {
        super(prefix);
    }

    public DuplicateException(String string) {
        super(prefix + string);
    }

    public DuplicateException(String string, Throwable thrwbl) {
        super(prefix + string, thrwbl);
    }

    public DuplicateException(Throwable thrwbl) {
        super(prefix, thrwbl);
    }

}
