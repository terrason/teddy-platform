package com.hyl.teddy.apis.profile;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.hyl.teddy.apis.profile.dao.FavoritesDao;
import com.hyl.teddy.apis.restaurant.web.RestaurantListResponse;
import java.util.Collections;
import java.util.Set;

@Service
public class FavoritesService {

    @Resource
    private FavoritesDao dao;

    public Set<Integer> getFavoriteRestaurantIds(int principal) {
        return dao.selectFavoriteRestaurentIds(principal);
    }

    public Set<Integer> getFavoriteDishIds(int principal) {
        return dao.selectFavoriteDishIds(principal);
    }

    public boolean isCustomerFavoredRestaurant(int principal, int restaurantId) {
        return getFavoriteRestaurantIds(principal).contains(restaurantId);
    }

    public boolean isCustomerFavoredDish(int principal, int dishId) {
        return getFavoriteDishIds(principal).contains(dishId);
    }

    public List<RestaurantListResponse> findRestaurantByCustId(int principal) {
        return dao.findRestaurantByCustId(principal);
    }

    public List<FavoritesDishResponse> findDisHByCustId(int principal) {
        return dao.findDisHByCustId(principal);
    }

    /**
     * 判断餐厅或者菜品是否收藏过
     */
    public boolean existFavorite(int customerId, int type, int targetId) {
        Set<Integer> favorites;
        if (type == 1) {
            favorites = getFavoriteRestaurantIds(customerId);
        } else if (type == 2) {
            favorites = getFavoriteDishIds(customerId);
        } else {
            favorites = Collections.EMPTY_SET;
        }
        return favorites.contains(targetId);
    }

    /**
     * 添加到收藏
     */
    public void addFavorite(int customerId, int type, int targertId) {
        dao.addFavorite(customerId, type, targertId);

    }

    /**
     * 取消收藏
     */
    public void removeFavorite(int customerId, int type, int targetId) {
        dao.removeFavorite(customerId, type, targetId);

    }
}
