package com.hyl.teddy.apis.profile.web;

import java.util.List;

import javax.annotation.Resource;
import javax.naming.AuthenticationException;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hyl.teddy.apis.BaseResponse;
import com.hyl.teddy.apis.CommonRuntimeException;
import com.hyl.teddy.apis.ResourceNotFoundException;
import com.hyl.teddy.apis.entity.CustAddress;
import com.hyl.teddy.apis.profile.DeliveryAddressResponse;
import com.hyl.teddy.apis.profile.AddressService;
import org.springframework.http.HttpStatus;

/**
 * teddy 个人中心 接口 送餐地址 新增送餐地址 修改送餐地址 删除送餐地址 设置默认送餐地址
 *
 * @author Administrator
 *
 */
@Controller
public class AddressController {

    @Resource
    private AddressService service;
    @Resource
    private MessageSource messageSource;

    /**
     * 7 送餐地址
     *
     * @param principal 当前用户id
     * @return
     * @throws AuthenticationException
     * @throws ResourceNotFoundException
     */
    @RequestMapping(value = "/profile/address", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse findAddress(@RequestParam(defaultValue = "0") int principal) throws AuthenticationException {
        if (principal == 0) {
            throw new AuthenticationException();
        }

        BaseResponse b = new BaseResponse();
        List<DeliveryAddressResponse> list = service.findAddress(principal);
        b.setData(list);

        return b;
    }

    /**
     * 8 新增送餐地址.
     *
     * @param principal 当前登陆用户id
     * @param name 姓名
     * @param mobile 手机号码
     * @param location 送餐地址
     * @param longitude 经度
     * @param latitude 纬度
     * @return
     * @throws AuthenticationException
     */
    @RequestMapping(value = "/profile/address", method = RequestMethod.POST)
    @ResponseBody
    public BaseResponse addAddress(@RequestParam(defaultValue = "0") int principal,
            @RequestParam String name,
            @RequestParam String mobile,
            @RequestParam String location,
            @RequestParam double longitude,
            @RequestParam double latitude) throws AuthenticationException {
        if (principal == 0) {
            throw new AuthenticationException();
        }
        BaseResponse b = new BaseResponse();

        CustAddress ad = new CustAddress();
        ad.setCustomerId(principal);
        ad.setName(name);
        ad.setMobile(mobile);
        ad.setLocation(location);
        int x = (int) (longitude * 1000000);
        int y = (int) (latitude * 1000000);
        ad.setLongitudeValue(x);
        ad.setLatitudeValue(y);
        //根据坐标 获得区域
        int regionId = service.findRegionId(x, y);
        if (regionId == 0) {
            throw new CommonRuntimeException(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "validate.payment.notSupportedArea");
        }
        ad.setRegionId(regionId);
        b.setData(service.addAddress(ad));
        return b;
    }

    /**
     * 9 修改送餐地址
     *
     * @param id 路径参数 送餐地址id
     * @param principal 当前登录用户id
     * @param name 姓名
     * @param mobile 手机号码
     * @param location 送餐地址
     * @param longitude 经度
     * @param latitude 纬度
     * @return
     * @throws AuthenticationException
     */
    @RequestMapping(value = "/profile/address/{id}", method = RequestMethod.POST)
    @ResponseBody
    public BaseResponse editAddress(@PathVariable int id,
            @RequestParam(defaultValue = "0") int principal,
            @RequestParam String name,
            @RequestParam String mobile,
            @RequestParam String location,
            @RequestParam double longitude,
            @RequestParam double latitude) throws AuthenticationException {
        BaseResponse b = new BaseResponse();
        if (principal == 0) {
            throw new AuthenticationException();
        }
        CustAddress ca = service.getAddress(id);
        if (ca == null) {
            b.setCode(HttpStatus.NOT_FOUND.value());
            b.setMessage(messageSource.getMessage("error.default", null, null));
            return b;
        }
        if (principal != ca.getCustomerId()) {
            b.setCode(HttpStatus.FORBIDDEN.value());
            b.setMessage(messageSource.getMessage("authorization.forbidden", null, null));
            return b;
        }

        ca.setName(name);
        ca.setMobile(mobile);
        ca.setLocation(location);
        int x = (int) (longitude * 1000000);
        int y = (int) (latitude * 1000000);
        ca.setLongitudeValue(x);
        ca.setLatitudeValue(y);
        int regionId = service.findRegionId(x, y);
        if (regionId == 0) {
            throw new CommonRuntimeException(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "validate.payment.notSupportedArea");
        }
        ca.setRegionId(regionId);
        service.updateAddress(ca);
        return b;
    }

    /**
     * 10 删除送餐地址
     *
     * @param id 路径参数 送餐地址id
     * @param principal 当前登录用户id
     * @return
     * @throws AuthenticationException
     */
    @RequestMapping(value = "/profile/address/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public BaseResponse removeAddress(@PathVariable int id, @RequestParam(defaultValue = "0") int principal) throws AuthenticationException {
        BaseResponse b = new BaseResponse();
        if (principal == 0) {
            throw new AuthenticationException();
        }
        CustAddress ca = service.getAddress(id);
        if (ca == null) {
            b.setCode(HttpStatus.NOT_FOUND.value());
            b.setMessage(messageSource.getMessage("error.default", null, null));
            return b;
        }
        if (principal != ca.getCustomerId()) {
            b.setCode(HttpStatus.FORBIDDEN.value());
            b.setMessage(messageSource.getMessage("authorization.forbidden", null, null));
            return b;
        }
        service.removeAddress(id);
        return b;
    }

    /**
     * 11 设置默认送餐地址(当前用户的其他地址设为非默认)
     *
     * @param principal 当前登陆用户id
     * @param id 默认送餐地址id
     * @return
     * @throws AuthenticationException
     */
    @RequestMapping(value = "/profile/address/default", method = RequestMethod.POST)
    @ResponseBody
    public BaseResponse putDefault(@RequestParam(defaultValue = "0") int principal, int id) throws AuthenticationException {
        if (principal == 0) {
            throw new AuthenticationException();
        }
        BaseResponse b = new BaseResponse();
        CustAddress ca = service.getAddress(id);
        if (ca == null) {
            b.setCode(HttpStatus.NOT_FOUND.value());
            b.setMessage(messageSource.getMessage("error.default", null, null));
            return b;
        }
        if (principal != ca.getCustomerId()) {
            b.setCode(HttpStatus.FORBIDDEN.value());
            b.setMessage(messageSource.getMessage("authorization.forbidden", null, null));
            return b;
        }
        service.putDefault(id, principal);
        return b;
    }
}
