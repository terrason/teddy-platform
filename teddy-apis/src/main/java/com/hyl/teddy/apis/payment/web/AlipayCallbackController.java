/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.payment.web;

import com.hyl.teddy.apis.AbstractComponent;
import com.hyl.teddy.apis.CommonRuntimeException;
import com.hyl.teddy.apis.entity.Payment;
import com.hyl.teddy.apis.payment.AlipayRefundRequest;
import com.hyl.teddy.apis.payment.AlipayService;
import com.hyl.teddy.apis.payment.PaymentService;
import com.hyl.teddy.apis.promotion.PromotionService;
import java.text.ParseException;
import java.util.Date;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author terrason
 */
@Controller
public class AlipayCallbackController extends AbstractComponent {

    @Resource
    private PaymentService paymentService;
    @Resource
    private PromotionService promotionService;
    @Resource
    private AlipayService alipayService;

    @RequestMapping("/alipayCallback")
    @ResponseBody
    public String alipayCallback(@RequestParam("out_trade_no") String orderno,
            @RequestParam("trade_no") String alipayno,
            @RequestParam("trade_status") String status,
            @RequestParam("total_fee") double fee) {
        logger.debug("支付宝返回结果...\n订单号：{}\n支付宝交易号：{}\n状态：{}", new Object[]{orderno, alipayno, status});
        //TODO 验证是否是支付宝发出的请求

        Payment payment = paymentService.getPayment(orderno);
        if (payment == null) {
            logger.warn("订单不存在！ orderno={}", orderno);
            return "success";
        }
        if ("WAIT_BUYER_PAY".equals(status)) {
            return "success";
        }
        if ("TRADE_CLOSED".equals(status)) {
            try {
                if (StringUtils.isNotBlank(payment.getCreateTime()) && new Date().after(DateUtils.addDays(DateUtils.parseDate(payment.getCreateTime(), "yyyy-MM-dd HH:mm:ss"), 1))) {
                    paymentService.firePaymentStatusChanged(payment, 9, "客户取消支付", null);
                }
            } catch (ParseException ex) {
                logger.warn("计算订单创建日期时出错", ex);
            }
            return "success";
        }
        if (payment.getStatus() < 2) {
            int cost = (int) Math.round(fee * 100);
            if (StringUtils.isBlank(payment.getAlipayNo())) {
                payment.setAlipayNo(alipayno);
            } else {
                payment.setAlipayNo(payment.getAlipayNo() + "|" + alipayno);
            }
            alipayService.savePaid(orderno, alipayno, fee);
            paymentService.firePaymentStatusChanged(payment, 2, null, payment.getPaid() + cost);
            return "success";
        }

        return "unknown";
    }

    private AlipayValidResult validateRefundResponse(String id, int successNum, String resultDetails) throws CommonRuntimeException {
        logger.debug("支付宝退款返回结果...\n退款批次号：{}\n处理结果：{}", id, resultDetails);
        //TODO 验证是否是支付宝发出的请求
        alipayService.createRefundResponse(id, successNum, resultDetails);

        String backFee = resultDetails.split("[$]")[0];
        String[] backFeeArray = backFee.split("\\^");
        if (backFeeArray.length != 3) {// this dose not happen! maybe ...
            logger.error("支付宝返回无法解析的数据：{}", backFee);
            throw new CommonRuntimeException(HttpStatus.BAD_REQUEST);
        }
        String alipayno = backFeeArray[0];
        double fee = -1;
        try {
            fee = Double.parseDouble(backFeeArray[1]);
        } catch (NumberFormatException ex) {
            logger.error("解析退款金额失败！支付宝返回无法解析的数据：{}", backFee);
            throw new CommonRuntimeException(HttpStatus.BAD_REQUEST);
        }
        String message = backFeeArray[2];
        if (StringUtils.isBlank(message)) {
            logger.error("解析退款状态失败！支付宝返回无法解析的数据：{}", backFee);
            throw new CommonRuntimeException(HttpStatus.BAD_REQUEST);
        }
        AlipayValidResult result = new AlipayValidResult(fee, message, alipayno);
        boolean successful = "SUCCESS".equals(message);
        if (!successful) {
            try {
                logger.warn("支付宝退款失败：{}", messageSource.getMessage("alipay.error." + message, null, null));
            } catch (NoSuchMessageException ex) {
                logger.warn("支付宝退款失败：{}", message);
            }
        }
        AlipayRefundRequest entity = alipayService.getRefundRequest(id);
        if (entity == null) {
            logger.error("未找到支付宝请求！batch_no={}", id);
            return result;
        }
        alipayService.updateRefundStatus(entity, successful);
        entity.setStatus(successful ? 1 : 0);

        String orderno = entity.getOrderno();
        Payment payment = paymentService.getPayment(orderno);
        if (payment == null) {
            logger.error("订单不存在！ orderno={}", orderno);
            return result;
        }
        result.setPayment(payment);
        return result;
    }

    @RequestMapping("/alipayCallback/cancel")
    @ResponseBody
    public String alipayCallback4Cancel(@RequestParam("batch_no") String id, @RequestParam("success_num") int successNum, @RequestParam("result_details") String resultDetails) {
        try {
            AlipayValidResult result = validateRefundResponse(id, successNum, resultDetails);
            Payment payment = result.getPayment();
            if (payment != null && payment.getStatus() != 9) {
                int paid = payment.getPaid() - (int) (result.getFee() * 100);
                payment.setPaid(paid);
                paymentService.cancel(result.getPayment());
            }
            return "success";
        } catch (CommonRuntimeException ex) {
            return "error";
        }
    }

    @RequestMapping("/alipayCallback/giveup")
    @ResponseBody
    public String alipayCallback4Giveup(@RequestParam("batch_no") String id, @RequestParam("success_num") int successNum, @RequestParam("result_details") String resultDetails) {
        try {
            AlipayValidResult result = validateRefundResponse(id, successNum, resultDetails);
            Payment payment = result.getPayment();
            if (payment != null && payment.getStatus() == 4) {
                int paid = payment.getPaid() - (int) (result.getFee() * 100);
                payment.setPaid(paid);
                paymentService.giveup(result.getPayment());
            }
            return "success";
        } catch (CommonRuntimeException ex) {
            return "error";
        }
    }

    @RequestMapping("/alipayCallback/exchange")
    @ResponseBody
    public String alipayCallback4Exchange(@RequestParam("batch_no") String id, @RequestParam("success_num") int successNum, @RequestParam("result_details") String resultDetails) {
        try {
            AlipayValidResult result = validateRefundResponse(id, successNum, resultDetails);
            Payment payment = result.getPayment();
            logger.debug("status={},editable={}", payment.getStatus(), payment.isEditable());
            if (payment != null && payment.getStatus() == 4 && !payment.isEditable()) {
                paymentService.exchange(payment, result.getFee());
            }
            return "success";
        } catch (CommonRuntimeException ex) {
            return "error";
        }
    }

    private class AlipayValidResult {

        private Payment payment;
        private String alipayno;
        private double fee;
        private String message;

        public AlipayValidResult(double fee, String message, String alipayno) {
            this.fee = fee;
            this.message = message;
            this.alipayno = alipayno;
        }

        public String getAlipayno() {
            return alipayno;
        }

        public void setAlipayno(String alipayno) {
            this.alipayno = alipayno;
        }

        public boolean isSuccess() {
            return "SUCCESS".equals(message);
        }

        public Payment getPayment() {
            return payment;
        }

        public void setPayment(Payment payment) {
            this.payment = payment;
        }

        public double getFee() {
            return fee;
        }

        public void setFee(double fee) {
            this.fee = fee;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
