/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.payment.listener;

import com.hyl.teddy.apis.entity.Payment;
import com.hyl.teddy.apis.payment.PaymentListenerAdaptor;
import com.hyl.teddy.apis.payment.PaymentStatusChangeEvent;
import org.springframework.stereotype.Service;

/**
 *
 * @author terrason
 */
@Service
public class PaymentEditableListener extends PaymentListenerAdaptor {

    @Override
    public void fireStatusChanged(PaymentStatusChangeEvent event) {
        Payment payment = event.getPayment();
        int status = event.getPayment().getStatus();
        if (payment.isEditable() && (status == 2 || status == 9)) {
            logger.debug("监听到订单状态改变为{}，回收订单可编辑权限...", status);
            payment.setEditable(false);
            event.getPaymentService().setEditable(payment.getId(), false);
            logger.debug("回收订单可编辑权限结束============>");
        }
    }

    @Override
    public int getPriority() {
        return 88888;
    }

}
