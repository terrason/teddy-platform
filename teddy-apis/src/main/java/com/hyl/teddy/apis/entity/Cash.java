package com.hyl.teddy.apis.entity;
/**
 *  代金券 cash
 * @author Administrator
 *
 */
public class Cash {
	private int id;
	private int value;//面值
	private String name;//名称
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
