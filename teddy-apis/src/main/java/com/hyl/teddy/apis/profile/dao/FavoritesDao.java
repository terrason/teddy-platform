package com.hyl.teddy.apis.profile.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.hyl.teddy.apis.profile.FavoritesDishResponse;
import com.hyl.teddy.apis.restaurant.thumbnails.DishImg;
import com.hyl.teddy.apis.restaurant.thumbnails.RestaurantIcon;
import com.hyl.teddy.apis.restaurant.web.RestaurantListResponse;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.jdbc.core.RowCallbackHandler;

@Repository
public class FavoritesDao {

    @Resource
    private JdbcTemplate jdbc;
    @Resource
    private RestaurantIcon restaurantIcon;
    @Resource
    private DishImg dishImg;

    /**
     * 查询用户收藏的餐厅
     */
    public List<RestaurantListResponse> findRestaurantByCustId(int principal) {
        String sql = "select rest.* ,group_concat(distinct rp.pattern) as tag,\n"
                + "(rest.enabled and (date_format(now(),'%H:%i') between rest.open_time and rest.close_time)) as enabledRestaurant\n"
                + "from  cust_favoraties f\n"
                + "left join restaurant rest on f.target_id = rest.id\n"
                + "left join restaurant_promotion rp on rest.id= rp.restaurant_id\n"
                + "where f.customer_id = ? and f.type = 1 group by rest.id";
        return jdbc.query(sql, new RowMapper<RestaurantListResponse>() {

            @Override
            public RestaurantListResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
                RestaurantListResponse r = new RestaurantListResponse();
                r.setId(rs.getInt("id"));
                r.setName(rs.getString("name"));
                r.setIcon(restaurantIcon.toThumbmailsMD(rs.getInt("icon")));
                r.setStar(rs.getInt("star"));
                r.setPromotion(rs.getString("promotion"));
                r.setPrice(rs.getInt("price"));
                r.setSold(rs.getInt("sold"));
                String tags = rs.getString("tag");
                if (tags != null) {
                    r.setTag(tags.split(","));
                } else {
                    r.setTag(ArrayUtils.EMPTY_STRING_ARRAY);
                }
                r.setEnabled(rs.getBoolean("enabledRestaurant"));
                return r;
            }

        }, principal);
    }

    /**
     * 查询用户收藏的菜品
     */
    public List<FavoritesDishResponse> findDisHByCustId(int principal) {
        String sql = "select dish.id,dish.`name`,dish.image,dish.price\n"
                + ",dish.restaurant_id,r.`name` restName\n"
                + ",(dish.enabled and r.enabled and (date_format(now(),'%H:%i') between r.open_time and r.close_time)) as enabled\n"
                + "from cust_favoraties f\n"
                + "left join dish on f.target_id=dish.id\n"
                + "left join restaurant r on dish.restaurant_id=r.id\n"
                + "where f.customer_id=? and f.`type`=2";
        return jdbc.query(sql, new RowMapper<FavoritesDishResponse>() {
            @Override
            public FavoritesDishResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
                FavoritesDishResponse d = new FavoritesDishResponse();
                d.setId(rs.getInt("id"));
                d.setRestaurantId(rs.getInt("restaurant_id"));
                d.setRestaurantName(rs.getString("restName"));
                d.setName(rs.getString("name"));
                d.setIcon(dishImg.toThumbmailsMD(rs.getInt("image")));
                d.setPrice(0.01 * rs.getInt("price"));
                d.setEnabled(rs.getBoolean("enabled"));
                return d;
            }
        }, principal);
    }

    /**
     * 收藏餐厅或者菜品
     *
     * @param customerId 用户id
     * @param type 1 餐厅 2菜品
     * @param targertId
     */
    @Caching(evict = {
        @CacheEvict(value = "customer_favorates_restaurents", condition = "#type==1", key = "#customerId"),
        @CacheEvict(value = "customer_favorate_dishes", condition = "#type==2", key = "#customerId")
    })
    public void addFavorite(int customerId, int type, int targertId) {
        String sql = " INSERT INTO cust_favoraties(customer_id, TYPE, target_id) VALUES (?, ?, ?) ";
        jdbc.update(sql, customerId, type, targertId);

    }

    /**
     * 取消餐厅或者菜品收藏
     *
     * @param customerId 用户id
     * @param type 1 餐厅 2 菜品
     * @param targetId
     */
    @Caching(evict = {
        @CacheEvict(value = "customer_favorates_restaurents", condition = "#type==1", key = "#customerId"),
        @CacheEvict(value = "customer_favorate_dishes", condition = "#type==2", key = "#customerId")
    })
    public void removeFavorite(int customerId, int type, int targetId) {
        String sql = " DELETE FROM cust_favoraties WHERE  customer_id = ? AND TYPE = ? AND target_id = ? ";
        jdbc.update(sql, customerId, type, targetId);

    }

    @Cacheable(value = "customer_favorates_restaurents", key = "#principal", unless = "#result == null")
    public Set<Integer> selectFavoriteRestaurentIds(int principal) {
        final Set<Integer> ids = new HashSet<>();
        jdbc.query("select target_id from cust_favoraties where customer_id=? and type=1", new RowCallbackHandler() {

            @Override
            public void processRow(ResultSet rs) throws SQLException {
                ids.add(rs.getInt(1));
            }

        }, principal);
        return ids;
    }

    @Cacheable(value = "customer_favorate_dishes", key = "#principal", unless = "#result == null")
    public Set<Integer> selectFavoriteDishIds(int principal) {
        final Set<Integer> ids = new HashSet<>();
        jdbc.query("select target_id from cust_favoraties where customer_id=? and type=2", new RowCallbackHandler() {

            @Override
            public void processRow(ResultSet rs) throws SQLException {
                ids.add(rs.getInt(1));
            }

        }, principal);
        return ids;
    }
}
