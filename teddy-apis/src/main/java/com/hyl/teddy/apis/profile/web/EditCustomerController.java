package com.hyl.teddy.apis.profile.web;

import javax.annotation.Resource;
import javax.naming.AuthenticationException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.hyl.teddy.apis.AbstractComponent;
import com.hyl.teddy.apis.BaseResponse;
import com.hyl.teddy.apis.attachment.AttachmentService;
import com.hyl.teddy.apis.attachment.UpfileFailedException;
import com.hyl.teddy.apis.entity.Customer;
import com.hyl.teddy.apis.profile.CustomerService;
import com.hyl.teddy.apis.profile.thumbnails.Avatar;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

/**
 * teddy个人中心 接口 修改密码 修改头像 修改昵称
 *
 * @author Administrator
 *
 */
@Controller
public class EditCustomerController extends AbstractComponent {

    @Resource
    private CustomerService customerService;
    @Resource
    private AttachmentService attachmentService;

    @Resource
    private Avatar avator;

    /**
     * 4 修改密码
     *
     * @param principal 当前用户id
     * @param oldPassword 原密码
     * @param newPassword 新密码
     * @return
     * @throws AuthenticationException
     */
    @RequestMapping(value = "/profile/passwd", method = RequestMethod.POST)
    @ResponseBody
    public BaseResponse editPassword(@RequestParam(defaultValue = "0") int principal,
            @RequestParam(required = false) String pwdold,
            @RequestParam String pwdnew) throws AuthenticationException {
        if (principal == 0) {
            throw new AuthenticationException();
        }
        Customer customer = customerService.getCustomer(principal);
        if (customer == null) {
            throw new AuthenticationException();
        }
        BaseResponse response = new BaseResponse();
        if (customer.isDisabled()) {
            response.setCode(HttpStatus.FORBIDDEN.value());
            response.setMessage(messageSource.getMessage("authentication.failed.disabled", null, null));
            return response;
        }
        if (StringUtils.isNotBlank(pwdold) && !pwdold.equals(customer.getPassword())) {
            response.setCode(HttpStatus.FAILED_DEPENDENCY.value());
            response.setMessage(messageSource.getMessage("authentication.failed.password", null, null));
            return response;
        }
        customer.setPassword(pwdnew);
        customerService.saveCustomer(customer);
        return response;
    }

    /**
     * 5 修改头像
     *
     * @param principal 当前登陆用户id
     * @param avatar 头像
     * @return
     * @throws AuthenticationException
     * @throws UpfileFailedException
     */
    @RequestMapping(value = "/profile/avatar", headers = "content-type=multipart/*", method = RequestMethod.POST)
    @ResponseBody
    public BaseResponse updateAvatar(@RequestParam(defaultValue = "0") int principal, @RequestParam MultipartFile avatar) throws AuthenticationException, UpfileFailedException {
        if (principal == 0) {
            throw new AuthenticationException();
        }
        Customer customer = customerService.getCustomer(principal);
        if (customer == null) {
            throw new AuthenticationException();
        }
        BaseResponse b = new BaseResponse();
        String avatarPath = "/cust/" + principal + "/avatar";
        attachmentService.removeAttachment(avatarPath, true);
        String imgPath = attachmentService.upfile(avatar, avatarPath);
        customer.setAvatar(imgPath);
        customerService.saveCustomer(customer);
        b.setData(avator.toThumbmailsMD(imgPath));
        return b;
    }

    /**
     * 6 修改昵称
     *
     * @param principal 当前用户id
     * @param nickname 昵称
     * @return
     * @throws AuthenticationException
     */
    @RequestMapping(value = "/profile/nickname", method = RequestMethod.POST)
    @ResponseBody
    public BaseResponse updateNickname(@RequestParam(defaultValue = "0") int principal, @RequestParam String nickname) throws AuthenticationException {
        if (principal == 0) {
            throw new AuthenticationException();
        }
        Customer customer = customerService.getCustomer(principal);
        if (customer == null) {
            throw new AuthenticationException();
        }
        customer.setNickname(nickname);
        customerService.saveCustomer(customer);
        return new BaseResponse();
    }

}
