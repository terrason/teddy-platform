package com.hyl.teddy.apis.restaurant.web;

import java.util.Collection;

/**
 * 餐厅菜品
 *
 * @author Administrator
 */
public class RestaurantDishResponse {

    /**
     * 餐厅ID
     */
    private int id;
    /**
     * 餐厅名
     */
    private String name;
    /**
     * 促销信息
     */
    private String notice;
    /**
     * 菜品分类
     */
    private Collection<DishCategoryResponse> category;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public Collection<DishCategoryResponse> getCategory() {
        return category;
    }

    public void setCategory(Collection<DishCategoryResponse> category) {
        this.category = category;
    }
}
