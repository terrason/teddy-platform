/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.asynchronous.operation;

import com.hyl.teddy.apis.CommonRuntimeException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 * 异步操作.
 *
 * @author Administrator
 */
@Service
public class AsyncOperation extends Thread {

    private final Logger logger = LoggerFactory.getLogger(AsyncOperation.class);
    private final BlockingQueue<Runnable> sharedQueue = new LinkedBlockingQueue<>();

    @Override
    public void run() {
        while (true) {
            try {
                Runnable runnable = sharedQueue.take();
                runnable.run();
            } catch (Exception ex) {
                logger.error("忽略异步操作错误:" + ex.getMessage(), ex);
            }
        }
    }

    public void put(Runnable runnable) {
        try {
            sharedQueue.put(runnable);
            notifyNewPut();
        } catch (InterruptedException ex) {
            throw new CommonRuntimeException(HttpStatus.EXPECTATION_FAILED, "加入异步操作出错", ex);
        }
    }

    private void notifyNewPut() {
        if (!isAlive()) {
            setDaemon(true);
            start();
        }
    }
}
