package com.hyl.teddy.apis.restaurant;

import com.hyl.teddy.apis.entity.RestaurantCategory;
import com.hyl.teddy.apis.restaurant.dao.CategoryDao;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * 餐厅分类列表Service
 *
 * @author Administrator
 */
@Service
public class CategoryService {

    @Resource
    private CategoryDao dao;

    public List<RestaurantCategory> categoryList() {
        return dao.categoryList();
    }
}
