/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.promotion;

/**
 *
 * @author terrason
 */
public interface PromotionDiscountHandler {

    /**
     * 优惠了多少钱.
     *
     * @param money 优惠费用 单位：分
     */
    public void setMoney(int money);
}
