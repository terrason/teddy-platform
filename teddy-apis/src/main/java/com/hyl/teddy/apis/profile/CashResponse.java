package com.hyl.teddy.apis.profile;

public class CashResponse {

    private int id;//代金券类型
    private int value;//面值
    private String name;//名称
    private int quantity;//代金券数量

    public CashResponse() {
    }

    public CashResponse(int cash, int volume) {
        id = cash;
        value = cash;
        name = cash + " 元代金券";
        quantity = volume;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

}
