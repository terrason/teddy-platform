package com.hyl.teddy.apis.restaurant.web;

import com.hyl.teddy.apis.BaseResponse;
import com.hyl.teddy.apis.entity.Dish;
import com.hyl.teddy.apis.restaurant.DishService;
import com.hyl.teddy.apis.restaurant.SearchDish;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 餐厅菜品
 * @author Administrator
 */
@Controller
@RequestMapping
public class DishController {
    @Resource
    private DishService dishService;
    /**
     * 
     * @param restaurant
     * @return 
     */
    @RequestMapping(value = "/dish",method=RequestMethod.GET)
    @ResponseBody
    public BaseResponse queryRestaurantDish(@RequestParam int restaurant){
        BaseResponse response = new BaseResponse();
        RestaurantDishResponse dr = dishService.queryRestaurantDish(restaurant);
        response.setData(dr);
        return response;
    }
    /**
     * 菜品全局搜索
     * @param keyword
     * @return 
     */
    @RequestMapping(value = "/search/dish",method=RequestMethod.GET)
    @ResponseBody
    public BaseResponse searchDish(@RequestParam String keyword){
        BaseResponse response = new BaseResponse();
        List<SearchDish> d = dishService.searchDish(keyword);
        response.setData(d);
        return response;
    }
    /**
     * 菜品餐厅搜索
     * @param restaurant
     * @param keyword
     * @return 
     */
    @RequestMapping(value="/dish/search",method=RequestMethod.GET)
    @ResponseBody
    public BaseResponse dishSearch(@RequestParam int restaurant,@RequestParam String keyword){
        BaseResponse response = new BaseResponse();
        List<Dish> d = dishService.dishSearch(restaurant,keyword);
        response.setData(d);
        return response;
    }
    /**
     * 菜品详情
     * @param id
     * @param principal
     * @return 
     */
    @RequestMapping(value="/dish/{id}",method=RequestMethod.GET)
    @ResponseBody
    public BaseResponse queryDish(@PathVariable int id,@RequestParam(defaultValue = "0")int principal){
        BaseResponse response = new BaseResponse();
        DishResponse dr = dishService.queryDish(id, principal);
        response.setData(dr);
        return response;
    }
}
