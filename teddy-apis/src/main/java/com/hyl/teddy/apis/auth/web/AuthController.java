package com.hyl.teddy.apis.auth.web;

import com.hyl.teddy.apis.AbstractComponent;
import com.hyl.teddy.apis.BaseResponse;
import com.hyl.teddy.apis.entity.Customer;
import com.hyl.teddy.apis.profile.CustomerService;
import com.hyl.teddy.apis.SmsException;
import com.hyl.teddy.apis.attachment.AttachmentService;
import com.hyl.teddy.apis.sms.SmsService;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 餐厅
 *
 * @author Administrator
 */
@Controller
public class AuthController extends AbstractComponent {

    @Resource
    private SmsService smss;
    @Resource
    private CustomerService cs;
    @Resource
    private AttachmentService attachmentService;

    /**
     * 用户注册
     *
     * @param mobile
     * @param password
     * @param smscode
     * @param token
     * @return
     */
    @RequestMapping(value = "/auth/register", method = RequestMethod.POST)
    @ResponseBody
    public BaseResponse customerRegister(@RequestParam String mobile,
            @RequestParam String password,
            @RequestParam String smscode,
            @RequestParam String token,
            @RequestParam(defaultValue = "") String pushKey) {
        BaseResponse response = new BaseResponse();
        Customer cust = cs.getCustomer(mobile);
        if (cust != null) {
            response.setCode(409);
            response.setMessage(messageSource.getMessage("error.signup.authorized", null, null));
            return response;
        }
        String realSmscode = cs.getSmscode(token);
        if (!smscode.equals(realSmscode)) {
            response.setCode(424);
            response.setMessage(messageSource.getMessage("validate.captcha.failed", null, null));
            return response;
        }
        Customer customer = regist(mobile, password, pushKey);
        login(customer, pushKey);

        response.setData(customer);
        return response;
    }

    private Customer regist(String mobile, String password, String pushKey) {
        Customer cust = new Customer();
        cust.setMobile(mobile);
        cust.setPassword(password);
        cust.setAvatar(CustomerService.DEFAULT_ARVATAR);
        cust.setNickname(StringUtils.overlay(mobile, "****", 3, 7));
        cust.setDisabled(false);
        cust.setScore(0);
        cust.setScoreHighest(0);
        cust.setPushKey(pushKey);
        cust.setMaiden(true);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String regTime = sdf.format(new Date());
        cust.setRegTime(regTime);
        cs.saveCustomer(cust);

        cs.setCustomerLevel(cust);
        return cust;
    }

    /**
     * 用户登录
     *
     * @param mobile
     * @param password
     * @param smscode
     * @param token
     * @param pushKey
     * @return
     */
    @RequestMapping(value = "/auth/login", method = RequestMethod.POST, params = "password")
    @ResponseBody
    public BaseResponse restaurantList(@RequestParam String mobile,
            @RequestParam String password,
            @RequestParam(defaultValue = "") String pushKey) {
        BaseResponse response = new BaseResponse();
        Customer cust = cs.getCustomer(mobile);
        if (cust == null) {
            // 密码登录时手机号码不存在
            response.setCode(HttpStatus.NOT_FOUND.value());
            response.setMessage(messageSource.getMessage("authentication.failed.username", null, null));
            return response;
        }
        if (!password.equals(cust.getPassword())) {
            response.setCode(HttpStatus.FAILED_DEPENDENCY.value());
            response.setMessage(messageSource.getMessage("authentication.failed.password", null, null));
            return response;
        }
        if (cust.isDisabled()) {
            // 用户禁用
            response.setCode(HttpStatus.FORBIDDEN.value());
            response.setMessage(messageSource.getMessage("authentication.failed.disabled", null, null));
            response.setData(cust);
            return response;
        }
        // 登陆成功
        login(cust, pushKey);
        response.setData(cust);
        return response;

    }

    @RequestMapping(value = "/auth/login", method = RequestMethod.POST, params = "!password")
    @ResponseBody
    public BaseResponse restaurantList(@RequestParam String mobile,
            @RequestParam String smscode,
            @RequestParam String token,
            @RequestParam(defaultValue = "") String pushKey) {
        BaseResponse response = new BaseResponse();

        String realSmscode = cs.getSmscode(token);
        if (!smscode.equals(realSmscode)) {
            response.setCode(424);
            response.setMessage(messageSource.getMessage("validate.captcha.failed", null, null));
            return response;
        }
        
        Customer cust = cs.getCustomer(mobile);
        if (cust == null) {//手机号码未注册过，自动注册为新会员.
            cust = regist(mobile, StringUtils.EMPTY, pushKey);
            login(cust, pushKey);
            response.setData(cust);
            return response;
        }
        // 用户禁用
        if (cust.isDisabled()) {
            // 用户禁用
            response.setCode(403);
            response.setMessage(messageSource.getMessage("authentication.failed.disabled", null, null));
            response.setData(cust);
            return response;
        }
        // 登陆成功
        login(cust, pushKey);
        response.setData(cust);

        return response;

    }

    private void login(Customer customer, String pushKey) {
        if (StringUtils.isNotBlank(pushKey) && !pushKey.equals(customer.getPushKey())) {
            customer.setPushKey(pushKey);
            cs.saveCustomer(customer);
        }

        customer.setAvatar(attachmentService.toResourceUrl(customer.getAvatar()));
    }

    /**
     * 手机获取验证码
     *
     * @param mobile
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/auth/smscode", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse getValidateCode(@RequestParam String mobile) throws IOException {
        BaseResponse response = new BaseResponse();

        String token = UUID.randomUUID().toString();
        String smscode = cs.getSmscode(token);
        try {
            smss.sendSmscode(mobile, smscode);
        } catch (SmsException ex) {
            response.setCode(HttpStatus.BAD_GATEWAY.value());
            response.setMessage(messageSource.getMessage("error.sms", null, null));
            logger.warn("用户注册短信验证码发送失败", ex);
        }
        response.setMessage(smscode);
        response.setData(token);
        return response;
    }

    /**
     * 忘记密码
     *
     * @param mobile
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/auth/passwd", method = RequestMethod.POST)
    @ResponseBody
    public BaseResponse ForgetPwd(HttpServletRequest request,
            @RequestParam String mobile,
            @RequestParam String password,
            @RequestParam String smscode,
            @RequestParam String token,
            @RequestParam(defaultValue = "") String pushKey)
            throws IOException {
        BaseResponse response = new BaseResponse();
        Customer cust = cs.getCustomer(mobile);
        if (cust == null) {
            response.setCode(404);
            response.setMessage(messageSource.getMessage("authentication.failed.username", null, null));
            response.setData(null);
            return response;
        }
        if (cust.isDisabled()) {
            response.setCode(403);
            response.setMessage(messageSource.getMessage("authentication.failed.disabled", null, null));
            response.setData(null);
            return response;
        }
        // 手机验证码验证处理
        String smscode1 = cs.getSmscode(token);
        if (smscode1 == null || !smscode1.equals(smscode)) {
            response.setCode(424);
            response.setMessage(messageSource.getMessage("validate.captcha.failed", null, null));
            return response;
        }

        cust.setPassword(password);
        cs.saveCustomer(cust);

        login(cust, pushKey);
        response.setData(cust);
        return response;
    }
}
