/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.jpush.web;

import com.hyl.teddy.apis.jpush.CourierJpushService;
import com.hyl.teddy.apis.jpush.CustomerJpushService;
import com.hyl.teddy.apis.jpush.DeliverJpushService;
import com.hyl.teddy.apis.jpush.JpushService;
import com.hyl.teddy.apis.jpush.PushClient;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author terrason
 */
@Controller
public class JpushContorller {

    @Resource
    private CustomerJpushService customerJpushService;
    @Resource
    private CourierJpushService courierJpushService;
    @Resource
    private DeliverJpushService deliverJpushService;

    @RequestMapping("/jpush/test")
    @ResponseBody
    public boolean push(@RequestParam("pushKey") String pushKey,
            @RequestParam("type") int type,
            @RequestParam("module") int module,
            @RequestParam(value = "orderno", required = false) String orderno) {
        JpushService jpushService = null;
        switch (type) {
            case 1:
                jpushService = customerJpushService;
                break;
            case 2:
                jpushService = deliverJpushService;
                break;
            case 3:
                jpushService = courierJpushService;
                break;
        }
        if (jpushService == null) {
            throw new IllegalArgumentException("不支持的推送");
        }
        PushClient client = jpushService.create("测试推送");
        client.data("module", module);
        switch (module) {
            case 0:
                client.data("url", "http://teddy.hylapp.com:10001/apis");
                break;
            default:
                client.data("orderno", "orderno");
        }
        return client.to(pushKey).push();
    }
}
