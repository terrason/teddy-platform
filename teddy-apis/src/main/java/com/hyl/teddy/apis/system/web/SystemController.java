package com.hyl.teddy.apis.system.web;

import com.hyl.teddy.apis.BaseResponse;
import com.hyl.teddy.apis.employee.EmployeeService;
import com.hyl.teddy.apis.employee.Version;
import com.hyl.teddy.apis.system.SystemResponse;
import com.hyl.teddy.apis.system.SystemService;

import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 餐厅
 *
 * @author Administrator
 */
@Controller
public class SystemController {

    @Resource
    private SystemService ss;
    @Resource
    private EmployeeService employeeService;

    /**
     * 启动整合接口
     *
     * @param code
     * @return
     */
    @RequestMapping(value = "/system", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse home() {
        BaseResponse response = new BaseResponse();
        SystemResponse sysResponse = ss.queryHomeResponse();
        response.setData(sysResponse);
        return response;
    }

    /**
     * 版本更新
     *
     * @param code
     * @return
     */
    @RequestMapping(value = "/system/upgrade", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse upgradeCustomer(@RequestParam int code) {
        return new BaseResponse(ss.getVersionForUpgrade(code));
    }

    /**
     * 版本更新(取菜员)
     *
     * @param code
     * @return
     */
    @RequestMapping(value = "/deliver/upgrade", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse upgradeDeliver(@RequestParam int code) {
        BaseResponse response = new BaseResponse();
        Version version = employeeService.queryUpgradeVersionForDeliver(code);
        response.setData(version);
        return response;
    }

    /**
     * 版本更新(配菜员)
     *
     * @param code
     * @return
     */
    @RequestMapping(value = "/courier/upgrade", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse upgradeCourier(@RequestParam int code) {
        BaseResponse response = new BaseResponse();
        Version version = employeeService.queryUpgradeVersionForCourier(code);
        response.setData(version);
        return response;
    }

    @RequestMapping(value = "/system/contact", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse contact() {
        return new BaseResponse(ss.getServiceTelephone());
    }

    @RequestMapping(value = "/system/aboutus", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse aboutus() {
        return new BaseResponse(ss.getAboutus());
    }

    @RequestMapping(value = "/system/support", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse support() {
        return new BaseResponse(ss.getSupport());
    }

    @RequestMapping(value = "/system/license")
    @ResponseBody
    public BaseResponse license() {
        return new BaseResponse(ss.getLicense());
    }

    @RequestMapping(value = "/system/cashrule")
    @ResponseBody
    public BaseResponse cashrule() {
        return new BaseResponse(ss.getCashrule());
    }
}
