/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.promotion;

import com.hyl.teddy.apis.entity.Customer;
import org.springframework.stereotype.Service;

import com.hyl.teddy.apis.entity.Payment;
import com.hyl.teddy.apis.payment.PaymentListenerAdaptor;
import com.hyl.teddy.apis.payment.PaymentStatusChangeEvent;
import com.hyl.teddy.apis.payment.PromotionResponse;
import com.hyl.teddy.apis.profile.CustomerService;
import com.hyl.teddy.apis.system.SystemService;
import javax.annotation.Resource;

/**
 *
 * @author Administrator
 */
@Service
public class PromotionService extends PaymentListenerAdaptor {

    @Resource
    private CustomerService customerService;
    @Resource
    private SystemService systemService;

    /**
     * 计算订单 在优惠之后的实际费用(分). 若实际费用为负值，返回100（1块钱）.
     *
     * @param payment 订单
     * @return 实际费用(分)
     */
    public int actualCost(Payment payment) {
        double primeCost = payment.primeCost();
        if (primeCost <= 0) {
            return 100;
        }
        double fee = primeCost + payment.getDeliveryValue();
        for (PromotionResponse promotion : payment.getPromotions()) {
            fee += discount(promotion.toPromotion(), primeCost, payment);
        }
        if (fee <= 0) {
            fee = 100.0;
        }

        return (int) Math.round(fee);
    }

    /**
     * 计算单个优惠政策的优惠费用.
     *
     * @param promotion 优惠政策
     * @param primeCost 原始费用 单位：分
     * @return 优惠费用 一般是负数
     */
    private double discount(Promotion promotion, double primeCost, Payment payment) {
        double discount = 0;
        switch (promotion) {
            case WIN_CASH: // 返还代金券  [50,1,5] 意为 满50元送1张5元代金券
                discount = 0;
                break;
            case VIP_DISCOUNT: // 会员优惠，百分比打折
                Customer customer = customerService.getCustomer(payment.getCustomerId());
                if (customer != null && customerService.isVip(customer)) {
                    discount = -1.0 * (100 - promotion.getParams()[0]) / 100 * primeCost;
                }
                break;
            case COST_CASH:// 可使用代金券,该优惠不接受参数，用户只能选择一张代金券
                discount = -payment.getCash() * 100;
                break;
            case COST_SCORE://满{0}元可用{1}积分抵1元现金
                if (primeCost >= promotion.getParams()[0] * 100 && promotion.getParams()[1] > 0) {
                    discount = -100.0 * payment.getScore() / promotion.getParams()[1];
                }
                break;
            case FREE_POSTAGE: //免邮费该优惠不接受参数，用户订单免配送费
                if (primeCost >= promotion.getParams()[0] * 100) {
                    discount = -payment.getDeliveryValue();
                }
                break;
            default:
                discount = 0;
                break;
        }
        promotion.setDiscount((int) Math.round(discount));
        return discount;
    }

    /**
     * 优惠反馈客户动作.
     */
    private void feedback(Promotion promotion, Payment payment) {
        int[] params = promotion.getParams();
        switch (promotion) {
            case WIN_CASH://返还代金券  [50,1,5] 意为 满50元送1张5元代金券
                int paid = payment.getPaid();
                if (paid >= params[0] * 100) {
                    Customer customer = customerService.getCustomer(payment.getCustomerId());
                    if (customer == null) {
                        logger.warn("赠送代金券失败：找不到指定用户【id={}】", payment.getCustomerId());
                        return;
                    }
                    customerService.earnCash(null, paid, paid);
                    logger.debug("赠送了{}张{}元代金券给客户（{}）", new Object[]{params[1], params[2], customer.getMobile()});
                }
                break;
        }
    }

    /**
     * 反馈客户积分动作.
     */
    private void feedbackScore(Payment payment) {
        int howmuchPerScore = systemService.getScorePerMoney();
        int paid = payment.getPaid();
        int score = paid / howmuchPerScore;
        if (score > 0) {
            Customer customer = customerService.getCustomer(payment.getCustomerId());
            if (customer == null) {
                logger.warn("反馈积分失败：找不到指定用户【id={}】", payment.getCustomerId());
                return;
            }
            customerService.earnScore(customer, score);
            logger.debug("用户【{}】获得{}积分", customer.getMobile(), score);
        }
    }

    @Override
    public void fireStatusChanged(PaymentStatusChangeEvent event) {
        Payment payment = event.getPayment();
        if (payment.getStatus() == 5) {
            logger.debug("监听到订单状态改变为5，正在应用优惠反馈客户...");
            for (PromotionResponse promotion : payment.getPromotions()) {
                feedback(promotion.toPromotion(), payment);
            }
            feedbackScore(payment);
            logger.debug("优惠反馈结束======>");
        }
    }

    @Override
    public int getPriority() {
        return 200;
    }
}
