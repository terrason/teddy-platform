package com.hyl.teddy.apis.payment.web;

import java.util.List;
import javax.annotation.Resource;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.hyl.teddy.apis.BaseResponse;
import com.hyl.teddy.apis.CommonRuntimeException;
import com.hyl.teddy.apis.entity.CustAddress;
import com.hyl.teddy.apis.entity.Customer;
import com.hyl.teddy.apis.profile.CustomerService;
import com.hyl.teddy.apis.entity.Payment;
import com.hyl.teddy.apis.entity.Restaurant;
import com.hyl.teddy.apis.payment.BuyResponse;
import com.hyl.teddy.apis.payment.GoodsResponse;
import com.hyl.teddy.apis.payment.PaymentService;
import com.hyl.teddy.apis.payment.PaymentResponse;
import com.hyl.teddy.apis.profile.AddressResponse;
import com.hyl.teddy.apis.promotion.PromotionService;
import com.hyl.teddy.apis.restaurant.CommentResponse;
import com.hyl.teddy.apis.restaurant.RestaurantService;
import com.hyl.teddy.apis.system.SystemService;
import java.io.IOException;
import java.util.Date;
import javax.naming.AuthenticationException;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;

/**
 * teddy 个人中心 接口 12 我的订单 13 订单状态 14 订单详情 15 订单加菜
 *
 * @author Administrator
 *
 */
@Controller
public class PaymentController {

    private final Logger logger = LoggerFactory.getLogger(PaymentController.class);
    @Resource
    private MessageSource messageSource;

    @Resource
    private CustomerService customerService;
    @Resource
    private PaymentService paymentService;

    @Resource
    private PromotionService promotionService;
    @Resource
    private RestaurantService restaurantService;
    @Resource
    private SystemService systemService;
    @Value("#{settings['url.activity.host']}")
    private String portalHost;

    /**
     * 12 我的订单
     *
     * @param principal 当前用户id
     * @return
     * @throws AuthenticationException
     */
    @RequestMapping(value = "/profile/bought", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse myOrders(@RequestParam(defaultValue = "0") int principal,
            @RequestParam(defaultValue = "0") int start,
            @RequestParam(defaultValue = "10") int size) throws AuthenticationException {
        if (principal == 0) {
            throw new AuthenticationException();
        }
        BaseResponse b = new BaseResponse();
        Customer customer = customerService.getCustomer(principal);
        if (customer == null) {
            b.setCode(404);
            b.setMessage(messageSource.getMessage("authentication.failed.username", null, null));
            return b;
        }

        List<BougthResponse> list = paymentService.queryPaymentsByPrincipal(principal, start, size);
        b.setData(list);
        return b;
    }

    /**
     * 13 订单状态
     *
     * @param orderno 路径参数 需要查看的订单流水号
     * @return
     */
    @RequestMapping(value = "/profile/bought-timeline/{orderno}", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse timeline(@PathVariable String orderno) {
        BaseResponse b = new BaseResponse();
        TimelineResponse time = paymentService.queryTimeline(orderno);
        if (time == null) {
            b.setCode(404);
            b.setMessage(messageSource.getMessage("mission.payment", null, null));
            return b;
        }

        b.setData(time);
        return b;
    }

    /**
     * 14 订单详情
     *
     * @param orderno 订单 orderno
     * @param principal 当前用户 id
     * @return
     * @throws AuthenticationException
     */
    @RequestMapping(value = "/profile/bought/{orderno}", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse boughtDetail(@PathVariable String orderno, @RequestParam(defaultValue = "0") int principal) throws AuthenticationException {
        if (principal == 0) {
            throw new AuthenticationException();
        }
        BaseResponse b = new BaseResponse();
        Payment payment = paymentService.getPayment(orderno);
        if (payment != null) {
            PaymentResponse detial = new PaymentResponse(payment);
            if (payment.getStatus() < 2) {
                int actualCost = promotionService.actualCost(payment);
                int cost = actualCost < payment.getPaid() ? actualCost : actualCost - payment.getPaid();
                detial.setCost(1.0 * cost / 100);
            } else {
                detial.setCost(1.0 * payment.getPaid() / 100);
            }
            b.setData(detial);
        } else {
            b.setCode(404);
            b.setMessage(messageSource.getMessage("mission.customer", null, null));
        }

        return b;
    }

    /**
     * 删除订单
     *
     * @param orderno 订单 orderno
     * @param principal 当前用户 id
     * @return
     * @throws AuthenticationException
     */
    @RequestMapping(value = "/profile/bought/{orderno}", method = RequestMethod.DELETE)
    @ResponseBody
    public BaseResponse boughtDetail(@PathVariable String orderno) {
        BaseResponse b = new BaseResponse();
        Payment payment = paymentService.getPayment(orderno);
        if (payment != null) {

            paymentService.removeCustomerPayment(payment);
        } else {
            b.setCode(404);
            b.setMessage(messageSource.getMessage("mission.payment", null, null));
        }

        return b;
    }

    /**
     * 15 订单加菜
     *
     * @param orderno 路径参数 订单编号
     * @param goods 订单详情加餐部分的json数据
     * @return double:需要退钱还是价钱的数额
     */
    @RequestMapping("/profile/bought-extra/{orderno}")
    @ResponseBody
    public BaseResponse addGoods(@PathVariable String orderno, @RequestParam String goods, @RequestParam double cost) {
        BaseResponse b = new BaseResponse();
        Payment payment = paymentService.getPayment(orderno);
        if (payment == null) {
            b.setCode(HttpStatus.NOT_FOUND.value());
            b.setMessage(messageSource.getMessage("mission.payment", null, null));
            return b;
        }

        try {
            double needCost = paymentService.addGoods(payment, goods, cost);
            if (needCost > 0) {
                b.setMessage(messageSource.getMessage("success.payment.extra.needpay", new Object[]{needCost}, null));
            } else if (needCost == 0) {
                b.setMessage(messageSource.getMessage("success.payment.extra.equals", null, null));
            } else {
                b.setMessage(messageSource.getMessage("success.payment.extra.rollback", new Object[]{-needCost}, null));
            }
            ExtraGoodsResponse egr = new ExtraGoodsResponse();
            egr.setFee(needCost);
            egr.setOrderno(payment.getOrderno());
            b.setData(egr);
        } catch (IOException ex) {
            logger.info("提交订单出错！无法解析的JSON字符串：{}", goods);
            logger.debug("加菜处理出错", ex);
            b.setCode(HttpStatus.NOT_ACCEPTABLE.value());// 订单数据无法解析
            b.setMessage(messageSource.getMessage("error.default", null, null));
        }
        return b;
    }

    /**
     * 确认订单
     *
     * @param restaurant
     * @return
     */
    @RequestMapping(value = "/buy", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse buy(@RequestParam int restaurant, @RequestParam(defaultValue = "0") int principal) throws CloneNotSupportedException, AuthenticationException {
        BaseResponse response = new BaseResponse();
        BuyResponse br = paymentService.buy(restaurant);
        if (principal != 0) {
            CustAddress address = customerService.findDefaultAddress(principal);
            if (address != null) {
                br.setAddress(new AddressResponse(address));
            }
        }
        Customer customer = customerService.getCustomer(principal);
        br.setPrincipal(customer);
        response.setData(br);
        return response;
    }

    /**
     * 提交订单
     *
     * @param goods
     * @return
     */
    @RequestMapping(value = "/buy", method = RequestMethod.POST)
    @ResponseBody
    public BaseResponse buy(@RequestParam String goods, @RequestParam double cost) throws AuthenticationException {
        BaseResponse response = new BaseResponse();
        GoodsResponse goodsDetail;
        try {
            logger.debug("收到下订单请求:{}", goods);
            //转JSON字符串为对象
            goodsDetail = new ObjectMapper().readValue(goods, GoodsResponse.class);
            Payment payment = paymentService.newPayment(goodsDetail);
            if (Double.isNaN(cost) || Math.abs(cost - payment.getCost()) > 0.1) {//对账误差超过1毛钱
                response.setCode(HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE.value());
                response.setMessage(messageSource.getMessage("validate.payment", new Object[]{payment.getCost()}, null));
                return response;
            }
            //验证客户条件
            Customer customer = customerService.getCustomer(payment.getCustomerId());
            if (customer.getScore() < payment.getScore()) {
                response.setCode(HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE.value());
                response.setMessage(messageSource.getMessage("validate.payment.score", new Object[]{customer.getScore()}, null));
                return response;
            }
            if (payment.getCash() > 0 && customer.getCashVolume(payment.getCash()) <= 0) {
                response.setCode(HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE.value());
                response.setMessage(messageSource.getMessage("validate.payment.cash", null, null));
                return response;
            }
            String orderno = paymentService.getAndIncreaseSequence();
            payment.setOrderno(orderno);
            paymentService.buy(payment);
            paymentService.firePaymentStatusChanged(payment, 0);
            response.setData(payment);
        } catch (IOException e) {
            logger.info("提交订单出错！无法解析的JSON字符串：{}", goods);
            logger.debug("提交订单出错！", e);
            response.setCode(HttpStatus.NOT_ACCEPTABLE.value());// 订单数据无法解析
            response.setMessage(messageSource.getMessage("error.default", null, null));
        }
        return response;
    }

    @RequestMapping(value = "/comment", method = RequestMethod.POST)
    @ResponseBody
    public BaseResponse createComment(@RequestParam(defaultValue = "0") int principal,
            @RequestParam String orderno,
            @RequestParam int star,
            @RequestParam(defaultValue = "") String content) throws AuthenticationException {
        BaseResponse response = new BaseResponse();
        if (principal == 0) {
            throw new AuthenticationException();
        }
        Customer customer = customerService.getCustomer(principal);
        if (customer == null) {
            throw new AuthenticationException();
        }
        Payment payment = paymentService.getPayment(orderno);
        if (payment == null) {
            throw new CommonRuntimeException(HttpStatus.NOT_FOUND, "missing.payment");
        }
        paymentService.createComment(payment, principal, star, content);
        int restaurantId = payment.getRestaurantId();
        Restaurant restaurant = restaurantService.getRestaurant(restaurantId);
        if (restaurant != null) {
            restaurant.getComments().add(0, new CommentResponse(payment.getId(), customer.getNickname(), star, DateFormatUtils.format(new Date(), "yyyy-MM-dd"), content));
        }

        return response;
    }

    /**
     * 21 确认取菜 取菜员 状态改为5 其他逻辑：订单表加确定取菜时间、计算餐厅销量、菜品销量、计算优惠政策相关、送积分
     *
     * @param orderno 订单号
     * @return
     */
    @RequestMapping(value = "/payment/ensure/deliver/{orderno}", method = RequestMethod.POST)
    @ResponseBody
    public BaseResponse ensureDeliver(@PathVariable String orderno) {
        Payment payment = paymentService.getPayment(orderno);
        if (payment == null) {
            BaseResponse response = new BaseResponse();
            response.setCode(HttpStatus.NOT_FOUND.value());
            response.setMessage(messageSource.getMessage("mission.payment", null, null));
            return response;
        }
        if (payment.getStatus() < 5) {
            paymentService.firePaymentStatusChanged(payment, 5);
        }
        return new BaseResponse();
    }

    /**
     * 22 确认配送 分配员 状态改为6
     *
     * @param orderno 订单号
     * @return
     */
    @RequestMapping(value = "/payment/ensure/center/{orderno}", method = RequestMethod.POST)
    @ResponseBody
    public BaseResponse ensureCenter(@PathVariable String orderno) {
        BaseResponse response = new BaseResponse();
        Payment payment = paymentService.getPayment(orderno);
        if (payment == null) {
            response.setCode(HttpStatus.NOT_FOUND.value());
            response.setMessage(messageSource.getMessage("mission.payment", null, null));
            return response;
        }
        if (payment.getCourierId() <= 0) {
            response.setCode(HttpStatus.FORBIDDEN.value());
            response.setMessage(messageSource.getMessage("mission.employee.courier", null, null));
            return response;
        }
        if (payment.getStatus() < 6) {
            paymentService.firePaymentStatusChanged(payment, 6);
        }
        return response;
    }

    /**
     * 23 确认送达 配送员 状态改为8
     *
     * @param orderno 订单号
     * @return
     */
    @RequestMapping(value = "/payment/ensure/courier/{orderno}", method = RequestMethod.POST)
    @ResponseBody
    public BaseResponse ensureCourier(@PathVariable String orderno) {
        Payment payment = paymentService.getPayment(orderno);
        if (payment == null) {
            BaseResponse response = new BaseResponse();
            response.setCode(HttpStatus.NOT_FOUND.value());
            response.setMessage(messageSource.getMessage("mission.payment", null, null));
            return response;
        }
        if (payment.getStatus() < 8) {
            paymentService.firePaymentStatusChanged(payment, 8);
        }
        return new BaseResponse();
    }

    /**
     * 订单列表.
     */
    @RequestMapping(value = "/payment", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse pymentList(@RequestParam int principal,
            @RequestParam String status,
            @RequestParam int type,
            @RequestParam(defaultValue = "0") int start,
            @RequestParam(defaultValue = "10") int size) {
        Payment4EmployeeResponse response = paymentService.queryPaymentsByEmployee(type, principal, status, start, size);
        return new BaseResponse(response);
    }

    /**
     * 订单详情
     *
     * @param principal
     * @return
     */
    @RequestMapping(value = "/payment/{orderno}", method = RequestMethod.GET)
    @ResponseBody
    public BaseResponse payment(@PathVariable String orderno) {
        BaseResponse response = new BaseResponse();
        Payment payment = paymentService.getPayment(orderno);
        if (payment == null) {
            response.setCode(HttpStatus.NOT_FOUND.value());
            response.setMessage(messageSource.getMessage("mission.customer", null, null));
            return response;
        }
        payment.setCost(1.0 * payment.getPaid() / 100);
        response.setData(payment);
        return response;
    }

    /**
     * 订单回退至客服.
     */
    @RequestMapping(value = "/payment/cancel/{orderno}")
    @ResponseBody
    public BaseResponse paymentCancle(@PathVariable String orderno) throws Error {
        BaseResponse response = new BaseResponse();
        Payment payment = paymentService.getPayment(orderno);
        if (payment == null) {
            response.setCode(HttpStatus.NOT_FOUND.value());
            response.setMessage(messageSource.getMessage("mission.customer", null, null));
        } else {
            paymentService.airline(payment);
        }

        return response;
    }

    /**
     * 配送员 无法送达
     */
    @RequestMapping(value = "/payment/cannot/courier/{orderno}")
    @ResponseBody
    public BaseResponse cancelPayment(@PathVariable String orderno, @RequestParam String cause) {
        BaseResponse response = new BaseResponse();
        Payment payment = paymentService.getPayment(orderno);
        if (payment == null) {
            response.setCode(HttpStatus.NOT_FOUND.value());
            response.setMessage(messageSource.getMessage("mission.customer", null, null));
            return response;
        }
        paymentService.cancelPayment(payment, cause);
        return response;
    }

    /**
     * 订单菜品回退（取菜员）
     */
    @RequestMapping(value = "/dish/cancel/{id}")
    @ResponseBody
    public BaseResponse dishcancel(@PathVariable int id, @RequestParam String orderno) {
        BaseResponse response = new BaseResponse();
        paymentService.disableGoodsItem(orderno, id);
        return response;
    }

    /**
     * 分享订单后获取代金券
     *
     * @param orderno
     * @return
     */
    @RequestMapping(value = "/payment/share")
    @ResponseBody
    @Transactional
    public BaseResponse sharePayment(@RequestParam String orderno) {
        BaseResponse response = new BaseResponse();
        Payment payment = paymentService.getPayment(orderno);
        if (payment.getShared() >= 0) {
            response.setCode(HttpStatus.ALREADY_REPORTED.value());
            response.setMessage(messageSource.getMessage("duplicate.activity.share", null, null));
            return response;
        }
        int valuse = systemService.getSharePayment();//获取静态资源share.payment
        paymentService.sharePayment(valuse, orderno);//改变shared
        payment.setShared(valuse);
        Customer customer = customerService.getCustomer(payment.getCustomerId());//获取本订单用户信息
        if (customer == null) {
            response.setCode(HttpStatus.FAILED_DEPENDENCY.value());
            response.setMessage(messageSource.getMessage("fail.activity.share.payment.customer", null, null));
            return response;
        }
        customerService.earnCash(customer, valuse, 1);//为用户添加代金券
        response.setMessage(messageSource.getMessage("success.activity.share", new Object[]{valuse}, null));
        response.setData(messageSource.getMessage("message.share", new Object[]{systemService.getShareReceive(), portalHost, orderno}, null));
        return response;
    }

    /**
     * 领取代金券
     *
     * @param principal
     * @return
     */
    @RequestMapping(value = "/receive/share")
    @ResponseBody
    @Transactional
    public BaseResponse shareReceive(@RequestParam int principal, @RequestParam String orderno) throws AuthenticationException {
        BaseResponse response = new BaseResponse();
        if (paymentService.getPayment(orderno) == null) {
            response.setCode(HttpStatus.NOT_FOUND.value());
            response.setMessage(messageSource.getMessage("mission.payment", null, null));
            return response;
        }
        if (paymentService.getPayment(orderno).getShared() == -1) {
            response.setCode(HttpStatus.FAILED_DEPENDENCY.value());
            response.setMessage(messageSource.getMessage("fail.activity.share.payment.unshare", null, null));
            return response;
        }
        if (paymentService.getPayment(orderno).getCustomerId() == principal) {
            response.setCode(HttpStatus.FORBIDDEN.value());
            response.setMessage(messageSource.getMessage("forbidden.activity.share.sharer", null, null));
            return response;
        }
        if (!paymentService.checkReceive(principal, orderno)) {
            response.setCode(HttpStatus.ALREADY_REPORTED.value());
            response.setMessage(messageSource.getMessage("duplicate.activity.share.customer", null, null));
            return response;
        }
        int valuse = systemService.getShareReceive();////获取静态资源share.receive
        Customer customer = customerService.getCustomer(principal);//获取用户信息
        if (customer == null) {
            throw new AuthenticationException();
        }
        customerService.earnCash(customer, valuse, 1);//为用户添加代金券
        paymentService.shareReceive(principal, orderno, valuse);//添加到领取记录表
        response.setData(valuse);
        return response;
    }
}
