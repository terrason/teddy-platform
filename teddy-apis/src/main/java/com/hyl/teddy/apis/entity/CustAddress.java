package com.hyl.teddy.apis.entity;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * 客户送餐地址 cush_address
 *
 * @author Administrator
 *
 */
public class CustAddress {

    private int id;
    private int customerId;//用户表id
    private String name;//姓名
    private String mobile;//电话
    private String location;//地址
    /**
     * 经度.
     */
    @JsonIgnore
    private int longitudeValue;
    private String longitude;
    /**
     * 纬度.
     */
    @JsonIgnore
    private int latitudeValue;
    private String latitude;//纬度
    private boolean defaultAddress;//是否默认地址
    private int regionId;//区域表id

    public CustAddress() {
    }

    public CustAddress(int id, int customerId, String name, String mobile, String location, int longitude, String longitudeString, int latitude, String latitudeString, boolean defaultAddress, int regionId) {
        this.id = id;
        this.customerId = customerId;
        this.name = name;
        this.mobile = mobile;
        this.location = location;
        this.longitudeValue = longitude;
        this.longitude = longitudeString;
        this.latitudeValue = latitude;
        this.latitude = latitudeString;
        this.defaultAddress = defaultAddress;
        this.regionId = regionId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * 经度.
     */
    public int getLongitudeValue() {
        return longitudeValue;
    }

    /**
     * 经度.
     */
    public void setLongitudeValue(int longitudeValue) {
        this.longitudeValue = longitudeValue;
        longitude = null;
    }

    /**
     * 经度.
     */
    public String getLongitude() {
        if (longitude == null) {
            longitude = String.valueOf(1.0 * longitudeValue / 1000000);
        }
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * 纬度.
     */
    public int getLatitudeValue() {
        return latitudeValue;
    }

    /**
     * 纬度.
     */
    public void setLatitudeValue(int latitudeValue) {
        this.latitudeValue = latitudeValue;
        latitude = null;
    }

    /**
     * 纬度.
     */
    public String getLatitude() {
        if (latitude == null) {
            latitude = String.valueOf(1.0 * latitudeValue / 1000000);
        }
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public boolean isDefaultAddress() {
        return defaultAddress;
    }

    public void setDefaultAddress(boolean defaultAddress) {
        this.defaultAddress = defaultAddress;
    }

    public int getRegionId() {
        return regionId;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

}
