package com.hyl.teddy.apis.asynchronous.jdbc;

import java.util.LinkedList;
import java.util.List;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;

public class Update {

    private List<Object[]> args = new LinkedList<Object[]>();
    private String sql;
    private BatchPreparedStatementSetter batch;

    public Update(String sql) {
        this.sql = sql;
    }

    public List<Object[]> toArgs() {
        List<Object[]> rtnVal = args;
        args = new LinkedList<Object[]>();
        return rtnVal;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public boolean isMultiple() {
        return args.size() > 1;
    }

    public boolean isBatched() {
        return batch != null;
    }

    public Update add(Object[] e) {
        args.add(e);
        return this;
    }

    public void setBatch(BatchPreparedStatementSetter batch) {
        this.batch = batch;
    }

    public BatchPreparedStatementSetter getBatch() {
        return batch;
    }

}
