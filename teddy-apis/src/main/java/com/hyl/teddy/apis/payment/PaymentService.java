package com.hyl.teddy.apis.payment;

import com.hyl.teddy.apis.CommonRuntimeException;
import com.hyl.teddy.apis.payment.web.Payment4EmployeeResponse;
import com.hyl.teddy.apis.entity.CustAddress;
import com.hyl.teddy.apis.entity.Customer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.hyl.teddy.apis.entity.Payment;
import com.hyl.teddy.apis.entity.PaymentGoods;
import com.hyl.teddy.apis.entity.Restaurant;
import com.hyl.teddy.apis.payment.dao.PaymentDao;
import com.hyl.teddy.apis.payment.web.BougthResponse;
import com.hyl.teddy.apis.payment.web.TimelineResponse;
import com.hyl.teddy.apis.profile.AddressService;
import com.hyl.teddy.apis.profile.CustomerService;
import com.hyl.teddy.apis.promotion.Promotion;
import com.hyl.teddy.apis.promotion.PromotionService;
import com.hyl.teddy.apis.restaurant.RestaurantService;
import com.hyl.teddy.apis.restaurant.dao.DishDao;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;
import javax.naming.AuthenticationException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PaymentService {

    private final Logger logger = LoggerFactory.getLogger(PaymentService.class);
    private final Map<String, Long> sequence = new HashMap<String, Long>();
    private final DecimalFormat sequenceFormattor = new DecimalFormat("000000");
    private final DecimalFormat paymentCostFormattor = new DecimalFormat("#.##");

    @Resource
    private MessageSource messageSource;
    @Resource
    private PaymentDao paymentDao;
    @Resource
    private DishDao dishDao;
    @Resource
    private PromotionService promotionService;
    @Resource
    private RestaurantService restaurantService;
    @Resource
    private AddressService addressService;
    @Resource
    private CustomerService customerService;
    @Resource
    private AlipayService alipayService;
    @Resource
    private CacheManager cacheManager;
    @Autowired(required = false)
    private Collection<PaymentListener> listeners = new TreeSet<>();

    public Collection<PaymentListener> getListeners() {
        if (!listeners.getClass().equals(TreeSet.class)) {
            listeners = new TreeSet<>(listeners);
        }
        return listeners;
    }

    /**
     * 格式化到角. #.#
     */
    public String formatPaymentCost(double cost) {
        return paymentCostFormattor.format(cost);
    }

    /**
     * 生成一个订单流水号.
     */
    public String getAndIncreaseSequence() {
        String prefix = DateFormatUtils.format(new Date(), "yyMMdd");

        synchronized (sequence) {
            Long current = sequence.get(prefix);
            if (current == null) {
                current = paymentDao.selectLastSequence(prefix);
                sequence.clear();
            }

            sequence.put(prefix, current + 1);
            String value = sequenceFormattor.format(current);
            return prefix + value;
        }
    }

    public Payment getPayment(String orderno) {
        return paymentDao.selectPayment(orderno);
    }

    /**
     * 新建一个游离态订单对象. 游离态的订单无订单号和订单ID.
     *
     * @param goods 订单购物列表
     * @return 游离态的订单对象
     */
    public Payment newPayment(GoodsResponse goods) throws AuthenticationException {
        Payment p = new Payment();
        p.setAddressId(goods.getAddress());
        p.setCustomerId(goods.getPrincipal());
        Customer customer = customerService.getCustomer(p.getCustomerId());
        if (customer == null) {
            throw new AuthenticationException();
        }
        p.setVip(customerService.isVip(customer));
        CustAddress address = addressService.getAddress(p.getAddressId());
        p.setAddress(address);
        if (!supportAddress(p)) {
            throw new CommonRuntimeException(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "validate.payment.notSupportedArea");
        }
        p.setRegionId(address.getRegionId());
        p.setRestaurantId(goods.getRestaurant());
        Restaurant restaurant = restaurantService.getRestaurant(p.getRestaurantId());
        p.setRestaurantName(restaurant.getName());
        p.setRestaurantImg(restaurant.getIcon());
        p.setRestaurantLocation(restaurant.getLocation());
        p.setPackageFeeValue(restaurant.getPackageFee());
        p.setDeliveryValue(restaurant.getDelivery());
        p.setStatus(0);
        p.setMemo(goods.getMemo());
        p.setCash(goods.getCash());
        p.setScore(goods.getScore());
        p.setEditable(false);
        p.setCreateTime(DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));

        p.setGoods(goods.getDishes());
        List<Promotion> queryPromotionByRestaurant = restaurantService.queryPromotionByRestaurant(p.getRestaurantId());
        List<PromotionResponse> promotions = new ArrayList<>();
        for (Promotion promotion : queryPromotionByRestaurant) {
            promotions.add(new PromotionResponse(promotion));
        }
        p.setPromotions(promotions);
        p.setCost(1.0 * promotionService.actualCost(p) / 100);
        return p;
    }

    /**
     * 根据当前用户id 获得订单信息：BougthResponse
     *
     * @param principal
     * @return
     */
    public List<BougthResponse> queryPaymentsByPrincipal(int principal, int start, int size) {
        List<BougthResponse> rs = new ArrayList<BougthResponse>();
        Collection<Payment> list = paymentDao.selectPayments4Principal(principal, start, size);
        if (list.size() > 0) {
            for (Payment p : list) {
                BougthResponse b = new BougthResponse();
                b.setId(p.getOrderno());
                b.setTitle(p.getRestaurantName());
                // 获得实际费用 保留两位小数
                if (p.getStatus() < 2) {
                    int actualCost = promotionService.actualCost(p);
                    int cost = actualCost - p.getPaid();
                    b.setCost(1.0 * cost / 100);
                } else {
                    b.setCost(1.0 * p.getPaid() / 100);
                }
                b.setCreateTime(p.getCreateTime());
                b.setStatus(converStatusToInt(p.getStatus()));
                b.setStatusText(converStatusToText(b.getStatus()));
                // 根据评价星级 判断是否评价
                if (p.getStar() > 0) {
                    b.setCommented(true);
                } else {
                    b.setCommented(false);
                }
                // 设置订单内容：所有菜品
                String content = "";
                for (int i = 0; i < p.getGoods().size(); i++) {
                    if (i >= 3) {
                        content += " 等";
                        break;
                    }
                    PaymentGoods good = p.getGoods().get(i);
                    content += "  " + good.getName();
                }
                b.setContent(content);
                b.setRestaurantImg(p.getRestaurantImg());
                rs.add(b);
            }
        }
        return rs;
    }

    public Payment4EmployeeResponse queryPaymentsByEmployee(int type, int principal, String status, int start, int size) {
        Payment4EmployeeResponse response = new Payment4EmployeeResponse();
        if (type == 3 && "3".equals(status)) {
            return response;
        }
        Collection<Payment> elements;
        if (type == 1) {//取菜员
            elements = paymentDao.selectPayments4Deliver(principal, status, start, size);
            response.setTotal(paymentDao.countPayment4Deliver(principal, status));
            if ("6,8,9".equals(status)) {
                response.setCancel(paymentDao.countCancelledPayment4Deliver(principal));
            }
        } else if (type == 2) {//配送员
            size = 9999;
            elements = paymentDao.selectPayments4Courier(principal, status, start, size);
            response.setTotal(paymentDao.countPayment4Courier(principal, status));
        } else if (type == 3) {//分配员
            elements = paymentDao.selectPayments4Center(principal, status, start, size);
            response.setTotal(paymentDao.countPayment4Center(principal, status));
            if ("6,8,9".equals(status)) {
                response.setCancel(paymentDao.countCancelledPayment4Center(principal));
            }
        } else {
            elements = Collections.EMPTY_LIST;
        }
        response.setElements(elements);
        return response;
    }

    public TimelineResponse queryTimeline(String orderno) {
        TimelineResponse time = paymentDao.selectTimelineByOrderno(orderno);
        // 转换订单状态
        if (time == null) {
            return null;
        }
        if (time.getStatus() < 6) {
            time.setCourierMobile(null);
            time.setCourierNumber(null);
        }
        int status = converStatusToInt(time.getStatus());
        time.setStatus(status);
        time.setStatusText(converStatusToText(status));
        List<TimeStep> steps = time.getSteps();
        if (!steps.isEmpty()) {//最后一次状态处理
            TimeStep ts = steps.get(0);
            steps.remove(0);
            time.setStatusTime(ts.getTime());
        }
        int size = steps.size();
        if (size > 0 && steps.get(size - 1).getStatus() == 0) {//创建订单状态不显示
            steps.remove(size - 1);
        }

        return time;
    }

    /**
     * 订单状态 转换订单状态 ——状态1-待支付 2-已下单 3-已接单 4-配送中 5-已完成 6-已取消
     *
     * @param status
     * @return
     */
    private int converStatusToInt(int status) {
        switch (status) {
            case 0:
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
            case 4:
            case 5:
                return 3;
            case 6:
                return 4;
            case 8:
                return 5;
            case 9:
                return 6;
            default:
                return 0;
        }
    }

    /**
     * 根据订单状态，设置订单状态文本
     *
     * @param status 订单客户端状态
     * @return
     */
    private String converStatusToText(int status) {
        String statusText = "";
        switch (status) {
            case 1:
                statusText = "待支付";
                break;
            case 2:
                statusText = "已下单";
                break;
            case 3:
                statusText = "已接单";
                break;
            case 4:
                statusText = "配送中";
                break;
            case 5:
                statusText = "已完成";
                break;
            case 6:
                statusText = "已取消";
                break;

        }
        return statusText;
    }

    /**
     * 订单加菜处理逻辑 TODO.
     *
     * @param payment 原订单
     * @param goods 加菜明细json字符串
     * @param cost 对账用订单费用
     * @return 还需支付的费用（单位：元）
     *
     */
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public double addGoods(Payment payment, String goods, double cost) throws IOException {
        GoodsResponse goodsDetail = new ObjectMapper().readValue(goods, GoodsResponse.class);
        if (payment.getAddressId() != goodsDetail.getAddress()) {
            payment.setAddressId(goodsDetail.getAddress());
            payment.setAddress(addressService.getAddress(goodsDetail.getAddress()));
        }

        if (!supportAddress(payment)) {
            throw new CommonRuntimeException(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "validate.payment.notSupportedArea");
        }
        //转JSON字符串为对象
        if (!goodsDetail.isEmpty()) {
            for (PaymentGoods goodItem : goodsDetail.getDishes()) {
                goodItem.setPaymentId(payment.getId());
                payment.addGoods(goodItem);
            }
        }
        try {
            //加菜之后的订单的实际费用
            int actualCost = promotionService.actualCost(payment);
            if (Double.isNaN(cost) || Math.abs(cost * 100 - actualCost) > 10) {//对账失败
                throw new CommonRuntimeException(HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE, "validate.payment", 0.01 * actualCost);
            }
            // 更新加菜订单的使用积分和代金券
            payment.setCash(goodsDetail.getCash());
            payment.setScore(goodsDetail.getScore());
            paymentDao.updatePaymentCost(payment);
            paymentDao.updatePaymentDiscount(payment);
            paymentDao.insertPaymentGoods(payment.getId(), goodsDetail.getDishes());
            // 释放该订单的缓存
            cacheManager.getCache("payment").evict(payment.getOrderno());
            double needCost = (1.0 * actualCost - payment.getPaid()) / 100;
            if (needCost >= 0) {
                changeOrderno(payment);
            } else {
                logger.warn("加菜金额处理错误：还需支付 {} 元！？", needCost);
            }
            firePaymentStatusChanged(payment, 0);
            return needCost;
        } catch (Exception ex) {
            Iterator<PaymentGoods> iterator = payment.getGoods().iterator();
            while (iterator.hasNext()) {
                PaymentGoods next = iterator.next();
                if (next.getStatus() == 0) {
                    iterator.remove();
                }
            }
            promotionService.actualCost(payment);
            throw ex;
        }
    }

    /**
     * 评论
     *
     * @param principal
     * @param restaurant
     * @param payment
     * @param paytime
     * @param star
     * @param comment
     */
    public void createComment(Payment payment, int principal, int star, String comment) {
        paymentDao.insertComment(principal, payment.getOrderno(), star, comment);
        //计算餐厅星评
        restaurantService.calcStar(payment.getRestaurantId());
    }

    /**
     * 确认订单
     *
     * @param restaurant
     * @return
     */
    public BuyResponse buy(int restaurant) {
        BuyResponse response = new BuyResponse();
        Restaurant rest = restaurantService.getRestaurant(restaurant);
        if (rest == null) {
            throw new CommonRuntimeException(HttpStatus.NOT_FOUND, "mission.restaurant");
        }
        response.setPackageFee(1.0 * rest.getPackageFee() / 100);
        response.setDelivery(1.0 * rest.getDelivery() / 100);

        List<Promotion> promotions = restaurantService.queryPromotionByRestaurant(restaurant);
        List<PromotionResponse> prs = new ArrayList<>();
        for (Promotion promotion : promotions) {
            PromotionResponse pr = new PromotionResponse();
            pr.setTag(promotion.name());
            pr.setParams(promotion.getParams());

            prs.add(pr);
        }
        response.setPromotion(prs);

        return response;
    }

    /**
     * 在客户订单列表中删除订单.
     */
    public void removeCustomerPayment(Payment payment) {
        if (payment.getStatus() < 2) {
            paymentDao.deletePayment(payment.getId(), payment.getOrderno());
        } else {
            paymentDao.updatePaymentVisiblily(payment.getId(), false);
        }
    }

    /**
     * 提交订单.
     */
    @Transactional
    public void buy(Payment payment) {
        int id = paymentDao.insertPayment(payment);
        payment.setId(id);
    }

    @Transactional
    public void firePaymentStatusChanged(Payment payment, int status) {
        firePaymentStatusChanged(payment, status, null, null);
    }

    /**
     * 修改订单状态，创建 payment_times
     *
     * @param orderno 订单号
     * @param status 订单状态
     * @param cause 变动原因
     * @param cost 费用. <strong>{@code cost != null} 时才保存</strong>
     * @return
     */
    @Transactional
    public void firePaymentStatusChanged(Payment payment, int status, String cause, Integer cost) {
        int originalStatus = payment.getStatus();
        String statusDesc = StringUtils.isBlank(cause) ? getStatusDescByStatus(status) : cause;
        if (cost != null) {
            payment.setPaid(cost);
        }
        payment.setStatus(status);
        payment.setStatusDesc(statusDesc);
        String description = getDescriptionByStatus(status);
        try {
            paymentDao.updateOrderStatus(payment.getId(), status, statusDesc, cost, payment.getAlipayNo());
            paymentDao.insertToPaymentTime(payment.getId(), status, description, cost);
            PaymentStatusChangeEvent event = new PaymentStatusChangeEvent(payment, originalStatus, this);
            for (PaymentListener listener : getListeners()) {
                logger.debug("消息发至监听器：{}", listener.getClass());
                listener.fireStatusChanged(event);
            }
            event.done();
        } catch (Exception e) {
            cacheManager.getCache("payment").evict(payment.getOrderno());
            throw e;
        }
    }

    /**
     * 将订单转交客服处理.
     *
     * @param orderno 订单号
     */
    public void airline(Payment payment) {
        firePaymentStatusChanged(payment, 4);
    }

    /**
     * 无法送达.
     */
    public void cancelPayment(Payment payment, String cause) {
        firePaymentStatusChanged(payment, 9, cause, null);
    }

    public void disableGoodsItem(String orderno, int dishId, boolean alsoDisableDish) {
        Payment payment = getPayment(orderno);
        if (payment == null) {
            throw new CommonRuntimeException(HttpStatus.NOT_FOUND, "mission.payment");
        }
        for (PaymentGoods goods : payment.getGoods()) {
            if (goods.getDishId() == dishId) {
                goods.setStatus(2);
            }
        }
        paymentDao.updateGoodsItemStatus(payment.getId(), dishId, 2);
        if (alsoDisableDish) {
            dishDao.updateDishStatus(dishId, false);
        }
    }

    public void disableGoodsItem(String orderno, int dishId) {
        disableGoodsItem(orderno, dishId, true);
    }

    public void updatePaymentDeliver(int deliverId, int id) {
        paymentDao.updatePaymentDeliver(deliverId, id);
    }

    public void updatePaymentCourier(int courierId, int id) {
        paymentDao.updatePaymentCourier(courierId, id);
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void changeOrderno(Payment payment) {
        String orderno = getAndIncreaseSequence();
        paymentDao.updatePaymentOrderno(payment.getId(), orderno);
        payment.setOrderno(orderno);
    }

    public void setEditable(int id, boolean editable) {
        paymentDao.updateEditable(id, editable);
    }

    /**
     * 换菜逻辑. 已退回部分费用。
     */
    @Transactional
    public void exchange(Payment payment, double fee) {
        logger.debug("订单换菜开始：释放订单加菜权限，回退客户积分及代金券...");
        Customer customer = customerService.getCustomer(payment.getCustomerId());
        if (customer == null) {
            logger.debug("回退客户积分及代金券处理非正常结束：找不到指定的客户【id={}】！", payment.getCustomerId());
            throw new CommonRuntimeException(HttpStatus.NOT_FOUND, messageSource.getMessage("mission.customer", null, null));
        }
        try {
            payment.setEditable(true);
            setEditable(payment.getId(), true);
            if (payment.getCash() > 0) {
                customerService.earnCash(customer, payment.getCash(), 1);
                logger.debug("已回退用户一张{}元代金券", payment.getCash());
            }
            if (payment.getScore() > 0) {
                customerService.rollbackScore(customer, payment.getScore());
                logger.debug("已回退用户{}积分", payment.getScore());
            }
            promotionService.actualCost(payment);
            paymentDao.updatePaymentDiscount(payment);
            int originalPaid = payment.getPaid();
            payment.setPaid(originalPaid - (int) (fee * 100));
            paymentDao.updatePaymentPaid(payment.getId(), payment.getPaid());
            paymentDao.insertToPaymentTime(payment.getId(), payment.getStatus(), "客户换菜", (int) -(fee * 100));
        } catch (Exception ex) {
            customerService.evictCache(customer.getId());
            throw ex;
        }
        logger.debug("订单换菜处理完成=========>");
    }

    /**
     * 退菜逻辑.
     */
    @Transactional
    public void giveup(Payment payment) {
        logger.debug("订单退菜开始...");
        if (payment.isEditable()) {
            payment.setEditable(false);
            setEditable(payment.getId(), false);
        }
        promotionService.actualCost(payment);
        paymentDao.updatePaymentDiscount(payment);
        firePaymentStatusChanged(payment, 2, "退菜", payment.getPaid());
        logger.debug("订单退菜处理完成=========>");
    }

    /**
     * 取消订单.
     */
    public void cancel(Payment payment) {
        logger.debug("取消订单开始...");
        firePaymentStatusChanged(payment, 9, "缺菜并取消订单", payment.getStatus() == 4 ? 0 : null);
        logger.debug("订单取消处理完成=========>");
    }

    /**
     * 判断订单的送餐地址是否被支持
     *
     * @param payment 订单
     * @return {@code true}--支持 {@code false}--不支持
     */
    public boolean supportAddress(Payment payment) {
        CustAddress address = payment.getAddress();
        if (address == null) {
            address = addressService.getAddress(payment.getAddressId());
        }
        if (address == null) {
            return false;
        }
        int regionId = addressService.findRegionId(address.getLongitudeValue(), address.getLatitudeValue());
        if (regionId == 0) {
            return false;
        }
        if (regionId != payment.getRegionId()) {
            payment.setRegionId(regionId);
        }
        return true;
    }

    /**
     * 根据订单状态 返回 插入paytment_times 表的描述.
     */
    private String getDescriptionByStatus(int status) {
        String description = "";
        switch (status) {
            case 0:
                description = "创建订单\n订单等待您的支付";
                break;
            case 1:
            case 2:
                description = "下单成功\n等待商家接单";
                break;
            case 3:
            case 4:
            case 5:
                description = "商家已接单\n即将为您配送";
                break;
            case 6:
                description = "熊仔正在配送\n等待您的美食吧～";
                break;
        }
        return description;
    }

    /**
     * 根据订单状态获得订单状态描述
     *
     * @param status
     * @return 1 待支付 ：订单等待您的支付 2 已下单 ：下单成功， 等待熊仔接单～ 5 已接单 ：熊仔以接单，等待商家发单 6 配送中
     * ：熊仔正在配送，等待您的美食吧 8 已完成 ：享用美食的同时，别忘了评价哦 9 已取消 ：由于缺货，订单被取消，熊仔万分抱歉！ 或者
     * 无法联系客户，请呼叫熊仔！
     */
    private String getStatusDescByStatus(int status) {
        String statusDesc = "";
        switch (status) {
            case 0:
            case 1:
                statusDesc = "订单等待您的支付";
                break;
            case 2:
                statusDesc = "下单成功， 等待熊仔接单～";
                break;
            case 3:
            case 4:
            case 5:
                statusDesc = "熊仔已接单，等待商家发单～";
                break;

            case 6:
                statusDesc = "熊仔正在配送，等待您的美食吧";
                break;
            case 8:
                statusDesc = "享用美食的同时，别忘了评价哦";
                break;
        }
        return statusDesc;
    }

    /**
     * 分享订单获得代金券
     *
     * @param value
     * @param orderno
     */
    public void sharePayment(int value, String orderno) {
        paymentDao.sharePayment(value, orderno);
    }

    public boolean checkReceive(int principal, String orderno) {
        return paymentDao.checkReceive(principal, orderno) == 0;
    }

    public void shareReceive(int principal, String orderno, int value) {
        paymentDao.shareReceive(principal, orderno, value);
    }
}
