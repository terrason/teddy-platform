package com.hyl.teddy.apis.entity;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.springframework.format.annotation.NumberFormat;

/**
 * 订单菜品明细 payment_goods
 *
 * @author Administrator
 *
 */
public class PaymentGoods {

    @JsonProperty("identity")
    private int id;
    @JsonIgnore
    private int paymentId;//订单id
    @JsonProperty("id")
    private int dishId;//菜品id
    private String name;//菜品名称
    @JsonIgnore
    private int priceValue;//价格.
    private int count;//数量
    private int status;//状态 0初始 1 取菜 2 退菜
    private int dishCategoryId;
    private String dishCategoryName;
    private String icon;

    @NumberFormat(pattern = "#.#")
    public double getPrice() {
        return 1.0 * priceValue / 100;
    }

    public void setPrice(double price) {
        priceValue = (int) Math.round(price * 100);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public int getDishId() {
        return dishId;
    }

    public void setDishId(int dishId) {
        this.dishId = dishId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPriceValue() {
        return priceValue;
    }

    public void setPriceValue(int priceValue) {
        this.priceValue = priceValue;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDishCategoryId() {
        return dishCategoryId;
    }

    public void setDishCategoryId(int dishCategoryId) {
        this.dishCategoryId = dishCategoryId;
    }

    public String getDishCategoryName() {
        return dishCategoryName;
    }

    public void setDishCategoryName(String dishCategoryName) {
        this.dishCategoryName = dishCategoryName;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return "PaymentGoods{" + "name=" + name + ", priceValue=" + priceValue + ", count=" + count + '}';
    }

}
