/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.thumbnails.impl;

import com.hyl.teddy.apis.attachment.AttachmentService;
import com.hyl.teddy.apis.thumbnails.ThumbSize;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public abstract class AttachmentThumbnailsGenerator {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    @Resource
    private AttachmentService attachmentService;

    /**
     * 生成缩略图的具体大小.
     */
    protected abstract Dimension size(ThumbSize pattern);

    /**
     * 生成缩略图.
     *
     * 将数据库中保存的{@code 资源值}转化成缩略图URL.
     */
    public String toThumbmails(int id, ThumbSize pattern) {
        return attachmentService.toResourceUrl(id, size(pattern));
    }

    /**
     * 批量生成缩略图数组.
     *
     * @param ids 以“,”分隔的多个缩略图{@code 资源值}.
     * @param size 尺寸
     * @return 缩略图
     */
    public Collection<String> toMultipleThumbmails(String ids, ThumbSize pattern) {
        if (StringUtils.isBlank(ids)) {
            return Collections.EMPTY_LIST;
        }
        List<String> list = new ArrayList<>();
        String[] resourceArray = ids.split(",");
        for (String resource : resourceArray) {
            try {
                int id = Integer.parseInt(resource);
                list.add(toThumbmails(id, pattern));
            } catch (NumberFormatException ex) {
                logger.warn("忽略不能识别的附件ID：{}", resource);
            }
        }
        return list;
    }

    public void setAttachmentService(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }

}
