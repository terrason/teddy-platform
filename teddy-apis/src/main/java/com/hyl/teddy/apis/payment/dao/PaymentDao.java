package com.hyl.teddy.apis.payment.dao;

import com.hyl.teddy.apis.employee.Employee;
import com.hyl.teddy.apis.employee.EmployeeService;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.hyl.teddy.apis.entity.CustAddress;
import com.hyl.teddy.apis.entity.Payment;
import com.hyl.teddy.apis.entity.PaymentGoods;
import com.hyl.teddy.apis.payment.TimeStep;
import com.hyl.teddy.apis.payment.web.TimelineResponse;
import com.hyl.teddy.apis.payment.PromotionResponse;
import com.hyl.teddy.apis.restaurant.thumbnails.DishImg;
import com.hyl.teddy.apis.restaurant.thumbnails.RestaurantIcon;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Collections;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

@Repository
public class PaymentDao {

    @Resource
    private JdbcTemplate jdbcTemplate;
    @Resource
    private EmployeeService employeeService;
    @Resource
    private RestaurantIcon restaurantIcon;
    @Resource
    private DishImg dishImg;

    /**
     * 根据订单id查询订单状态
     *
     * @param paymentId
     * @return
     */
    public TimelineResponse selectTimelineByOrderno(String orderno) {
        TimelineResponse timeline;
        try {
            timeline = jdbcTemplate.queryForObject("SELECT a.id,a.create_time,a.status,a.status_desc AS statusDesc,b.username AS courierNumber,b.mobile AS courierMobile\n"
                    + "FROM payment a\n"
                    + "LEFT JOIN employee b ON a. courier_id = b.id\n"
                    + "WHERE a.orderno = ?", new RowMapper<TimelineResponse>() {
                        @Override
                        public TimelineResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
                            TimelineResponse t = new TimelineResponse();
                            t.setId(rs.getInt("id"));
                            t.setCreateTime(DateFormatUtils.format(rs.getTimestamp("create_time"), "yyyy-MM-dd HH:mm:ss"));
                            t.setCourierNumber(rs.getString("courierNumber"));
                            t.setStatus(rs.getInt("status"));
                            t.setCourierMobile(rs.getString("courierMobile"));
                            t.setStatusDesc(rs.getString("statusDesc"));
                            return t;
                        }

                    }, orderno);
        } catch (IncorrectResultSizeDataAccessException e) {
            return null;
        }

        List<TimeStep> steps = jdbcTemplate.query("SELECT * FROM payment_times WHERE payment_id = ? and status in(0,2,3,6,8,9) order by id desc", new RowMapper<TimeStep>() {

            @Override
            public TimeStep mapRow(ResultSet rs, int rowNum) throws SQLException {
                TimeStep timeStep = new TimeStep();
                timeStep.setTime(DateFormatUtils.format(rs.getTime("create_time"), "HH:mm:ss"));
                timeStep.setStatus(rs.getInt("status"));
                timeStep.setDescription(rs.getString("description"));
                return timeStep;
            }

        }, timeline.getId());
        timeline.setSteps(steps);
        return timeline;
    }

    /**
     * 修改订单客户消费部分. 包括积分和代金券.
     *
     * @param payment
     */
    public int updatePaymentCost(Payment payment) {
        return jdbcTemplate.update("UPDATE payment SET score = ?, cash = ?, address_id=?,region_id=? WHERE id = ? ",
                payment.getScore(),
                payment.getCash(),
                payment.getAddressId(),
                payment.getRegionId(),
                payment.getId());
    }

    /**
     * 修改订单各种优惠折扣.
     */
    public void updatePaymentDiscount(final Payment payment) {
        if (payment.getPromotions().isEmpty()) {
            return;
        }
        jdbcTemplate.batchUpdate("update payment_promotion set money=? where id=?", new BatchPreparedStatementSetter() {

            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                PromotionResponse promotionResponse = payment.getPromotions().get(i);
                ps.setInt(1, promotionResponse.getDiscount());
                ps.setInt(2, promotionResponse.getId());
            }

            @Override
            public int getBatchSize() {
                return payment.getPromotions().size();
            }
        });
    }

    /**
     * 新增加菜订单的菜品明细.
     */
    public void insertPaymentGoods(final int paymentId, List<PaymentGoods> goods) {
        String sql = " INSERT INTO payment_goods(payment_id,dish_id,dish_name,price,`count`,status) values (?, ?, ?, ?, ?, 0) ";
        List<Object[]> params = new ArrayList<>();
        for (PaymentGoods goodItem : goods) {
            if (goodItem.getStatus() == 0) {
                params.add(new Object[]{
                    paymentId,
                    goodItem.getDishId(),
                    goodItem.getName(),
                    goodItem.getPriceValue(),
                    goodItem.getCount()
                });
            }
        }
        jdbcTemplate.batchUpdate(sql, params);
    }

    /**
     * 评论
     *
     * @param principal
     * @param restaurant
     * @param payment
     * @param paytime
     * @param star
     * @param comment
     */
    @CacheEvict(value = "payment", key = "#orderno")
    public void insertComment(int principal, String orderno, int star, String comment) {
        jdbcTemplate.update("update payment set star=?,comment=?,comment_time=now() where orderno=?", star, comment, orderno);
    }

    /**
     * 查询指定日期的订单最大序列号.
     *
     * @param day 日期 yyMMdd
     * @return 最大序列号
     */
    public long selectLastSequence(String day) {
        String max = jdbcTemplate.queryForObject("select ifnull(max(orderno)+1,'000000000001') from payment where orderno like ?", String.class, day + "%");
        return Long.parseLong(max.substring(6));
    }

    /**
     * 提交订单
     *
     * @param orderno
     * @param goodsDetail
     */
    public int insertPayment(final Payment payment) {
        final String sql = "INSERT INTO payment(orderno,address_id,customer_id,region_id,restaurant_id,restaurant_name,package,delivery,status,memo,cash,score,vip,create_time) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,now())";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection cnctn) throws SQLException {
                PreparedStatement ps = cnctn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, payment.getOrderno());
                ps.setInt(2, payment.getAddressId());
                ps.setInt(3, payment.getCustomerId());
                ps.setInt(4, payment.getRegionId());
                ps.setInt(5, payment.getRestaurantId());
                ps.setString(6, payment.getRestaurantName());
                ps.setInt(7, payment.getPackageFeeValue());
                ps.setInt(8, payment.getDeliveryValue());
                ps.setInt(9, 0);
                ps.setString(10, payment.getMemo());
                ps.setInt(11, payment.getCash());
                ps.setInt(12, payment.getScore());
                ps.setBoolean(13, payment.isVip());
                return ps;
            }
        }, keyHolder);
        final int paymentId = keyHolder.getKey().intValue();

        jdbcTemplate.batchUpdate("insert into payment_goods(payment_id,dish_id,dish_name,price,`count`,status) values (?,?,?,?,?,0)", new BatchPreparedStatementSetter() {

            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                PaymentGoods goods = payment.getGoods().get(i);
                ps.setInt(1, paymentId);
                ps.setInt(2, goods.getDishId());
                ps.setString(3, goods.getName());
                ps.setInt(4, goods.getPriceValue());
                ps.setInt(5, goods.getCount());
            }

            @Override
            public int getBatchSize() {
                return payment.getGoods().size();
            }
        });

        jdbcTemplate.batchUpdate("insert into payment_promotion(payment_id,pattern,param0,param1,param2,param3,money) values (?,?,?,?,?,?,?)", new BatchPreparedStatementSetter() {

            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                PromotionResponse promotion = payment.getPromotions().get(i);
                ps.setInt(1, paymentId);
                ps.setString(2, promotion.getTag());
                int[] promotionParams = promotion.getParams();
                ps.setInt(3, promotionParams[0]);
                ps.setInt(4, promotionParams[1]);
                ps.setInt(5, promotionParams[2]);
                ps.setInt(6, promotionParams[3]);
                ps.setInt(7, promotion.getDiscount());
            }

            @Override
            public int getBatchSize() {
                return payment.getPromotions().size();
            }
        });
        return paymentId;
    }

    /**
     * 修改订单状态
     *
     * @param paymentId
     * @param status
     * @param statusDesc
     */
    public int updateOrderStatus(int paymentId, int status, String statusDesc, Integer cost, String alipayNo) {
        StringBuilder sql = new StringBuilder();
        ArrayList<Object> params = new ArrayList<>();
        params.add(status);
        params.add(statusDesc);
        sql.append("update payment set status=?,status_desc=?");
        if (cost != null) {
            sql.append(",paid=?");
            params.add(cost);
        }
        if (status == 5) {
            sql.append(",ready_time=now()");
        } else if (status == 6) {
            sql.append(",assign_time=now()");
        } else if (status == 2) {
            sql.append(",alipay_no=?");
            params.add(alipayNo);
        }

        sql.append(" where id=?");
        params.add(paymentId);

        return jdbcTemplate.update(sql.toString(), params.toArray());
    }

    /**
     * 新增状态改变明细
     *
     * @param orderno
     * @param status
     * @param descption
     */
    public int insertToPaymentTime(int paymentId, int status, String description, Integer cost) {
        String sql = " INSERT INTO payment_times(payment_id, STATUS,create_time,description,cost_change) values(?,?,now(),?,?)";
        return jdbcTemplate.update(sql, paymentId, status, description, cost == null ? 0 : cost);

    }

    @Cacheable(value = "payment", key = "#orderno", unless = "#result == null")
    public Payment selectPayment(String orderno) {
        if (StringUtils.isBlank(orderno)) {
            return null;
        }
        List<Payment> list = jdbcTemplate.query("select p.id,p.orderno,p.deliver_id,p.courier_id,p.address_id,p.customer_id,p.region_id,p.restaurant_id\n"
                + ",rest.`name` restaurant_name,rest.location restLocation,rest.tel restTel,rest.icon restIcon\n"
                + ",p.package,p.delivery,p.status,p.memo,p.cash,p.score,p.create_time,p.ready_time,p.assign_time,p.star,p.comment\n"
                + ",p.editable,p.paid,p.alipay_no,p.vip,p.shared\n"
                + ",addr.id addrId,addr.`name` addrName,addr.mobile addrMobile,addr.location,addr.longitude,addr.latitude\n"
                + "from payment p\n"
                + "left join restaurant rest on p.restaurant_id=rest.id\n"
                + "left join cust_address addr on p.address_id=addr.id\n"
                + "where p.orderno=?", new RowMapper<Payment>() {

                    @Override
                    public Payment mapRow(ResultSet rs, int rowNum) throws SQLException {
                        Payment p = new Payment();
                        p.setId(rs.getInt("id"));
                        p.setOrderno(rs.getString("orderno"));
                        p.setDeliverId(rs.getInt("deliver_id"));
                        p.setCourierId(rs.getInt("courier_id"));
                        Employee courier = employeeService.getEmployee(p.getCourierId());
                        if (courier != null) {
                            p.setCourierName(courier.getName());
                            p.setCourierNumber(courier.getUsername());
                            p.setCourierContact(courier.getMobile());
                        }
                        p.setAddressId(rs.getInt("address_id"));
                        p.setCustomerId(rs.getInt("customer_id"));
                        p.setRegionId(rs.getInt("region_id"));
                        p.setRestaurantId(rs.getInt("restaurant_id"));
                        p.setRestaurantName(rs.getString("restaurant_name"));
                        p.setRestaurantImg(restaurantIcon.toThumbmailsMD(rs.getInt("restIcon")));
                        p.setRestaurantLocation(rs.getString("restLocation"));
                        p.setRestaurantContact(rs.getString("restTel"));
                        p.setPackageFeeValue(rs.getInt("package"));
                        p.setDeliveryValue(rs.getInt("delivery"));
                        p.setStatus(rs.getInt("status"));
                        p.setMemo(rs.getString("memo"));
                        p.setCash(rs.getInt("cash"));
                        p.setScore(rs.getInt("score"));
                        p.setEditable(rs.getBoolean("editable"));
                        p.setPaid(rs.getInt("paid"));
                        p.setAlipayNo(rs.getString("alipay_no"));
                        p.setVip(rs.getBoolean("vip"));
                        p.setShared(rs.getInt("shared"));

                        Date createDate = rs.getTimestamp("create_time");
                        Date readyDate = rs.getTimestamp("ready_time");
                        Date assignDate = rs.getTimestamp("assign_time");

                        p.setCreateTime(createDate == null ? null : DateFormatUtils.format(createDate, "yyyy-MM-dd HH:mm:ss"));
                        p.setReadyTime(readyDate == null ? null : DateFormatUtils.format(readyDate, "yyyy-MM-dd HH:mm:ss"));
                        p.setAssignTime(assignDate == null ? null : DateFormatUtils.format(assignDate, "yyyy-MM-dd HH:mm:ss"));

                        p.setStar(rs.getInt("star"));
                        p.setComment(rs.getString("comment"));

                        CustAddress addr = new CustAddress();
                        addr.setId(rs.getInt("addrId"));
                        addr.setName(rs.getString("addrName"));
                        addr.setCustomerId(p.getCustomerId());
                        addr.setLatitudeValue(rs.getInt("latitude"));
                        addr.setLongitudeValue(rs.getInt("longitude"));
                        addr.setLocation(rs.getString("location"));
                        addr.setMobile(rs.getString("addrMobile"));
                        p.setAddress(addr);
                        return p;
                    }

                }, orderno);
        if (list.isEmpty()) {
            return null;
        }
        Payment payment = list.get(0);

        //查询订单的菜品
        List<PaymentGoods> goods = jdbcTemplate.query("select g.id,g.payment_id,g.dish_id,g.dish_name,g.price,g.count,g.status,c.id categoryId,c.`name` categoryName,d.image\n"
                + "from payment_goods g\n"
                + "left join dish d on g.dish_id=d.id\n"
                + "left join dish_category c on d.category_id=c.id\n"
                + "where g.payment_id=?", new RowMapper<PaymentGoods>() {

                    @Override
                    public PaymentGoods mapRow(ResultSet rs, int rowNum) throws SQLException {
                        PaymentGoods g = new PaymentGoods();
                        g.setId(rs.getInt("id"));
                        g.setPaymentId(rs.getInt("payment_id"));
                        g.setDishId(rs.getInt("dish_id"));
                        g.setName(rs.getString("dish_name"));
                        g.setPriceValue(rs.getInt("price"));
                        g.setCount(rs.getInt("count"));
                        g.setStatus(rs.getInt("status"));
                        g.setDishCategoryId(rs.getInt("categoryId"));
                        g.setDishCategoryName(rs.getString("categoryName"));
                        g.setIcon(dishImg.toThumbmailsXS(rs.getInt("image")));
                        return g;
                    }

                }, payment.getId());
        payment.setGoods(goods);

        //查询订单的优惠政策
        List<PromotionResponse> promotions = jdbcTemplate.query("SELECT * FROM payment_promotion where payment_id=?", new RowMapper<PromotionResponse>() {

            @Override
            public PromotionResponse mapRow(ResultSet rs, int rowNum) throws SQLException {
                PromotionResponse promotion = new PromotionResponse();
                promotion.setId(rs.getInt("id"));
                promotion.setTag(rs.getString("pattern"));
                promotion.setParams(new int[]{rs.getInt("param0"), rs.getInt("param1"), rs.getInt("param2"), rs.getInt("param3")});
                promotion.setDiscount(rs.getInt("money"));
                return promotion;
            }

        }, payment.getId());
        payment.setPromotions(promotions);
        return payment;
    }

    /**
     * 根据用户id查询订单
     */
    public Collection<Payment> selectPayments4Principal(int principal, int start, int size) {
        final Map<Integer, Payment> payments = new LinkedHashMap<Integer, Payment>();
        jdbcTemplate.query("select p.id,p.orderno,p.deliver_id,p.courier_id,p.address_id,p.customer_id,p.region_id,p.restaurant_id\n"
                + ",rest.`name` restaurant_name,rest.location restLocation,rest.tel restTel,rest.icon restIcon\n"
                + ",p.package,p.delivery,p.status,p.memo,p.cash,p.score,p.create_time,p.ready_time,p.assign_time,p.star,p.comment\n"
                + ",p.editable,p.paid,p.vip\n"
                + ",addr.id addrId,addr.`name` addrName,addr.mobile addrMobile,addr.location,addr.longitude,addr.latitude\n"
                + "from payment p\n"
                + "left join restaurant rest on p.restaurant_id=rest.id\n"
                + "left join cust_address addr on p.address_id=addr.id\n"
                + "where p.customer_id = ? and p.visible=1 order by p.create_time desc limit ?,?",
                new PaymentRowCallbackHandler(payments),
                principal,
                start,
                size);

        if (payments.isEmpty()) {
            return Collections.EMPTY_LIST;
        }
        fillPayments(payments);
        return payments.values();
    }

    /**
     * 取菜员订单列表.
     */
    public Collection<Payment> selectPayments4Deliver(int principal, String status, int start, int size) {
        Date now = new Date();
        Date timeBegin = DateUtils.truncate(now, Calendar.DATE);
        Date timeEnd = DateUtils.addDays(timeBegin, 1);
        final Map<Integer, Payment> payments = new LinkedHashMap<Integer, Payment>();
        jdbcTemplate.query("select p.id,p.orderno,p.deliver_id,p.courier_id,p.address_id,p.customer_id,p.region_id,p.restaurant_id\n"
                + ",rest.`name` restaurant_name,rest.location restLocation,rest.tel restTel,rest.icon restIcon\n"
                + ",p.package,p.delivery,p.status,p.memo,p.cash,p.score,p.create_time,p.ready_time,p.assign_time,p.star,p.comment\n"
                + ",p.editable,p.paid,p.vip\n"
                + ",addr.id addrId,addr.`name` addrName,addr.mobile addrMobile,addr.location,addr.longitude,addr.latitude\n"
                + "from payment p\n"
                + "left join restaurant rest on p.restaurant_id=rest.id\n"
                + "left join cust_address addr on p.address_id=addr.id\n"
                + "where p.deliver_id = ?\n"
                + "and (p.create_time between ? and ?)\n"
                + "and p.status in (" + status + ") limit ?,?",
                new PaymentRowCallbackHandler(payments),
                principal,
                timeBegin,
                timeEnd,
                start,
                size);

        if (payments.isEmpty()) {
            return Collections.EMPTY_LIST;
        }
        fillPayments(payments);
        return payments.values();
    }

    public int countPayment4Deliver(int principal, String status) {
        Date now = new Date();
        Date timeBegin = DateUtils.truncate(now, Calendar.DATE);
        Date timeEnd = DateUtils.addDays(timeBegin, 1);
        return jdbcTemplate.queryForObject("select count(1) from payment where deliver_id = ?"
                + " and (create_time between ? and ?)"
                + " and status in (" + status + ")", Integer.class, principal, timeBegin, timeEnd
        );
    }

    public int countCancelledPayment4Deliver(int principal) {
        Date now = new Date();
        Date timeBegin = DateUtils.truncate(now, Calendar.DATE);
        Date timeEnd = DateUtils.addDays(timeBegin, 1);
        return jdbcTemplate.queryForObject("select count(1) from payment where deliver_id = ?"
                + " and (create_time between ? and ?)"
                + " and status = 9", Integer.class, principal, timeBegin, timeEnd
        );
    }

    /**
     * 配送员订单列表.
     */
    public Collection<Payment> selectPayments4Courier(int principal, String status, int start, int size) {
        Date now = new Date();
        Date timeBegin = DateUtils.truncate(now, Calendar.DATE);
        Date timeEnd = DateUtils.addDays(timeBegin, 1);
        final Map<Integer, Payment> payments = new LinkedHashMap<Integer, Payment>();
        jdbcTemplate.query("select p.id,p.orderno,p.deliver_id,p.courier_id,p.address_id,p.customer_id,p.region_id,p.restaurant_id\n"
                + ",rest.`name` restaurant_name,rest.location restLocation,rest.tel restTel,rest.icon restIcon\n"
                + ",p.package,p.delivery,p.status,p.memo,p.cash,p.score,p.create_time,p.ready_time,p.assign_time,p.star,p.comment\n"
                + ",p.editable,p.paid,p.vip\n"
                + ",addr.id addrId,addr.`name` addrName,addr.mobile addrMobile,addr.location,addr.longitude,addr.latitude\n"
                + "from payment p\n"
                + "left join restaurant rest on p.restaurant_id=rest.id\n"
                + "left join cust_address addr on p.address_id=addr.id\n"
                + "where p.courier_id = ?\n"
                + "and (p.create_time between ? and ?)\n"
                + "and p.status in (" + status + ") limit ?,?",
                new PaymentRowCallbackHandler(payments),
                principal,
                timeBegin,
                timeEnd,
                start,
                size);

        if (payments.isEmpty()) {
            return Collections.EMPTY_LIST;
        }
        fillPayments(payments);
        return payments.values();
    }

    public int countPayment4Courier(int principal, String status) {
        Date now = new Date();
        Date timeBegin = DateUtils.truncate(now, Calendar.DATE);
        Date timeEnd = DateUtils.addDays(timeBegin, 1);
        return jdbcTemplate.queryForObject("select count(1) from payment where courier_id = ?"
                + " and (create_time between ? and ?)"
                + " and status in (" + status + ")", Integer.class, principal, timeBegin, timeEnd
        );
    }

    public int countCancelledPayment4Courier(int principal) {
        Date now = new Date();
        Date timeBegin = DateUtils.truncate(now, Calendar.DATE);
        Date timeEnd = DateUtils.addDays(timeBegin, 1);
        return jdbcTemplate.queryForObject("select count(1) from payment where courier_id = ?"
                + " and (create_time between ? and ?)"
                + " and status = 9", Integer.class, principal, timeBegin, timeEnd
        );
    }

    /**
     * 分配员订单列表. 目前系统中只有一个分配员，不作查询处理，后期增加服务中心时需修改。
     */
    public Collection<Payment> selectPayments4Center(int principal, String status, int start, int size) {
        Date now = new Date();
        Date timeBegin = DateUtils.truncate(now, Calendar.DATE);
        Date timeEnd = DateUtils.addDays(timeBegin, 1);
        final Map<Integer, Payment> payments = new LinkedHashMap<Integer, Payment>();
        jdbcTemplate.query("select p.id,p.orderno,p.deliver_id,p.courier_id,p.address_id,p.customer_id,p.region_id,p.restaurant_id\n"
                + ",rest.`name` restaurant_name,rest.location restLocation,rest.tel restTel,rest.icon restIcon\n"
                + ",p.package,p.delivery,p.status,p.memo,p.cash,p.score,p.create_time,p.ready_time,p.assign_time,p.star,p.comment\n"
                + ",p.editable,p.paid,p.vip\n"
                + ",addr.id addrId,addr.`name` addrName,addr.mobile addrMobile,addr.location,addr.longitude,addr.latitude\n"
                + "from payment p\n"
                + "left join restaurant rest on p.restaurant_id=rest.id\n"
                + "left join cust_address addr on p.address_id=addr.id\n"
                + "where 1=1\n"
                + "and (p.create_time between ? and ?)\n"
                + "and p.status in (" + status + ") limit ?,?",
                new PaymentRowCallbackHandler(payments),
                timeBegin,
                timeEnd,
                start,
                size);

        if (payments.isEmpty()) {
            return Collections.EMPTY_LIST;
        }
        fillPayments(payments);
        return payments.values();
    }

    /**
     * 计算分配员订单总数量. 目前系统中只有一个分配员，不作查询处理，后期增加服务中心时需修改。
     */
    public int countPayment4Center(int principal, String status) {
        Date now = new Date();
        Date timeBegin = DateUtils.truncate(now, Calendar.DATE);
        Date timeEnd = DateUtils.addDays(timeBegin, 1);
        return jdbcTemplate.queryForObject("select count(1) from payment where 1=1"
                + " and (create_time between ? and ?)"
                + " and status in (" + status + ")", Integer.class, timeBegin, timeEnd
        );
    }

    /**
     * 计算分配员订单列表中取消的订单数量. 目前系统中只有一个分配员，不作查询处理，后期增加服务中心时需修改。
     */
    public int countCancelledPayment4Center(int principal) {
        Date now = new Date();
        Date timeBegin = DateUtils.truncate(now, Calendar.DATE);
        Date timeEnd = DateUtils.addDays(timeBegin, 1);
        return jdbcTemplate.queryForObject("select count(1) from payment where 1=1"
                + " and (create_time between ? and ?)"
                + " and status = 9", Integer.class, timeBegin, timeEnd
        );
    }

    private void fillPayments(final Map<Integer, Payment> payments) {
        //封装查询条件 菜品和优惠政策 可共用
        Iterator<Integer> pids = payments.keySet().iterator();
        StringBuilder andPaymentWhere = new StringBuilder();
        ArrayList<Object> params = new ArrayList<Object>();
        andPaymentWhere.append(" WHERE sub.payment_id in (");
        while (pids.hasNext()) {
            andPaymentWhere.append("?,");
            params.add(pids.next());
        }
        andPaymentWhere.deleteCharAt(andPaymentWhere.length() - 1);
        andPaymentWhere.append(")");

        //查询订单的菜品
        jdbcTemplate.query("select sub.* from payment_goods sub left  join payment p on sub.payment_id = p.id" + andPaymentWhere.toString(), new RowCallbackHandler() {

            @Override
            public void processRow(ResultSet rs) throws SQLException {
                PaymentGoods g = new PaymentGoods();
                g.setId(rs.getInt("id"));
                g.setPaymentId(rs.getInt("payment_id"));
                g.setDishId(rs.getInt("dish_id"));
                g.setName(rs.getString("dish_name"));
                g.setPriceValue(rs.getInt("price"));
                g.setCount(rs.getInt("count"));
                g.setStatus(rs.getInt("status"));
                payments.get(g.getPaymentId()).addGoods(g);
            }

        }, params.toArray());

        //查询订单的优惠政策
        jdbcTemplate.query("SELECT sub.* FROM payment_promotion sub LEFT JOIN payment p ON sub.payment_id = p.id" + andPaymentWhere.toString(), new RowCallbackHandler() {

            @Override
            public void processRow(ResultSet rs) throws SQLException {
                PromotionResponse promotion = new PromotionResponse();
                promotion.setTag(rs.getString("pattern"));
                promotion.setParams(new int[]{rs.getInt("param0"), rs.getInt("param1"), rs.getInt("param2"), rs.getInt("param3")});
                promotion.setDiscount(rs.getInt("money"));
                int paymentId = rs.getInt("payment_id");
                payments.get(paymentId).addPromotion(promotion);
            }

        }, params.toArray());
    }

    public void updateGoodsItemStatus(int paymentId, int dishId, int status) {
        jdbcTemplate.update("update payment_goods set status=? where payment_id=? and dish_id=?", status, paymentId, dishId);
    }

    public void updatePaymentDeliver(int deliverId, int id) {
        jdbcTemplate.update("update payment set deliver_id=?,assign_time=now() where id=?", deliverId, id);
    }

    public void updatePaymentCourier(int courierId, int id) {
        jdbcTemplate.update("update payment set courier_id=? where id=?", courierId, id);
    }

    public void updatePaymentOrderno(int id, String newOrderno) {
        jdbcTemplate.update("update payment set orderno=? where id =?", newOrderno, id);
    }

    public void updateEditable(int id, boolean editable) {
        jdbcTemplate.update("update payment set editable=? where id=?", editable, id);
    }

    @CacheEvict(value = "payment", key = "#orderno")
    public void deletePayment(int id, String orderno) {
        jdbcTemplate.update("delete from payment where id=?", id);
    }

    public void updatePaymentVisiblily(int id, boolean visible) {
        jdbcTemplate.update("update payment set visible=? where id=?", visible, id);
    }

    public void updatePaymentRegionId(int id, int regionId) {
        jdbcTemplate.update("update payment set region_id=? where id=?", regionId, id);
    }

    public void updatePaymentPaid(int id, int paid) {
        jdbcTemplate.update("update payment set paid=? where id=?", paid, id);
    }

    private class PaymentRowCallbackHandler implements RowCallbackHandler {

        private final Map<Integer, Payment> payments;

        public PaymentRowCallbackHandler(Map<Integer, Payment> payments) {
            this.payments = payments;
        }

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            Payment p = new Payment();
            p.setId(rs.getInt("id"));
            p.setOrderno(rs.getString("orderno"));
            p.setDeliverId(rs.getInt("deliver_id"));
            p.setCourierId(rs.getInt("courier_id"));
            Employee courier = employeeService.getEmployee(p.getCourierId());
            if (courier != null) {
                p.setCourierName(courier.getName());
                p.setCourierNumber(courier.getUsername());
                p.setCourierContact(courier.getMobile());
            }
            p.setAddressId(rs.getInt("address_id"));
            p.setCustomerId(rs.getInt("customer_id"));
            p.setRegionId(rs.getInt("region_id"));
            p.setRestaurantId(rs.getInt("restaurant_id"));
            p.setRestaurantName(rs.getString("restaurant_name"));
            p.setRestaurantLocation(rs.getString("restLocation"));
            p.setRestaurantImg(restaurantIcon.toThumbmailsMD(rs.getInt("restIcon")));
            p.setRestaurantContact(rs.getString("restTel"));
            p.setPackageFeeValue(rs.getInt("package"));
            p.setDeliveryValue(rs.getInt("delivery"));
            p.setStatus(rs.getInt("status"));
            p.setMemo(rs.getString("memo"));
            p.setCash(rs.getInt("cash"));
            p.setScore(rs.getInt("score"));
            p.setEditable(rs.getBoolean("editable"));
            p.setPaid(rs.getInt("paid"));
            p.setCost(1.0 * p.getPaid() / 100);
            p.setVip(rs.getBoolean("vip"));

            Date createDate = rs.getTimestamp("create_time");
            Date readyDate = rs.getTimestamp("ready_time");
            Date assignDate = rs.getTimestamp("assign_time");

            p.setCreateTime(createDate == null ? null : DateFormatUtils.format(createDate, "yyyy-MM-dd HH:mm:ss"));
            p.setReadyTime(readyDate == null ? null : DateFormatUtils.format(readyDate, "yyyy-MM-dd HH:mm:ss"));
            p.setAssignTime(assignDate == null ? null : DateFormatUtils.format(assignDate, "yyyy-MM-dd HH:mm:ss"));

            p.setStar(rs.getInt("star"));
            p.setComment(rs.getString("comment"));

            CustAddress addr = new CustAddress();
            addr.setId(rs.getInt("addrId"));
            addr.setName(rs.getString("addrName"));
            addr.setCustomerId(p.getCustomerId());
            addr.setLatitudeValue(rs.getInt("latitude"));
            addr.setLongitudeValue(rs.getInt("longitude"));
            addr.setLocation(rs.getString("location"));
            addr.setMobile(rs.getString("addrMobile"));
            p.setAddress(addr);

            payments.put(p.getId(), p);
        }

    }

    /**
     * 分享订单获得代金券
     *
     * @param value
     * @param orderno
     */
    public void sharePayment(int value, String orderno) {
        jdbcTemplate.update("UPDATE payment SET shared=? WHERE orderno = ?;", value, orderno);
    }

    /**
     * 是否可领取
     *
     * @param principal
     * @param orderno
     * @return
     */
    public int checkReceive(int principal, String orderno) {
        return jdbcTemplate.queryForObject("SELECT count(1) from share_record where orderno=? and customer_id = ?", Integer.class, orderno, principal);
    }

    public void shareReceive(int principal, String orderno, int value) {
        jdbcTemplate.update("INSERT INTO share_record(customer_id,orderno,cash_value) VALUES(?,?,?) ", principal, orderno, value);
    }
}
