package com.hyl.teddy.apis.restaurant.web;

import com.hyl.teddy.apis.entity.Dish;
import java.util.ArrayList;
import java.util.List;

/**
 * 菜品类别 dish_category
 *
 * @author Administrator
 *
 */
public class DishCategoryResponse {

    private int id;
    private String name;
    /**
     * 菜品
     */
    private List<Dish> dishes = new ArrayList<Dish>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(List<Dish> dishes) {
        this.dishes = dishes;
    }

    public void addDish(Dish dish) {
        dishes.add(dish);
    }
}
