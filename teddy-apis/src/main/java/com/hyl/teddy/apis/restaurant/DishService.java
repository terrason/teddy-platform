package com.hyl.teddy.apis.restaurant;

import com.hyl.teddy.apis.entity.Dish;
import com.hyl.teddy.apis.profile.FavoritesService;
import com.hyl.teddy.apis.restaurant.dao.DishDao;
import com.hyl.teddy.apis.restaurant.web.DishResponse;
import com.hyl.teddy.apis.restaurant.web.RestaurantDishResponse;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * 菜品Service
 *
 * @author Administrator
 */
@Service
public class DishService {

    @Resource
    private DishDao dao;
    @Resource
    private FavoritesService favoritesService;

    /**
     * 餐厅菜品
     *
     * @param restaurant
     * @return
     */
    public RestaurantDishResponse queryRestaurantDish(int restaurant) {
        return dao.queryRestaurantDish(restaurant);
    }

    /**
     * 菜品全局搜索
     *
     * @param keyword
     * @return
     */
    public List<SearchDish> searchDish(String keyword) {
        return dao.searchDish(keyword);
    }

    /**
     * 餐厅菜品搜索
     *
     * @param restaurant
     * @param keyword
     * @return
     */
    public List<Dish> dishSearch(int restaurant, String keyword) {
        return dao.dishSearch(restaurant, keyword);
    }

    /**
     * 菜品详情
     *
     * @param id
     * @param principal
     * @return
     */
    public DishResponse queryDish(int id, int principal) {
        DishResponse dish = dao.selectDishWithPrincipal(id);
        dish.setFavored(favoritesService.isCustomerFavoredDish(principal, id));
        return dish;
    }
}
