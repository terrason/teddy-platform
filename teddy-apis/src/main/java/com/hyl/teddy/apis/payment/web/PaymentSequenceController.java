/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hyl.teddy.apis.payment.web;

import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hyl.teddy.apis.payment.PaymentService;

/**
 *
 * @author Administrator
 */
@Controller
public class PaymentSequenceController {

    @Resource
    private PaymentService paymentService;

    @RequestMapping(value = "/payment/sequence")
    public String nextSequence() {
        return paymentService.getAndIncreaseSequence();
    }
}
