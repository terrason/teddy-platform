package com.hyl.teddy.apis.system.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.hyl.teddy.apis.system.VersionCustomer;
import java.util.HashMap;
import org.springframework.cache.annotation.Cacheable;

@Repository
public class SystemDao {

    @Resource
    private JdbcTemplate jdbcTemplate;

    public VersionCustomer selectVersionForUpgrade(int code) {
        List<VersionCustomer> list = jdbcTemplate.query("select id,version,code,log,download_url,download_times from version_customer where code > ? order by code desc limit 1", new RowMapper<VersionCustomer>() {

            @Override
            public VersionCustomer mapRow(ResultSet rs, int rowNum) throws SQLException {
                VersionCustomer version = new VersionCustomer();
                version.setId(rs.getInt("id"));
                version.setCode(rs.getInt("code"));
                version.setDownloadId(rs.getInt("download_url"));
                version.setDownloadTimes(rs.getString("download_times"));
                version.setLog(rs.getString("log"));
                version.setVersion(rs.getString("version"));
                return version;
            }
        }, code);
        return list.isEmpty() ? null : list.get(0);
    }

    @Cacheable("static_resource")
    public Map<String, String> selectStaticResource() {
        final Map<String, String> configurations = new HashMap<>();
        jdbcTemplate.query("select code,content from static_resource", new RowCallbackHandler() {

            @Override
            public void processRow(ResultSet rs) throws SQLException {
                configurations.put(rs.getString("code"), rs.getString("content"));
            }
        }, (Object[]) null);
        return configurations;
    }

}
