<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title><%= documentTitle %></title>
<link rel="stylesheet" href="resource/style.css" />
<link rel="stylesheet" href="resource/font-awesome.css" />
<script type="text/javascript" src="resource/main.js?config=TeX-AMS_HTML"></script>
<script type="text/javascript" src="resource/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="resource/scrolltopcontrol.js"></script>
</head>
<body><div class="container"><%= documentHTML %></div></body>
</html>