[toc]

# 泰迪熊点餐接口文档

返回码说明
```
{
    code://返回代号：0成功；其它 失败
    message://提示信息
    data://接口需求的具体数据
}
```

## 登录注册相关接口

### 获取短信验证码

**接口地址** 
`GET`   `/auth/smscode`
> eg.      http://www.hylapp.com:10001/apis/auth/smscode

**请求参数**   
参数|数据类型|必填|参数说明
---|-------|----|-----
mobile	|string	|<i class="fa fa-check"></i>|接收短信的手机号码

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
502|与短信接口通讯异常|短信验证码发送失败

**响应结果:**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data": "aba25484-c9db-403c-810d-5da308fe48aa" //短信验证码token值
}
```

### 用户注册

**接口地址** 
`POST`   `/auth/register`

**请求参数:**  
|参数|数据类型|必填|参数说明|   
|-|-|-|-|
|mobile|String|<i class="fa fa-check"></i>|手机号|
|password|String|<i class="fa fa-check"></i>|密码|
|smscode|String|<i class="fa fa-check"></i>|验证码|
|token|String|<i class="fa fa-check"></i>|验证码唯一票据|

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
424|短信验证码错误|验证码错误
409|手机号重复|您的手机号码已注册过，请直接登录哦~

**返回参数:**
```javascript
{
	code:0, //0-表示成功 其它表示失败
	message: null,
	data: CustomerInfo
}
```
[另请参考`CustomerInfo` 说明](#customerinfo)


###用户登录
客户登录分为密码登录和短信验证码登录两种方式。后者在找不到注册用户的情形下，自动注册为初级会员。

**接口地址** 
`POST`   `/auth/login`

**请求参数**  
|参数|数据类型|必填|参数说明|   
|-|-|-|-|
|mobile|String|<i class="fa fa-check"></i>|手机号|
|password|String|<i class="fa fa-exclamation">1</i>|密码，传入该参数表示使用密码方式登录|
|smscode|String|<i class="fa fa-exclamation">2</i>|验证码，验证码方式登录时必填|
|token|String|<i class="fa fa-exclamation">2</i>|验证码唯一票据，验证码方式登录时必填|
|pushKey|String|<i class="fa fa-check"></i>|Jpush设备ID|

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
404|密码登录时手机号码不存在|用户不存在
424|密码错误或验证码错误|密码错误/验证码错误
403|用户被禁用|帐号被禁用

**返回参数**  
```javascript
{
	code:0, //0-表示成功 其它表示失败
	message: null,
	data: CustomerInfo
}
```
[另请参考`CustomerInfo` 说明](#customerinfo)

###忘记密码

**接口地址** 
`POST`   `/auth/passwd`

**请求参数**  
|参数|数据类型|必填|参数说明|   
|-|-|-|-|
|mobile|String|<i class="fa fa-check"></i>|手机号|
|password|String|<i class="fa fa-check"></i>|密码|
|smscode|String|<i class="fa fa-check"></i>|验证码|
|token|String|<i class="fa fa-check"></i>|验证码唯一票据|

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
404|手机号码不存在|用户不存在
424|验证码错误|验证码错误
403|用户被禁用|帐号被禁用

**返回参数**  
```javascript
{
	code:0, //0-表示成功 其它表示失败
	message: null,
	data: CustomerInfo
}
```
[另请参考`CustomerInfo` 说明](#customerinfo)


## 个人中心

### 我的积分(已开通)

**接口地址** 
`GET`   `/profile/score`
> eg. http://www.hylapp.com:10001/apis/profile/score

**请求参数:**  
参数|数据类型|必填|参数说明
---|-------|----|-----
|principal|int|<i class="fa fa-check"></i>|当前登录用户ID|

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
401|未传入操作人ID|您需要重新登录才能继续操作哦~
404|用户不存在|系统繁忙，请稍候再试……

**响应结果:**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":1003 //用户当前积分
}
```

### 我的代金券(已开通)

**接口地址** 
`GET`   `/profile/cash`
> eg. http://www.hylapp.com:10001/apis/profile/cash

**请求参数:**  
参数|数据类型|必填|参数说明
---|-------|----|-----
principal|int|<i class="fa fa-check"></i>|当前登录用户ID

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
401|未传入操作人ID|您需要重新登录才能继续操作哦~
404|用户不存在|系统繁忙，请稍候再试……

**响应结果:**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":[ //代金券
        {
            id:2,  //代金券类型
            value:5,//代金券面值，￥5
            name:"5元代金券", //代金券名字
            quantity:1 //代金券数量
        },
        ...
    ]
}
```

### 我的消息(已开通)

**接口地址** 
`GET`   `/profile/message`
> eg. http://www.hylapp.com:10001/apis/profile/message

**请求参数:**  
参数|数据类型|必填|参数说明
---|-------|----|-----
|principal|int|<i class="fa fa-check"></i>|当前登录用户ID|

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
401|未传入操作人ID|您需要重新登录才能继续操作哦~
404|用户不存在|系统繁忙，请稍候再试……

**响应结果:**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":[ //消息列表
        {
            id:5,
            title:"代金券领取", //代金券名字
            createTime:"2014-09-09"
        },
        ...
    ]
}
```


###修改密码(已开通)

**接口地址** 
`POST`   `/profile/passwd`

**请求参数**  
|参数|数据类型|必填|参数说明|   
|-|-|-|-|
|principal|int|<i class="fa fa-check"></i>|当前登录用户ID|
|pwdold|String||原密码|
|pwdnew|String|<i class="fa fa-check"></i>|新密码|
**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
401|用户不存在|您需要重新登录才能继续操作哦~
424|密码错误|密码错误
403|用户被禁用|帐号被禁用

**返回参数**  
```javascript
{
	code:0, //0-表示成功 其它表示失败
	message: null,
	data: null
}
```

###修改头像(已开通)

**接口地址** 
`POST`   `/profile/avatar`

**请求参数:**  
参数|数据类型|必填|参数说明
---|-------|----|-----
|principal|int|<i class="fa fa-check"></i>|当前登录用户ID|
|avatar|MultipartFile|<i class="fa fa-check"></i>|头像|

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
401|未传入操作人ID|您需要重新登录才能继续操作哦~

**响应结果:**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":null
}
```

###修改昵称(已开通)

**接口地址** 
`POST`   `/profile/nickname`

**请求参数:**  
参数|数据类型|必填|参数说明
---|-------|----|-----
|principal|int|<i class="fa fa-check"></i>|当前登录用户ID|
|nickname|String|<i class="fa fa-check"></i>|用户昵称|

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
401|未传入操作人ID|您需要重新登录才能继续操作哦~

**响应结果:**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":null
}
```

### 收货地址(已开通)

**接口地址** 
`GET`   `/profile/address`
> eg. http://www.hylapp.com:10001/apis/profile/address

**请求参数:**  
参数|数据类型|必填|参数说明
---|-------|----|-----
|principal|int|<i class="fa fa-check"></i>|当前登录用户ID|

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
401|未传入操作人ID|您需要重新登录才能继续操作哦~

**响应结果:**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":[ //送餐地址
	    {
		    id:23,  //送餐地址ID, 订餐时传入此参数
            location:"合肥市 政务区 潜山路与东流路交叉口蔚蓝商务港B坐1406",
		    name:"萨达姆",
		    mobile:"18919601457",
		    defaultAddress:true//是否是默认送餐地址
	    },
	    ...
    ]
}
```
[另请参考 `提交订单`](#提交订单)

### 新增送餐地址(已开通)
如果当前无登录用户，应引导用户登录/注册。

**接口地址** 
`POST`   `/profile/address`

**请求参数:**  
参数|数据类型|必填|参数说明
---|-------|----|-----
|principal|int|<i class="fa fa-check"></i>|当前登录用户ID|
|name|String|<i class="fa fa-check"></i>|姓名|
|mobile|String|<i class="fa fa-check"></i>|手机号码|
|location|String|<i class="fa fa-check"></i>|送餐地址|
|longitude|number|<i class="fa fa-check"></i>|地图坐标--经度|
|latitude|number|<i class="fa fa-check"></i>|地图坐标--纬度|

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
401|未传入操作人ID|您需要重新登录才能继续操作哦~
415|暂不支持的送餐地址|请谅解，目前暂不能配送该区域，敬请期待！

**响应结果:**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":16 //送餐地址ID，可以传入订餐接口
}
```

### 修改送餐地址(已开通)

**接口地址** 
`POST`   `/profile/address/{id}`

**路径参数**
`id` 送餐地址ID

**请求参数:**  
参数|数据类型|必填|参数说明
---|-------|----|-----
|principal|int|<i class="fa fa-check"></i>|当前登录用户ID|
|name|String|<i class="fa fa-check"></i>|姓名|
|mobile|String|<i class="fa fa-check"></i>|手机号码|
|location|String|<i class="fa fa-check"></i>|送餐地址|
|longitude|number|<i class="fa fa-check"></i>|地图坐标--经度|
|latitude|number|<i class="fa fa-check"></i>|地图坐标--纬度|

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
401|未传入操作人ID|您需要重新登录才能继续操作哦~
404|送餐地址ID不存在|系统繁忙，请稍候再试……
415|暂不支持的送餐地址|请谅解，目前暂不能配送该区域，敬请期待！

**响应结果:**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":null
}
```

### 删除送餐地址(已开通)

**接口地址** 
`DELETE`   `/profile/address/{id}`

**路径参数**
`id` 送餐地址ID

**请求参数:**  
参数|数据类型|必填|参数说明
---|-------|----|-----
|principal|int|<i class="fa fa-check"></i>|当前登录用户ID|

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
401|未传入操作人ID|您需要重新登录才能继续操作哦~
404|送餐地址ID不存在|系统繁忙，请稍候再试……

**响应结果:**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":null
}
```

### 设置默认送餐地址(已开通)

**接口地址** 
`POST`   `/profile/address/default`

**请求参数:**  
参数|数据类型|必填|参数说明
---|-------|----|-----
|principal|int|<i class="fa fa-check"></i>|当前登录用户ID|
|id|int|<i class="fa fa-check"></i>|默认送餐地址ID|

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
401|未传入操作人ID|您需要重新登录才能继续操作哦~
404|送餐地址ID不存在|系统繁忙，请稍候再试……

**响应结果:**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":null
}
```

### 我的订单(已开通)

**接口地址** 
`GET`   `/profile/bought`
> eg. http://www.hylapp.com:10001/apis/profile/bought

**请求参数:**  
参数|数据类型|必填|参数说明
---|-------|----|-----
|principal|int|<i class="fa fa-check"></i>|当前登录用户ID|
|start   |int|           |分页参数——起始位置 默认：`0`
|size    |int|           |分页参数——分页大小 默认：`10`

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
401|未传入操作人ID|您需要重新登录才能继续操作哦~
404|用户不存在|系统繁忙，请稍候再试……

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":[ //订单列表
        {
            id:5,
            title:"聚实惠餐厅",
            content:"鸭腿饭，鸡腿饭，鸡腿烧鸭腿，青辣椒炒红辣椒，鸡蛋炖鸭蛋",
            cost:60.5,
            createTime:"2014-09-09 10:50",
            "status": 2,//状态1-待支付 2-已下单 3-已接单 4-配送中 5-已完成 6-已取消
            "statusText": "已下单",//状态说明
            commented : false //是否已评价,
            restaurantImg:"http://teddy.hylapp.com/static/restaurant/default.png"
        },
        ...
    ]
}
```

### 订单状态(已开通)

**接口地址** 
`GET` `/profile/bought-timeline/{orderno}`
> eg. http://www.hylapp.com:10001/apis/profile/bought-timeline/140918000021

**路径参数**
`orderno` `String` 需要查看的订单号

**请求参数** 
无

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
404|订单不存在|系统繁忙，请稍候再试……

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":{
	    createTime:"2014-09-10 09:40",//订餐时间 评论时可作参数传递
	    status : 5,//状态1-待支付 2-已下单 3-已接单 4-配送中 5-已完成 6-已取消
	    statusText:"已完成"//状态说明
	    statusDesc:"享用美食的同事，别忘了评价哦"
	    statusTime:"09:50:00"//订单状态最后改变时间 	
	    courierNumber : "PS001",//配送员编号 未配送时为null
	    courierMobile : "18918965551",//配送员手机号码 未配送时为null
	    steps:[
		    {
			    time:"10:23",//时刻
			    description:"熊仔正在配送，等待您的美食吧~"
		    },
		    {
			    time:"10:11",//时刻
			    description:"商家已接单，等待您的美食吧~"
		    },
		    {
			    time:"9:40",//时刻
			    description:"下单成功，等待商家接单"
		    }
	    ]
    }
}
```
[另请参考 `评论`](#评论)

### 订单详情(已开通)

**接口地址** 
`GET`   `/profile/bought/{orderno}`
> eg. http://www.hylapp.com:10001/apis/profile/bought/140921000003

**路径参数**
`orderno` `String` 需要查看的订单号

**请求参数**  
参数|数据类型|必填|参数说明
---|-------|----|-----
|principal|int|<i class="fa fa-check"></i>|当前登录用户ID|

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
401|未传入操作人ID|您需要重新登录才能继续操作哦~
404|用户或订单不存在|系统繁忙，请稍候再试……

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":{ //订单详情
	    "orderno": "141016000002",//订单编号
	    "createTime": "2014-10-16 10:20:58",//订餐时间（下单时间） 评论时可作参数传递
	    "address": {//送餐地址信息
		    "id": 1,
		    "name": null,
		    "defaultAddress": false,
		    "mobile": "13665441452",
		    "location": "蔚蓝商务港B坐",
		    "longitude": 136.231221,
		    "latitude": 37.211234
		},
		"restaurantId": 1,
		"restaurantName": "几时回",
		"packageFee": 21,//打包费
		"delivery": 10,//配送费
		"vip": false,//是否为VIP订单（客户在下单时是否为VIP）
		"promotion": [//优惠信息
		    {
		        "tag": "COST_SCORE",
		        "params": [
		            50,
		            10,
		            1,
		            0
		        ],
		        "money": -12
		    },
		    ...
		],
		"editable": false,//是否可编辑 用于订单加菜
		"goods": {//订单交互部分
		    "principal": 1,//客户ID
		    "address": 1,//送餐地址ID
		    "restaurant": 1,//餐厅ID
		    "memo": "不要辣椒",//客户备注
		    "dishes": [//订单明细
		        {
		            "id": 104,//菜品明细ID
		            "name": "三杯鸡",//菜名
		            "count": 1,
		            "status": 0,//菜品明细状态 0-表示初始状态 1-已取菜 2-已退菜
		            "price": 23//菜品明细单价
		        },
		        ...
		    ],
		    "cash": 0,//客户使用的代金券面值 未使用代金券为0
		    "score": 120//客户使用的积分 未使用积分为0
		},
		"cost": 78
    }
}
```
[另请参考`订单详情`](#payment)
[另请参考`订单数据`](#goods)
[另请参考`优惠标签`](#优惠标签)

### 删除订单(已开通)
注意使用**DELETE**提交请求

**接口地址** 
`DELETE`   `/profile/bought/{orderno}`
> eg. http://www.hylapp.com:10001/apis/profile/bought/140921000003  

**请求参数**  
无

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
404|订单不存在|很抱歉，您正在查看的订单不存在，请刷新页面后重试！

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":null
}
```

### 订单加菜(已开通)

**接口地址** 
`POST` `/profile/bought-extra/{id}`

**路径参数**
`id` `String` 订单编号

**请求参数** 
参数|数据类型|必填|参数说明
---|-------|----|-----
goods|String|<i class="fa fa-check"></i>|订单详情(只包含新加的菜) JSON数据[参考`订单数据`](#goods)

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
406|订单数据无法解析|系统繁忙，请稍候再试……
416|订单对账错误|系统繁忙，请稍候再试……
415|暂不支持的送餐地址|请谅解，目前暂不能配送该区域，敬请期待！

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":{
	    fee:50,//正数表示需要加钱（支付的钱），0-不需要，负数表示返回用户的钱，不需要支付，提醒即可
	    orderno:"141023000045"//新的订单号
    }
}
```

### 我的收藏（餐厅）(已开通)

**接口地址** 
`GET`   `/profile/favorite/restaurant`
> eg. http://www.hylapp.com:10001/apis/profile/restaurant

**请求参数**  
参数|数据类型|必填|参数说明
---|-------|----|-----
|principal|int|<i class="fa fa-check"></i>|当前登录用户ID|

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
401|未传入操作人ID|您需要重新登录才能继续操作哦~
404|用户不存在|系统繁忙，请稍候再试……

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":[ //餐厅列表
        {
            id:5,
            name:"聚实惠餐厅",
            icon:"http://static.taddy.com/upfile/2014/9/10/lasjfoegaljoeg/icon-xs.png",
            star:9,//每2分表示一颗星，例如9表示4个半星。
            promotion:"10元起送，免配送费",
            price:30,
            sold:125,
            createTime:"2014-09-09 10:50",
            tag:["WIN_CASH","VIP_DISCOUNT","COST_CASH","COST_SCORE","FREE_POSTAGE","DEDUCT"],//优惠标签
            enabled:true//是否营业
        },
        ...
    ]
}
```
[另请参考`优惠标签`](#优惠标签)

### 我的收藏（菜品）(已开通)

**接口地址** 
`GET`   `/profile/favorite/dish`
> eg. http://www.hylapp.com:10001/apis/profile/restaurant

**请求参数**  
参数|数据类型|必填|参数说明
---|-------|----|-----
|principal|int|<i class="fa fa-check"></i>|当前登录用户ID|

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
401|未传入操作人ID|您需要重新登录才能继续操作哦~
404|用户不存在|系统繁忙，请稍候再试……

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":[ //菜品列表
        {
            id:5,
            restaurantId:14,//所属餐厅ID
            restaurantName:"聚食惠餐厅",//所属餐厅名字
            name:"猪排饭",
            icon:"http://static.taddy.com/upfile/2014/9/10/lasjfoegaljoeg/icon-xs.png",
            price:28,//价格
            enabled:true//是否有此菜
        },
        ...
    ]
}
```

### 添加到收藏(已开通)
收藏餐厅 、 收藏菜品

**接口地址** 
`POST`   `/profile/favorite`

**请求参数**  
参数|数据类型|必填|参数说明
---|-------|----|-----
|principal|int|<i class="fa fa-check"></i>|当前登录用户ID|
|restaurant|int|<i class="fa fa-exclamation">1</i>|收藏餐厅ID|
|dish|int|<i class="fa fa-exclamation">2</i>|收藏菜品ID|

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
401|未传入操作人ID|您需要重新登录才能继续操作哦~
208|重复收藏|您已经收藏过该餐厅

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":null
}
```
### 取消收藏(已开通)
收藏餐厅 、 收藏菜品

**接口地址** 
`POST`   `/profile/favorite`

**请求参数**  
参数|数据类型|必填|参数说明
---|-------|----|-----
|principal|int|<i class="fa fa-check"></i>|当前登录用户ID|
|restaurant|int|<i class="fa fa-exclamation">1</i>|收藏餐厅ID|
|dish|int|<i class="fa fa-exclamation">2</i>|收藏菜品ID|

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
401|未传入操作人ID|您需要重新登录才能继续操作哦~
209|尚未收藏|您还没有收藏过该餐厅

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":null
}
```
### 分享订单(已开通)

**接口地址** 
`GET`   `/payment/share`
> eg.   http://www.hylapp.com:10001/apis/payment/share

**请求参数**  
参数|数据类型|必填|参数说明
---|-------|----|-----
|orderno|String|<i class="fa fa-check"></i>|订单号|

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
404|订单创建者不存在|订单信息不合法
208|已分享|您已经分享过了

**响应结果**
```javascript
{
    code: 0, //0 - 接口调用成功，其他值表示失败
    message: "恭喜您，获得一张100元代金券",
    data:"http://www.tdxmeal.com/activity/share/141111111111"//领取代金券地址
}
```
### 查看意见反馈历史(已开通)

**接口地址** 
`GET`   `/profile/feedback`
> eg. http://www.hylapp.com:10001/apis/profile/restaurant

**请求参数:**  
参数|数据类型|必填|参数说明
---|-------|----|-----
|principal|int|<i class="fa fa-check"></i>|当前登录用户ID|

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
401|未传入操作人ID|您需要重新登录才能继续操作哦~
404|用户不存在|系统繁忙，请稍候再试……

**响应结果:**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":[ //意见反馈列表
        {
            id:5,
			content:"",
			createTime:"2014-09-10",
			replyContent:"我们会尽快改进",
			replyTime:"2014-09-11"
        },
        ...
    ]
}
```

### 意见反馈(已开通)

**接口地址** 
`POST`   `/profile/feedback`

**请求参数**  
参数|数据类型|必填|参数说明
---|-------|----|-----
|principal|int|<i class="fa fa-check"></i>|当前登录用户ID|
|content|String|<i class="fa fa-check"></i>|反馈内容|

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
401|未传入操作人ID|您需要重新登录才能继续操作哦~

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":{
            id:5,
            content:"工号0001的配送员服务态度好差！",//反馈内容
            createTime:"2014-09-10"
    }
}
```

## 订餐主要业务

### 餐厅广告位(已开通)

**接口地址** 
`GET` `/restaurant/ad`
> eg. http://www.hylapp.com:10001/apis/restaurant/ad

**请求参数** 
无

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":[//广告列表
	    {
		    id:1,
		    image:"http://static.taddy.com/upfile/2014/9/10/lasjfoegaljoeg/icon-xs.png",
		    url:"http://static.taddy.com/activity/12/815.html"//广告跳转地址，默认使用内嵌浏览器打开
	    },...
    ]
}
```

### 餐厅分类列表(已开通)
**接口地址** 
`GET` `/restaurant/category`
> eg. http://www.hylapp.com:10001/apis/restaurant/category

**请求参数** 
无

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":[//餐厅分类列表
	    {
		    id:1,
		    name:"中餐"
	    },
	    {
		    id:2,
		    name:"西餐"
	    },
	    {
		    id:3,
		    name:"韩式料理"
	    },...
    ]
}
```

### 餐厅列表(已开通)
可传入筛选条件.

**接口地址** 
`GET` `/restaurant`
> eg. http://www.hylapp.com:10001/apis/restaurant

**请求参数** 
参数|数据类型|必填|参数说明
---|-------|----|-----
|category|int||餐厅种类ID 不填或传入`0`表示本项筛选条件无效|
|delivery|int||配送方式 不填或传入`0`表示本项筛选条件无效<ul><li>1-熊仔配送</li> <li>2-商家配送</li></ul> 默认：1|
|orderby|int||排序方式 <ul><li>1-评价最好</li> <li>2-人均消费</li></ul> 默认：1|
|start   |int|           |分页参数——起始位置 默认：`0`
|size    |int|           |分页参数——分页大小 默认：`10`


**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
406|不支持的排序方式|系统繁忙，请稍候再试……

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":[//餐厅列表
	    {
            id:5,
            name:"聚实惠餐厅",
            icon:"http://static.taddy.com/upfile/2014/9/10/lasjfoegaljoeg/icon-xs.png",
            star:9,//每2分表示一颗星，例如9表示4个半星。
            promotion:"10元起送，免配送费",
            price:30,
            sold:125,
            createTime:"2014-09-09 10:50",
            tag:["WIN_CASH","VIP_DISCOUNT","COST_CASH","COST_SCORE","FREE_POSTAGE","DEDUCT"],//优惠标签
            enabled:true//是否营业
        },...
    ]
}
```
[另请参考`优惠标签`](#优惠标签)

### 餐厅菜品(已开通)

**接口地址** 
`GET` `/dish`
> eg. http://www.hylapp.com:10001/apis/dish

**请求参数** 
参数|数据类型|必填|参数说明
---|-------|----|-----
|restaurant|int|<i class="fa fa-check"></i>|餐厅ID|

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
404|餐厅ID不存在|系统繁忙，请稍候再试……

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":{
	    id:10,//餐厅ID
	    name:"聚食惠餐厅",//餐厅名称
	    notice:"暂无公告",
	    category:[//菜品分类
		    {
			    id:1,
			    name:"推荐",
			    dishes:[//菜品
				    {
					    id:4,//菜品ID
					    name:"猪排饭",//菜品名称
					    image:"http://static.taddy.com/upfile/2014/9/10/sfehiafe/pig.png",
					    price:38.5, //单价
					    sold:43, //销量
					    enabled:true//是否有此菜
				    },
				    {
					    id:5,//菜品ID
					    name:"鸡腿饭",//菜品名称
					 image:"http://static.taddy.com/upfile/2014/9/10/sfehiafe/chick.png",
					    price:20, //单价
					    sold:42, //销量
					    enabled:true//是否有此菜
				    }...
			    ]
		    },...
	    ]
    }
}
```

### 餐厅全局搜索(已开通)

**接口地址** 
`GET` `/search/restaurant`
> eg. http://www.hylapp.com:10001/apis/search/restaurant

**请求参数** 
参数|数据类型|必填|参数说明
---|-------|----|-----
|keyword|String|<i class="fa fa-check"></i>|搜索关键字|
|start   |int|           |分页参数——起始位置 默认：`0`
|size    |int|           |分页参数——分页大小 默认：`10`

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":[//餐厅列表
	    {
            id:5,
            name:"聚实惠餐厅",
            icon:"http://static.taddy.com/upfile/2014/9/10/lasjfoegaljoeg/icon-xs.png",
            star:9,//每2分表示一颗星，例如9表示4个半星。
            promotion:"10元起送，免配送费",
            price:30,
            sold:125,
            createTime:"2014-09-09 10:50",
            tag:["WIN_CASH","VIP_DISCOUNT","COST_CASH","COST_SCORE","FREE_POSTAGE","DEDUCT"],//优惠标签
            enabled:true//是否营业
        },...
	]
}
```


### 菜品全局搜索(已开通)

**接口地址** 
`GET` `/search/dish`
> eg. http://www.hylapp.com:10001/apis/search/dish

**请求参数** 
参数|数据类型|必填|参数说明
---|-------|----|-----
|keyword|String|<i class="fa fa-check"></i>|搜索关键字|
|start   |int|           |分页参数——起始位置 默认：`0`
|size    |int|           |分页参数——分页大小 默认：`10`

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":[//菜品列表
	    {
		    id:4,//菜品ID
		    name:"猪排饭",//菜品名称
		    image:"http://static.taddy.com/upfile/2014/9/10/sfehiafe/pig.png",
		    price:38.5, //单价
		    sold:43, //销量
		    enabled:true//是否有此菜
	    },
	    {
		    id:5,//菜品ID
		    name:"鸡腿饭",//菜品名称
		    image:"http://static.taddy.com/upfile/2014/9/10/sfehiafe/chick.png",
		    price:20, //单价
		    sold:42, //销量
		    enabled:true//是否有此菜
	    }...
	]
}
```

### 餐厅菜品搜索(已开通)

**接口地址** 
`GET` `/dish/search`
> eg. http://www.hylapp.com:10001/apis/dish/search

**请求参数** 
参数|数据类型|必填|参数说明
---|-------|----|-----
|restaurant|int|<i class="fa fa-check"></i>|餐厅ID|
|keyword|String|<i class="fa fa-check"></i>|搜索关键字|


**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
404|餐厅ID不存在|系统繁忙，请稍候再试……

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":[//菜品
	    {
		    id:4,//菜品ID
		    name:"猪排饭",//菜品名称
		    icon:"http://static.taddy.com/upfile/2014/9/10/sfehiafe/pig.png",
		    price:38.5, //单价
		    sold:43, //销量
		    enabled:true//是否有此菜
	    },
	    {
		    id:5,//菜品ID
		    name:"鸡腿饭",//菜品名称
		    icon:"http://static.taddy.com/upfile/2014/9/10/sfehiafe/chick.png",
		    price:20, //单价
		    sold:42, //销量
		    enabled:true//是否有此菜
	    }...
	]
}
```

### 菜品详情(已开通)

**接口地址** 
`GET` `/dish/{id}`
> eg. http://www.hylapp.com:10001/apis/dish/1

**请求参数** 
参数|数据类型|必填|参数说明
---|-------|----|-----
|principal|int||当前登录用户ID|

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":{
	    id:5,
	    name:"猪排饭",
	    favored:false,//当前用户是否收藏过该菜品。 true--已收藏；false--未收藏。若未传入登录用户ID，这里始终返回 false .
	    image:"http://static.taddy.com/upfile/2014/9/10/lasjfoegaljoeg/icon-xs.png",
	    price:38.5, //单价
	    sold:43, //销量
	    description:"猪排饭猪排饭猪排饭猪排饭猪
				排饭猪排饭猪排饭猪排饭猪排饭猪排
				饭猪排饭猪排饭",
    }
}
```

### 餐厅详情(已开通)

**接口地址** 
`GET` `/restaurant/{id}`
> eg. http://www.hylapp.com:10001/apis/restaurant/1

**请求参数** 
参数|数据类型|必填|参数说明
---|-------|----|-----
|principal|int||当前登录用户ID|

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":{
	    id:5,
	    name:"聚实惠餐厅",
	    favored:false,//当前用户是否收藏过该餐厅。 true--已收藏；false--未收藏。若未传入登录用户ID，这里始终返回 false .
	    image:[
		    "http://static.taddy.com/upfile/2014/9/10/lasjfoegaljoeg/icon-xs.png",
		    "http://static.taddy.com/upfile/2014/9/10/lasjfoegaljoeg/icon-xs.png",
		    ...
		],
	    promotion:"10元起送，免配送费",
	    businessHours:"10:00-22:00",
	    description:"<div>...</div>",
	    location:"合肥市高新区玉兰大道8号",
	    tel:"05516812451",
	    hot:[  //热门菜
		    {
			    id:10,
			    name:"猪排饭",
			    enabled:true//是否有此菜
		    },
		    {
			    id:11,
			    name:"鸡腿饭",
			    enabled:true//是否有此菜
		    },
		    {
			    id:12,
			    name:"鸭腿饭",
			    enabled:true//是否有此菜
		    },
		    ...
	    ],
	    star:9, //每2分表示一颗星，例如9表示4个半星。
	    comments:[ //用户评价
		    {
			    id:1,//用户ID
			    nickname:"小强",
			    star:9,//每2分表示一颗星，例如9表示4个半星。
			    createTime:"2014-09-10 23:21",
			    content:"猪排饭很好吃"
		    },
		    ...
	    ]
    }
}
```

### 确认订单

**接口地址** 
`GET` `/buy`
> eg. http://www.hylapp.com:10001/apis/buy

**请求参数** 
参数|数据类型|必填|参数说明
---|-------|----|-----
restaurant|int|<i class="fa fa-check"></i>|餐厅ID
|principal|int||当前登录用户ID|

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":{
	    packageFee:0,//打包费
	    delivery:9,//配送费
	    promotion:[
		    {
			    tag:"WIN_CASH",//优惠标签
			    params:[10,1]//满10元送1代金券
		    },
		    ...
	    ],
	    address:{
		    id:23,  //送餐地址ID, 订餐时传入此参数
		    name:"萨达姆",
		    mobile:"18919601457",
		    defaultAddress:true,//是否是默认送餐地址
		    location:"潜山路与东流路交叉口蔚蓝商务港B坐1406",
		    longitude:117.241179, //地图坐标--经度
		    latitude:31.820578 //地图坐标--纬度
	    },
	    principal:CustomerInfo
    }
}
```
[另请参考`CustomerInfo` 说明](#customerinfo)
[另请参考`优惠标签`](#优惠标签)
[另请参考`我的积分`](#我的积分)
[另请参考`我的代金券`](#我的代金券)

### 提交订单

**接口地址** 
`POST` `/buy`

**请求参数** 
参数|数据类型|必填|参数说明
---|-------|----|-----
goods|String|<i class="fa fa-check"></i>|订单详情 JSON数据[参考`订单数据`](#goods)
cost|double|<i class="fa fa-check"></i>|实际花费 单位：元

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
406|订单数据无法解析|系统繁忙，请稍候再试……
416|订单对账错误|系统繁忙，请稍候再试……
415|暂不支持的送餐地址|请谅解，目前暂不能配送该区域，敬请期待！

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":payment//订单详情
}
```
[另请参考 `订单状态`](#订单状态)
[另请参考 `订单详情`](#订单详情)
[另请参考 `订单数据`](#goods)

### 评论

**接口地址** 
`POST` `/comment`

**请求参数** 
参数|数据类型|必填|参数说明
---|-------|----|-----
principal|int|<i class="fa fa-check"></i>|当前登录用户ID
orderno|String|<i class="fa fa-check"></i>|订单号
star|int|<i class="fa fa-check"></i>|星评 **每2分表示一颗星，例如9表示4个半星。**
content|String||评论内容

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
401|未传入操作人ID|您需要重新登录才能继续操作哦~

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":null
}
```

## 系统级相关接口

### 启动整合接口
客户端（买家）在打开APP就应马上调用的接口
该接口将版本更新、获取熊仔联系方式、获取系统配置、菜品种类图标

**接口地址** 
`GET` `/system`
> eg.  http://www.hylapp.com:10001/apis/system

**请求参数**  
参数|数据类型|必填|参数说明
---|-------|----|-----
|code|int|<i class="fa fa-check"></i>|版本号|

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":{
		contact : "0551578465",			//熊仔电话
		displayComment  :  false,		//显示文字评论
		module : [     //餐厅种类
			{
				id : 12,
				name : "中餐",
				icon : "http://teddy.hylapp.com/static/module/test-md.png"
			},
			...
		],
		advertise:[
			{
				id:1,
				image:"http://static.taddy.com/upfile/2014/9/10/lasjfoegaljoeg/icon-xs.png",
				url:"http://static.taddy.com/activity/12/815.html"//广告跳转地址，默认使用
			}...
		]
	}
}
```

### 版本更新（买家APP）

**接口地址** 
`GET` `/system/upgrade`
> eg. http://www.hylapp.com:10001/apis/system/upgrade

**请求参数**  
参数|数据类型|必填|参数说明
---|-------|----|-----
|code|int|<i class="fa fa-check"></i>|版本号|

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":{  //无版本更新时为null
	    version:"10.0.0.3", 		//版本号
	    log:"更新日志：....（略）",	//更新日志
	    url:"http://....apk",		//下载地址
	    times:5	
    }
}
```

### 熊仔电话

**接口地址** 
`GET` `/system/contact`
> eg. http://www.hylapp.com:10001/apis/system/contact

**请求参数**  无

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":"0551578465"
}
```

### 关于我们

**接口地址** 
`GET` `/system/aboutus`
> eg. http://www.hylapp.com:10001/apis/system/aboutus

**请求参数** 
无

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":"<div>...</div>"
}
```

### 技术支持

**接口地址** 
`GET` `/system/support`
> eg. http://www.hylapp.com:10001/apis/system/support

**请求参数**  无

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":"<div>...</div>"
}
```

### 许可证

**接口地址** 
`GET` `/system/license`
> eg. http://www.hylapp.com:10001/apis/system/license

**请求参数** 
无

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":"<div>...</div>"
}
```

### 代金券使用规则

**接口地址** 
`GET` `/system/cashrule`
> eg. http://www.hylapp.com:10001/apis/system/cashrule

**请求参数**  无

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":"<div>...</div>"
}
```

## 取菜员、配送员APP相关接口

### 版本更新（取菜员）

**接口地址** 
`GET` `/deliver/upgrade`
> eg. http://www.hylapp.com:10001/apis/deliver/upgrade

**请求参数**  
参数|数据类型|必填|参数说明
---|-------|----|-----
|code|int|<i class="fa fa-check"></i>|版本号|

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":{   
	    version:"10.0.0.3", //版本号
	    log:"更新日志：....（略）",  //更新日志
	    url:"http://....apk", //下载地址
	}
}
```

### 版本更新（分配员）

**接口地址** 
`GET` `/center/upgrade`
> eg. http://www.hylapp.com:10001/apis/center/upgrade

**请求参数**  
参数|数据类型|必填|参数说明
---|-------|----|-----
|code|int|<i class="fa fa-check"></i>|版本号|

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":{   
	    version:"10.0.0.3", //版本号
	    log:"更新日志：....（略）",  //更新日志
	    url:"http://....apk", //下载地址
	}
}
```

### 版本更新（配送员）

**接口地址** 
`GET` `/courier/upgrade`
> eg. http://www.hylapp.com:10001/apis/courier/upgrade

**请求参数**  
参数|数据类型|必填|参数说明
---|-------|----|-----
|code|int|<i class="fa fa-check"></i>|版本号|

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data":{   
	    version:"10.0.0.3", //版本号
	    log:"更新日志：....（略）",  //更新日志
	    url:"http://....apk", //下载地址
	}
}
```

### 登录（取菜员、分配员、配送员）

**接口地址** 
`POST` `/employee/login`

**请求参数**  
参数|数据类型|必填|参数说明
---|-------|----|-----
|username|String|<i class="fa fa-check"></i>|工号|
|password|String|<i class="fa fa-check"></i>|密码|
|pushKey|String|<i class="fa fa-check"></i>|jpush推送KEY|

**响应结果**
```javascript
{
    "code": 0, //0 - 接口调用成功，其他值表示失败
    "message": null,
    "data": {
	    id: 23,//员工ID
	    account: "ps001",//工号
	    name: "张三",//姓名
	    hotline : "055165745125", //客服电话
	    type:1//员工类型 1-取菜员 2-配送员 3-分配员
    }
}
```

###修改密码（取菜员、分配员、配送员）

**接口地址** 
`POST`   `/employee/passwd`

**请求参数**  
|参数|数据类型|必填|参数说明|   
|-|-|-|-|
|principal|int|<i class="fa fa-check"></i>|当前登录用户ID|
|pwdold|String|<i class="fa fa-check"></i>|原密码|
|pwdnew|String|<i class="fa fa-check"></i>|新密码|
**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
401|用户不存在|您需要重新登录才能继续操作哦~
424|密码错误|密码错误
403|用户被禁用|帐号被禁用

**返回参数**  
```javascript
{
	code:0, //0-表示成功 其它表示失败
	message: null,
	data: null
}
```

### 订单列表（取菜员、分配员、配送员）

**接口地址** 
`GET`   `/payment`
> eg.   
>取菜员 未确认订单： http://www.hylapp.com:10001/apis/payment?principal=1&type=1&status=3
>取菜员 未配送订单（无数据）：http://www.hylapp.com:10001/apis/payment?principal=1&type=1&status=5
>取菜员 已完成订单：http://www.hylapp.com:10001/apis/payment?principal=1&type=1&status=6,8,9
>取菜员 客服处理订单：http://www.hylapp.com:10001/apis/payment?principal=1&type=1&status=4
>分配员 未确认订单（无数据）：http://www.hylapp.com:10001/apis/payment?principal=8&type=3&status=3
>分配员 未配送订单：http://www.hylapp.com:10001/apis/payment?principal=8&type=3&status=5
>分配员 已完成订单：http://www.hylapp.com:10001/apis/payment?principal=8&type=3&status=6,8,9
>分配员 客服处理订单：http://www.hylapp.com:10001/apis/payment?principal=8&type=3&status=4
>配送员 未完成订单（分配无效）：http://www.hylapp.com:10001/apis/payment?principal=9&type=2&status=3,5,6
>配送员 已完成订单（分配无效）：http://www.hylapp.com:10001/apis/payment?principal=9&type=2&status=8,9

**请求参数**  
|参数|数据类型|必填|参数说明|   
|-|-|-|-|
|principal|int|<i class="fa fa-check"></i>|当前登录用户ID|
|status|String|<i class="fa fa-check"></i>|订单状态，多个状态用`,`分隔。 [参照订单状态图](http://teddy.hylapp.com/docs/state-payment.svg)|
|type|int|<i class="fa fa-check"></i>|员工类型 1-取菜员 2-配送员 3-分配员
|start   |int|           |分页参数——起始位置 默认：`0`
|size    |int|           |分页参数——分页大小 默认：`10`

**返回参数**  
```javascript
{
	code:0, //0-表示成功 其它表示失败
	message: null,
	data: {
		total:21,//订单数
		cancel:0,//取消数 为0时不显示
		elements:[
			payment,
			payment,
			...
		]
	}
}
```

### 订单详情（取菜员、分配员、配送员）

**接口地址** 
`GET`   `/payment/{orderno}`
> eg.   http://www.hylapp.com:10001/apis/payment/140911000003

**路径参数**
`orderno` `String` 订单号

**请求参数**  
无

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
404|订单不存在|系统繁忙，请稍候重试……

**返回参数**  
```javascript
{
	code:0, //0-表示成功 其它表示失败
	message: null,
	data: Payment //订单详情
}
```
[另请参考 `订单详情`](#payment)


### 订单回退至客服（取菜员）

**接口地址** 
`POST`   `/payment/cancel/{orderno}`
> eg.   http://www.hylapp.com:10001/apis/payment/cancel/14091000001

**路径参数**
`orderno` `String` 订单ID

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
404|订单不存在|系统繁忙，请稍候重试……

**返回参数**  
```javascript
{
	code:0, //0-表示成功 其它表示失败
	message: null,
	data: null
}
```
### 菜品回退（取菜员）

**接口地址** 
`POST`   `/dish/cancel/{id}`
> eg.   http://www.hylapp.com:10001/apis/dish/cancel/14

**路径参数**
`id` `int` 菜品ID

**请求参数**  
|参数|数据类型|必填|参数说明|   
|-|-|-|-|
|orderno|String|<i class="fa fa-check"></i>|订单号|

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
404|菜品不存在|系统繁忙，请稍候重试……

**返回参数**  
```javascript
{
	code:0, //0-表示成功 其它表示失败
	message: null,
	data: null
}
```
### 确认取菜（取菜员）

**接口地址** 
`POST`   `/payment/ensure/deliver/{orderno}`
> eg.   http://www.hylapp.com:10001/apis/payment/ensure/deliver/141030000012

**路径参数**
`orderno` `String` 订单号

**请求参数**  
无

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
404|订单菜品不存在|系统繁忙，请稍候重试……

**返回参数**  
```javascript
{
	code:0, //0-表示成功 其它表示失败
	message: null,
	data: null
}
```
### 确认配送（分配员）

**接口地址** 
`POST`   `/payment/ensure/center/{orderno}`
> eg.   http://www.hylapp.com:10001/apis/payment/ensure/center/141031000012

**路径参数**
`orderno` `String` 订单号

**请求参数**  
无

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
404|菜品不存在|系统繁忙，请稍候重试……

**返回参数**  
```javascript
{
	code:0, //0-表示成功 其它表示失败
	message: null,
	data: null
}
```
### 确认送达（配送员）

**接口地址** 
`POST`   `/payment/ensure/courier/{ordorno}`
> eg.   http://www.hylapp.com:10001/apis/payment/ensure/courier/140901000001

**路径参数**
`orderno` `String` 订单号

**请求参数**  
无

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
404|订单不存在|系统繁忙，请稍候重试……

**返回参数**  
```javascript
{
	code:0, //0-表示成功 其它表示失败
	message: null,
	data: null
}
```
### 无法送达（配送员）

**接口地址** 
`POST`   `/payment/cannot/courier/{ordorno}`
> eg.   http://www.hylapp.com:10001/apis/payment/cannot/courier/140901000001

**路径参数**
`orderno` `String` 订单号

**请求参数**  
`cause` `String` 取消原因

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
404|订单不存在|系统繁忙，请稍候重试……

**返回参数**  
```javascript
{
	code:0, //0-表示成功 其它表示失败
	message: null,
	data: null
}
```
### 更新坐标（配送员）

**接口地址** 
`POST`   `/coordinate/update`
> eg.   http://www.hylapp.com:10001/apis/coordinate/update

**请求参数**  
|参数|数据类型|必填|参数说明|   
|-|-|-|-|
|principal|int|<i class="fa fa-check"></i>|当前登录用户ID|
|longitude|String|<i class="fa fa-check"></i>|经度|
|latitude|String|<i class="fa fa-check"></i>|纬度|

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
404|配送员不存在|系统繁忙，请稍候重试……

**返回参数**  
```javascript
{
	code:0, //0-表示成功 其它表示失败
	message: null,
	data: null
}
```

##内部接口

###换菜接口
**接口地址** 
`POST`   `/inner/payment-exchange/{orderno}`
> eg.   http://www.hylapp.com:10001/apis/inner/payment-exchange/141101000001

**路径参数**  
`orderno` `String` 订单号

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
404|订单不存在|很抱歉，您正在查看的订单不存在，请刷新页面后重试！

**返回参数**  
```javascript
{
	code:0, //0-表示成功 其它表示失败
	message: null,
	data: null
}
```

###退菜接口
**接口地址** 
`POST`   `/inner/payment-giveup/{orderno}`
> eg.   http://www.hylapp.com:10001/apis/inner/payment-exchange/141101000001

**路径参数**  
`orderno` `String` 订单号

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
404|订单不存在|很抱歉，您正在查看的订单不存在，请刷新页面后重试！

**返回参数**  
```javascript
{
	code:0, //0-表示成功 其它表示失败
	message: null,
	data: null
}
```

###取消订单接口
**接口地址** 
`POST`   `/inner/payment-cancel/{orderno}`
> eg.   http://www.hylapp.com:10001/apis/inner/payment-exchange/141101000001

**路径参数**  
`orderno` `String` 订单号

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
404|订单不存在|很抱歉，您正在查看的订单不存在，请刷新页面后重试！

**返回参数**  
```javascript
{
	code:0, //0-表示成功 其它表示失败
	message: null,
	data: null
}
```

###退订单明细接口
**接口地址** 
`POST`   `/inner/payment/{orderno}/dish/{id}`
> eg.   http://www.hylapp.com:10001/apis/inner/payment/141101000001/dish/1

**路径参数**  
`orderno` `String` 订单号
`id` `int` 菜品ID 为餐厅菜品ID非订单明细唯一标识

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
404|订单不存在|很抱歉，您正在查看的订单不存在，请刷新页面后重试！

**返回参数**  
```javascript
{
	code:0, //0-表示成功 其它表示失败
	message: null,
	data: null
}
```

###分配取菜员接口
**接口地址** 
`POST`   `/inner/payment/{orderno}/deliver/{employee}`
> eg.   http://www.hylapp.com:10001/apis/inner/payment/141101000001/deliver/1

**路径参数**  
`orderno` `String` 订单号
`employee` `int` 取菜员ID

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
404|订单不存在|很抱歉，您正在查看的订单不存在，请刷新页面后重试！

**返回参数**  
```javascript
{
	code:0, //0-表示成功 其它表示失败
	message: null,
	data: null
}
```

###分配配送员接口
**接口地址** 
`POST`   `/inner/payment/{orderno}/courier/{employee}`
> eg.   http://www.hylapp.com:10001/apis/inner/payment/141101000001/courier/1

**路径参数**  
`orderno` `String` 订单号
`employee` `int` 配送员ID

**运行时异常**
code| 错误原因  |返回消息
-----|---------|----
404|订单不存在|很抱歉，您正在查看的订单不存在，请刷新页面后重试！

**返回参数**  
```javascript
{
	code:0, //0-表示成功 其它表示失败
	message: null,
	data: null
}
```

##附录

### 优惠标签
`WIN_CASH`		返还代金券 <span class="label label-danger">返</span> 
> 参数定义： 满{0}元送{1}张{2}元代金券
>例：`params : [50,1,5]` 意为 *满10元送1张5元代金券*
 
`VIP_DISCOUNT` 	会员优惠，百分比打折 <span class="label label-danger">VIP</span> 
> 参数定义： 会员优惠{0}折
>例：`params : [80]` 意为 *会员优惠8折，即80%*

`COST_CASH`		可使用代金券 <span class="label label-danger">券</span>
> 参数定义： 可使用代金券
>例：`params : []` 该优惠不接受参数，用户只能选择一张代金券

`COST_SCORE`	可积分抵现金 <span class="label label-danger">积</span>
> 参数定义： 满{0}元可用{1}积分抵1元现金
>例：`params : [50,10]` 意为 *满50元可用10积分抵1元现金，用户只能输入10的倍数*

`FREE_POSTAGE`	免邮费	<span class="label label-danger">免</span>
> 参数定义： 满{0}元免邮费
>例：`params : [10]` 意为 *满10元免邮费*


###CustomerInfo
客户实体对象说明
```
{
    id:1,//用户ID
    mobile:"18919601457",//用户手机号
    nickname:"Jack",//用户姓名
    maiden:true,//是否新注册用户，true--新注册用户，false——老用户
    score:0,//积分
    level:0,//0 表示初级会员 1-表示高级会员 2-表示vip会员
    avatar:""//头像URL
}
```

### Payment
`订单详情` 对象说明
```javascript
{
	    orderno:"140921000003",//订单编号
	    createTime:"2014-09-10 09:40",//订餐时间（下单时间） 评论时可作参数传递
	    readyTime:null,//取菜员确认时间
	    assignTime:null,//配送员接单时间
	    createTime:"2014-09-10 09:40",//订餐时间 评论时可作参数传递
	    address:{
		    id:23,  //送餐地址ID, 订餐时传入此参数
		    mobile:"18919601457",
		    name: "王总",
		    location:"潜山路与东流路交叉口蔚蓝商务港B坐1406",
		    longitude:117.241179, //地图坐标--经度
		    latitude:31.820578 //地图坐标--纬度
	    },
	    vip:false,//客户在下订单时是否是VIP
	    restaurantId: 15,//餐厅ID
	    restaurantName:"聚实惠中式餐厅",
	    restaurantImg:"http://teddy.hylapp.com/static/restaurant/defualt.png",
	    restaurantContact:"18915124532",//餐厅电话
	    restaurantLocation:"万达广场3楼",
	    courierNumber:"PS001",//配送员配送号
	    courierName:"张斌",//配送员姓名
	    courierContact:"18951241521",//配送员联系方式
	    packageFee:0,//打包费
	    delivery:9,//配送费
	    status: 9,//订单状态
	    promotion:[
		    {
			    tag:"WIN_CASH",//优惠标签
			    params:[10,1],//满10元送1元代金券
			    money:0//折扣的费用 常为负值
		    },
		    ...
	    ],
	    editable:false,//是否可编辑，用于客户加菜
	    goods:Goods, //订单详情 [另请参考`订单详情`](#goods)
	    cost:"100.4",//实际费用 客户端可用来对账
	    primeCost:68.3 //餐厅实际价格
}
```
[另请参考`订单数据`](#goods)
[另请参考`优惠标签`](#优惠标签)
[另请参考`CustomerInfo` 说明](#customerinfo)

### Goods

订单数据 对象说明
订单数据包含客户购物的基本信息

```javascript
{
	principal:1,//当前登录用户ID
	address:31,//送餐地址ID
	restaurant:23,//餐厅ID
	memo:"不要辣椒",//备注
	dishes:[//菜品
		{
			id:12,
			name:"猪排饭",
			price:34,
			count:2,
			status:0 //0-表示初始状态 1-已取菜 2-已退菜 3-退菜已处理
		},
		{
			id:13,
			name:"鸡腿饭",
			price:11,
			count:1,
			status:0 //0-表示初始状态 1-已取菜 2-已退菜 3-退菜已处理
		},
		...
	],
	cash:5, //客户使用的代金券面值，没有写0
	score:60 //客户消费抵押积分，没有写0
}
```

## 二维码扫描说明

二维码的功能包括两个方面：
1. 跳转到后台临时创建的活动页
2. 任何一款其它APP打开后能跳转到应用下载页面

针对以上需求，这里规定下客户端需实现的功能。

客户端在扫描二维码后会得到一个url，例如：
`http://teddy.hylapp.com/acitivity/14092003?key1=dafe&key2=dsfae&needLogin=true`
客户端根据此URL拼接一个新的URL，并用内嵌浏览器打开新的URL。拼接步骤如下：
1. 在url中查找字符串*`needLogin=true`*,若能查找到此字符串,则表示该项活动需用户登录，客户端先引导用户登录，然后在url后追加登录用户ID参数：`&principal=1`。
2.  始终在URL后面增加特殊参数：`&spt=hylapp`

将得到的URL在内嵌浏览器中打开即可。

##推送消息说明
推送消息给客户端时候附带的数据，客户端可根据数据进行相信处理

*	**`module`** 推送功能标识。 可能的值如下：
		*    `0` - 推送的信息是活动信息，客户端需跳转到`url`参数表示的活动页面（同时附加上当前登录用户ID，如：`&principal=1`）。
		*     `1` - 取菜员新订单通知，点击后直接打开（并刷新）未完成列表。 此时`orderno`参数表示新的订单号。
		*     `2` -配送员新订单通知，点击后直接打开（并刷新）未完成列表。 此时`orderno`参数表示新的订单号。
		*    `3` -用户缺菜通知，点击后直接打开（并刷新）当前订单。 此时`orderno`参数表示新的订单号。
		*    `4` -用户订单完成通知，点击后直接打开餐厅评价页面。 此时`orderno`参数表示待评价的订单号。


